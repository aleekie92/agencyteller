package com.coretec.agencyteller.utils;

/**
 * Created by kelvinoff on 17/11/21.
 */

public interface RequiresAuthorization {
    String getPhoneNumber();
}

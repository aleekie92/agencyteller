package com.coretec.agencyteller.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefs {

    private static SharedPreferences mSharedPref;

    public static final String PREF_UNIQUE_ID = "PREF_UNIQUE_ID";
    public static final String IDENTIFIER = "IDENTIFIER";
    public static final String IDENTIFIER1 = "IDENTIFIER1";

    public static final String DEVICE_ID = "DEVICE_ID";
    public static final String TELLER_ACCOUNT_NUMBER = "TELLER_ACCOUNT_NUMBER";
    public static final String ACCOUNT_BALANCE = "ACCOUNT_BALANCE";
    public static final String AGENT_CODE = "AGENT_CODE";
    public static final String AGENT_NAME = "AGENT_NAME";
    public static final String SACCO_NAME = "SACCO_NAME";
    public static final String SACCO_MOTTO = "SACCO_MOTTO";
    public static final String AGENT_TYPE = "AGENT_TYPE";
    public static final String AGENT_BRANCH = "AGENT_BRANCH";
    public static final String AGENT_ACTIVITY = "AGENT_ACTIVITY";
    public static final String MEMBER_ACCOUNT_NUMBER = "MEMBER_ACCOUNT_NUMBER";
    public static final String CASHIER_ACCOUNT_NUMBER = "CASHIER_ACCOUNT_NUMBER";
    public static final String LOAN_NUMBER = "LOAN_NUMBER";
    public static final String LOAN_TYPE = "LOAN_TYPE";
    public static final String BANK_NUMBER = "BANK_NUMBER";
    public static final String CHEQUE_TYPE = "CHEQUE_TYPE";
    public static final String CHEQUE_MATURITY = "CHEQUE_MATURITY";
    public static final String ACC_NAME = "ACC_NAME";
    public static final String ACC_BAL_NAME = "ACC_BAL_NAME";
    public static final String TREASURY_ACCOUNT_NUMBER = "TREASURY_ACCOUNT_NUMBER";
    public static final String TELLER_DESTINATION_AC = "TELLER_DESTINATION_AC";
    public static final String LIEN_NUMBER = "LIEN_NUMBER";

    public static final String CHEQUE_NUMBER = "CHEQUE_NUMBER";
    public static final String INCOME_ACCOUNT_NUMBER = "INCOME_ACCOUNT_NUMBER";

    //Member Registration Variable
    public static final String FULL_NAMES = "FULL_NAMES";
    public static final String PHONE_NUMBER = "PHONE_NUMBER";
    public static final String ID_NUMBER = "ID_NUMBER";
    public static final String DOB = "DOB";
    public static final String GENDER = "GENDER";
    public static final String COUNTY = "COUNTY";

    //Msacoo Application
    public static final String APPLICATION_TYPE = "APPLICATION_TYPE";

    //Transfers
    public static final String TRANSFER_ACC_ONE = "TRANSFER_ACC_ONE";
    public static final String TRANSFER_SELF_ONE = "TRANSFER_SELF_ONE";
    public static final String TRANSFER_ACC_TWO = "TRANSFER_ACC_TWO";

    //ATM Application
    public static final String CARD_TYPE = "CARD_TYPE";

    //STO Application
    public static final String LOAN_ACCOUNT = "LOAN_ACCOUNT";
    public static final String STO_LOAN_NUMBER = "STO_LOAN_NUMBER";
    public static final String RECOVERY_CODE = "RECOVERY_CODE";
    public static final String START_DATE = "START_DATE";
    public static final String STO_DESTINATION_AC = "STO_DESTINATION_AC";
    public static final String STO_TYPE_CODE = "STO_TYPE_CODE";
    public static final String STO_BANK_NUMBER = "STO_BANK_NUMBER";

    public static final String SIGNATURE_IMAGE = "SIGNATURE_IMAGE";
    public static final String USER_IMAGE = "USER_IMAGE";

    private SharedPrefs()
    {

    }

    public static void init(Context context)
    {
        if(mSharedPref == null)
            mSharedPref = context.getSharedPreferences(context.getPackageName(), Activity.MODE_PRIVATE);
    }

    public static String read(String key, String defValue) {
        return mSharedPref.getString(key, defValue);
    }

    public static void write(String key, String value) {
        SharedPreferences.Editor prefsEditor = mSharedPref.edit();
        prefsEditor.putString(key, value);
        prefsEditor.apply();
    }

    public static boolean read(String key, boolean defValue) {
        return mSharedPref.getBoolean(key, defValue);
    }

    public static void write(String key, boolean value) {
        SharedPreferences.Editor prefsEditor = mSharedPref.edit();
        prefsEditor.putBoolean(key, value);
        prefsEditor.apply();
    }

    public static Integer read(String key, int defValue) {
        return mSharedPref.getInt(key, defValue);
    }

    public static void write(String key, Integer value) {
        SharedPreferences.Editor prefsEditor = mSharedPref.edit();
        prefsEditor.putInt(key, value).apply();
    }

}

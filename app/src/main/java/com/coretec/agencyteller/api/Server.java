package com.coretec.agencyteller.api;

import android.content.Context;
import android.util.Log;

import com.coretec.agencyteller.activities.SplashActivity;
import com.coretec.agencyteller.api.requests.GenerateTokenRequest;
import com.coretec.agencyteller.api.responses.GenerateTokenResponse;
import com.coretec.agencyteller.utils.Const;
import com.orhanobut.logger.Logger;

/**
 * Created by kelvinoff on 17/10/06.
 */

public class Server {

    public static void generateToken(final SplashActivity splashActivity) {

        final String TAG = "token generated";
        final Context ctx = null;
        GenerateTokenRequest generateToken = new GenerateTokenRequest();
        String URL = ApiTest.MSACCO_AGENT + ApiTest.GenerateToken;
        Log.i(TAG, URL);

        ApiTest.instance(splashActivity).request(URL, generateToken, new ApiTest.RequestListener() {
            @Override
            public void onSuccess(String response) {
                Logger.json(response);
                GenerateTokenResponse generateTokenResponse = ApiTest.instance(splashActivity).mGson.fromJson(response, GenerateTokenResponse.class);

                /*if (generateTokenResponse.is_successful) {
                    Const.getInstance().setLoginToken(generateTokenResponse.token);
                    splashActivity.onTokenGenerated(generateTokenResponse);
                } else {
                    splashActivity.onError("Wrong credentials");
                }*/
            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {
                //splashActivity.onError(error);
            }
        });
    }

    public interface GenerateTokenListener {
        void onTokenGenerated(GenerateTokenResponse generateTokenResponse);

        void onError(String error);
    }
}

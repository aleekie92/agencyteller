package com.coretec.agencyteller.api.responses;

/**
 * Created by kelvinoff on 17/10/09.
 */

public class ChangePinResponse extends BaseResponse {
    private String error;
    private boolean pin_valid;

    public ChangePinResponse() {

    }

    public boolean isPin_valid() {
        return pin_valid;
    }

    public void setPin_valid(boolean pin_valid) {
        this.pin_valid = pin_valid;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}

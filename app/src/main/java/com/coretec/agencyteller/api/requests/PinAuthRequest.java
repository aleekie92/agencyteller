package com.coretec.agencyteller.api.requests;

import com.coretec.agencyteller.api.ApiTest;

/**
 * Created by kelvinoff on 18/01/17.
 */

public class PinAuthRequest extends MemberAuthRequest {
    public String pin;

    public PinAuthRequest() {

    }

    @Override
    public String getAuthenticationUrl() {
        return ApiTest.AuthenticateCustomerLogin;
    }

    public PinAuthRequest(String pin) {
        this.pin = pin;
    }
}

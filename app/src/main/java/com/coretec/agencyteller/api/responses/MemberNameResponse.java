package com.coretec.agencyteller.api.responses;

/**
 * Created by kelvinoff on 17/10/09.
 */

public class MemberNameResponse extends BaseResponse {

    private String error;
    private String customer_name;

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public MemberNameResponse() {

    }

}

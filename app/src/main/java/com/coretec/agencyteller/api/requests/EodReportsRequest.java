package com.coretec.agencyteller.api.requests;

import com.coretec.agencyteller.utils.Const;

/**
 * Created by kelvinoff on 17/10/10.
 */

public class EodReportsRequest extends BaseRequest {
    public String corporateno;
    public String requestdate;
    public String agentid;
    public String terminalid;
    public double longitude;
    public double latitude;


    public EodReportsRequest() {
        super();
    }

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }
}

package com.coretec.agencyteller.api.responses;

import java.io.Serializable;

/**
 * Created by kelvinoff on 17/10/10.
 */

public class BaseResponse implements Serializable {
    public AuthorizationResponse authorization_response;
    public boolean is_successful = false;
}

package com.coretec.agencyteller.api.responses;

/**
 * Created by kelvinoff on 17/10/07.
 */

public class GenerateTokenResponse extends BaseResponse {
    public String token;

    public GenerateTokenResponse() {

    }
}

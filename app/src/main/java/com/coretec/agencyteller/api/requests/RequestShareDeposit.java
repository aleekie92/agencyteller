package com.coretec.agencyteller.api.requests;

import com.coretec.agencyteller.utils.Const;

/**
 * Created by kelvinoff on 17/10/10.
 */

public class RequestShareDeposit extends BaseRequest {

    public String agentid;
    public String corporate_no;
    public String memberaccno;
    public int amount;
    public String terminalid;
    public double longitude;
    public double latitude;
    public String date;
    public String depositorsphoneno;
    public  String depositorsname;



    public RequestShareDeposit() {
        super();
    }

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }
}

package com.coretec.agencyteller.api;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.cloudpos.jniinterface.FingerPrintInterface;
import com.coretec.agencyteller.R;
import com.coretec.agencyteller.api.requests.BaseRequest;
import com.coretec.agencyteller.api.requests.BiometricAuthRequest;
import com.coretec.agencyteller.api.requests.MemberAuthRequest;
import com.coretec.agencyteller.api.requests.PinAuthRequest;
import com.coretec.agencyteller.api.responses.MemberAuthResponse;
import com.coretec.agencyteller.utils.AppController;
import com.coretec.agencyteller.utils.PreferenceFileKeys;
import com.coretec.agencyteller.utils.RequiresAuthorization;
import com.coretec.agencyteller.utils.Utils;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by kelvinoff on 17/10/06.
 */

public class ApiTest {
    //    public static String MSACCO_AGENT = "http://192.168.43.116/AgencyCoretec/api/Msacco/";
//    public static String MSACCO_AGENT = "http://41.72.203.234:35051/MsaccoPlus/api/Msacco/";
//    public static String MSACCO_AGENT = "http://41.72.203.234:8686/api/Agency/";
//    public static String MSACCO_AGENT = "http://41.72.203.234:8788/api/AgencyBanking/";
    public static String MSACCO_AGENT = "http://41.72.203.234:35088/AgencyNew2017/api/AgencyBanking/";

    public static String GenerateToken = "GenerateToken";
    public static String AgentAuthentication = "AgentAuthentication";
    public static String GetMiniStatement = "GetMiniStatement";
    public static String GetBalanceInquiry = "GetBalanceEnquiry";
    public static String FundsDeposits = "FundsDeposits";
    public static String AgentFundsWithdrawal = "AgentFundsWithdrawal";
    public static String AuthenticateCustomerLogin = "AuthenticateCustomerLogin";
    public static String GetActiveFOSAAccounts = "GetActiveFOSAAccounts";
    public static String GetCustomername = "GetCustomername";
    public static String GetAllMemberSaccoDetails = "GetAllMemberSaccoDetails";
    public static String GetEODReport = "GetEODReport";
    public static String GetMemberActiveLoans = "GetMemberActiveLoans";
    public static String LoanRepayment = "LoanRepayment";
    public static String ChangePinCustomer = "ChangePinCustomer";
    public static String AccountOpenning = "AccountOpenning";
    public static String AccountRegistration = "AccountRegistration";
    public static String GetMembername = "GetMembername";
    public static String MemberActivation = "MemberActivation";
    public static String ShareDeposits = "ShareDeposits";
    public static String GetAgentAccountInfo = "GetAgentAccountInfo";
    public static String AuthenticateCustomerBiometric = "AuthenticateCustomerBiometric";

    private Context mContext;

    public Gson mGson = new GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .setLenient()
            .serializeNulls()
            .create();
    private boolean isFingerprintOpened = false;

    SharedPreferences sharedPreferences;

    private ApiTest() {

    }

    private ApiTest(Context mContext) {
        this.mContext = mContext;
    }

    final void memberAuthDialog(final RequiresAuthorization requiresAuthorizationRequest, final AuthRequestListener authRequestListener) {

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(mContext);
        View mView = ((Activity) mContext).getLayoutInflater().inflate(R.layout.member_auth, null);
        final EditText mPin = (EditText) mView.findViewById(R.id.member_auth__password);
        final TextInputLayout layoutPin = (TextInputLayout) mView.findViewById(R.id.layout_member_auth__password);

        mBuilder.setView(mView);

        final AppCompatButton btnConfirmMemberAuth = (AppCompatButton) mView.findViewById(R.id.btn_confirm_member_auth);
        final AppCompatButton btnFingerPrintMemberAuth = (AppCompatButton) mView.findViewById(R.id.btn_fingerprint_member_auth);
        final LinearLayout mFingerPrintAuth = mView.findViewById(R.id.fingerprint_auth);
        final LinearLayout mPinAuth = mView.findViewById(R.id.pin_auth);


        final AlertDialog dialog = mBuilder
               /* .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setNeutralButton("Fingerprint", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })*/
                .create();

/*        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Button mConfirm = ((AlertDialog) dialog).getButton(DialogInterface.BUTTON_POSITIVE);
                mConfirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        final String pin = mPin.getText().toString().trim();

                        if (pin.isEmpty()) {
                            layoutPin.setError("Please Enter Your Pin");

                        } else if (pin.trim().length() > 4 || pin.trim().length() < 4) {
                            layoutPin.setError("Enter the 4 digit pin");
                        } else {
                            authMemberRequest(requiresAuthorizationRequest, authRequestListener, new PinAuthRequest(pin));
                        }
                    }
                });

                Button mCancel = ((AlertDialog) dialog).getButton(DialogInterface.BUTTON_NEGATIVE);

                mCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                Button mFingerprintAuth = ((AlertDialog) dialog).getButton(DialogInterface.BUTTON_NEUTRAL);
                mFingerprintAuth.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mPinAuth.setVisibility(View.GONE);
                        mFingerPrintAuth.setVisibility(View.VISIBLE);
                        final Object mCond = new Object();
                        PtGlobal mPtGlobal = new PtGlobal(mContext);
                        try {
                            // Initialize PTAPI interface
                            // Note: If PtBridge development technology is in place and a network
                            // connection cannot be established, this call hangs forever.
                            mPtGlobal.initialize();

                        } catch (java.lang.UnsatisfiedLinkError ule) {
                            // Library wasn't loaded properly during PtGlobal object construction
                            mPtGlobal = null;

                        } catch (PtException e) {
                            mPtGlobal = null;
                        }
                        if (mPtGlobal != null) {
                            PtConnectionAdvancedI mConn = null;
                            try {
                                mConn = (PtConnectionAdvancedI) mPtGlobal.open("");
                            } catch (PtException e) {
                                e.printStackTrace();
                            }
                            try {
                                PtInfo mSensorInfo = mConn != null ? mConn.info() : null;
                            } catch (PtException e) {
                                e.printStackTrace();
                            }

                            synchronized (mCond) {
                                final Thread mRunningOp = new OpGrab(mConn, PtConstants.PT_GRAB_TYPE_THREE_QUARTERS_SUBSAMPLE,
                                        mContext) {
                                    @Override
                                    protected void onDisplayMessage(String message) {
                                        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                                        // (message);
                                    }

                                    @Override
                                    protected void onFinished() {
                                        synchronized (mCond) {
                                            mCond.notifyAll();  //notify onDestroy that operation has finished
                                        }
                                    }
                                };
                                mRunningOp.start();
                            }
                        }
                    }
                });
            }
        });*/

        dialog.show();


        btnConfirmMemberAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                final String pin = mPin.getText().toString().trim();

                if (pin.isEmpty()) {
                    layoutPin.setError("Please Enter Your Pin");

                } else if (pin.trim().length() > 4 || pin.trim().length() < 4) {
                    layoutPin.setError("Enter the 4 digit pin");
                } else {
                    authMemberRequest(requiresAuthorizationRequest, authRequestListener, new PinAuthRequest(pin));
                }
            }
        });

        btnFingerPrintMemberAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPinAuth.setVisibility(View.GONE);
                mFingerPrintAuth.setVisibility(View.VISIBLE);

                Utils.showAlertDialog(mContext, "FingerPrint Authentication", "Touch Sensor And Click OK To Authenticate", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                         @SuppressLint("HandlerLeak")
                         final Handler handler = new Handler() {
                            @Override
                            public void handleMessage(Message msg) {
                                switch (msg.what) {
                                    case 0:
                                        // Error
                                        Toast.makeText(mContext, (String) msg.obj, Toast.LENGTH_SHORT).show();
                                        break;
                                    case 1:
                                        String imageData = (String) msg.obj;
                                        Log.e("ImageData", imageData);
                                        authMemberRequest(requiresAuthorizationRequest, authRequestListener, new BiometricAuthRequest(imageData));
                                        //Fingerprint acquired
                                        break;
                                    default:
                                        //Something else
                                        break;
                                }
                            }
                        };

                        new Thread() {

                            @Override
                            public void run() {
                                try {
                                    readFingerprint(handler);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        closeFingerPrint(handler);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }.start();

                    }
                });

            }
        });

    }

    private void readFingerprint(Handler handler) throws Exception {
        if (!isFingerprintOpened) {
            int result = FingerPrintInterface.open();
            if (result < 0) {
                sendMsg(mContext.getString(R.string.fingerprint_open_failed), handler);
                throw new Exception();
            } else {
                isFingerprintOpened = true;
                sendMsg(mContext.getString(R.string.fingerprint_openned), handler);
            }
        } else {
            sendMsg(mContext.getString(R.string.fingerprint_already_open), handler);
            throw new Exception();
        }
        byte[] pImgBuffer = new byte[100 * 1024];
        int nImgLength = 100 * 1024;
//        byte[] pImgBuffer = new byte[33 * 1024];
//        int nImgLength = 33 * 1024;
        int[] pRealImaLength = new int[1];
        int[] pImgWidth = new int[1];
        int[] pImgHeight = new int[1];
        if (isFingerprintOpened) {
            int result = FingerPrintInterface.getLastImage(pImgBuffer, nImgLength, pRealImaLength,
                    pImgWidth, pImgHeight);
            if (result < 0) {
                sendMsg(mContext.getString(R.string.could_not_get_fingerprint), handler);
                throw new Exception();
            } else {
                BitmapFactory.Options opts = new BitmapFactory.Options();
                opts.outWidth = pImgHeight[0];
                opts.outHeight = pImgHeight[0];
                Bitmap bitmap = BitmapFactory.decodeByteArray(pImgBuffer, 0, pRealImaLength[0],
                        opts);
//                bitmap.
//                sendMsg(context.getString(R.string.get_image_success));
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                byte[] b = byteArrayOutputStream.toByteArray();
                String imageData = Base64.encodeToString(b, Base64.DEFAULT);
                System.out.println(imageData);
                handler.obtainMessage(1, imageData).sendToTarget();
            }
        } else {
            sendMsg(mContext.getString(R.string.device_not_open), handler);
            throw new Exception();
        }
    }

    private void closeFingerPrint(Handler handler) throws Exception {
        if (isFingerprintOpened) {
            int result = FingerPrintInterface.close();
            if (result < 0) {
                sendMsg(mContext.getString(R.string.fingerprint_close_failed), handler);
                throw new Exception();
            } else {
                isFingerprintOpened = false;
                sendMsg(mContext.getString(R.string.fingerprint_close_success), handler);
            }
        } else {
            sendMsg(mContext.getString(R.string.device_not_open), handler);
            throw new Exception();
        }
    }

    private void sendMsg(String msg, Handler handler) {
        handler.obtainMessage(0, msg).sendToTarget();
    }

    public void authMemberRequest(RequiresAuthorization requiresAuthorizationRequest,
                                  final AuthRequestListener authRequestListener, @NonNull MemberAuthRequest authRequest) {

        String TAG = "Auth Member request";
        sharedPreferences = mContext.getSharedPreferences(PreferenceFileKeys.PREFS_FILE, Context.MODE_PRIVATE);

        String URL = ApiTest.MSACCO_AGENT + authRequest.getAuthenticationUrl();

        authRequest.accountidentifier = requiresAuthorizationRequest.getPhoneNumber();
        authRequest.accountidentifiercode = "0";
        authRequest.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
        authRequest.terminalid = Utils.getIMEI(mContext);
        authRequest.longitude = Utils.getLong(mContext);
        authRequest.latitude = Utils.getLat(mContext);
        authRequest.date = Utils.getFormattedDate();

//        Toast.makeText(this, loginRequest.toString(), Toast.LENGTH_SHORT).show();
        Log.e(TAG, authRequest.getBody().toString());

        // TODO: implement cancel logic

        ApiTest.instance(mContext).request(URL, authRequest, new ApiTest.RequestListener() {
            @Override
            public void onSuccess(String response) {
                MemberAuthResponse memberAuthResponse = ApiTest.instance(mContext).mGson.fromJson(response, MemberAuthResponse.class);
                if (memberAuthResponse.is_successful) {
                    // TODO: proceed to request
                    authRequestListener.onSuccess("Pin success");
                } else {
                    // TODO: Request failed
                    Toast.makeText(mContext, memberAuthResponse.getError(), Toast.LENGTH_SHORT).show();
                    authRequestListener.onError("Authentication failed");
                }
            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });


    }

    public void request(final String url, final BaseRequest request, final RequestListener requestListener) {
        request(RequestMethod.POST, url, request, requestListener);
    }

    public void request(final RequestMethod method, final String url, final BaseRequest request, final RequestListener requestListener) {
        if (request instanceof RequiresAuthorization) {
            memberAuthDialog((RequiresAuthorization) request, new AuthRequestListener() {
                @Override
                public void onSuccess(String response) {
                    if (method == RequestMethod.GET) {
                    } else {
                        post(url, request, requestListener);
                    }
                }

                @Override
                public void onError(String error) {
                    requestListener.onError(error);
                }

                @Override
                public void onCancel() {
                    requestListener.onError("Auth canceled");
                }
            });
        } else {
            if (method == RequestMethod.GET) {

            } else {
                post(url, request, requestListener);
            }
        }
    }

    public void post(String url, final BaseRequest request, final RequestListener requestListener) {
        final String mRequestBody = mGson.toJson(request.getBody());
        Log.e("Request", mRequestBody);
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("LOG_VOLLEY", response);
                requestListener.onSuccess(response);

                /// requestListener.onTokenExpired();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("LOG_VOLLEY", error.toString());
                requestListener.onError(error.getMessage());
            }
        })

        {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", mRequestBody, "utf-8");
                    return null;
                }
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                600000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public static ApiTest instance(Context mContext) {
        return new ApiTest(mContext);
    }

    public interface RequestListener {
        void onSuccess(String response);

        void onTokenExpired();

        void onError(String error);
    }

    public interface AuthRequestListener {
        void onSuccess(String response);

        void onError(String error);

        void onCancel();
    }

    public enum RequestMethod {
        POST,
        GET
    }

}

package com.coretec.agencyteller.api.requests;

import com.coretec.agencyteller.utils.Const;

/**
 * Created by kelvinoff on 17/10/10.
 */

public class RequestMemberName extends BaseRequest {

    public String corporateno;
    public String accountidentifier;
    public String accountcode;
    public String agentid;
    public String terminalid;

    public RequestMemberName() {
        super();
    }

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }
}

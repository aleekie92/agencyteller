package com.coretec.agencyteller.api.requests;

import com.coretec.agencyteller.utils.Const;

/**
 * Created by kelvinoff on 17/10/10.
 */

public class AccountBalanceRequest  extends BaseRequest {
    public String corporate_no;
    public String msisdn;
    public String account_number;

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getRequestToken();
    }
}

package com.coretec.agencyteller.api.requests;

import com.coretec.agencyteller.utils.Const;

/**
 * Created by kelvinoff on 17/10/10.
 */

public class LoginRequest extends BaseRequest {
    public String agentpin;
    public String agentcode;
    public String terminalid;

    public LoginRequest(){
        super();
    }
    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }
}

package com.coretec.agencyteller.api.requests;

import com.coretec.agencyteller.utils.Const;

/**
 * Created by kelvinoff on 17/10/10.
 */

public class RequestWithdrawal extends BaseRequest /*implements RequiresAuthorization*/ {

    public String corporate_no;
    public String accountidentifier;
    public String accountidentifiercode;
    public String accountno;
    public String agentid;
    public boolean iscashwithdrawal;
    public String terminalid;
    public double longitude;
    public double latitude;
    public int amount;
    public String date;
    public String mpesaPhoneNumber;

    public RequestWithdrawal() {
        super();
    }

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }


    /*@Override
    public String getPhoneNumber() {
        return accountidentifier;
    }*/
}

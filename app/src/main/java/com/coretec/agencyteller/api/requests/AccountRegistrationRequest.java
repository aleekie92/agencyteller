package com.coretec.agencyteller.api.requests;

import com.coretec.agencyteller.utils.Const;

/**
 * Created by kelvinoff on 17/10/10.
 */

public class AccountRegistrationRequest extends BaseRequest {


    public String nationality;
    public String biometrics;
    public String gender;
    public String dob;
    public String emailaddress;
    public String phoneno;
    public double amount;
    public String accountname;
    public String corporate_no;
    public String national_id;
    public String agentid;
    public String terminalid;
    public double longitude;
    public double latitude;
    public String date;

    public AccountRegistrationRequest() {
        super();
    }

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }
}

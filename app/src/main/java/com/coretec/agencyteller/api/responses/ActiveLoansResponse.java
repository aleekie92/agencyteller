package com.coretec.agencyteller.api.responses;

import com.coretec.agencyteller.model.Loans;

import java.io.Serializable;
import java.util.List;

/**
 * Created by kelvinoff on 17/10/09.
 */

public class ActiveLoansResponse extends BaseResponse implements Serializable {

    private String error;
    private String sacconame;
    private String transactiondate;
    private List<Loans> active_loans;

    public ActiveLoansResponse() {

    }

    public ActiveLoansResponse(String error, String sacconame, String transactiondate, List<Loans> active_loans) {
        this.error = error;
        this.sacconame = sacconame;
        this.transactiondate = transactiondate;
        this.active_loans = active_loans;
    }


    public List<Loans> getActive_loans() {
        return active_loans;
    }

    public void setActive_loans(List<Loans> active_loans) {
        this.active_loans = active_loans;
    }

    public String getSacconame() {
        return sacconame;
    }

    public void setSacconame(String sacconame) {
        this.sacconame = sacconame;
    }

    public String getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}

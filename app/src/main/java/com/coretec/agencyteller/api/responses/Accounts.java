package com.coretec.agencyteller.api.responses;

import java.io.Serializable;

/**
 * Created by kelvinoff on 17/10/09.
 */

public class Accounts implements Serializable {

    private String referenceno;
    private double amount;
    private String transactiontype;

    public Accounts() {
    }

    public Accounts(String referenceno, double amount, String transactiontype) {
        this.referenceno = referenceno;
        this.amount = amount;
        this.transactiontype = transactiontype;
    }

    public String getReferenceno() {
        return referenceno;
    }

    public void setReferenceno(String referenceno) {
        this.referenceno = referenceno;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getTransactiontype() {
        return transactiontype;
    }

    public void setTransactiontype(String transactiontype) {
        this.transactiontype = transactiontype;
    }
}

package com.coretec.agencyteller.api.responses;

import java.io.Serializable;

/**
 * Created by kelvinoff on 17/10/09.
 */

public class WithdrawalResponse extends BaseResponse implements Serializable {

    private String error;
    private String corporate_no;
    private String customername;
    private int amount;
    private String receiptno;
    private String transactiontype;
    private String sacconame;
    private String transactiondate;

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getReceiptno() {
        return receiptno;
    }

    public void setReceiptno(String receiptno) {
        this.receiptno = receiptno;
    }

    public String getTransactiontype() {
        return transactiontype;
    }

    public void setTransactiontype(String transactiontype) {
        this.transactiontype = transactiontype;
    }

    public String getSacconame() {
        return sacconame;
    }

    public void setSacconame(String sacconame) {
        this.sacconame = sacconame;
    }

    public String getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getCorporate_no() {
        return corporate_no;
    }

    public void setCorporate_no(String corporate_no) {
        this.corporate_no = corporate_no;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public WithdrawalResponse() {

    }

}

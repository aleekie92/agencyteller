package com.coretec.agencyteller.api;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class Api {

    //public static String URL = "http://192.168.12.80:2020/api/AgencyBanking/";
    //Live API
    //public static String URL = "http://192.168.1.247:8080/Api/AgencyBanking/";//wakenyapamoja

     public static String URL = "http://192.168.86.8:2020/Api/AgencyBanking/";//capital sacco

    public static String TellerLogin = "TellerLogin";
    public static String GetMemberAccounts = "GetMemberAccounts";
    public static String GetLienPlaced = "GetLienPlaced";
    public static String GetIncomeAccounts = "GetIncomeAccounts";
    public static String GetMemberDetails = "GetMemberDetails";
    public static String GetAccountSignatories = "GetAccountSignatories";
    public static String GetAgentTransactions = "GetAgentTransactions";
    public static String GetAgentAccount = "GetAgentAccount";
    public static String CashDepositTransaction = "CashDepositTransaction";
    public static String CashWithdrawalTransaction = "CashWithdrawalTransaction";
    public static String MemberRegistration = "MemberRegistration";
    public static String ChequeDepositTransaction = "ChequeDepositTransaction";
    public static String CreditReceiptTransaction = "CreditReceiptTransaction";
    public static String MsaccoApplicationTransaction = "MsaccoApplicationTransaction";
    public static String AtmApplicationTransaction = "AtmApplicationTransaction";
    public static String AccountTransferTransaction = "AccountTransferTransaction";
    public static String LienHoldingTransaction = "LienHoldingTransaction";
    public static String ClearLien = "ClearLien";
    public static String GetCounties = "GetCounties";
    public static String GetTellerSummary = "GetTellerSummary";
    public static String PrintReceipt = "PrintReceipt";
    public static String StandingOrderTransaction = "StandingOrderTransaction";
    public static String BankersChequeTransaction = "BankersChequeTransaction";
    public static String GetMemberStatement = "GetMemberStatement";
    public static String GetChequeBanks = "GetChequeBanks";
    public static String GetBankBranches = "GetBankBranches";
    public static String GetRecoveryModes = "GetRecoveryModes";
    public static String GetChequeTypes = "GetChequeTypes";
    public static String GetChequeNos = "GetChequeNos";
    public static String GetATMCardTypes = "GetATMCardTypes";
    public static String GetMemberLoans = "GetMemberLoans";
    public static String MemberBalanceEnquiry = "MemberBalanceEnquiry";
    public static String SetMemberPictures = "SetMemberPictures";
    public static String GetMemberPictures = "GetMemberPictures";
    public static String GetSignatoryPictures = "GetSignatoryPictures";
    public static String AcceptTreasuryTransaction = "AcceptTreasuryTransaction";
    public static String RejectTreasuryTransaction = "RejectTreasuryTransaction";
    public static String CancelApproval = "CancelApproval";
    public static String DelegateApproval = "DelegateApproval";
    public static String TreasuryTransaction = "TreasuryTransaction";
    public static String GetTreasuryAccounts = "GetTreasuryAccounts";
    public static String ChangePassword = "ChangePassword";
    private static String results;
    public static String password;

    public static String getVolley(final Context context , String function , String jsonRequest , final VolleyCallback callback)
    {

        try {
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            String url = Api.URL+function;
            Log.i("URL",url);
            Log.i("REQUEST",jsonRequest);

            JSONObject jsonBody = new JSONObject();
            jsonBody.put("jj","jj");
     /*       jsonBody.put("authorization_credentials", "Android Volley Demo");
            jsonBody.put("api_key", "BNK");
            jsonBody.put("token",);
            jsonBody.put("corporate_no",);
            jsonBody.put("msisdn",);
            jsonBody.put("account_number",)*/
            final String requestBody = jsonRequest;

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i("RESPONSE",response);

                    try {
                        JSONObject object = new JSONObject(response);
                        /*if(!object.getBoolean("created_successfully")){
                            if (object.getString("error").equalsIgnoreCase("Invalid Token Supplied")){
                                Util.alertMasseage(context,"Session Expired","You need to close the app and login again.");
                            }
                        }*/

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    results = response;
                    callback.onSuccess(results);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                    //((Activity)context).recreate();
                    String msg = "Please check your internet connection!";
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                    builder1.setMessage(msg);
                    builder1.setTitle("Alert!");
                    builder1.setCancelable(false);

                    builder1.setPositiveButton(
                            "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    ((Activity)context).recreate();
                                    //onBackPressed();
                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                    Toast.makeText(context, "Please check your internet connection!", Toast.LENGTH_LONG).show();
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    600000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            requestQueue.add(stringRequest);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return results;
    }

    public interface VolleyCallback{
        void onSuccess(String result);
    }

    public static void setPassword(String password) {
        Api.password = password;
    }

    public static String getPassword() {
        return password;
    }


}

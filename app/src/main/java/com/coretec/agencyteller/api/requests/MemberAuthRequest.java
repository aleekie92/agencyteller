package com.coretec.agencyteller.api.requests;

import com.coretec.agencyteller.utils.Const;

/**
 * Created by kelvinoff on 17/10/10.
 */

public abstract class MemberAuthRequest extends BaseRequest {
    public String accountidentifier;
    public String accountidentifiercode;
    public String agentid;
    public String terminalid;
    public double longitude;
    public double latitude;
    public String date;

    public MemberAuthRequest() {
        super();
    }

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }

    public abstract String getAuthenticationUrl();
}

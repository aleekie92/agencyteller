package com.coretec.agencyteller.api.requests;

/**
 * Created by kelvinoff on 17/10/10.
 */

public abstract class BaseRequest {
    public AuthorizationCredentials authorization_credentials =  new AuthorizationCredentials();

    public String url = "";

    public BaseRequest() {
        setCredentials();
    }
    public abstract void setCredentials();

    public Object getBody(){
        return this;
    }
 /*   public void setCredentials() {
        authorization_credentials.api_key = LoginResponse.getInstance().getToken();
        authorization_credentials.token = LoginResponse.getInstance().getToken();
    }*/
}

package com.coretec.agencyteller.api.requests;

/**
 * Created by kelvinoff on 17/10/10.
 */

public class AuthorizationCredentials {
    public String api_key;
    public String token;
    public String app_secret;
}

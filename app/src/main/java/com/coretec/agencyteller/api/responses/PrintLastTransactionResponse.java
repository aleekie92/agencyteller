package com.coretec.agencyteller.api.responses;

import java.io.Serializable;

/**
 * Created by kelvinoff on 17/10/09.
 */

public class PrintLastTransactionResponse extends BaseResponse implements Serializable {

    public String error;
    public String receiptno;
    public String transactiontype;
    public String sacconame;
    public String amount;
    public String transactiondate;


    public String getReceiptno() {
        return receiptno;
    }

    public void setReceiptno(String receiptno) {
        this.receiptno = receiptno;
    }


    public String getTransactiontype() {
        return transactiontype;
    }

    public void setTransactiontype(String transactiontype) {
        this.transactiontype = transactiontype;
    }

    public String getSacconame() {
        return sacconame;
    }

    public void setSacconame(String sacconame) {
        this.sacconame = sacconame;
    }

    public String getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getError() {

        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

}

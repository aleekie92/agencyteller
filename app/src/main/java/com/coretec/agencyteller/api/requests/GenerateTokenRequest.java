package com.coretec.agencyteller.api.requests;

/**
 * Created by kelvinoff on 17/10/11.
 */

public class GenerateTokenRequest extends BaseRequest {

    public GenerateTokenRequest(){
        super();
    }
    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.app_secret ="12345";
    }

    @Override
    public Object getBody() {
        return authorization_credentials;
    }
}

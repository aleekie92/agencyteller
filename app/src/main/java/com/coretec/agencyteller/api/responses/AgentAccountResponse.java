package com.coretec.agencyteller.api.responses;

import java.io.Serializable;

/**
 * Created by kelvinoff on 17/10/09.
 */

public class AgentAccountResponse extends BaseResponse implements Serializable {

    public String error;
    public String sacconame;
    public String corporateno;
    public String date;
    private AgentDetails agentdetails;


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public AgentDetails getAgentdetails() {
        return agentdetails;
    }

    public void setAgentdetails(AgentDetails agentdetails) {
        this.agentdetails = agentdetails;
    }

    public String getSacconame() {
        return sacconame;
    }

    public void setSacconame(String sacconame) {
        this.sacconame = sacconame;
    }


    public String getCorporateno() {
        return corporateno;
    }

    public void setCorporateno(String corporateno) {
        this.corporateno = corporateno;
    }

    public String getError() {

        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

}

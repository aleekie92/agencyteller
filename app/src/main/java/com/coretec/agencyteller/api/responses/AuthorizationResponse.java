package com.coretec.agencyteller.api.responses;

import java.io.Serializable;

/**
 * Created by kelvinoff on 17/10/10.
 */

public class AuthorizationResponse implements Serializable {
    public String token;
    public int status_code;
}

package com.coretec.agencyteller.api.requests;

import com.coretec.agencyteller.api.ApiTest;

/**
 * Created by kelvinoff on 18/01/17.
 */

public class BiometricAuthRequest extends MemberAuthRequest {
    public String fingerprint;

    public BiometricAuthRequest() {
    }

    @Override
    public String getAuthenticationUrl() {
        return ApiTest.AuthenticateCustomerBiometric;
    }

    public BiometricAuthRequest(String fingerprint) {
        this.fingerprint = fingerprint;
    }
}

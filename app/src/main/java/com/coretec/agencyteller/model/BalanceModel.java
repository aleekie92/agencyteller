package com.coretec.agencyteller.model;

public class BalanceModel {

    private String account;
    private String description ;
    private String balance;

    public BalanceModel (String account, String description,  String balance) {
        super();
        this.account = account;
        this.description = description;
        this.balance = balance;

    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }
}

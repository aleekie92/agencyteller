package com.coretec.agencyteller.model;

public class SignatoryList {

    private Integer no ;
    private String name;
    private String account;
    private String memberNo;
    private String nationalID;
    private Boolean signatory;
    private Boolean mustSign;
    private Boolean mustBePresent;

    /*public SignatoryList () {

    }*/

    public SignatoryList(Integer no, String name, String account, String memberNo,
                         String nationalID, Boolean signatory, Boolean mustSign, Boolean mustBePresent) {
        super();
        this.no = no;
        this.name = name;
        this.account = account;
        this.memberNo = memberNo;
        this.nationalID = nationalID;
        this.signatory = signatory;
        this.mustSign = mustSign;
        this.mustBePresent = mustBePresent;

    }

    public Integer getNo() {
        return no;
    }

    public void setNo(Integer no) {
        this.no = no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getMemberNo() {
        return memberNo;
    }

    public void setMemberNo(String memberNo) {
        this.memberNo = memberNo;
    }

    public String getNationalID() {
        return nationalID;
    }

    public void setNationalID(String nationalID) {
        this.nationalID = nationalID;
    }

    public Boolean getSignatory() {
        return signatory;
    }

    public void setSignatory(Boolean signatory) {
        this.signatory = signatory;
    }

    public Boolean getMustSign() {
        return mustSign;
    }

    public void setMustSign(Boolean mustSign) {
        this.mustSign = mustSign;
    }

    public Boolean getMustBePresent() {
        return mustBePresent;
    }

    public void setMustBePresent(Boolean mustBePresent) {
        this.mustBePresent = mustBePresent;
    }
}

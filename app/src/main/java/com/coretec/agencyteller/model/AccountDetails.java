package com.coretec.agencyteller.model;

import java.io.Serializable;

/**
 * Created by kelvinoff on 17/10/09.
 */

public class AccountDetails implements Serializable {

    private String accountno;
    private String accountname;
    private String accounttype;
    private String accountbalance;
    private String phonenumber;
    private String transactionstatus;


    public String getAccountno() {
        return accountno;
    }

    public void setAccountno(String accountno) {
        this.accountno = accountno;
    }

    public String getAccountname() {
        return accountname;
    }

    public void setAccountname(String accountname) {
        this.accountname = accountname;
    }

    public String getAccounttype() {
        return accounttype;
    }

    public void setAccounttype(String accounttype) {
        this.accounttype = accounttype;
    }

    public String getAccountbalance() {
        return accountbalance;
    }

    public void setAccountbalance(String accountbalance) {
        this.accountbalance = accountbalance;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getTransactionstatus() {
        return transactionstatus;
    }

    public void setTransactionstatus(String transactionstatus) {
        this.transactionstatus = transactionstatus;
    }

    @Override
    public String toString() {

        return accounttype + " - " + accountno;
    }
}

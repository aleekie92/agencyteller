package com.coretec.agencyteller.model;

/**
 * Created by kelvinoff on 17/10/02.
 */

public class Accounts {
    private String name;
    private int image;

    public Accounts() {

    }

    public Accounts(String name, int image) {
        this.name = name;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}

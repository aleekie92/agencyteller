package com.coretec.agencyteller.model;

public class ModelPending {

    private String no ;
    private String date;
    private String amount;
    private String type;
    private String account;
    private String approver;

    public ModelPending (String no, String date, String amount, String type, String account, String approver) {
        super();
        this.no = no;
        this.date = date;
        this.amount = amount;
        this.type = type;
        this.account = account;
        this.approver = approver;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getApprover() {
        return approver;
    }

    public void setApprover(String approver) {
        this.approver = approver;
    }
}

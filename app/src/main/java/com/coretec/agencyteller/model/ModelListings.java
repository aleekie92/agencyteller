package com.coretec.agencyteller.model;

public class ModelListings {

    private String no ;
    private String date;
    private String amount;
    private String type;
    //private String source;
    private String account;
    //private String treasuryType;

    public ModelListings (String no, String date, String amount, String type, String account) {
        super();
        this.no = no;
        this.date = date;
        this.amount = amount;
        this.type = type;
        //this.source = source;
        this.account = account;
        //this.treasuryType = treasuryType;

    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }
}

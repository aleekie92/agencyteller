package com.coretec.agencyteller.activities.teller;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyteller.R;
import com.coretec.agencyteller.api.Api;
import com.coretec.agencyteller.utils.DecimalDigitsInputFilter;
import com.coretec.agencyteller.utils.EnglishNumberToWords;
import com.coretec.agencyteller.utils.PreferenceFileKeys;
import com.coretec.agencyteller.utils.SharedPrefs;
import com.coretec.agencyteller.utils.Utils;
import com.coretec.agencyteller.wizarpos.function.printer.PrinterCommand;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class ChequeDeposit extends AppCompatActivity {

    EditText teller_account;
    private EditText identifier;
    private Spinner identifierSpinner;
    private Spinner accountSpinner;
    private ArrayList<String> arrayList = new ArrayList<>();

    private Spinner bankSpinner;
    private ArrayList<String> bankList = new ArrayList<>();

    private Spinner chequeSpinner;
    private ArrayList<String> chequeList = new ArrayList<>();

    TextView date;
    DatePickerDialog datePickerDialog;
    private Button submit;
    private Button search;

    private EditText amount, cheque_no, drawer_name, deposited_by, narration;

    private LinearLayout details;
    private TextView names;

    private ProgressBar progressBar;
    private ProgressBar bankPb;
    private ProgressBar chequePb;
    private ProgressDialog progressDialog;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPrefs.init(getApplicationContext());
        setContentView(R.layout.teller_cheque_deposit);

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

        //Toolbar declarations...Reusable
        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView mTitle = toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getText(R.string.cheque_deposit));
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);

        teller_account = findViewById(R.id.et_teller_account);
        teller_account.setFocusable(false);
        teller_account.setText(SharedPrefs.read(SharedPrefs.TELLER_ACCOUNT_NUMBER, null));

        search = findViewById(R.id.btn_search);

        details = findViewById(R.id.details_layout);
        names = findViewById(R.id.tv_details_name);

        identifierSpinner = findViewById(R.id.identifier_spinner);
        accountSpinner = findViewById(R.id.accountsSpinner);
        bankSpinner = findViewById(R.id.banksSpinner);
        chequeSpinner = findViewById(R.id.chequeTypeSpinner);
        identifier = findViewById(R.id.cheque_identifier);
        progressBar = findViewById(R.id.accountsPB);
        bankPb = findViewById(R.id.banksPB);
        chequePb = findViewById(R.id.chequeTypePB);
        date =  findViewById(R.id.cheque_date);
        submit = findViewById(R.id.button_submit);

        amount = findViewById(R.id.et_amount);
        cheque_no = findViewById(R.id.et_cheque_no);
        drawer_name = findViewById(R.id.edittext_drawer_name);
        deposited_by = findViewById(R.id.edittext_deposited_by);
        narration = findViewById(R.id.edittext_narration);

        bankPb.setVisibility(View.VISIBLE);
        chequePb.setVisibility(View.VISIBLE);
        amount.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(8,2)});
        getBanks();
        getChequeTypes();

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(ChequeDeposit.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                                /*date.setText(year + "-"
                                        + (monthOfYear + 1) + "-" + dayOfMonth);*/

                                Calendar calendar= Calendar.getInstance();
                                calendar.set(year,monthOfYear,dayOfMonth);
                                String s =String.format(Locale.getDefault(),"%1$tY-%1$tm-%1$td",calendar);
                                date.setText(s);

                                String dateSelected = date.getText().toString();
                                //SharedPrefs.write(SharedPrefs.DOB, dateSelected);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
                //datePickerDialog.updateDate(2001, 1, 1);
            }
        });

        accountSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //((TextView) parent.getChildAt(0)).setTextColor(Color.GRAY);
                String item = parent.getItemAtPosition(position).toString();
                String text = accountSpinner.getSelectedItem().toString();

                String account_number = text.substring(0, text.indexOf("-"));
                SharedPrefs.write(SharedPrefs.MEMBER_ACCOUNT_NUMBER, account_number);
                Log.e("DESTINATION ACCOUNT", account_number);
                Log.e("ACCOUNT DETAILS", text);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        chequeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //((TextView) parent.getChildAt(0)).setTextColor(Color.GRAY);
                String item = parent.getItemAtPosition(position).toString();
                String text = chequeSpinner.getSelectedItem().toString();

                if (!text.equals("cheque-type")) {
                    String chqType = text.substring(0, text.indexOf("-"));
                    SharedPrefs.write(SharedPrefs.CHEQUE_TYPE, chqType);
                    Log.e("CHEQUE TYPE", text);

                    //Getting the last part of a string
                    String path = chequeSpinner.getSelectedItem().toString();
                    String segments[] = path.split("-");
                    String chqMaturity = segments[segments.length - 1];
                    SharedPrefs.write(SharedPrefs.CHEQUE_MATURITY, chqMaturity);
                    Log.e("MATURITY", chqMaturity);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        bankSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //((TextView) parent.getChildAt(0)).setTextColor(Color.GRAY);
                String item = parent.getItemAtPosition(position).toString();
                String text = bankSpinner.getSelectedItem().toString();

                String bank_number = text.substring(0, text.indexOf("-"));
                SharedPrefs.write(SharedPrefs.BANK_NUMBER, bank_number);
                Log.e("BANK NUMBER", bank_number);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        identifierSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                String item = parent.getItemAtPosition(position).toString();
                String text = identifierSpinner.getSelectedItem().toString();

                int idLength = 8;
                int acLength = 14;
                int memLength = 14;
                InputFilter[] fArray = new InputFilter[1];
                fArray[0] = new InputFilter.LengthFilter(idLength);

                InputFilter[] aArray = new InputFilter[1];
                aArray[0] = new InputFilter.LengthFilter(acLength);

                InputFilter[] mArray = new InputFilter[1];
                mArray[0] = new InputFilter.LengthFilter(memLength);

                switch (text) {
                    case "ID NUMBER":
                        SharedPrefs.write(SharedPrefs.IDENTIFIER, "0");
                        identifier.setText(null);
                        identifier.setInputType(InputType.TYPE_CLASS_NUMBER);
                        identifier.setFilters(fArray);
                        break;
                    case "OLD A/C NUMBER":
                        SharedPrefs.write(SharedPrefs.IDENTIFIER, "4");
                        identifier.setText(null);
                        identifier.setInputType(InputType.TYPE_CLASS_TEXT);
                        identifier.setFilters(aArray);
                        break;
                    case "MEMBER NUMBER":
                        identifier.setText(null);
                        identifier.setInputType(InputType.TYPE_CLASS_NUMBER);
                        SharedPrefs.write(SharedPrefs.IDENTIFIER, "1");
                        identifier.setFilters(mArray);
                        break;
                }

                Log.e("SELECTED", text);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = identifier.getText().toString();

                if (id.length()<7 && id.length()>0) {
                    identifier.setError("Please input a valid identifier!");
                } else if (id.length()==0){
                    identifier.setError("Cannot search an empty field!");
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(ChequeDeposit.this, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                    requestAccounts(id);
                }
            }
        });

        TextWatcher textWatcher = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String idNumber = identifier.getText().toString();

                /*if (idNumber.length() >= 7) {
                    requestAccounts(idNumber);
                }*/

                if (idNumber.length() >= 5 && idNumber.length() <= 8) {
                    arrayList.clear();
                    accountSpinner = findViewById(R.id.accountsSpinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(ChequeDeposit.this, android.R.layout.simple_spinner_item, arrayList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    accountSpinner.setAdapter(adapter);
                }
                if (idNumber.length() < 5) {
                    //submitAccRegistrationRequest.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                }

            }
        };

        identifier.addTextChangedListener(textWatcher);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String idNo = identifier.getText().toString();
                String en_amount = amount.getText().toString();
                String drawer = drawer_name.getText().toString();
                String depositor = deposited_by.getText().toString();
                String chqNo = cheque_no.getText().toString();
                String narraTn = narration.getText().toString();
                String chqDate = date.getText().toString();

                if (idNo.isEmpty()) {
                    identifier.setError("Please input ID number!");
                } else if (en_amount.isEmpty()) {
                    amount.setError("Please enter amount!");
                } else if (drawer.isEmpty()) {
                    drawer_name.setError("Please input the drawer name!");
                } else if (depositor.isEmpty()) {
                    deposited_by.setError("Please input name of depositor!");
                } else if (chqNo.isEmpty()) {
                    cheque_no.setError("Please input the cheque number!");
                } else if (narraTn.isEmpty()) {
                    narration.setError("Please input the narration!");
                } else if (chqDate.isEmpty()) {
                    Toast.makeText(ChequeDeposit.this, "Please select the cheque date!", Toast.LENGTH_SHORT).show();
                } else {
                    progressDialog = new ProgressDialog(ChequeDeposit.this);
                    progressDialog.setMessage("Submitting your request...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    submitChequeDeposit(identifier.getText().toString(), amount.getText().toString(), deposited_by.getText().toString());
                    submit.setClickable(false);
                }
            }
        });

    }

    void submitChequeDeposit (final String Id, final String amou_nt, String depo) {

        String get_date = date.getText().toString();
        String final_date = get_date + "T00:00:00Z";

        Api.getVolley(this, Api.ChequeDepositTransaction, "{\n" +
                "    \"action\": \"" + 0 + "\",\n" +
                "    \"transactionType\": \"" + 5 + "\",\n" +
                "    \"deviceID\": \"" + SharedPrefs.read(SharedPrefs.DEVICE_ID, null) + "\",\n" +
                "    \"agentCode\": \"" + SharedPrefs.read(SharedPrefs.AGENT_CODE, null) + "\",\n" +
                "    \"amount\": \"" + amou_nt + "\",\n" +
                "    \"depositedBy\": \"" + depo + "\",\n" +
                "    \"sourceAccount\": \"" + SharedPrefs.read(SharedPrefs.TELLER_ACCOUNT_NUMBER, null) + "\",\n" +
                "    \"destinationAccount\": \"" + SharedPrefs.read(SharedPrefs.MEMBER_ACCOUNT_NUMBER, null) + "\",\n" +
                "    \"chequeNo\": \"" + cheque_no.getText().toString() + "\",\n" +
                "    \"chequeType\": \"" + SharedPrefs.read(SharedPrefs.CHEQUE_TYPE, null) + "\",\n" +
                "    \"chequeBank\": \"" + SharedPrefs.read(SharedPrefs.BANK_NUMBER, null) + "\",\n" +
                "    \"chequeDate\": \"" + final_date + "\",\n" +
                "    \"drawer\": \"" + drawer_name.getText().toString() + "\",\n" +
                "    \"narration\": \"" + narration.getText().toString() + "\"\n" +
                "}", new Api.VolleyCallback() {



            @Override
            public void onSuccess(String Response) {
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));
                        Toast.makeText(ChequeDeposit.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                        final String tCode = jsonObject.getString("data");

                        int result = PrinterInterface.open();
                        writetest(Id, amou_nt, tCode);
                        PrinterInterface.close();
                        progressDialog.dismiss();

                        Utils.showAlertDialog(ChequeDeposit.this, "Success!", jsonObject.getString("message")+"\n"+" Press Ok to print another Receipt ", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                int result = PrinterInterface.open();
                                writetest1(Id, amou_nt, tCode);
                                PrinterInterface.close();
                                onBackPressed();
                            }
                        });

                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        progressDialog.dismiss();
                        Toast.makeText(ChequeDeposit.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                        String msg = jsonObject.getString("message");
                        //final String qualKgs = SharedPrefs.read(SharedPrefs.LOAN_QUALIFIED_KGS, null);
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(ChequeDeposit.this);
                        builder1.setMessage(msg);
                        builder1.setTitle("Alert!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void getChequeTypes () {

        Api.getVolley(this, Api.GetChequeTypes,"{\n" +
                "}",new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                chequePb.setVisibility(View.GONE);
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            String chequeCode = jsonObject1.getString("code");
                            String chequeDescription = jsonObject1.getString("description");
                            String chequeMaturity = jsonObject1.getString("maturity");
                            String chqDetails = chequeCode + "-" + chequeMaturity;

                            chequeList.add(chqDetails);

                            Log.e("CHEQUE DETAILS", chqDetails);

                        }
                    } if (jsonObject.getString("error").equalsIgnoreCase("true")){
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        chequePb.setVisibility(View.GONE);
                        Toast.makeText(ChequeDeposit.this, "Could not fetch cheque types!", Toast.LENGTH_SHORT).show();
                    }

                    chequeSpinner = findViewById(R.id.chequeTypeSpinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(ChequeDeposit.this, android.R.layout.simple_spinner_item, chequeList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    chequeSpinner.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void getBanks () {

        Api.getVolley(this, Api.GetChequeBanks,"{\n" +
                "    \"agentCode\": \"" + SharedPrefs.read(SharedPrefs.AGENT_CODE, null) + "\",\n" +
                "    \"filter\": \"" + true + "\"\n" +
                "}",new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                bankPb.setVisibility(View.GONE);
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            String bankNo = jsonObject1.getString("no");
                            String bankName = jsonObject1.getString("name");
                            String bankBal = jsonObject1.getString("balance");
                            String bankDetails = bankNo + "-" + bankName;

                            bankList.add(bankDetails);

                            Log.e("BANK DETAILS", bankDetails);

                        }
                    } if (jsonObject.getString("error").equalsIgnoreCase("true")){
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        bankPb.setVisibility(View.GONE);
                        Toast.makeText(ChequeDeposit.this, "Could not fetch bank!", Toast.LENGTH_SHORT).show();
                    }

                    bankSpinner = findViewById(R.id.banksSpinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(ChequeDeposit.this, android.R.layout.simple_spinner_item, bankList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    bankSpinner.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void requestAccounts (String id) {

        int identifier = Integer.parseInt(SharedPrefs.read(SharedPrefs.IDENTIFIER, null));

        Api.getVolley(this, Api.GetMemberAccounts,"{\n" +
                "    \"identifier\": \"" + id + "\",\n" +
                "    \"identifierType\": \"" + identifier + "\",\n" +
                "    \"natureOfAccount\": \"" + 2 + "\"\n" +
                "}",new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                progressBar.setVisibility(View.GONE);
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            String accountName = jsonObject1.getString("name");
                            String accountNo = jsonObject1.getString("no");
                            String desc = jsonObject1.getString("description");
                            String accountDetails = accountNo + "-" + desc;

                            arrayList.add(accountDetails);
                            details.setVisibility(View.VISIBLE);
                            names.setText(accountName);

                            Log.e("ACCOUNT DETAILS", accountDetails);

                        }
                    } if (jsonObject.getString("error").equalsIgnoreCase("true")){
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(ChequeDeposit.this, "Error!", Toast.LENGTH_SHORT).show();
                    }

                    accountSpinner = findViewById(R.id.accountsSpinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(ChequeDeposit.this, android.R.layout.simple_spinner_item, arrayList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    accountSpinner.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public void writetest(String idNo, String amount, String transactionCode) {

        String accountNumber = SharedPrefs.read(SharedPrefs.MEMBER_ACCOUNT_NUMBER, null);
        String firstAc = accountNumber.substring(0, 4);
        String lastAc = accountNumber.substring(8, accountNumber.length());
        String maskedAc = firstAc + "****" + lastAc;

        int number = Integer.parseInt(amount);

        String return_val_in_english = EnglishNumberToWords.convert(number);

        try {

            byte[] start = null;
            byte[] saccoName = null;
            byte[] type = null;

            byte[] terminalNo = null;
            byte[] transaction = null;
            byte[] time = null;
            byte[] transDate = null;
            byte[] account_dposited_to = null;
            byte[] idNumber = null;
            byte[] fullNames = null;
            byte[] fName = null;
            byte[] depositedBy = null;

            //byte[] phoneNumber = null;
            byte[] chequeNumber = null;
            byte[] arrydrawer = null;
            byte[] chequeDate = null;
            byte[] chqMaturityDate = null;
            //byte[] chequeType = null;
            byte[] arryDepositAmt = null;
            byte[] arryWordsHeader = null;
            byte[] arryInWords = null;

            byte[] acknowLedge = null;
            byte[] nameVerify = null;
            byte[] idVerify = null;
            byte[] signature = null;

            byte[] narration_txt = null;

            byte[] nameAgent = null;
            byte[] customerReceipt = null;
            byte[] motto = null;

            SimpleDateFormat timeStampFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
            Date myDate = new Date();
            String timeCurrent = timeStampFormat.format(myDate);
            String aName = SharedPrefs.read(SharedPrefs.AGENT_NAME, null);
            String depositorsName = deposited_by.getText().toString();

            String cheque_num = cheque_no.getText().toString();
            String bank_num = SharedPrefs.read(SharedPrefs.BANK_NUMBER, null);
            String cheque_type = SharedPrefs.read(SharedPrefs.CHEQUE_TYPE, null);
            String cheque_date = date.getText().toString();

            String chqMaturity = SharedPrefs.read(SharedPrefs.CHEQUE_MATURITY, null);
            int maturityNum = Integer.parseInt(chqMaturity);

            String dt = cheque_date;  // Start date - HH:mm
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Calendar c = Calendar.getInstance();
            try {
                c.setTime(sdf.parse(dt));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            c.add(Calendar.DATE, maturityNum);  // number of days to add, can also use Calendar.DAY_OF_MONTH in place of Calendar.DATE
            SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");
            String output = sdf.format(c.getTime());

            String narra_tion = narration.getText().toString();
            String setNarration = null;
            if (narra_tion.isEmpty()) {
                setNarration = "N/A";
            } else {
                setNarration = narration.getText().toString();
            }

            try {
                start = String.valueOf("              ").getBytes("GB2312");
                saccoName = String.valueOf(SharedPrefs.read(SharedPrefs.SACCO_NAME, null)).getBytes("GB2312");
                type = String.valueOf("        CHEQUE DEPOSIT     ").getBytes("GB2312");

                terminalNo = String.valueOf("Terminal ID : " + SharedPrefs.read(SharedPrefs.DEVICE_ID, null)).getBytes("GB2312");
                transaction = String.valueOf("Transaction No : " + transactionCode).getBytes("GB2312");
                time = String.valueOf("Print Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                transDate = String.valueOf("Trans. Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                account_dposited_to = String.valueOf("Account NO : " + accountNumber).getBytes("GB2312");
                idNumber = String.valueOf("ID Number : " + String.valueOf(idNo)).getBytes("GB2312");
                fullNames = String.valueOf("Customer Full Names : ").getBytes("GB2312");
                fName = String.valueOf(String.valueOf(names.getText().toString())).getBytes("GB2312");

                //phoneNumber = String.valueOf("Phone Number : " + String.valueOf(maskedOP)).getBytes("GB2312");
                chequeNumber = String.valueOf("Cheque Number : " + cheque_num).getBytes("GB2312");
                arrydrawer = String.valueOf("Drawer : " + drawer_name.getText().toString()).getBytes("GB2312");
                chequeDate = String.valueOf("Cheque Date : " + cheque_date).getBytes("GB2312");
                chqMaturityDate = String.valueOf("Chq Maturity Date : " + output).getBytes("GB2312");
                arryDepositAmt = String.valueOf("Amount : KSH" + amount).getBytes("GB2312");
                arryWordsHeader = String.valueOf("Amount in Words: ").getBytes("GB2312");
                arryInWords = String.valueOf(return_val_in_english +" Kenya Shillings Only").getBytes("GB2312");

                narration_txt = String.valueOf("Narration : " + setNarration).getBytes("GB2312");
                depositedBy = String.valueOf("Deposited By " + depositorsName).getBytes("GB2312");

                acknowLedge = String.valueOf("I acknowledge deposit of the above amount").getBytes("GB2312");
                nameVerify = String.valueOf("Name___________________________").getBytes("GB2312");
                idVerify = String.valueOf("ID No___________________________").getBytes("GB2312");
                signature = String.valueOf("Signature_______________________").getBytes("GB2312");

                nameAgent = String.valueOf("SERVED BY " + String.valueOf(aName)).getBytes("GB2312");
                customerReceipt = String.valueOf("         CUSTOMER COPY         ").getBytes("GB2312");
                motto = String.valueOf(SharedPrefs.read(SharedPrefs.SACCO_MOTTO, null)).getBytes("GB2312");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(start);
            writeLineBreak(1);
            write(saccoName);
            writeLineBreak(2);
            write(type);
            writeLineBreak(1);


            write(terminalNo);
            writeLineBreak(1);
            write(transaction);
            writeLineBreak(1);
            write(time);
            writeLineBreak(1);
            write(transDate);
            writeLineBreak(1);
            write(account_dposited_to);
            writeLineBreak(1);
            /*write(idNumber);
            writeLineBreak(1);*/
            write(fullNames);
            writeLineBreak(0);
            write(fName);
            writeLineBreak(2);

            write(chequeNumber);
            writeLineBreak(1);
            write(arrydrawer);
            writeLineBreak(1);
            write(chequeDate);
            writeLineBreak(1);
            write(chqMaturityDate);
            writeLineBreak(1);
            write(arryDepositAmt);
            writeLineBreak(1);
            write(arryWordsHeader);
            writeLineBreak(0);
            write(arryInWords);
            writeLineBreak(2);

            write(narration_txt);
            writeLineBreak(1);
            write(depositedBy);
            writeLineBreak(2);

            write(acknowLedge);
            writeLineBreak(1);
            write(nameVerify);
            writeLineBreak(1);
            write(idVerify);
            writeLineBreak(1);
            write(signature);
            writeLineBreak(2);

            write(nameAgent);
            writeLineBreak(2);
            write(customerReceipt);
            writeLineBreak(2);
            write(motto);
            // print line break
            writeLineBreak(4);

            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void writetest1(String idNo, String amount, String transactionCode) {

        String accountNumber = SharedPrefs.read(SharedPrefs.MEMBER_ACCOUNT_NUMBER, null);
        String firstAc = accountNumber.substring(0, 4);
        String lastAc = accountNumber.substring(8, accountNumber.length());
        String maskedAc = firstAc + "****" + lastAc;

        int number = Integer.parseInt(amount);

        String return_val_in_english = EnglishNumberToWords.convert(number);

        try {

            byte[] start = null;
            byte[] saccoName = null;
            byte[] type = null;

            byte[] terminalNo = null;
            byte[] transaction = null;
            byte[] time = null;
            byte[] transDate = null;
            byte[] account_dposited_to = null;
            byte[] idNumber = null;
            byte[] fullNames = null;
            byte[] fName = null;
            byte[] depositedBy = null;

            byte[] chequeNumber = null;
            byte[] arrydrawer = null;
            byte[] chequeDate = null;
            byte[] chqMaturityDate = null;
            //byte[] chequeType = null;
            byte[] arryDepositAmt = null;
            byte[] arryWordsHeader = null;
            byte[] arryInWords = null;

            byte[] acknowLedge = null;
            byte[] nameVerify = null;
            byte[] idVerify = null;
            byte[] signature = null;

            byte[] narration_txt = null;

            byte[] nameAgent = null;
            byte[] customerReceipt = null;
            byte[] motto = null;

            SimpleDateFormat timeStampFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
            Date myDate = new Date();
            String timeCurrent = timeStampFormat.format(myDate);
            String aName = SharedPrefs.read(SharedPrefs.AGENT_NAME, null);
            String depositorsName = deposited_by.getText().toString();

            String cheque_num = cheque_no.getText().toString();
            String bank_num = SharedPrefs.read(SharedPrefs.BANK_NUMBER, null);
            String cheque_type = SharedPrefs.read(SharedPrefs.CHEQUE_TYPE, null);
            String cheque_date = date.getText().toString();

            String chqMaturity = SharedPrefs.read(SharedPrefs.CHEQUE_MATURITY, null);
            int maturityNum = Integer.parseInt(chqMaturity);

            String dt = cheque_date;  // Start date
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Calendar c = Calendar.getInstance();
            try {
                c.setTime(sdf.parse(dt));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            c.add(Calendar.DATE, maturityNum);  // number of days to add, can also use Calendar.DAY_OF_MONTH in place of Calendar.DATE
            SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");
            String output = sdf.format(c.getTime());

            String narra_tion = narration.getText().toString();
            String setNarration = null;
            if (narra_tion.isEmpty()) {
                setNarration = "N/A";
            } else {
                setNarration = narration.getText().toString();
            }

            try {
                start = String.valueOf("              ").getBytes("GB2312");
                saccoName = String.valueOf(SharedPrefs.read(SharedPrefs.SACCO_NAME, null)).getBytes("GB2312");
                type = String.valueOf("        CHEQUE DEPOSIT     ").getBytes("GB2312");

                terminalNo = String.valueOf("Terminal ID : " + SharedPrefs.read(SharedPrefs.DEVICE_ID, null)).getBytes("GB2312");
                transaction = String.valueOf("Transaction No : " + transactionCode).getBytes("GB2312");
                time = String.valueOf("Print Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                transDate = String.valueOf("Trans. Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                account_dposited_to = String.valueOf("Account NO : " + accountNumber).getBytes("GB2312");
                idNumber = String.valueOf("ID Number : " + String.valueOf(idNo)).getBytes("GB2312");
                fullNames = String.valueOf("Customer Full Names : ").getBytes("GB2312");
                fName = String.valueOf(String.valueOf(names.getText().toString())).getBytes("GB2312");

                //phoneNumber = String.valueOf("Phone Number : " + String.valueOf(maskedOP)).getBytes("GB2312");
                chequeNumber = String.valueOf("Cheque Number : " + cheque_num).getBytes("GB2312");
                arrydrawer = String.valueOf("Drawer : " + drawer_name.getText().toString()).getBytes("GB2312");
                chequeDate = String.valueOf("Cheque Date : " + cheque_date).getBytes("GB2312");
                chqMaturityDate = String.valueOf("Chq Maturity Date : " + output).getBytes("GB2312");
                arryDepositAmt = String.valueOf("Amount : KSH" + amount).getBytes("GB2312");
                arryWordsHeader = String.valueOf("Amount in Words: ").getBytes("GB2312");
                arryInWords = String.valueOf(return_val_in_english +" Kenya Shillings Only").getBytes("GB2312");

                narration_txt = String.valueOf("Narration : " + setNarration).getBytes("GB2312");
                depositedBy = String.valueOf("Deposited By " + depositorsName).getBytes("GB2312");

                acknowLedge = String.valueOf("I acknowledge deposit of the above amount").getBytes("GB2312");
                nameVerify = String.valueOf("Name___________________________").getBytes("GB2312");
                idVerify = String.valueOf("ID No___________________________").getBytes("GB2312");
                signature = String.valueOf("Signature_______________________").getBytes("GB2312");

                nameAgent = String.valueOf("SERVED BY " + String.valueOf(aName)).getBytes("GB2312");
                customerReceipt = String.valueOf("         AGENT COPY         ").getBytes("GB2312");
                motto = String.valueOf(SharedPrefs.read(SharedPrefs.SACCO_MOTTO, null)).getBytes("GB2312");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(start);
            writeLineBreak(1);
            write(saccoName);
            writeLineBreak(2);
            write(type);
            writeLineBreak(1);


            write(terminalNo);
            writeLineBreak(1);
            write(transaction);
            writeLineBreak(1);
            write(time);
            writeLineBreak(1);
            write(transDate);
            writeLineBreak(1);
            write(account_dposited_to);
            writeLineBreak(1);
            /*write(idNumber);
            writeLineBreak(1);*/
            write(fullNames);
            writeLineBreak(0);
            write(fName);
            writeLineBreak(2);

            write(chequeNumber);
            writeLineBreak(1);
            write(arrydrawer);
            writeLineBreak(1);
            write(chequeDate);
            writeLineBreak(1);
            write(chqMaturityDate);
            writeLineBreak(1);
            write(arryDepositAmt);
            writeLineBreak(1);
            write(arryWordsHeader);
            writeLineBreak(0);
            write(arryInWords);
            writeLineBreak(2);

            write(narration_txt);
            writeLineBreak(1);
            write(depositedBy);
            writeLineBreak(2);

            write(acknowLedge);
            writeLineBreak(1);
            write(nameVerify);
            writeLineBreak(1);
            write(idVerify);
            writeLineBreak(1);
            write(signature);
            writeLineBreak(2);

            write(nameAgent);
            writeLineBreak(2);
            write(customerReceipt);
            writeLineBreak(2);
            write(motto);
            // print line break
            writeLineBreak(4);

            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void write(final byte[] arryData) {
        int result = PrinterInterface.write(arryData, arryData.length);
    }

    // print line break
    private void writeLineBreak(int lineNumber) {
        // write(new byte[]{'\n'});
        write(PrinterCommand.getCmdEscDN(lineNumber));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                //Toast.makeText(Account.this, "Back has been presses!", Toast.LENGTH_LONG).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }

}

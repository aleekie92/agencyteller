package com.coretec.agencyteller.activities.receipts;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.widget.TextView;

import com.coretec.agencyteller.R;
import com.coretec.agencyteller.wizarpos.mvc.base.ActionCallback;

public class LoanRepaymentReceipt extends AppCompatActivity {
    public static final String DEPOSIT_RESPONSE_EXTRA = "DEPOSIT_RESPONSE_EXTRA";
    public static final String DEPOSIT_ACCOUNT_EXTRA = "DEPOSIT_ACCOUNT_EXTRA";
    private TextView deposit_transaction_date, deposit_terminal_id, deposit_receipt_number, deposit_customer_name;
    private TextView deposit_amount, deposit_sacco_name, depoAccountNumber;
    private AppCompatButton printDepositReceipt;
    public static Handler handler;
    public static ActionCallback callback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loan_repayment_receipt);


    }
}

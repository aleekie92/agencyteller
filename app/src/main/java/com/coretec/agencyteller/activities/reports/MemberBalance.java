package com.coretec.agencyteller.activities.reports;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyteller.R;
import com.coretec.agencyteller.adapters.BalanceAdapter;
import com.coretec.agencyteller.api.Api;
import com.coretec.agencyteller.model.BalanceModel;
import com.coretec.agencyteller.utils.PreferenceFileKeys;
import com.coretec.agencyteller.utils.SharedPrefs;
import com.coretec.agencyteller.wizarpos.function.printer.PrinterCommand;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MemberBalance extends AppCompatActivity {

    private EditText identifier;
    private Spinner identifierSpinner;
    private Button search, print;

    private TextView member_name;
    private LinearLayout linear_header, linear_rv;
    public RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;

    private ProgressBar progressBar;
    private ProgressDialog progressDialog;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPrefs.init(getApplicationContext());
        setContentView(R.layout.activity_member_balance);

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

        //Toolbar declarations...Reusable
        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView mTitle = toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getText(R.string.member_bal));
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);

        identifierSpinner = findViewById(R.id.identifier_spinner);
        identifier = findViewById(R.id.mini_identifier);
        progressBar = findViewById(R.id.accountsPB);
        search = findViewById(R.id.btn_search);
        print = findViewById(R.id.button_print);

        member_name = findViewById(R.id.tv_member_name);
        linear_rv = findViewById(R.id.linear_recycler_view);
        linear_header = findViewById(R.id.linear_name_header);
        mRecyclerView = findViewById(R.id.recycler_view);

        identifierSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.GRAY);
                String item = parent.getItemAtPosition(position).toString();
                String text = identifierSpinner.getSelectedItem().toString();

                int idLength = 8;
                int acLength = 14;
                int memLength = 14;

                InputFilter[] fArray = new InputFilter[1];
                fArray[0] = new InputFilter.LengthFilter(idLength);

                InputFilter[] aArray = new InputFilter[1];
                aArray[0] = new InputFilter.LengthFilter(acLength);

                InputFilter[] mArray = new InputFilter[1];
                mArray[0] = new InputFilter.LengthFilter(memLength);

                if (text.equals("ID NUMBER")) {
                    SharedPrefs.write(SharedPrefs.IDENTIFIER, "0");
                    identifier.setText(null);
                    identifier.setInputType(InputType.TYPE_CLASS_NUMBER);
                    identifier.setFilters(fArray);
                } else if (text.equals("OLD A/C NUMBER")) {
                    SharedPrefs.write(SharedPrefs.IDENTIFIER, "4");
                    identifier.setText(null);
                    identifier.setInputType(InputType.TYPE_CLASS_TEXT);
                    identifier.setFilters(aArray);
                } else if (text.equals("MEMBER NUMBER")) {
                    identifier.setText(null);
                    identifier.setInputType(InputType.TYPE_CLASS_NUMBER);
                    SharedPrefs.write(SharedPrefs.IDENTIFIER, "1");
                    identifier.setFilters(mArray);
                }

                Log.e("SELECTED", text);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = identifier.getText().toString();

                if (id.length()<7 && id.length()>0) {
                    identifier.setError("Please input a valid identifier!");
                } else if (id.length()==0){
                    identifier.setError("Cannot search an empty field!");
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(MemberBalance.this, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                    //submit.setVisibility(View.VISIBLE);
                    //requestAccounts(id);
                    requestAccountsBalance(id);
                }
            }
        });

        print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog = new ProgressDialog(MemberBalance.this);
                progressDialog.setMessage("Printing balance...");
                progressDialog.setCancelable(false);
                progressDialog.show();

                int result = PrinterInterface.open();
                header();
                PrinterInterface.close();
                getBalances(identifier.getText().toString());
            }
        });

    }

    public void setuprecyclerview(List<BalanceModel> modList) {

        mRecyclerView.setLayoutManager(new LinearLayoutManager(MemberBalance.this));
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(mRecyclerView.getContext(), DividerItemDecoration.VERTICAL));

        mAdapter = new BalanceAdapter(MemberBalance.this, modList);
        mRecyclerView.setAdapter(mAdapter);

    }

    private void getBalances (String id) {

        int identifier = Integer.parseInt(SharedPrefs.read(SharedPrefs.IDENTIFIER, null));

        Api.getVolley(this, Api.MemberBalanceEnquiry,"{\n" +
                "    \"identifier\": \"" + id + "\",\n" +
                "    \"identifierType\": \"" + identifier + "\"\n" +
                "}",new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                progressBar.setVisibility(View.GONE);
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {

                        List<BalanceModel> balModelList= new ArrayList<>();

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            JSONObject balObject = jsonObject1.getJSONObject("balance");
                            //String memberName = jsonObject1.getString("memberName");
                            String accountNo = jsonObject1.getString("no");
                            String desc = jsonObject1.getString("name");
                            String balance = balObject.getString("actual");

                           // SharedPrefs.write(SharedPrefs.ACC_BAL_NAME, memberName);

                            //Log.d("nameee","nameee----->"+memberName);

                            //String memberName = jsonObject1.getString("memberName");
                            /*String accountNo = jsonObject1.getString("account");
                            String desc = jsonObject1.getString("description");
                            String balance = jsonObject1.getString("balance");*/

                            int result = PrinterInterface.open();
                            writetest(accountNo, desc, balance);
                            PrinterInterface.close();
                            progressDialog.dismiss();
                            String msg = "Balance printed successfully!";
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(MemberBalance.this);
                            builder1.setMessage(msg);
                            builder1.setTitle("Success!");
                            builder1.setCancelable(false);

                            builder1.setPositiveButton(
                                    "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                            onBackPressed();
                                        }
                                    });

                            AlertDialog alert11 = builder1.create();
                            alert11.show();

                        }

                        setuprecyclerview(balModelList);

                    } if (jsonObject.getString("error").equalsIgnoreCase("true")){
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        progressBar.setVisibility(View.GONE);
                        linear_header.setVisibility(View.GONE);
                        linear_rv.setVisibility(View.GONE);
                        Toast.makeText(MemberBalance.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                int result = PrinterInterface.open();
                footer();
                PrinterInterface.close();
            }
        });

    }

    private void requestAccountsBalance (String id) {

        int identifier = Integer.parseInt(SharedPrefs.read(SharedPrefs.IDENTIFIER, null));

        Api.getVolley(this, Api.MemberBalanceEnquiry,"{\n" +
                "    \"identifier\": \"" + id + "\",\n" +
                "    \"identifierType\": \"" + identifier + "\"\n" +
                "}",new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                progressBar.setVisibility(View.GONE);
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {

                        List<BalanceModel> balModelList= new ArrayList<>();

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            JSONObject balObject = jsonObject1.getJSONObject("balance");
                            //String memberName = jsonObject1.getString("memberName");
                            String accountNo = jsonObject1.getString("no");
                            String desc = jsonObject1.getString("name");
                            String balance = balObject.getString("actual");

                            BalanceModel newMod = new BalanceModel(accountNo, desc, balance);
                            balModelList.add(newMod);

                            //SharedPrefs.write(SharedPrefs.ACC_NAME, memberName);

                            linear_header.setVisibility(View.VISIBLE);
                            linear_rv.setVisibility(View.VISIBLE);
                            //member_name.setText(memberName);

                        }

                        setuprecyclerview(balModelList);

                    } if (jsonObject.getString("error").equalsIgnoreCase("true")){
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        progressBar.setVisibility(View.GONE);
                        linear_header.setVisibility(View.GONE);
                        linear_rv.setVisibility(View.GONE);
                        Toast.makeText(MemberBalance.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public void header(){

        //String middle = idNos.substring(2, 6);
        /*String accountNumber = SharedPrefs.read(SharedPrefs.MEMBER_ACCOUNT_NUMBER, null);
        String firstAc = accountNumber.substring(0, 4);
        String lastAc = accountNumber.substring(8, accountNumber.length());
        String maskedAc = firstAc + "****" + lastAc;*/
        try {
            byte[] start = null;
            byte[] saccoName = null;
            byte[] type = null;
            byte[] gno = null;
            byte[] accName = null;

            try {
                start = String.valueOf("              ").getBytes("GB2312");
                saccoName = String.valueOf(SharedPrefs.read(SharedPrefs.SACCO_NAME, null)).getBytes("GB2312");
                type = String.valueOf("     BALANCE RECEIPT     ").getBytes("GB2312");
                //gno = String.valueOf("A/C Number : " + String.valueOf(maskedAc)).getBytes("GB2312");
                accName = String.valueOf("A/C Name : " + SharedPrefs.read(SharedPrefs.ACC_BAL_NAME, null)).getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();
            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(start);
            writeLineBreak(1);
            write(saccoName);
            writeLineBreak(2);
            write(type);
            writeLineBreak(1);
            /*write(gno);
            writeLineBreak(1);*/
           /* write(accName);
            writeLineBreak(1);*/
            PrinterInterface.end();

        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void writetest(String datePosting, String desc, String amnt) {
        try {
            byte[] titles = null;
            byte[] date = null;
            byte[] descriptn = null;
            byte[] mini_amount = null;

            try {
                //titles = String.valueOf("   TITLE    " ).getBytes("GB2312");
                date = String.valueOf("Account : " + String.valueOf(datePosting)).getBytes("GB2312");
                descriptn = String.valueOf("Description : " + String.valueOf(desc)).getBytes("GB2312");
                mini_amount = String.valueOf("Balance : " + String.valueOf(amnt)).getBytes("GB2312");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            //write(titles);
            //writeLineBreak(1);
            write(date);
            writeLineBreak(0);
            write(descriptn);
            writeLineBreak(0);
            write(mini_amount);
            writeLineBreak(3);
            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void footer(){
        try {
            byte[] time = null;
            byte[] nameAgent = null;
            byte[] motto = null;

            SimpleDateFormat timeStampFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
            Date myDate = new Date();
            String timeCurrent = timeStampFormat.format(myDate);
            String aName = SharedPrefs.read(SharedPrefs.AGENT_NAME, null);

            try {
                time = String.valueOf("   Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                nameAgent = String.valueOf("SERVED BY " + String.valueOf(aName)).getBytes("GB2312");
                motto = String.valueOf(SharedPrefs.read(SharedPrefs.SACCO_MOTTO, null)).getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();
            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(time);
            writeLineBreak(2);
            write(nameAgent);
            writeLineBreak(1);
            write(motto);
            // print line break
            writeLineBreak(4);
            PrinterInterface.end();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void write(final byte[] arryData) {
        int result = PrinterInterface.write(arryData, arryData.length);
    }

    // print line break
    private void writeLineBreak(int lineNumber) {
        // write(new byte[]{'\n'});
        write(PrinterCommand.getCmdEscDN(lineNumber));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                //Toast.makeText(Account.this, "Back has been presses!", Toast.LENGTH_LONG).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }

}

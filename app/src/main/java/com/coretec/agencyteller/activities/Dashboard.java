package com.coretec.agencyteller.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.coretec.agencyteller.R;
import com.coretec.agencyteller.activities.member.AtmApplication;
import com.coretec.agencyteller.activities.member.MsaccoApplication;
import com.coretec.agencyteller.activities.member.StandingOrder;
import com.coretec.agencyteller.activities.reports.MemberBalance;
import com.coretec.agencyteller.activities.reports.Ministatement;
import com.coretec.agencyteller.activities.reports.BalanceInquiry;
import com.coretec.agencyteller.activities.reports.PaymentReceipt;
import com.coretec.agencyteller.activities.reports.TellerSummary;
import com.coretec.agencyteller.activities.teller.Transactions;
import com.coretec.agencyteller.utils.SharedPrefs;

public class Dashboard extends AppCompatActivity {

    CardView member_reg, teller_trans, reports, member_app;
    AlertDialog reportsDialog;
    AlertDialog memberApplicationDialog;
    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard);

        member_reg = findViewById(R.id.card_member_registration);
        teller_trans = findViewById(R.id.card_teller_transactions);
        reports = findViewById(R.id.card_reports);
        member_app = findViewById(R.id.card_member_applications);

        member_reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Dashboard.this, MemberRegistration.class));
            }
        });
        teller_trans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Dashboard.this, Transactions.class));
            }
        });

        reports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(Dashboard.this, ReportsActivity.class));
                createDialogReports();
            }
        });

        member_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDialogApp();
            }
        });


    }

    private void createDialogReports() {

        LayoutInflater myInflater = LayoutInflater.from(this);
        View v = myInflater.inflate(R.layout.dialog_reports, null);

        //define and access view attributes
        //EditText amount = v.findViewById(R.id.amountEt);
        //Buy airtime static options
        LinearLayout member_bal = v.findViewById(R.id.option_member_balance);
        LinearLayout mini_statement = v.findViewById(R.id.option_ministatement);
        LinearLayout teller_summary = v.findViewById(R.id.option_teller);
        LinearLayout balance_inquiry = v.findViewById(R.id.option_balance);
        LinearLayout payment_receipt = v.findViewById(R.id.option_payment);

        //on click listeners
        member_bal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Dashboard.this, MemberBalance.class));
            }
        });
        mini_statement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Dashboard.this, Ministatement.class));
            }
        });
        teller_summary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Dashboard.this, TellerSummary.class));
            }
        });
        balance_inquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Dashboard.this, BalanceInquiry.class));
            }
        });
        payment_receipt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = "Normal";
                Intent i = new Intent(Dashboard.this, PaymentReceipt.class);
                i.putExtra("transaction_number", title);

                Dashboard.this.startActivity(i);
                //startActivity(new Intent(Dashboard.this, PaymentReceipt.class));
            }
        });

        /*WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        Window windowAlDl = reportsDialog.getWindow();

        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;

        windowAlDl.setAttributes(layoutParams);*/

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(v);
        reportsDialog = builder.create();
        reportsDialog.show();

    }

    private void createDialogApp() {

        LayoutInflater myInflater = LayoutInflater.from(this);
        View v = myInflater.inflate(R.layout.dialog_member_app, null);

        //define and access view attributes
        //EditText amount = v.findViewById(R.id.amountEt);
        //Buy airtime static options
        LinearLayout msacco_app = v.findViewById(R.id.msacco_app);
        LinearLayout atm_app = v.findViewById(R.id.atm_app);
        LinearLayout sto_app = v.findViewById(R.id.standing_order);

        //on click listeners
        msacco_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Dashboard.this, MsaccoApplication.class));
            }
        });
        atm_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Dashboard.this, AtmApplication.class));
            }
        });
        sto_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Dashboard.this, StandingOrder.class));
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(v);
        memberApplicationDialog = builder.create();
        memberApplicationDialog.show();

    }

    @Override
    public void onBackPressed() {
        Toast.makeText(this, "Please press again to exit", Toast.LENGTH_SHORT).show();
        //super.onBackPressed();
        if (doubleBackToExitPressedOnce) {
            String msg = "Are you sure you want to logout app? To logout just press yes below!";
            android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(Dashboard.this);
            builder1.setMessage(msg);
            builder1.setTitle("Exit!");
            builder1.setCancelable(false);

            builder1.setPositiveButton(
                    "yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            finish();



                            Intent i = new Intent(Dashboard.this, LoginActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            //i.addFlags(IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                            //onBackPressed();
                        }
                    });

            builder1.setNegativeButton(
                    "CANCEL",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            //onBackPressed();
                        }
                    });

            android.app.AlertDialog alert11 = builder1.create();
            alert11.show();
            //super.onBackPressed();
            //System.exit(1);
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        //Toast.makeText(this, "Press back again to EXIT app!", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

}

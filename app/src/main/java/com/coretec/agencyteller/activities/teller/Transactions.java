package com.coretec.agencyteller.activities.teller;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.coretec.agencyteller.R;
import com.coretec.agencyteller.activities.Dashboard;
import com.coretec.agencyteller.activities.treasury.TellerTransfers;
import com.coretec.agencyteller.activities.treasury.TreasuryRequest;
import com.coretec.agencyteller.activities.treasury.TreasuryReturns;

public class Transactions extends AppCompatActivity {

    CardView cash_deposit, cash_withdrawal, cheque_deposit, banker_cheque, cash_receipt, credit_receipt, transfers, lien_holding;
    CardView return_to_treasury, inter_teller_transfers, teller_listing, teller_approvals, approved_transaction, pending_approvals, request_treasury;

    AlertDialog lienDialog;
    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.teller_transactions);

        cash_deposit = findViewById(R.id.card_cash_deposit);
        cash_withdrawal = findViewById(R.id.card_cash_withdrawal);
        cheque_deposit = findViewById(R.id.card_cheque_deposit);
        banker_cheque = findViewById(R.id.card_banker_cheque);
        cash_receipt = findViewById(R.id.card_cash_receipt);
        credit_receipt = findViewById(R.id.card_credit_receipt);
        transfers = findViewById(R.id.card_transfers);
        lien_holding = findViewById(R.id.card_lien);
        teller_listing = findViewById(R.id.card_teller_listings);
        teller_approvals = findViewById(R.id.card_teller_approvals);
        approved_transaction = findViewById(R.id.card_approved_transactions);
        pending_approvals = findViewById(R.id.card_pending_approvals);
        request_treasury = findViewById(R.id.card_request_treasury);

        return_to_treasury = findViewById(R.id.card_treasury_returns);
        inter_teller_transfers = findViewById(R.id.card_teller_transfers);

        cash_deposit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Transactions.this, CashDeposit.class));
            }
        });

        cash_withdrawal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Transactions.this, CashWithdrawal.class));
            }
        });

        cheque_deposit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Transactions.this, ChequeDeposit.class));
            }
        });

        banker_cheque.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Transactions.this, BankerCheque.class));
            }
        });

        cash_receipt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Transactions.this, CashReceipt.class));
            }
        });

        credit_receipt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Transactions.this, CreditReceipt.class));
            }
        });

        transfers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Transactions.this, Transfers.class));
            }
        });

        return_to_treasury.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Transactions.this, TreasuryReturns.class));
            }
        });

        inter_teller_transfers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Transactions.this, TellerTransfers.class));
            }
        });

        lien_holding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDialogLien();
            }
        });

        teller_listing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Transactions.this, TellerListings.class));
            }
        });

        teller_approvals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Transactions.this, TellerApprovals.class));
            }
        });

        approved_transaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(Transactions.this, ApprovedTransactions.class));
                createDialogApprovals();
            }
        });

        pending_approvals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Transactions.this, PendingApprovals.class));
            }
        });

        request_treasury.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Transactions.this, TreasuryRequest.class));
            }
        });

    }

    private void createDialogApprovals() {

        LayoutInflater myInflater = LayoutInflater.from(this);
        View v = myInflater.inflate(R.layout.dialog_apprvls, null);

        //define and access view attributes
        //EditText amount = v.findViewById(R.id.amountEt);
        //Buy airtime static options
        LinearLayout issue = v.findViewById(R.id.option_issue);
        LinearLayout receive = v.findViewById(R.id.option_receive);

        //on click listeners
        issue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Transactions.this, ApprovedTransactions.class));
            }
        });
        receive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Transactions.this, Receive.class));
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(v);
        lienDialog = builder.create();
        lienDialog.show();

    }

    private void createDialogLien() {

        LayoutInflater myInflater = LayoutInflater.from(this);
        View v = myInflater.inflate(R.layout.dialog_lien, null);

        //define and access view attributes
        //EditText amount = v.findViewById(R.id.amountEt);
        //Buy airtime static options
        LinearLayout hold_lien = v.findViewById(R.id.option_hold);
        LinearLayout clear_lien = v.findViewById(R.id.option_clear);

        //on click listeners
        hold_lien.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Transactions.this, LienHolding.class));
            }
        });
        clear_lien.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Transactions.this, LienClearing.class));
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(v);
        lienDialog = builder.create();
        lienDialog.show();

    }

    @Override
    public void onBackPressed() {
        Toast.makeText(this, "Please press again to exit", Toast.LENGTH_SHORT).show();
        //super.onBackPressed();
        Toast.makeText(this, "Please press again to exit", Toast.LENGTH_SHORT).show();
        if (doubleBackToExitPressedOnce) {
            String msg = "Are you sure you want to exit?";
            android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(Transactions.this);
            builder1.setMessage(msg);
            builder1.setTitle("Exit!");
            builder1.setCancelable(false);

            builder1.setPositiveButton(
                    "yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            finish();
                            Intent i = new Intent(Transactions.this, Dashboard.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            //i.addFlags(IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                        }
                    });

            builder1.setNegativeButton(
                    "CANCEL",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            //onBackPressed();
                        }
                    });

            android.app.AlertDialog alert11 = builder1.create();
            alert11.show();
            //super.onBackPressed();
            //System.exit(1);
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        //Toast.makeText(this, "Press back again to EXIT app!", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

}

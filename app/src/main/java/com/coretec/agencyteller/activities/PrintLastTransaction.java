package com.coretec.agencyteller.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyteller.R;
import com.coretec.agencyteller.api.ApiTest;
import com.coretec.agencyteller.api.requests.RequestPrintLastTransaction;
import com.coretec.agencyteller.api.responses.PrintLastTransactionResponse;
import com.coretec.agencyteller.utils.PreferenceFileKeys;
import com.coretec.agencyteller.utils.Utils;
import com.coretec.agencyteller.wizarpos.function.printer.PrinterCommand;

import java.io.UnsupportedEncodingException;

/**
 * Created by kelvinoff on 18/02/05.
 */

public class PrintLastTransaction extends AppCompatActivity {

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_last_transaction);

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

        sharedPreferences = getSharedPreferences(PreferenceFileKeys.PREFS_FILE, Context.MODE_PRIVATE);
        requestLastTransaction();
    }

    void requestLastTransaction() {

        final String TAG = "printLastTransaction";

        String URL = ApiTest.MSACCO_AGENT + ApiTest.GetBalanceInquiry;

        RequestPrintLastTransaction printLastTransaction = new RequestPrintLastTransaction();
        printLastTransaction.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
        //printLastTransaction.terminalid = MainActivity.imei;

        Log.e(TAG, printLastTransaction.getBody().toString());

        ApiTest.instance(this).request(URL, printLastTransaction, new ApiTest.RequestListener() {
            @Override
            public void onSuccess(String response) {
                Log.e(TAG, response);

                final PrintLastTransactionResponse lastTransactionResponse = ApiTest.instance(PrintLastTransaction.this).mGson.fromJson(response, PrintLastTransactionResponse.class);
                if (lastTransactionResponse.is_successful) {

                    final String agentName = sharedPreferences.getString(PreferenceFileKeys.AGENT_ACCOUNT_NAME, "");

                    int result = PrinterInterface.open();
                    writetest(agentName, lastTransactionResponse.getAmount(), lastTransactionResponse.getSacconame(), lastTransactionResponse.getReceiptno(), lastTransactionResponse.getTransactiondate(), lastTransactionResponse.getTransactiontype());
                    PrinterInterface.close();

                    Utils.showAlertDialog(PrintLastTransaction.this, "Successfully Printed", "Last Transaction Printed ");

                } else {
                    Utils.showAlertDialog(PrintLastTransaction.this, "Printing Failed", lastTransactionResponse.getError());
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });

    }


    public void writetest(String agentName, String amount, String saccoame, String receiptNumber, String dateed, String transactionType) {
        try {

            byte[] arrySaccoName = null;
            byte[] arryWithdrawAmt = null;
            byte[] arryAmt = null;

            byte[] arryMotto = null;
            byte[] terminalNo = null;
            byte[] receiptNo = null;
            byte[] date = null;
            byte[] transactionTYpe = null;
            byte[] servedBy = null;
            byte[] signature = null;


            try {
                arrySaccoName = String.valueOf("       " + String.valueOf(saccoame)).getBytes("GB2312");
                terminalNo = String.valueOf(" Terminal ID :" + Utils.getIMEI(PrintLastTransaction.this)).getBytes("GB2312");
                receiptNo = String.valueOf(" Copy Receipt Number : " + receiptNumber).getBytes("GB2312");
                date = String.valueOf("Date : " + dateed).getBytes("GB2312");
                transactionTYpe = String.valueOf("      " + transactionType).getBytes("GB2312");
                arryWithdrawAmt = String.valueOf(" Total Amount is :").getBytes("GB2312");
                arryAmt = String.valueOf(String.valueOf("    KSH " + amount)).getBytes("GB2312");
                servedBy = String.valueOf(" You were Served By " + agentName).getBytes("GB2312");
                signature = String.valueOf("Signature__________________________").getBytes("GB2312");

                arryMotto = String.valueOf("            "+sharedPreferences.getString(PreferenceFileKeys.AGENT_SACCO_MOTTO, "")).getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(arrySaccoName);
            writeLineBreak(2);
            write(terminalNo);
            writeLineBreak(2);
            write(receiptNo);
            writeLineBreak(2);
            write(date);
            writeLineBreak(2);
            write(transactionTYpe);
            writeLineBreak(4);

            write(arryWithdrawAmt);
            writeLineBreak(2);
            write(arryAmt);
            writeLineBreak(2);
            write(servedBy);
            // print line break
            writeLineBreak(4);
            write(signature);
            writeLineBreak(5);
            write(arryMotto);
            writeLineBreak(5);
            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void write(final byte[] arryData) {
        int result = PrinterInterface.write(arryData, arryData.length);
    }

    // print line break
    private void writeLineBreak(int lineNumber) {
        // write(new byte[]{'\n'});
        write(PrinterCommand.getCmdEscDN(lineNumber));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }
}

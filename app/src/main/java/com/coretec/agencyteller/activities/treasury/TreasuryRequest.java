package com.coretec.agencyteller.activities.treasury;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.coretec.agencyteller.R;
import com.coretec.agencyteller.api.Api;
import com.coretec.agencyteller.utils.SharedPrefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class TreasuryRequest extends AppCompatActivity {

    private EditText teller_account;
    private EditText amount;
    private Button submit;

    private Spinner accountSpinner;
    private ArrayList<String> arrayList = new ArrayList<>();

    private ProgressBar treasuryPb;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPrefs.init(getApplicationContext());
        setContentView(R.layout.treasury_request);

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

        //Toolbar declarations...Reusable
        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView mTitle = toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getText(R.string.treasury_request));
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);

        teller_account = findViewById(R.id.et_teller_ac);
        teller_account.setFocusable(false);
        teller_account.setText(SharedPrefs.read(SharedPrefs.TELLER_ACCOUNT_NUMBER, null));

        accountSpinner = findViewById(R.id.spinner_accounts);
        treasuryPb = findViewById(R.id.teller_progress);
        amount = findViewById(R.id.et_amount);

        submit = findViewById(R.id.button_submit);

        treasuryPb.setVisibility(View.VISIBLE);
        getTreasuryAccounts();

        accountSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //((TextView) parent.getChildAt(0)).setTextColor(Color.GRAY);
                String item = parent.getItemAtPosition(position).toString();
                String text = accountSpinner.getSelectedItem().toString();

                String account_number = text.substring(0, text.indexOf("-"));
                SharedPrefs.write(SharedPrefs.TREASURY_ACCOUNT_NUMBER, account_number);
                Log.e("CASHIER ACCOUNT", account_number);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String aMount = amount.getText().toString();

                if (aMount.isEmpty()) {
                    amount.setError("Please input the amount!");
                } else {
                    progressDialog = new ProgressDialog(TreasuryRequest.this);
                    progressDialog.setMessage("Submitting your request...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    submitTreasuryReturns();
                }
            }
        });

    }

    void getTreasuryAccounts() {

        Api.getVolley(this, Api.GetTreasuryAccounts, "{\n" +
                "    \"theTransactionType\": " + 7 + ",\n" +
                "    \"theAgentCode\": \"" + SharedPrefs.read(SharedPrefs.AGENT_CODE, null) + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));
                        treasuryPb.setVisibility(View.GONE);
                        arrayList.clear();
                        accountSpinner = findViewById(R.id.spinner_accounts);
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(TreasuryRequest.this, android.R.layout.simple_spinner_item, arrayList);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        accountSpinner.setAdapter(adapter);

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            JSONObject accountObject = jsonObject1.getJSONObject("account");

                            String accountName = accountObject.getString("name");
                            String accountNo = accountObject.getString("no");
                            String accountBal = accountObject.getString("balance");
                            String accountDetails = accountNo + "-" + accountName;

                            //SharedPrefs.write(SharedPrefs.ACC_NAME, accountName);

                            arrayList.add(accountDetails);

                            Log.e("ACCOUNT DETAILS", accountDetails);

                        }
                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        treasuryPb.setVisibility(View.GONE);
                        String msg = jsonObject.getString("message");
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(TreasuryRequest.this);
                        builder1.setMessage(msg);
                        builder1.setTitle("Alert!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                        //Toast.makeText(TreasuryReturns.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                    }

                    accountSpinner = findViewById(R.id.spinner_accounts);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(TreasuryRequest.this, android.R.layout.simple_spinner_item, arrayList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    accountSpinner.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    void submitTreasuryReturns() {

        Api.getVolley(this, Api.TreasuryTransaction, "{\n" +
                "    \"action\": \"" + 0 + "\",\n" +
                "    \"transactionType\": \"" + 6 + "\",\n" +
                "    \"deviceID\": \"" + SharedPrefs.read(SharedPrefs.DEVICE_ID, null) + "\",\n" +
                "    \"agentCode\": \"" + SharedPrefs.read(SharedPrefs.AGENT_CODE, null) + "\",\n" +
                "    \"amount\": \"" + amount.getText().toString() + "\",\n" +
                "    \"sourceAccount\": \"" + SharedPrefs.read(SharedPrefs.TREASURY_ACCOUNT_NUMBER, null) + "\",\n" +
                "    \"destinationAccount\": \"" + SharedPrefs.read(SharedPrefs.TELLER_ACCOUNT_NUMBER, null) + "\",\n" +
                "    \"treasuryTransactionType\": \"" + 7 + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));
                        Toast.makeText(TreasuryRequest.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                        /*int result = PrinterInterface.open();
                        writetest();
                        PrinterInterface.close();*/
                        progressDialog.dismiss();

                        String msg = jsonObject.getString("message");
                        //final String qualKgs = SharedPrefs.read(SharedPrefs.LOAN_QUALIFIED_KGS, null);
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(TreasuryRequest.this);
                        builder1.setMessage(msg);
                        builder1.setTitle("Alert!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        onBackPressed();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        progressDialog.dismiss();
                        Toast.makeText(TreasuryRequest.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                        String msg = jsonObject.getString("message");
                        //final String qualKgs = SharedPrefs.read(SharedPrefs.LOAN_QUALIFIED_KGS, null);
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(TreasuryRequest.this);
                        builder1.setMessage(msg);
                        builder1.setTitle("Alert!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

}

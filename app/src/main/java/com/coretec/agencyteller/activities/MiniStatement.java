package com.coretec.agencyteller.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyteller.R;
import com.coretec.agencyteller.activities.receipts.MiniStatementReceipt;
import com.coretec.agencyteller.api.ApiTest;
import com.coretec.agencyteller.api.requests.AccountDetailsRequest;
import com.coretec.agencyteller.api.requests.MiniStatementRequest;
import com.coretec.agencyteller.api.responses.AccountDetailsResponse;
import com.coretec.agencyteller.api.responses.MiniStatementResponse;
import com.coretec.agencyteller.api.responses.SaccoAccounts;
import com.coretec.agencyteller.model.AccountDetails;
import com.coretec.agencyteller.model.Transactions;
import com.coretec.agencyteller.utils.PreferenceFileKeys;
import com.coretec.agencyteller.utils.Utils;
import com.coretec.agencyteller.wizarpos.function.ActionCallbackImpl;
import com.coretec.agencyteller.wizarpos.function.printer.PrinterCommand;
import com.coretec.agencyteller.wizarpos.mvc.base.ActionCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import static com.coretec.agencyteller.utils.Utils.getFormattedDate;
import static com.coretec.agencyteller.utils.Utils.getLat;
import static com.coretec.agencyteller.utils.Utils.getLong;

/**
 * Created by kelvinoff on 17/10/04.
 */

public class MiniStatement extends AppCompatActivity {

    private Button btnConfirmMinistatement;
    private EditText identifier_number_ministatement;
    private TextInputLayout input_layout_ministatement_phone_number;
    //    private TextInputLayout input_layout_ministatement_corporate_number;
    private ProgressBar progressMiniStatement;
    public static Handler handler;
    public static ActionCallback callback;
    private String ministatementPhoneNumber;

    EditText ministatement_identifier;
    SharedPreferences sharedPreferences;

    private Spinner spinner;
    private ArrayList<String> arrayList = new ArrayList<>();
    private ArrayAdapter<AccountDetails> adapter;
    String accountNo = "";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ministatement);

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

        handler = new Handler();
        callback = new ActionCallbackImpl(handler);

        sharedPreferences = getSharedPreferences(PreferenceFileKeys.PREFS_FILE, Context.MODE_PRIVATE);


        progressMiniStatement = (ProgressBar) findViewById(R.id.ministatement_progressbar);
        identifier_number_ministatement = (EditText) findViewById(R.id.ministatement_identifier);

        input_layout_ministatement_phone_number = (TextInputLayout) findViewById(R.id.input_layout_ministatement_identifier);

        btnConfirmMinistatement = (Button) findViewById(R.id.btn_confirm_ministatement);

        btnConfirmMinistatement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ministatementPhoneNumber = identifier_number_ministatement.getText().toString().trim();

                if (ministatementPhoneNumber.isEmpty()) {
                    input_layout_ministatement_phone_number.setError("Enter Customer Account Number ");
                } else {
                    Log.e("phone number", ministatementPhoneNumber);

                    progressMiniStatement.setVisibility(View.VISIBLE);
//                    memberAuthDialog();
                    requestMiniStatement();
                    btnConfirmMinistatement.setActivated(false);
                }

            }
        });


        spinner = (Spinner) findViewById(R.id.withdrawal_accounts);
        adapter = new ArrayAdapter<AccountDetails>(getApplicationContext(),
                R.layout.simple_spinner_dropdown_item, new ArrayList<AccountDetails>());
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                AccountDetails accountDetails = (AccountDetails) parent.getItemAtPosition(position);
                accountNo = accountDetails.getAccountno();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ministatement_identifier = (EditText) findViewById(R.id.ministatement_identifier);
        ministatement_identifier.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String identifier = ministatement_identifier.getText().toString();
                if (identifier.length() >= 7) {
//                Toast.makeText(CashDeposit.this, accountNumber, Toast.LENGTH_SHORT).show();
                    requestGetAccountDetails(identifier);
                }
            }
        });


    }

    void requestGetAccountDetails(String identifier) {

        final String TAG = "MemberAccounts";
//        EasyLocationMod easyLocationMod = new EasyLocationMod(CashWithdrawal.this);

        String URL = ApiTest.MSACCO_AGENT + ApiTest.GetAllMemberSaccoDetails;

        final AccountDetailsRequest accountDetails = new AccountDetailsRequest();
        accountDetails.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
        //accountDetails.corporateno = MainActivity.corporateno;
        accountDetails.accountidentifier = identifier;
        accountDetails.accountidentifiercode = "0";
        accountDetails.transactiontype = "A";
        //accountDetails.terminalid = MainActivity.imei;
        accountDetails.longitude = getLong(this);
        accountDetails.latitude = getLat(this);
        accountDetails.date = getFormattedDate();

        Log.e(TAG, accountDetails.getBody().toString());

        ApiTest.instance(this).request(URL, accountDetails, new ApiTest.RequestListener() {
            @Override
            public void onSuccess(String response) {


                AccountDetailsResponse accountDetailsResponse = ApiTest.instance(MiniStatement.this)
                        .mGson.fromJson(response, AccountDetailsResponse.class);
                if (accountDetailsResponse.is_successful) {
                    List<AccountDetails> accountDetailsList = new ArrayList<>();
                    for (SaccoAccounts saccoAccounts : accountDetailsResponse.getSaccoAccounts()) {
                        accountDetailsList.addAll(saccoAccounts.getAccountDetails());
                    }
                    adapter.addAll(accountDetailsList);
                    spinner.setAdapter(adapter);

                } else {
//                    Toast.makeText(MiniStatement.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    JSONArray jsonArray = jsonObject.getJSONArray("saccoaccounts");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        JSONArray jsonArray1 = jsonObject1.getJSONArray("accountdetails");
                        for (int l = 0; l < jsonArray1.length(); l++) {
                            JSONObject jsonObject2 = jsonArray1.getJSONObject(l);
                            arrayList.add(String.valueOf(jsonObject2.getString("accountno") + " " + jsonObject2.getString("accounttype")));
                            // mpesaPhoneNumber = jsonObject2.getString("phonenumber");
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

              /*  Log.e(TAG, response);
//                displayBalance(response);
                AccountDetailsResponse accountDetailsResponse = ApiTest.instance(CashWithdrawal.this).mGson.fromJson(response, AccountDetailsResponse.class);

                if (accountDetailsResponse.is_successful) {
                    saccoAccounts.addAll(accountDetailsResponse.getSaccoAccounts());
                    for (int i = 0; i < saccoAccounts.size(); i++){
                        String saccoAcc = accountDetailsResponse.getSaccoAccounts().get(i).getSacconame();
                        Log.e(TAG, saccoAcc);
                    }
                }*/
            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }


    public void requestMiniStatement() {
        final String TAG = "request ministatement";

        String URL = ApiTest.MSACCO_AGENT + ApiTest.GetMiniStatement;

        MiniStatementRequest request = new MiniStatementRequest();
        //request.corporate_no = MainActivity.corporateno;
        request.accountidentifier = identifier_number_ministatement.getText().toString();
        request.accountidentifiercode = "0";
        request.accountno = accountNo;
        request.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
        //request.terminalid = MainActivity.imei;
        request.longitude = getLong(this);
        request.latitude = getLat(this);
        request.date = getFormattedDate();


        Log.e(TAG, request.getBody().toString());

        ApiTest.instance(this).request(URL, request, new ApiTest.RequestListener() {
            @Override
            public void onSuccess(String response) {
                Log.e(TAG, response);
                progressMiniStatement.setVisibility(View.GONE);

                MiniStatementResponse miniStatementResponse = ApiTest.instance(MiniStatement.this).mGson.fromJson(response, MiniStatementResponse.class);

                if (miniStatementResponse.is_successful) {

                    final String agentName = sharedPreferences.getString(PreferenceFileKeys.AGENT_ACCOUNT_NAME, "");

                    int result = PrinterInterface.open();
                    writetest(agentName, miniStatementResponse.getTransactions(), miniStatementResponse.getSacconame(), miniStatementResponse.getCustomer_name(), miniStatementResponse.getReceiptno(), miniStatementResponse.getTransactiondate(), miniStatementResponse.getTransactiontype());
                    PrinterInterface.close();
                    Utils.showAlertDialog(MiniStatement.this, "Ministatement Fetched Successfully", "Mini statement Retrieved Successfully");
                } else {
                    Utils.showAlertDialog(MiniStatement.this, "MiniStatement Failed", miniStatementResponse.getError());
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    public void writetest(String agentName, List<Transactions> transactionsList, String saccoame, String customerName, String receiptNumber, String dateed, String transactionType) {
        try {

            byte[] arrySaccoName = null;
            byte[] arryCustomerName = null;
            byte[] arryDepositAmt = null;
            byte[] arryDate = null;
            byte[] arryDescription = null;
            byte[] arryAmount = null;

            byte[] arryMotto = null;
            byte[] terminalNo = null;
            byte[] receiptNo = null;
            byte[] date = null;
            byte[] transactionTYpe = null;
            byte[] servedBy = null;
            byte[] signature = null;
            byte[] id = null;


            try {
                arrySaccoName = String.valueOf("       " + String.valueOf(saccoame)).getBytes("GB2312");
                terminalNo = String.valueOf(" Terminal ID :" + Utils.getIMEI(MiniStatement.this)).getBytes("GB2312");
                receiptNo = String.valueOf(" Receipt Number : " + receiptNumber).getBytes("GB2312");
                date = String.valueOf("Date : " + dateed).getBytes("GB2312");
                arryCustomerName = String.valueOf("Name : " + customerName).getBytes("GB2312");
                transactionTYpe = String.valueOf("      " + transactionType).getBytes("GB2312");
                arryDepositAmt = String.valueOf("   Deposited Amount is :").getBytes("GB2312");
                id = String.valueOf("ID :_________________________").getBytes("GB2312");
                signature = String.valueOf("SIGNATURE : _____________________").getBytes("GB2312");
                servedBy = String.valueOf(" You were Served By : " + agentName).getBytes("GB2312");
                arryMotto = String.valueOf("            "+sharedPreferences.getString(PreferenceFileKeys.AGENT_SACCO_MOTTO, "")).getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};


            write(cmd);
            write(arrySaccoName);
            writeLineBreak(2);
            write(terminalNo);
            writeLineBreak(2);
            write(receiptNo);
            writeLineBreak(2);
            write(date);
            writeLineBreak(2);
            write(arryCustomerName);
            writeLineBreak(2);
            write(transactionTYpe);
            // print line break
            writeLineBreak(4);

            write(arryDepositAmt);

            for (int i = 0; i < transactionsList.size(); i++) {
                arryDate = String.valueOf(transactionsList.get(i).getPosting_date()).getBytes("GB2312");
                write(arryDate);

                arryAmount = String.valueOf("  " + transactionsList.get(i).getAmount()).getBytes("GB2312");
                write(arryAmount);

                arryDescription = String.valueOf("  " + transactionsList.get(i).getDescription()).getBytes("GB2312");
                write(arryDescription);
                writeLineBreak(2);

            }

            writeLineBreak(2);
            write(id);
            writeLineBreak(3);
            write(signature);
            writeLineBreak(2);
            write(servedBy);
            writeLineBreak(4);
            write(arryMotto);
            writeLineBreak(5);
            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void write(final byte[] arryData) {
        int result = PrinterInterface.write(arryData, arryData.length);
    }

    // print line break
    private void writeLineBreak(int lineNumber) {
        // write(new byte[]{'\n'});
        write(PrinterCommand.getCmdEscDN(lineNumber));
    }


    public void receiptDialog(MiniStatementResponse miniStatementResponse) {

        Intent intent = new Intent(this, MiniStatementReceipt.class);
        intent.putExtra(MiniStatementReceipt.MINISTATEMENT_RESPONSE_EXTRA, miniStatementResponse);
        startActivity(intent);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }
}

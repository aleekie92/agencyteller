package com.coretec.agencyteller.activities.teller;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyteller.R;
import com.coretec.agencyteller.api.Api;
import com.coretec.agencyteller.utils.EnglishNumberToWords;
import com.coretec.agencyteller.utils.PreferenceFileKeys;
import com.coretec.agencyteller.utils.SharedPrefs;
import com.coretec.agencyteller.utils.Utils;
import com.coretec.agencyteller.wizarpos.function.printer.PrinterCommand;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class CashDeposit extends AppCompatActivity {

    EditText teller_account;
    private EditText identifier;
    private Spinner identifierSpinner;
    private Spinner accountSpinner;
    private ArrayList<String> arrayList = new ArrayList<>();

    private EditText amount;
    private EditText deposited_by;
    private EditText narration;
    private Button submit;
    private Button search;

    private LinearLayout details;
    private TextView names;

    private ProgressBar progressBar;
    private ProgressDialog progressDialog;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPrefs.init(getApplicationContext());
        setContentView(R.layout.teller_cash_deposit);

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

        //Toolbar declarations...Reusable
        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView mTitle = toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getText(R.string.cash_deposit));
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);

        teller_account = findViewById(R.id.et_teller_account);
        teller_account.setFocusable(false);
        teller_account.setText(SharedPrefs.read(SharedPrefs.TELLER_ACCOUNT_NUMBER, null));

        identifierSpinner = findViewById(R.id.identifier_spinner);
        accountSpinner = findViewById(R.id.accountsSpinner);
        identifier = findViewById(R.id.deposit_identifier);
        progressBar = findViewById(R.id.accountsPB);
        search = findViewById(R.id.btn_search);

        details = findViewById(R.id.details_layout);
        names = findViewById(R.id.tv_details_name);

        amount = findViewById(R.id.et_amount);
        deposited_by = findViewById(R.id.edittext_deposited_by);
        narration = findViewById(R.id.edittext_narration);
        submit = findViewById(R.id.button_submit);

        accountSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //((TextView) parent.getChildAt(0)).setTextColor(Color.GRAY);
                String item = parent.getItemAtPosition(position).toString();
                String text = accountSpinner.getSelectedItem().toString();

                String account_number = text.substring(0, text.indexOf("-"));
                SharedPrefs.write(SharedPrefs.MEMBER_ACCOUNT_NUMBER, account_number);
                Log.e("DESTINATION ACCOUNT", account_number);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        identifierSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                String item = parent.getItemAtPosition(position).toString();
                String text = identifierSpinner.getSelectedItem().toString();

                int idLength = 8;
                int acLength = 14;
                int memLength = 14;
                InputFilter[] fArray = new InputFilter[1];
                fArray[0] = new InputFilter.LengthFilter(idLength);

                InputFilter[] aArray = new InputFilter[1];
                aArray[0] = new InputFilter.LengthFilter(acLength);

                InputFilter[] mArray = new InputFilter[1];
                mArray[0] = new InputFilter.LengthFilter(memLength);

                if (text.equals("ID NUMBER")) {
                    SharedPrefs.write(SharedPrefs.IDENTIFIER, "0");
                    identifier.setText(null);
                    identifier.setInputType(InputType.TYPE_CLASS_NUMBER);
                    identifier.setFilters(fArray);
                } else if (text.equals("OLD A/C NUMBER")) {
                    SharedPrefs.write(SharedPrefs.IDENTIFIER, "4");
                    identifier.setText(null);
                    identifier.setInputType(InputType.TYPE_CLASS_TEXT);
                    identifier.setFilters(aArray);
                } else if (text.equals("MEMBER NUMBER")) {
                    identifier.setText(null);
                    identifier.setInputType(InputType.TYPE_CLASS_NUMBER);
                    SharedPrefs.write(SharedPrefs.IDENTIFIER, "1");
                    identifier.setFilters(mArray);
                }

                Log.e("SELECTED", text);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = identifier.getText().toString();

                if (id.length()<7 && id.length()>0) {
                    identifier.setError("Please input a valid identifier!");
                } else if (id.length()==0){
                    identifier.setError("Cannot search an empty field!");
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(CashDeposit.this, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                    requestAccounts(id);
                }
            }
        });

        TextWatcher textWatcher = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String idNumber = identifier.getText().toString();

                /*if (idNumber.length() >= 7) {
                    requestAccounts(idNumber);
                }*/

                if (idNumber.length() >= 5 && idNumber.length() <= 8) {
                    arrayList.clear();
                    accountSpinner = findViewById(R.id.accountsSpinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(CashDeposit.this, android.R.layout.simple_spinner_item, arrayList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    accountSpinner.setAdapter(adapter);

                    details.setVisibility(View.GONE);
                }
                if (idNumber.length() < 5) {
                    //submitAccRegistrationRequest.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                }

            }
        };

        identifier.addTextChangedListener(textWatcher);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String idNo = identifier.getText().toString();
                String en_amount = amount.getText().toString();
                String depositor = deposited_by.getText().toString();
                String the_narration = narration.getText().toString();

                if (idNo.isEmpty()) {
                    identifier.setError("Please input ID number!");
                } else if (en_amount.isEmpty()) {
                    amount.setError("Please enter amount!");
                } else if (depositor.isEmpty()) {
                    deposited_by.setError("Please input name of depositor!");
                } else if (the_narration.isEmpty()) {
                    narration.setError("Please input the narration!");
                } else {
                    progressDialog = new ProgressDialog(CashDeposit.this);
                    progressDialog.setMessage("Submitting your request...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    requestPostDeposit(identifier.getText().toString(), amount.getText().toString(), deposited_by.getText().toString());
                    submit.setClickable(false);
                }
            }
        });

    }

    private void requestAccounts(String id) {

        int identifier = Integer.parseInt(SharedPrefs.read(SharedPrefs.IDENTIFIER, null));

        Api.getVolley(this, Api.GetMemberAccounts, "{\n" +
                "    \"identifier\": \"" + id + "\",\n" +
                "    \"identifierType\": \"" + identifier + "\",\n" +
                "    \"natureOfAccount\": \"" + 2 + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                progressBar.setVisibility(View.GONE);
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));
                        arrayList.clear();
                        accountSpinner = findViewById(R.id.accountsSpinner);
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(CashDeposit.this, android.R.layout.simple_spinner_item, arrayList);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        accountSpinner.setAdapter(adapter);

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            String accountName = jsonObject1.getString("name");
                            String accountNo = jsonObject1.getString("no");
                            String desc = jsonObject1.getString("description");
                            String accountDetails = accountNo + "-" + desc;

                            SharedPrefs.write(SharedPrefs.ACC_NAME, accountName);

                            arrayList.add(accountDetails);
                            details.setVisibility(View.VISIBLE);
                            names.setText(accountName);

                            Log.e("ACCOUNT DETAILS", accountDetails);

                        }
                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(CashDeposit.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                    }

                    accountSpinner = findViewById(R.id.accountsSpinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(CashDeposit.this, android.R.layout.simple_spinner_item, arrayList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    accountSpinner.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    void requestPostDeposit(final String Id, final String amou_nt, String depo) {

        Api.getVolley(this, Api.CashDepositTransaction, "{\n" +
                "    \"action\": \"" + 0 + "\",\n" +
                "    \"transactionType\": \"" + 4 + "\",\n" +
                "    \"deviceID\": \"" + SharedPrefs.read(SharedPrefs.DEVICE_ID, null) + "\",\n" +
                "    \"agentCode\": \"" + SharedPrefs.read(SharedPrefs.AGENT_CODE, null) + "\",\n" +
                "    \"amount\": \"" + amou_nt + "\",\n" +
                "    \"narration\": \"" + narration.getText().toString() + "\",\n" +
                "    \"depositedBy\": \"" + depo + "\",\n" +
                "    \"sourceAccount\": \"" + SharedPrefs.read(SharedPrefs.TELLER_ACCOUNT_NUMBER, null) + "\",\n" +
                "    \"destinationAccount\": \"" + SharedPrefs.read(SharedPrefs.MEMBER_ACCOUNT_NUMBER, null) + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));
                        Toast.makeText(CashDeposit.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                        JSONObject dataObject = jsonObject.getJSONObject("data");

                        final String tCode = dataObject.getString("no");
                        final String baLance = dataObject.getString("balance");

                        int result = PrinterInterface.open();
                        writetest(Id, amou_nt, tCode);
                        PrinterInterface.close();
                        progressDialog.dismiss();

                        Utils.showAlertDialog(CashDeposit.this, "Success!", jsonObject.getString("message")+ "\n" +" Press Ok to print another Receipt ", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                int result = PrinterInterface.open();
                                writetest1(Id, amou_nt, tCode);
                                PrinterInterface.close();
                                onBackPressed();
                            }
                        });

                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        progressDialog.dismiss();
                        Toast.makeText(CashDeposit.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                        String msg = jsonObject.getString("message");
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(CashDeposit.this);
                        builder1.setMessage(msg);
                        builder1.setTitle("Alert!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public void writetest(String idNo, String the_amount, String transactionCode) {

        //String middle = idNos.substring(2, 6);
        String first = idNo.substring(0, 2);
        String last = idNo.substring(6, idNo.length());
        String maskedId = first + "****" + last;

        //Old Phone Number
        /*String firstOP = phone.substring(0, 7);
        String lastOP = phone.substring(11, phone.length());
        String maskedOP = firstOP + "*****"+lastOP;*/

        String accountNumber = SharedPrefs.read(SharedPrefs.MEMBER_ACCOUNT_NUMBER, null);
        String firstAc = accountNumber.substring(0, 4);
        String lastAc = accountNumber.substring(8, accountNumber.length());
        String maskedAc = firstAc + "****" + lastAc;

        int number = Integer.parseInt(the_amount);
        String return_val_in_english = EnglishNumberToWords.convert(number);

        try {

            byte[] start = null;
            byte[] saccoName = null;
            byte[] type = null;

            byte[] terminalNo = null;
            byte[] transaction = null;
            byte[] time = null;
            byte[] transDate = null;
            byte[] account_dposited_to = null;
            byte[] idNumber = null;
            byte[] fullNames = null;
            byte[] fName = null;
            byte[] depositedBy = null;
            byte[] arryNarration = null;

            //byte[] phoneNumber = null;
            byte[] arryDepositAmt = null;
            byte[] arryWordsHeader = null;
            byte[] arryInWords = null;

            byte[] acknowLedge = null;
            byte[] nameVerify = null;
            byte[] idVerify = null;
            byte[] signature = null;

            byte[] nameAgent = null;
            byte[] customerReceipt = null;
            byte[] motto = null;

            SimpleDateFormat timeStampFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
            Date myDate = new Date();
            String timeCurrent = timeStampFormat.format(myDate);
            String aName = SharedPrefs.read(SharedPrefs.AGENT_NAME, null);
            String depositorsName = deposited_by.getText().toString();


            try {
                start = String.valueOf("              ").getBytes("GB2312");
                saccoName = String.valueOf(SharedPrefs.read(SharedPrefs.SACCO_NAME, null)).getBytes("GB2312");
                type = String.valueOf("        CASH DEPOSIT     ").getBytes("GB2312");

                terminalNo = String.valueOf("Terminal ID : " + SharedPrefs.read(SharedPrefs.DEVICE_ID, null)).getBytes("GB2312");
                transaction = String.valueOf("Transaction No : " + transactionCode).getBytes("GB2312");
                time = String.valueOf("Print Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                transDate = String.valueOf("Trans. Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                account_dposited_to = String.valueOf("Account NO : " + accountNumber).getBytes("GB2312");
                idNumber = String.valueOf("ID Number : " + String.valueOf(idNo)).getBytes("GB2312");
                fullNames = String.valueOf("Customer Full Names : ").getBytes("GB2312");
                fName = String.valueOf(String.valueOf(names.getText().toString())).getBytes("GB2312");

                arryDepositAmt = String.valueOf("Amount : KSH" + the_amount).getBytes("GB2312");
                arryWordsHeader = String.valueOf("Amount in Words: ").getBytes("GB2312");
                arryInWords = String.valueOf(return_val_in_english + " Kenya Shillings Only").getBytes("GB2312");
                depositedBy = String.valueOf("Deposited By " + depositorsName).getBytes("GB2312");
                arryNarration = String.valueOf("Narration : " + narration.getText().toString()).getBytes("GB2312");

                acknowLedge = String.valueOf("I acknowledge deposit of the above amount").getBytes("GB2312");
                nameVerify = String.valueOf("Name___________________________").getBytes("GB2312");
                idVerify = String.valueOf("ID No___________________________").getBytes("GB2312");
                signature = String.valueOf("Signature_______________________").getBytes("GB2312");

                nameAgent = String.valueOf("SERVED BY " + String.valueOf(aName)).getBytes("GB2312");
                customerReceipt = String.valueOf("         CUSTOMER COPY         ").getBytes("GB2312");
                motto = String.valueOf(SharedPrefs.read(SharedPrefs.SACCO_MOTTO, null)).getBytes("GB2312");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(start);
            writeLineBreak(1);
            write(saccoName);
            writeLineBreak(2);
            write(type);
            writeLineBreak(1);

            write(terminalNo);
            writeLineBreak(1);
            write(transaction);
            writeLineBreak(1);
            write(time);
            writeLineBreak(1);
            write(transDate);
            writeLineBreak(1);
            write(account_dposited_to);
            writeLineBreak(1);
            /*write(idNumber);
            writeLineBreak(1);*/
            write(fullNames);
            writeLineBreak(0);
            write(fName);
            writeLineBreak(2);

            write(arryDepositAmt);
            writeLineBreak(1);
            write(arryWordsHeader);
            writeLineBreak(0);
            write(arryInWords);
            writeLineBreak(1);
            write(depositedBy);
            writeLineBreak(1);
            write(arryNarration);
            writeLineBreak(2);

            write(acknowLedge);
            writeLineBreak(1);
            write(nameVerify);
            writeLineBreak(1);
            write(idVerify);
            writeLineBreak(1);
            write(signature);
            writeLineBreak(2);

            write(nameAgent);
            writeLineBreak(2);
            write(customerReceipt);
            writeLineBreak(2);
            write(motto);
            // print line break
            writeLineBreak(4);

            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void writetest1(String idNo, String the_amount, String transactionCode) {
        //String middle = idNos.substring(2, 6);
        String first = idNo.substring(0, 2);
        String last = idNo.substring(6, idNo.length());
        String maskedId = first + "****" + last;

        //Old Phone Number
        /*String firstOP = phone.substring(0, 7);
        String lastOP = phone.substring(11, phone.length());
        String maskedOP = firstOP + "*****"+lastOP;*/

        String accountNumber = SharedPrefs.read(SharedPrefs.MEMBER_ACCOUNT_NUMBER, null);
        String firstAc = accountNumber.substring(0, 4);
        String lastAc = accountNumber.substring(8, accountNumber.length());
        String maskedAc = firstAc + "****" + lastAc;

        int number = Integer.parseInt(the_amount);
        String return_val_in_english = EnglishNumberToWords.convert(number);

        try {

            byte[] start = null;
            byte[] saccoName = null;
            byte[] type = null;

            byte[] terminalNo = null;
            byte[] transaction = null;
            byte[] time = null;
            byte[] transDate = null;
            byte[] account_dposited_to = null;
            byte[] idNumber = null;
            byte[] fullNames = null;
            byte[] fName = null;
            byte[] depositedBy = null;
            byte[] arryNarration = null;

            //byte[] phoneNumber = null;
            byte[] arryDepositAmt = null;
            byte[] arryWordsHeader = null;
            byte[] arryInWords = null;

            byte[] acknowLedge = null;
            byte[] nameVerify = null;
            byte[] idVerify = null;
            byte[] signature = null;

            byte[] nameAgent = null;
            byte[] customerReceipt = null;
            byte[] motto = null;

            SimpleDateFormat timeStampFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
            Date myDate = new Date();
            String timeCurrent = timeStampFormat.format(myDate);
            String aName = SharedPrefs.read(SharedPrefs.AGENT_NAME, null);
            String depositorsName = deposited_by.getText().toString();

            try {
                start = String.valueOf("              ").getBytes("GB2312");
                saccoName = String.valueOf(SharedPrefs.read(SharedPrefs.SACCO_NAME, null)).getBytes("GB2312");
                type = String.valueOf("        CASH DEPOSIT     ").getBytes("GB2312");

                terminalNo = String.valueOf("Terminal ID : " + SharedPrefs.read(SharedPrefs.DEVICE_ID, null)).getBytes("GB2312");
                transaction = String.valueOf("Transaction No : " + transactionCode).getBytes("GB2312");
                time = String.valueOf("Print Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                transDate = String.valueOf("Trans. Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                account_dposited_to = String.valueOf("Account NO : " + accountNumber).getBytes("GB2312");
                idNumber = String.valueOf("ID Number : " + String.valueOf(idNo)).getBytes("GB2312");
                fullNames = String.valueOf("Customer Full Names : ").getBytes("GB2312");
                fName = String.valueOf(String.valueOf(names.getText().toString())).getBytes("GB2312");

                arryDepositAmt = String.valueOf("Amount : KSH" + the_amount).getBytes("GB2312");
                arryWordsHeader = String.valueOf("Amount in Words: ").getBytes("GB2312");
                arryInWords = String.valueOf(return_val_in_english + " Kenya Shillings Only").getBytes("GB2312");
                depositedBy = String.valueOf("Deposited By " + depositorsName).getBytes("GB2312");
                arryNarration = String.valueOf("Narration : " + narration.getText().toString()).getBytes("GB2312");

                acknowLedge = String.valueOf("I acknowledge deposit of the above amount").getBytes("GB2312");
                nameVerify = String.valueOf("Name___________________________").getBytes("GB2312");
                idVerify = String.valueOf("ID No___________________________").getBytes("GB2312");
                signature = String.valueOf("Signature_______________________").getBytes("GB2312");

                nameAgent = String.valueOf("SERVED BY " + String.valueOf(aName)).getBytes("GB2312");
                customerReceipt = String.valueOf("         AGENT COPY         ").getBytes("GB2312");

                motto = String.valueOf(SharedPrefs.read(SharedPrefs.SACCO_MOTTO, null)).getBytes("GB2312");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(start);
            writeLineBreak(1);
            write(saccoName);
            writeLineBreak(2);
            write(type);
            writeLineBreak(1);

            write(terminalNo);
            writeLineBreak(1);
            write(transaction);
            writeLineBreak(1);
            write(time);
            writeLineBreak(1);
            write(transDate);
            writeLineBreak(1);
            write(account_dposited_to);
            writeLineBreak(1);
            /*write(idNumber);
            writeLineBreak(1);*/
            write(fullNames);
            writeLineBreak(0);
            write(fName);
            writeLineBreak(2);

            write(arryDepositAmt);
            writeLineBreak(1);
            write(arryWordsHeader);
            writeLineBreak(0);
            write(arryInWords);
            writeLineBreak(1);
            write(depositedBy);
            writeLineBreak(1);
            write(arryNarration);
            writeLineBreak(2);

            write(acknowLedge);
            writeLineBreak(1);
            write(nameVerify);
            writeLineBreak(1);
            write(idVerify);
            writeLineBreak(1);
            write(signature);
            writeLineBreak(2);

            write(nameAgent);
            writeLineBreak(2);
            write(customerReceipt);
            writeLineBreak(2);
            write(motto);
            // print line break
            writeLineBreak(4);

            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void write(final byte[] arryData) {
        int result = PrinterInterface.write(arryData, arryData.length);
    }

    // print line break
    private void writeLineBreak(int lineNumber) {
        // write(new byte[]{'\n'});
        write(PrinterCommand.getCmdEscDN(lineNumber));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                //Toast.makeText(Account.this, "Back has been presses!", Toast.LENGTH_LONG).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }

}

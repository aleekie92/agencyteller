package com.coretec.agencyteller.activities.teller;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.coretec.agencyteller.R;
import com.coretec.agencyteller.adapters.TellerListingsAdapter;
import com.coretec.agencyteller.api.Api;
import com.coretec.agencyteller.model.ModelListings;
import com.coretec.agencyteller.utils.SharedPrefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class TellerListings extends AppCompatActivity {

    public RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;
    Button search;

    ProgressDialog progressDialog;
    DatePickerDialog datePickerDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPrefs.init(getApplicationContext());
        setContentView(R.layout.teller_listings);

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

        //Toolbar declarations...Reusable
        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView mTitle = toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getText(R.string.teller_listing));
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);

        mRecyclerView = findViewById(R.id.recycler_view);

        search = findViewById(R.id.btn_search);

        progressDialog = new ProgressDialog(TellerListings.this);
        progressDialog.setMessage("Fetching data, please wait...");
        progressDialog.setCancelable(true);
        progressDialog.show();
        getAgentTransactions(SharedPrefs.read(SharedPrefs.AGENT_CODE, null));

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(TellerListings.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                                /*date.setText(year + "-"
                                        + (monthOfYear + 1) + "-" + dayOfMonth);*/

                                Calendar calendar= Calendar.getInstance();
                                calendar.set(year,monthOfYear,dayOfMonth);
                                String s =String.format(Locale.getDefault(),"%1$tY-%1$tm-%1$td",calendar);
                                //date.setText(s);

                                progressDialog = new ProgressDialog(TellerListings.this);
                                progressDialog.setMessage("Fetching data, please wait...");
                                progressDialog.setCancelable(true);
                                progressDialog.show();
                                getAgentTransactions1(SharedPrefs.read(SharedPrefs.AGENT_CODE, null), s);

                                //String dateSelected = date.getText().toString();
                                //SharedPrefs.write(SharedPrefs.DOB, dateSelected);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
                //datePickerDialog.updateDate(2001, 1, 1);
            }
        });

    }

    public void setuprecyclerview(List<ModelListings> modList) {

        mRecyclerView.setLayoutManager(new LinearLayoutManager(TellerListings.this));
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(mRecyclerView.getContext(), DividerItemDecoration.VERTICAL));

        mAdapter = new TellerListingsAdapter(TellerListings.this, modList);
        mRecyclerView.setAdapter(mAdapter);

    }

    private void getAgentTransactions(String accountNumber) {

        Api.getVolley(this, Api.GetAgentTransactions, "{\n" +
                "    \"AgentCode\": \"" + accountNumber + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                //progressBar.setVisibility(View.GONE);
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        progressDialog.dismiss();
                        List<ModelListings> modelListingsList= new ArrayList<>();

                        JSONObject dataObject = jsonObject.getJSONObject("data");

                        if (dataObject.getString("history").equals("null")) {
                            String msg = "Could not find today's transactions for this teller. Please choose another date.";
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(TellerListings.this);
                            builder1.setMessage(msg);
                            builder1.setTitle("Alert!");
                            builder1.setCancelable(false);

                            builder1.setPositiveButton(
                                    "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                            //onBackPressed();
                                        }
                                    });

                            AlertDialog alert11 = builder1.create();
                            alert11.show();
                        } else {

                            JSONArray jsonArray = dataObject.getJSONArray("history");

                            for (int i = 0; i < jsonArray.length(); i++) {

                                try {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    ModelListings newMod = new ModelListings(jsonObject1.getString("no"), jsonObject1.getString("date"),
                                            jsonObject1.getString("amount"), jsonObject1.getString("type"), jsonObject1.getString("account"));
                                    modelListingsList.add(newMod);

                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                }

                            }
                        }

                        setuprecyclerview(modelListingsList);
                        //mAdapter.notifyDataSetChanged();

                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        progressDialog.dismiss();

                        String msg = jsonObject.getString("message");
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(TellerListings.this);
                        builder1.setMessage(msg);
                        builder1.setTitle("Alert!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        //onBackPressed();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                        //Toast.makeText(TellerListings.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void getAgentTransactions1(String accountNumber, String date) {

        Api.getVolley(this, Api.GetAgentTransactions, "{\n" +
                "    \"AgentCode\": \"" + accountNumber + "\",\n" +
                "    \"dateFilter\": \"" + date + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                //progressBar.setVisibility(View.GONE);
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        progressDialog.dismiss();
                        List<ModelListings> modelListingsList= new ArrayList<>();

                        JSONObject dataObject = jsonObject.getJSONObject("data");

                        if (dataObject.getString("history").equals("null")) {
                            String msg = "Could not find any transactions for this teller at the selected date. Please choose another date.";
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(TellerListings.this);
                            builder1.setMessage(msg);
                            builder1.setTitle("Alert!");
                            builder1.setCancelable(false);

                            builder1.setPositiveButton(
                                    "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                            //onBackPressed();
                                        }
                                    });

                            AlertDialog alert11 = builder1.create();
                            alert11.show();
                        } else {

                            JSONArray jsonArray = dataObject.getJSONArray("history");

                            for (int i = 0; i < jsonArray.length(); i++) {

                                try {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    ModelListings newMod = new ModelListings(jsonObject1.getString("no"), jsonObject1.getString("date"),
                                            jsonObject1.getString("amount"), jsonObject1.getString("type"), jsonObject1.getString("account"));
                                    modelListingsList.add(newMod);

                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                }

                            }
                        }

                        setuprecyclerview(modelListingsList);
                        //mAdapter.notifyDataSetChanged();

                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        progressDialog.dismiss();
                        Toast.makeText(TellerListings.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public void onResume() {
        super.onResume();
        mRecyclerView.setAdapter(mAdapter);
        //mAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                //Toast.makeText(Account.this, "Back has been presses!", Toast.LENGTH_LONG).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }

}

package com.coretec.agencyteller.activities.teller;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyteller.R;
import com.coretec.agencyteller.api.Api;
import com.coretec.agencyteller.utils.EnglishNumberToWords;
import com.coretec.agencyteller.utils.PreferenceFileKeys;
import com.coretec.agencyteller.utils.SharedPrefs;
import com.coretec.agencyteller.utils.Utils;
import com.coretec.agencyteller.wizarpos.function.printer.PrinterCommand;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Transfers extends AppCompatActivity {

    private Spinner select_spinner;
    private LinearLayout other_selected;

    private EditText identifier;
    private Spinner identifierSpinner;
    private Spinner accountSpinner;
    private ArrayList<String> arrayList = new ArrayList<>();

    private Spinner accountSelfSpinner;
    private ArrayList<String> selfList = new ArrayList<>();

    private ProgressBar progressBar;
    ImageView arrow;

    private EditText identifier1;
    private Spinner identifierSpinner1;
    private Spinner accountSpinner1;
    private ArrayList<String> arrayList1 = new ArrayList<>();

    private EditText transacted_by;

    private ProgressBar progressBar1;
    private EditText amount;
    private Button submit, btn_view;
    private Button search;
    private Button search1;

    private ImageView picture;
    private ImageView signature;
    LinearLayout pic_layout;
    LinearLayout customer_details, signatory_details;
    TextView name, book_balance, balance, info_base_area, signatories;
    TextView destination_name, txt_destination;

    private ProgressDialog progressDialog;
    private ProgressDialog progressDialog1;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPrefs.init(getApplicationContext());
        setContentView(R.layout.teller_transfers);

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

        //Toolbar declarations...Reusable
        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView mTitle = toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getText(R.string.transfers));
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);

        //Beginning Signatories declarations
        customer_details = findViewById(R.id.linear_details);
        signatory_details = findViewById(R.id.linear_signatories);
        name = findViewById(R.id.tv_name);
        destination_name = findViewById(R.id.tv_name1);
        balance = findViewById(R.id.tv_balance);
        book_balance = findViewById(R.id.tv_book_balance);
        info_base_area = findViewById(R.id.tv_info_base_area);
        signatories = findViewById(R.id.tv_signatories);
        btn_view = findViewById(R.id.btn_view);
        transacted_by = findViewById(R.id.edittext_transacted_by);
        txt_destination = findViewById(R.id.destination_txt);
        search = findViewById(R.id.btn_search);
        search1 = findViewById(R.id.btn_search1);
        //End of Signatories declarations

        pic_layout = findViewById(R.id.pictures_layout);
        picture = findViewById(R.id.picture_imageview);
        signature = findViewById(R.id.signature_imageview);

        other_selected = findViewById(R.id.other_selected);
        submit = findViewById(R.id.button_submit);
        amount = findViewById(R.id.et_amount);

        select_spinner = findViewById(R.id.source);

        identifierSpinner = findViewById(R.id.identifier_spinner);
        accountSpinner = findViewById(R.id.accountsSpinner);
        identifier = findViewById(R.id.source_identifier);
        progressBar = findViewById(R.id.accountsPB);

        accountSelfSpinner = findViewById(R.id.accounts1Spinner);
        arrow = findViewById(R.id.iv_arrow);

        identifierSpinner1 = findViewById(R.id.destination_spinner);
        accountSpinner1 = findViewById(R.id.destinationAccounts);
        identifier1 = findViewById(R.id.et_identifier);
        progressBar1 = findViewById(R.id.destinationPB);

        select_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                String text = select_spinner.getSelectedItem().toString();

                switch (text) {
                    case "Other":
                        other_selected.setVisibility(View.VISIBLE);
                        accountSelfSpinner.setVisibility(View.GONE);
                        arrow.setVisibility(View.GONE);
                        txt_destination.setVisibility(View.GONE);
                        break;
                    case "Self":
                        other_selected.setVisibility(View.GONE);
                        accountSelfSpinner.setVisibility(View.VISIBLE);
                        arrow.setVisibility(View.VISIBLE);
                        txt_destination.setVisibility(View.GONE);
                        break;
                    default:
                        other_selected.setVisibility(View.GONE);
                        accountSelfSpinner.setVisibility(View.GONE);
                        arrow.setVisibility(View.GONE);
                        break;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Source accounts search
        accountSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                String item = parent.getItemAtPosition(position).toString();
                String text = accountSpinner.getSelectedItem().toString();

                String sourceAccount = text.substring(0, text.indexOf("-"));
                SharedPrefs.write(SharedPrefs.TRANSFER_ACC_ONE, sourceAccount);
                SharedPrefs.write(SharedPrefs.MEMBER_ACCOUNT_NUMBER, sourceAccount);
                Log.e("SOURCE", sourceAccount);

                switch (text) {
                    case "select-account":
                        break;
                    default:
                        requestBalance(sourceAccount);
                        checkSignatories(sourceAccount);
                        break;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Self source accounts search
        accountSelfSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                String item = parent.getItemAtPosition(position).toString();
                String text = accountSelfSpinner.getSelectedItem().toString();

                String sourceAccount = text.substring(0, text.indexOf("-"));
                SharedPrefs.write(SharedPrefs.TRANSFER_SELF_ONE, sourceAccount);
                Log.e("SOURCE", sourceAccount);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        identifierSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                String item = parent.getItemAtPosition(position).toString();
                String text = identifierSpinner.getSelectedItem().toString();

                int idLength = 8;
                int acLength = 14;
                int memLength = 14;
                InputFilter[] fArray = new InputFilter[1];
                fArray[0] = new InputFilter.LengthFilter(idLength);

                InputFilter[] aArray = new InputFilter[1];
                aArray[0] = new InputFilter.LengthFilter(acLength);

                InputFilter[] mArray = new InputFilter[1];
                mArray[0] = new InputFilter.LengthFilter(memLength);

                if (text.equals("ID NUMBER")) {
                    SharedPrefs.write(SharedPrefs.IDENTIFIER, "0");
                    identifier.setText(null);
                    identifier.setInputType(InputType.TYPE_CLASS_NUMBER);
                    identifier.setFilters(fArray);
                } else if (text.equals("OLD A/C NUMBER")) {
                    SharedPrefs.write(SharedPrefs.IDENTIFIER, "4");
                    identifier.setText(null);
                    identifier.setInputType(InputType.TYPE_CLASS_TEXT);
                    identifier.setFilters(aArray);
                } else if (text.equals("MEMBER NUMBER")) {
                    identifier.setText(null);
                    identifier.setInputType(InputType.TYPE_CLASS_NUMBER);
                    SharedPrefs.write(SharedPrefs.IDENTIFIER, "1");
                    identifier.setFilters(mArray);
                }

                Log.e("SELECTED", text);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = identifier.getText().toString();

                if (id.length()<7 && id.length()>0) {
                    identifier.setError("Please input a valid identifier!");
                } else if (id.length()==0){
                    identifier.setError("Cannot search an empty field!");
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(Transfers.this, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                    requestAccounts(id);
                    requestAccountsDepositable(id);
                    getPicture(id);
                    getSignature(id);
                }
            }
        });

        TextWatcher textWatcher = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String idNumber = identifier.getText().toString();

                /*if (idNumber.length() >= 7) {
                    requestAccounts(idNumber);
                    requestAccountsDepositable(idNumber);
                    getPicture(idNumber);
                    getSignature(idNumber);
                }*/

                if (idNumber.length() >= 5 && idNumber.length() <= 8) {
                    arrayList.clear();
                    selfList.clear();
                    accountSpinner = findViewById(R.id.accountsSpinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(Transfers.this, android.R.layout.simple_spinner_item, arrayList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    accountSelfSpinner = findViewById(R.id.accounts1Spinner);
                    ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(Transfers.this, android.R.layout.simple_spinner_item, selfList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    pic_layout.setVisibility(View.GONE);
                    customer_details.setVisibility(View.GONE);
                    signatory_details.setVisibility(View.GONE);

                    accountSelfSpinner.setAdapter(adapter1);
                    accountSpinner.setAdapter(adapter);
                }
                if (idNumber.length() < 5) {
                    //submitAccRegistrationRequest.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                }

            }
        };
        identifier.addTextChangedListener(textWatcher);

        //Destination accounts search
        accountSpinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                String item = parent.getItemAtPosition(position).toString();
                String text = accountSpinner1.getSelectedItem().toString();

                String destAccount = text.substring(0, text.indexOf("-"));
                SharedPrefs.write(SharedPrefs.TRANSFER_ACC_TWO, destAccount);
                Log.e("DESTINATION", destAccount);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        identifierSpinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                String item = parent.getItemAtPosition(position).toString();
                String text = identifierSpinner1.getSelectedItem().toString();


                int idLength = 8;
                int acLength = 14;
                int memLength = 14;

                InputFilter[] fArray = new InputFilter[1];
                fArray[0] = new InputFilter.LengthFilter(idLength);

                InputFilter[] aArray = new InputFilter[1];
                aArray[0] = new InputFilter.LengthFilter(acLength);

                InputFilter[] mArray = new InputFilter[1];
                mArray[0] = new InputFilter.LengthFilter(memLength);

                if (text.equals("ID NUMBER")) {
                    SharedPrefs.write(SharedPrefs.IDENTIFIER1, "0");
                    identifier1.setText(null);
                    identifier1.setInputType(InputType.TYPE_CLASS_NUMBER);
                    identifier1.setFilters(fArray);
                } else if (text.equals("OLD A/C NUMBER")) {
                    SharedPrefs.write(SharedPrefs.IDENTIFIER1, "4");
                    identifier1.setText(null);
                    identifier1.setInputType(InputType.TYPE_CLASS_TEXT);
                    identifier1.setFilters(aArray);
                } else if (text.equals("MEMBER NUMBER")) {
                    SharedPrefs.write(SharedPrefs.IDENTIFIER1, "1");
                    identifier1.setText(null);
                    identifier1.setInputType(InputType.TYPE_CLASS_NUMBER);
                    identifier1.setFilters(mArray);
                }

                Log.e("SELECTED", text);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        search1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = identifier1.getText().toString();

                if (id.length()<7 && id.length()>0) {
                    identifier1.setError("Please input a valid identifier!");
                } else if (id.length()==0){
                    identifier1.setError("Cannot search an empty field!");
                } else {
                    progressBar1.setVisibility(View.VISIBLE);
                    progressBar1.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(Transfers.this, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                    String idNumber = identifier1.getText().toString();
                    String firstId = identifier.getText().toString();

                    if (idNumber.length() == 1 && firstId.isEmpty()) {
                        identifier.setError("Fill in this field to proceed to destination!");
                        //identifier1.setError("Fill in the first field to proceed to destination!");
                        identifier.requestFocus();
                        identifier1.setText(null);
                        Toast.makeText(Transfers.this, "Enter identifier in source to proceed", Toast.LENGTH_LONG).show();
                    } else {

                        if (idNumber.length() >= 7) {
                            if (idNumber.equals(firstId)) {

                                arrayList1.clear();
                                accountSpinner1 = findViewById(R.id.destinationAccounts);
                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(Transfers.this, android.R.layout.simple_spinner_item, arrayList1);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                accountSpinner1.setAdapter(adapter);

                                identifier1.setError("Cannot enter same identifier here. Use self to proceed or change identifier");
                                identifier1.setText(null);
                                progressBar1.setVisibility(View.GONE);
                                Toast.makeText(Transfers.this, "Cannot enter same identifier here!", Toast.LENGTH_LONG).show();
                            } else {
                                requestAccounts1(idNumber);
                            }
                        }

                        if (idNumber.length() >= 5 && idNumber.length() <= 8) {
                            arrayList1.clear();
                            accountSpinner1 = findViewById(R.id.destinationAccounts);
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(Transfers.this, android.R.layout.simple_spinner_item, arrayList1);
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                            accountSpinner1.setAdapter(adapter);

                        }
                        if (idNumber.length() < 5) {
                            //submitAccRegistrationRequest.setVisibility(View.VISIBLE);
                            progressBar1.setVisibility(View.GONE);
                        }
                    }
                }
            }
        });

        TextWatcher textWatcher1 = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                String idNumber = identifier1.getText().toString();
                String firstId = identifier.getText().toString();

                if (idNumber.length() == 1 && firstId.isEmpty()) {
                    identifier.setError("Fill in this field to proceed to destination!");
                    //identifier1.setError("Fill in the first field to proceed to destination!");
                    identifier.requestFocus();
                    identifier1.setText(null);
                    Toast.makeText(Transfers.this, "Enter identifier in source to proceed", Toast.LENGTH_LONG).show();
                } else {

                    if (idNumber.length() >= 7) {
                        if (idNumber.equals(firstId)) {

                            arrayList1.clear();
                            accountSpinner1 = findViewById(R.id.destinationAccounts);
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(Transfers.this, android.R.layout.simple_spinner_item, arrayList1);
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                            accountSpinner1.setAdapter(adapter);

                            identifier1.setError("Cannot enter same identifier here. Use self to proceed or change identifier");
                            identifier1.setText(null);
                            progressBar1.setVisibility(View.GONE);
                            Toast.makeText(Transfers.this, "Cannot enter same identifier here!", Toast.LENGTH_LONG).show();
                        } else {
                            //requestAccounts1(idNumber);
                        }
                    }

                    if (idNumber.length() >= 5 && idNumber.length() <= 8) {
                        arrayList1.clear();
                        accountSpinner1 = findViewById(R.id.destinationAccounts);
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(Transfers.this, android.R.layout.simple_spinner_item, arrayList1);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        accountSpinner1.setAdapter(adapter);

                        /*progressBar1.setVisibility(View.VISIBLE);
                        progressBar1.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(Transfers.this, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);*/
                    }
                    if (idNumber.length() < 5) {
                        //submitAccRegistrationRequest.setVisibility(View.VISIBLE);
                        progressBar1.setVisibility(View.GONE);
                    }
                }

            }
        };
        identifier1.addTextChangedListener(textWatcher1);

        btn_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Transfers.this, DialogSignatories.class));
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = select_spinner.getSelectedItem().toString();
                String sourceId = identifier.getText().toString();
                String the_amount = amount.getText().toString();
                String destinationId = identifier1.getText().toString();
                String trBy = transacted_by.getText().toString();

                if (text.equals("Other")) {
                    txt_destination.setVisibility(View.GONE);
                    if (sourceId.isEmpty()) {
                        identifier.setError("Please enter this value!");
                    } else if (the_amount.isEmpty()) {
                        amount.setError("Please enter the amount!");
                    } else if (trBy.isEmpty()) {
                        transacted_by.setError("Please enter this field!");
                    } else if (destinationId.isEmpty()) {
                        identifier1.setError("Please enter this value!");
                    } else {
                        progressDialog = new ProgressDialog(Transfers.this);
                        progressDialog.setMessage("Submitting your request...");
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                        submitOtherTransfer();
                        submit.setClickable(false);
                    }
                } else if (text.equals("Self")) {
                    txt_destination.setVisibility(View.GONE);
                    if (sourceId.isEmpty()) {
                        identifier.setError("Please enter this value!");
                    } else if (the_amount.isEmpty()) {
                        amount.setError("Please enter the amount!");
                    } else if (trBy.isEmpty()) {
                        transacted_by.setError("Please enter this field!");
                    }  else if (SharedPrefs.read(SharedPrefs.TRANSFER_ACC_ONE, null).equals(SharedPrefs.read(SharedPrefs.TRANSFER_SELF_ONE, null))) {
                        String msg = "Cannot transfer to the same account. Please select different source and destination account to proceed!";
                        //final String qualKgs = SharedPrefs.read(SharedPrefs.LOAN_QUALIFIED_KGS, null);
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(Transfers.this);
                        builder1.setMessage(msg);
                        builder1.setTitle("Alert!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                    } else {
                        progressDialog = new ProgressDialog(Transfers.this);
                        progressDialog.setMessage("Submitting your request...");
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                        submitSelfTranfer();
                        submit.setClickable(false);
                    }
                } else if (text.equals("Destination")) {
                    txt_destination.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    public Bitmap StringToBitMap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    private void getPicture(String id) {

        int identifier = Integer.parseInt(SharedPrefs.read(SharedPrefs.IDENTIFIER, null));

        Api.getVolley(this, Api.GetMemberPictures, "{\n" +
                "    \"identifier\": \"" + id + "\",\n" +
                "    \"identifierType\": \"" + identifier + "\",\n" +
                "    \"fileType\": \"" + 0 + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                progressBar1.setVisibility(View.GONE);
                Log.e("RESULTS", Response);

                /*if (Response.length() == 71) {
                    picture.setImageDrawable(ContextCompat.getDrawable(CashWithdrawal.this, R.drawable.no_image));
                    pic_layout.setVisibility(View.VISIBLE);
                } else {

                    Bitmap decodeTxt = StringToBitMap(Response);

                    picture.setImageBitmap(decodeTxt);
                    pic_layout.setVisibility(View.VISIBLE);
                }*/

                Bitmap decodeTxt = StringToBitMap(Response);

                picture.setImageBitmap(decodeTxt);
                pic_layout.setVisibility(View.VISIBLE);
            }
        });

    }

    private void getSignature(String id) {

        int identifier = Integer.parseInt(SharedPrefs.read(SharedPrefs.IDENTIFIER, null));

        Api.getVolley(this, Api.GetMemberPictures, "{\n" +
                "    \"identifier\": \"" + id + "\",\n" +
                "    \"identifierType\": \"" + identifier + "\",\n" +
                "    \"fileType\": \"" + 1 + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                progressBar1.setVisibility(View.GONE);
                Log.e("RESULTS", Response);

                /*if (Response.equals("{\"error\":true,\"message\":\"Member Signature Not Found\",\"data\":null}")) {
                    signature.setImageDrawable(ContextCompat.getDrawable(CashWithdrawal.this, R.drawable.no_image));
                    pic_layout.setVisibility(View.VISIBLE);
                } else {

                    Bitmap decodeTxt = StringToBitMap(Response);

                    signature.setImageBitmap(decodeTxt);
                    pic_layout.setVisibility(View.VISIBLE);
                }*/

                Bitmap decodeTxt = StringToBitMap(Response);

                signature.setImageBitmap(decodeTxt);
                pic_layout.setVisibility(View.VISIBLE);
            }
        });

    }

    private void checkSignatories(String accountNumber) {

        Api.getVolley(this, Api.GetAccountSignatories, "{\n" +
                "    \"accountType\": \"" + 1 + "\",\n" +
                "    \"accountNo\": \"" + accountNumber + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                progressBar.setVisibility(View.GONE);
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        signatory_details.setVisibility(View.VISIBLE);
                        signatories.setText("Signatories found, press button below to view");
                        btn_view.setVisibility(View.VISIBLE);
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));

                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        signatory_details.setVisibility(View.VISIBLE);
                        signatories.setText(jsonObject.getString("message"));
                        btn_view.setVisibility(View.GONE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void requestBalance(String account_no) {

        Api.getVolley(this, Api.GetMemberAccounts, "{\n" +
                "    \"identifier\": \"" + account_no + "\",\n" +
                "    \"identifierType\": \"" + 3 + "\",\n" +
                "    \"natureOfAccount\": \"" + 1 + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                progressBar.setVisibility(View.GONE);
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {

                        customer_details.setVisibility(View.VISIBLE);

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            JSONObject balanceObject = jsonObject1.getJSONObject("balance");

                            String accountName = jsonObject1.getString("name");
                            String accountNo = jsonObject1.getString("no");
                            String accountBalance = balanceObject.getString("available");
                            String bookBalance = balanceObject.getString("actual");
                            String desc = jsonObject1.getString("description");
                            String infoBase = jsonObject1.getString("infoBaseArea");
                            //SharedPrefs.write(SharedPrefs.ACC_NAME, accountName);

                            name.setText(accountName);
                            balance.setText(accountBalance);
                            book_balance.setText(bookBalance);

                            if (infoBase.equals("null")) {
                                info_base_area.setText("");
                            } else {
                                info_base_area.setText(infoBase);
                            }

                        }
                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(Transfers.this, "Could not fetch data! Please try again.", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void requestAccounts(String id) {

        int identifier = Integer.parseInt(SharedPrefs.read(SharedPrefs.IDENTIFIER, null));

        Api.getVolley(this, Api.GetMemberAccounts, "{\n" +
                "    \"identifier\": \"" + id + "\",\n" +
                "    \"identifierType\": \"" + identifier + "\",\n" +
                "    \"natureOfAccount\": \"" + 1 + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                progressBar.setVisibility(View.GONE);
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));

                        arrayList.clear();
                        accountSpinner = findViewById(R.id.accountsSpinner);
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(Transfers.this, android.R.layout.simple_spinner_item, arrayList);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        accountSpinner.setAdapter(adapter);

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            String accountNo = jsonObject1.getString("no");
                            String desc = jsonObject1.getString("description");
                            String name = jsonObject1.getString("name");
                            String accountDetails = accountNo + "-" + desc;

                            arrayList.add(accountDetails);

                            SharedPrefs.write(SharedPrefs.ACC_NAME, name);

                            Log.e("ACCOUNT DETAILS", accountDetails);

                        }
                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        progressBar.setVisibility(View.GONE);
                        //Toast.makeText(Transfers.this, "Error!", Toast.LENGTH_SHORT).show();
                    }

                    accountSpinner = findViewById(R.id.accountsSpinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(Transfers.this, android.R.layout.simple_spinner_item, arrayList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    accountSpinner.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void requestAccountsDepositable(String id) {

        int identifier = Integer.parseInt(SharedPrefs.read(SharedPrefs.IDENTIFIER, null));

        Api.getVolley(this, Api.GetMemberAccounts, "{\n" +
                "    \"identifier\": \"" + id + "\",\n" +
                "    \"identifierType\": \"" + identifier + "\",\n" +
                "    \"natureOfAccount\": \"" + 2 + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                progressBar.setVisibility(View.GONE);
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));

                        selfList.clear();
                        accountSelfSpinner = findViewById(R.id.accounts1Spinner);
                        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(Transfers.this, android.R.layout.simple_spinner_item, selfList);
                        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        accountSelfSpinner.setAdapter(adapter1);

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            String accountNo = jsonObject1.getString("no");
                            String desc = jsonObject1.getString("description");
                            String name = jsonObject1.getString("name");
                            String accountDetails = accountNo + "-" + desc;

                            selfList.add(accountDetails);

                            SharedPrefs.write(SharedPrefs.ACC_NAME, name);

                            Log.e("ACCOUNT DETAILS", accountDetails);

                        }
                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        progressBar.setVisibility(View.GONE);
                        //Toast.makeText(Transfers.this, "Error!", Toast.LENGTH_SHORT).show();
                    }

                    accountSelfSpinner = findViewById(R.id.accounts1Spinner);
                    ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(Transfers.this, android.R.layout.simple_spinner_item, selfList);
                    adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    accountSelfSpinner.setAdapter(adapter1);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void requestAccounts1(String id) {

        int identifier1 = Integer.parseInt(SharedPrefs.read(SharedPrefs.IDENTIFIER, null));

        Api.getVolley(this, Api.GetMemberAccounts, "{\n" +
                "    \"identifier\": \"" + id + "\",\n" +
                "    \"identifierType\": \"" + identifier1 + "\",\n" +
                "    \"natureOfAccount\": \"" + 2 + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                progressBar1.setVisibility(View.GONE);
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));
                        arrayList1.clear();
                        accountSpinner1 = findViewById(R.id.destinationAccounts);
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(Transfers.this, android.R.layout.simple_spinner_item, arrayList1);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        accountSpinner1.setAdapter(adapter);

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            String accountNo = jsonObject1.getString("no");
                            String accountName = jsonObject1.getString("name");
                            String desc = jsonObject1.getString("description");
                            String accountDetails = accountNo + "-" + desc;

                            arrayList1.add(accountDetails);
                            destination_name.setText(accountName);

                            Log.e("ACCOUNT DETAILS", accountDetails);

                        }
                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        progressBar1.setVisibility(View.GONE);
                        //Toast.makeText(Transfers.this, "Error!", Toast.LENGTH_SHORT).show();
                    }

                    accountSpinner1 = findViewById(R.id.destinationAccounts);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(Transfers.this, android.R.layout.simple_spinner_item, arrayList1);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    accountSpinner1.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    void submitOtherTransfer() {

        Api.getVolley(this, Api.AccountTransferTransaction, "{\n" +
                "    \"action\": \"" + 0 + "\",\n" +
                "    \"transactionType\": \"" + 16 + "\",\n" +
                "    \"deviceID\": \"" + SharedPrefs.read(SharedPrefs.DEVICE_ID, null) + "\",\n" +
                "    \"agentCode\": \"" + SharedPrefs.read(SharedPrefs.AGENT_CODE, null) + "\",\n" +
                "    \"amount\": \"" + amount.getText().toString() + "\",\n" +
                "    \"sourceAccount\": \"" + SharedPrefs.read(SharedPrefs.TRANSFER_ACC_ONE, null) + "\",\n" +
                "    \"destinationAccount\": \"" + SharedPrefs.read(SharedPrefs.TRANSFER_ACC_TWO, null) + "\",\n" +
                "    \"transferType\": \"" + 1 + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));
                        Toast.makeText(Transfers.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                        final String tCode = jsonObject.getString("data");

                        int result = PrinterInterface.open();
                        writetransfer(tCode);
                        PrinterInterface.close();
                        progressDialog.dismiss();

                        Utils.showAlertDialog(Transfers.this, "Amount Transferred Successfully", " Press Ok to print another Receipt ", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                int result = PrinterInterface.open();
                                writetransfer1(tCode);
                                PrinterInterface.close();
                                onBackPressed();
                            }
                        });

                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        progressDialog.dismiss();
                        Toast.makeText(Transfers.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                        String msg = jsonObject.getString("message");
                        //final String qualKgs = SharedPrefs.read(SharedPrefs.LOAN_QUALIFIED_KGS, null);
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(Transfers.this);
                        builder1.setMessage(msg);
                        builder1.setTitle("Alert!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    void submitSelfTranfer() {

        Api.getVolley(this, Api.AccountTransferTransaction, "{\n" +
                "    \"action\": \"" + 0 + "\",\n" +
                "    \"transactionType\": \"" + 16 + "\",\n" +
                "    \"deviceID\": \"" + SharedPrefs.read(SharedPrefs.DEVICE_ID, null) + "\",\n" +
                "    \"agentCode\": \"" + SharedPrefs.read(SharedPrefs.AGENT_CODE, null) + "\",\n" +
                "    \"amount\": \"" + amount.getText().toString() + "\",\n" +
                "    \"sourceAccount\": \"" + SharedPrefs.read(SharedPrefs.TRANSFER_ACC_ONE, null) + "\",\n" +
                "    \"destinationAccount\": \"" + SharedPrefs.read(SharedPrefs.TRANSFER_SELF_ONE, null) + "\",\n" +
                "    \"transferType\": \"" + 0 + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));
                        Toast.makeText(Transfers.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                        final String tCode = jsonObject.getString("data");

                        int result = PrinterInterface.open();
                        writetest(tCode);
                        PrinterInterface.close();
                        progressDialog.dismiss();

                        Utils.showAlertDialog(Transfers.this, "Amount Transferred Successfully", " Press Ok to print another Receipt ", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                int result = PrinterInterface.open();
                                writetest1(tCode);
                                PrinterInterface.close();
                                onBackPressed();
                            }
                        });

                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        progressDialog.dismiss();
                        Toast.makeText(Transfers.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                        String msg = jsonObject.getString("message");
                        //final String qualKgs = SharedPrefs.read(SharedPrefs.LOAN_QUALIFIED_KGS, null);
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(Transfers.this);
                        builder1.setMessage(msg);
                        builder1.setTitle("Alert!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public void writetest(String transactionCode) {

        String the_amount = amount.getText().toString();
        int number = Integer.parseInt(the_amount);
        String return_val_in_english = EnglishNumberToWords.convert(number);

        try {

            byte[] start = null;
            byte[] saccoName = null;
            byte[] type = null;

            byte[] terminalNo = null;
            byte[] transaction = null;
            byte[] time = null;
            byte[] transDate = null;
            byte[] account_from = null;
            byte[] from_name = null;
            byte[] account_to = null;
            byte[] to_name = null;
            byte[] fullNames = null;
            byte[] fName = null;

            //byte[] phoneNumber = null;
            byte[] arryDepositAmt = null;
            byte[] arryWordsHeader = null;
            byte[] arryInWords = null;
            byte[] transactedBy = null;

            byte[] acknowLedge = null;
            byte[] nameVerify = null;
            byte[] idVerify = null;
            byte[] signature = null;

            byte[] nameAgent = null;
            byte[] customerReceipt = null;
            byte[] motto = null;

            SimpleDateFormat timeStampFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
            Date myDate = new Date();
            String timeCurrent = timeStampFormat.format(myDate);
            String aName = SharedPrefs.read(SharedPrefs.AGENT_NAME, null);
            //String depositorsName = deposited_by.getText().toString();

            String accountFrom = SharedPrefs.read(SharedPrefs.TRANSFER_ACC_ONE, null);
            String accountTo = SharedPrefs.read(SharedPrefs.TRANSFER_SELF_ONE, null);

            try {
                start = String.valueOf("              ").getBytes("GB2312");
                saccoName = String.valueOf(SharedPrefs.read(SharedPrefs.SACCO_NAME, null)).getBytes("GB2312");
                type = String.valueOf("      TRANSFER TO SELF     ").getBytes("GB2312");

                terminalNo = String.valueOf("Terminal ID : " + SharedPrefs.read(SharedPrefs.DEVICE_ID, null)).getBytes("GB2312");
                transaction = String.valueOf("Transaction No : " + transactionCode).getBytes("GB2312");
                time = String.valueOf("Print Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                transDate = String.valueOf("Trans. Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                account_from = String.valueOf("Account From : " + accountFrom).getBytes("GB2312");
                from_name = String.valueOf(" Name : " + name.getText().toString()).getBytes("GB2312");
                account_to = String.valueOf("Account To : " + accountTo).getBytes("GB2312");
                to_name = String.valueOf(" Name : " +  name.getText().toString()).getBytes("GB2312");
                fullNames = String.valueOf("Customer Full Names : ").getBytes("GB2312");
                fName = String.valueOf(String.valueOf(SharedPrefs.read(SharedPrefs.ACC_NAME, null))).getBytes("GB2312");

                arryDepositAmt = String.valueOf("Transferred Amount : KSH " + the_amount).getBytes("GB2312");
                arryWordsHeader = String.valueOf("Amount in Words: ").getBytes("GB2312");
                arryInWords = String.valueOf(return_val_in_english + " Kenya Shillings Only").getBytes("GB2312");
                transactedBy = String.valueOf("Transacted By: " + transacted_by.getText().toString()).getBytes("GB2312");

                acknowLedge = String.valueOf("I acknowledge transfer of the above amount").getBytes("GB2312");
                nameVerify = String.valueOf("Name___________________________").getBytes("GB2312");
                idVerify = String.valueOf("ID No___________________________").getBytes("GB2312");
                signature = String.valueOf("Signature_______________________").getBytes("GB2312");

                nameAgent = String.valueOf("SERVED BY " + String.valueOf(aName)).getBytes("GB2312");
                customerReceipt = String.valueOf("         CUSTOMER COPY         ").getBytes("GB2312");

                motto = String.valueOf(SharedPrefs.read(SharedPrefs.SACCO_MOTTO, null)).getBytes("GB2312");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(start);
            writeLineBreak(1);
            write(saccoName);
            writeLineBreak(2);
            write(type);
            writeLineBreak(1);

            write(terminalNo);
            writeLineBreak(1);
            write(transaction);
            writeLineBreak(1);
            write(time);
            writeLineBreak(1);
            write(transDate);
            writeLineBreak(1);
            write(account_from);
            writeLineBreak(0);
            write(from_name);
            writeLineBreak(1);
            write(account_to);
            writeLineBreak(0);
            write(to_name);
            writeLineBreak(2);
            /*write(fullNames);
            writeLineBreak(0);
            write(fName);
            writeLineBreak(2);*/

            write(arryDepositAmt);
            writeLineBreak(1);
            write(arryWordsHeader);
            writeLineBreak(0);
            write(arryInWords);
            writeLineBreak(1);
            write(transactedBy);
            writeLineBreak(1);

            write(acknowLedge);
            writeLineBreak(1);
            write(nameVerify);
            writeLineBreak(1);
            write(idVerify);
            writeLineBreak(1);
            write(signature);
            writeLineBreak(2);

            write(nameAgent);
            writeLineBreak(2);
            write(customerReceipt);
            writeLineBreak(2);
            write(motto);
            // print line break
            writeLineBreak(4);

            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void writetest1(String transactionCode) {
        String the_amount = amount.getText().toString();
        int number = Integer.parseInt(the_amount);
        String return_val_in_english = EnglishNumberToWords.convert(number);

        try {

            byte[] start = null;
            byte[] saccoName = null;
            byte[] type = null;

            byte[] terminalNo = null;
            byte[] transaction = null;
            byte[] time = null;
            byte[] transDate = null;
            byte[] account_from = null;
            byte[] from_name = null;
            byte[] account_to = null;
            byte[] to_name = null;
            byte[] fullNames = null;
            byte[] fName = null;

            //byte[] phoneNumber = null;
            byte[] arryDepositAmt = null;
            byte[] arryWordsHeader = null;
            byte[] arryInWords = null;
            byte[] transactedBy = null;

            byte[] acknowLedge = null;
            byte[] nameVerify = null;
            byte[] idVerify = null;
            byte[] signature = null;

            byte[] nameAgent = null;
            byte[] agentReceipt = null;
            byte[] motto = null;

            SimpleDateFormat timeStampFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
            Date myDate = new Date();
            String timeCurrent = timeStampFormat.format(myDate);
            String aName = SharedPrefs.read(SharedPrefs.AGENT_NAME, null);
            //String depositorsName = deposited_by.getText().toString();

            String accountFrom = SharedPrefs.read(SharedPrefs.TRANSFER_ACC_ONE, null);
            String accountTo = SharedPrefs.read(SharedPrefs.TRANSFER_SELF_ONE, null);

            try {
                start = String.valueOf("              ").getBytes("GB2312");
                saccoName = String.valueOf(SharedPrefs.read(SharedPrefs.SACCO_NAME, null)).getBytes("GB2312");
                type = String.valueOf("      TRANSFER TO SELF     ").getBytes("GB2312");

                terminalNo = String.valueOf("Terminal ID : " + SharedPrefs.read(SharedPrefs.DEVICE_ID, null)).getBytes("GB2312");
                transaction = String.valueOf("Transaction No : " + transactionCode).getBytes("GB2312");
                time = String.valueOf("Print Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                transDate = String.valueOf("Trans. Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                account_from = String.valueOf("Account From : " + accountFrom).getBytes("GB2312");
                from_name = String.valueOf(" Name : " + name.getText().toString()).getBytes("GB2312");
                account_to = String.valueOf("Account To : " + accountTo).getBytes("GB2312");
                to_name = String.valueOf(" Name : " +  name.getText().toString()).getBytes("GB2312");
                fullNames = String.valueOf("Customer Full Names : ").getBytes("GB2312");
                fName = String.valueOf(String.valueOf(SharedPrefs.read(SharedPrefs.ACC_NAME, null))).getBytes("GB2312");

                arryDepositAmt = String.valueOf("Transferred Amount : KSH " + the_amount).getBytes("GB2312");
                arryWordsHeader = String.valueOf("Amount in Words: ").getBytes("GB2312");
                arryInWords = String.valueOf(return_val_in_english + " Kenya Shillings Only").getBytes("GB2312");
                transactedBy = String.valueOf("Transacted By: " + transacted_by.getText().toString()).getBytes("GB2312");

                acknowLedge = String.valueOf("I acknowledge transfer of the above amount").getBytes("GB2312");
                nameVerify = String.valueOf("Name___________________________").getBytes("GB2312");
                idVerify = String.valueOf("ID No___________________________").getBytes("GB2312");
                signature = String.valueOf("Signature_______________________").getBytes("GB2312");

                nameAgent = String.valueOf("SERVED BY " + String.valueOf(aName)).getBytes("GB2312");
                agentReceipt = String.valueOf("         AGENT COPY         ").getBytes("GB2312");

                motto = String.valueOf(SharedPrefs.read(SharedPrefs.SACCO_MOTTO, null)).getBytes("GB2312");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(start);
            writeLineBreak(1);
            write(saccoName);
            writeLineBreak(2);
            write(type);
            writeLineBreak(1);

            write(terminalNo);
            writeLineBreak(1);
            write(transaction);
            writeLineBreak(1);
            write(time);
            writeLineBreak(1);
            write(transDate);
            writeLineBreak(1);
            write(account_from);
            writeLineBreak(0);
            write(from_name);
            writeLineBreak(1);
            write(account_to);
            writeLineBreak(0);
            write(to_name);
            writeLineBreak(2);
            /*write(fullNames);
            writeLineBreak(0);
            write(fName);
            writeLineBreak(2);*/

            write(arryDepositAmt);
            writeLineBreak(1);
            write(arryWordsHeader);
            writeLineBreak(0);
            write(arryInWords);
            writeLineBreak(1);
            write(transactedBy);
            writeLineBreak(1);

            write(acknowLedge);
            writeLineBreak(1);
            write(nameVerify);
            writeLineBreak(1);
            write(idVerify);
            writeLineBreak(1);
            write(signature);
            writeLineBreak(2);

            write(nameAgent);
            writeLineBreak(2);
            write(agentReceipt);
            writeLineBreak(2);
            write(motto);
            // print line break
            writeLineBreak(4);

            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void writetransfer(String transactionCode) {

        String the_amount = amount.getText().toString();
        int number = Integer.parseInt(the_amount);
        String return_val_in_english = EnglishNumberToWords.convert(number);

        try {

            byte[] start = null;
            byte[] saccoName = null;
            byte[] type = null;

            byte[] terminalNo = null;
            byte[] transaction = null;
            byte[] time = null;
            byte[] transDate = null;
            byte[] account_from = null;
            byte[] from_name = null;
            byte[] account_to = null;
            byte[] to_name = null;
            byte[] fullNames = null;
            byte[] fName = null;

            //byte[] phoneNumber = null;
            byte[] arryDepositAmt = null;
            byte[] arryWordsHeader = null;
            byte[] arryInWords = null;
            byte[] transactedBy = null;

            byte[] acknowLedge = null;
            byte[] nameVerify = null;
            byte[] idVerify = null;
            byte[] signature = null;

            byte[] nameAgent = null;
            byte[] customerReceipt = null;
            byte[] motto = null;

            SimpleDateFormat timeStampFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
            Date myDate = new Date();
            String timeCurrent = timeStampFormat.format(myDate);
            String aName = SharedPrefs.read(SharedPrefs.AGENT_NAME, null);
            //String depositorsName = deposited_by.getText().toString();

            String accountFrom = SharedPrefs.read(SharedPrefs.TRANSFER_ACC_ONE, null);
            String accountTo = SharedPrefs.read(SharedPrefs.TRANSFER_ACC_TWO, null);

            try {
                start = String.valueOf("              ").getBytes("GB2312");
                saccoName = String.valueOf(SharedPrefs.read(SharedPrefs.SACCO_NAME, null)).getBytes("GB2312");
                type = String.valueOf("      TRANSFER TO OTHER     ").getBytes("GB2312");

                terminalNo = String.valueOf("Terminal ID : " + SharedPrefs.read(SharedPrefs.DEVICE_ID, null)).getBytes("GB2312");
                transaction = String.valueOf("Transaction No : " + transactionCode).getBytes("GB2312");
                time = String.valueOf("Print Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                transDate = String.valueOf("Trans. Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                account_from = String.valueOf("Account From : " + accountFrom).getBytes("GB2312");
                from_name = String.valueOf(" Name : " + name.getText().toString()).getBytes("GB2312");
                account_to = String.valueOf("Account To : " + accountTo).getBytes("GB2312");
                to_name = String.valueOf(" Name : " + destination_name.getText().toString()).getBytes("GB2312");
                fullNames = String.valueOf("Customer Full Names : ").getBytes("GB2312");
                fName = String.valueOf(String.valueOf(SharedPrefs.read(SharedPrefs.ACC_NAME, null))).getBytes("GB2312");

                arryDepositAmt = String.valueOf("Transferred Amount : KSH " + the_amount).getBytes("GB2312");
                arryWordsHeader = String.valueOf("Amount in Words: ").getBytes("GB2312");
                arryInWords = String.valueOf(return_val_in_english + " Kenya Shillings Only").getBytes("GB2312");
                transactedBy = String.valueOf("Transacted By: " + transacted_by.getText().toString()).getBytes("GB2312");

                acknowLedge = String.valueOf("I acknowledge transfer of the above amount").getBytes("GB2312");
                nameVerify = String.valueOf("Name___________________________").getBytes("GB2312");
                idVerify = String.valueOf("ID No___________________________").getBytes("GB2312");
                signature = String.valueOf("Signature_______________________").getBytes("GB2312");

                nameAgent = String.valueOf("SERVED BY " + String.valueOf(aName)).getBytes("GB2312");
                customerReceipt = String.valueOf("         CUSTOMER COPY         ").getBytes("GB2312");

                motto = String.valueOf(SharedPrefs.read(SharedPrefs.SACCO_MOTTO, null)).getBytes("GB2312");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(start);
            writeLineBreak(1);
            write(saccoName);
            writeLineBreak(2);
            write(type);
            writeLineBreak(1);

            write(terminalNo);
            writeLineBreak(1);
            write(transaction);
            writeLineBreak(1);
            write(time);
            writeLineBreak(1);
            write(transDate);
            writeLineBreak(1);
            write(account_from);
            writeLineBreak(0);
            write(from_name);
            writeLineBreak(1);
            write(account_to);
            writeLineBreak(0);
            write(to_name);
            writeLineBreak(2);
            /*write(fullNames);
            writeLineBreak(0);
            write(fName);
            writeLineBreak(2);*/

            write(arryDepositAmt);
            writeLineBreak(1);
            write(arryWordsHeader);
            writeLineBreak(0);
            write(arryInWords);
            writeLineBreak(1);
            write(transactedBy);
            writeLineBreak(1);

            write(acknowLedge);
            writeLineBreak(1);
            write(nameVerify);
            writeLineBreak(1);
            write(idVerify);
            writeLineBreak(1);
            write(signature);
            writeLineBreak(2);

            write(nameAgent);
            writeLineBreak(2);
            write(customerReceipt);
            writeLineBreak(2);
            write(motto);
            // print line break
            writeLineBreak(4);

            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void writetransfer1(String transactionCode) {
        String the_amount = amount.getText().toString();
        int number = Integer.parseInt(the_amount);
        String return_val_in_english = EnglishNumberToWords.convert(number);

        try {

            byte[] start = null;
            byte[] saccoName = null;
            byte[] type = null;

            byte[] terminalNo = null;
            byte[] transaction = null;
            byte[] time = null;
            byte[] transDate = null;
            byte[] account_from = null;
            byte[] from_name = null;
            byte[] account_to = null;
            byte[] to_name = null;
            byte[] fullNames = null;
            byte[] fName = null;

            //byte[] phoneNumber = null;
            byte[] arryDepositAmt = null;
            byte[] arryWordsHeader = null;
            byte[] arryInWords = null;
            byte[] transactedBy = null;

            byte[] acknowLedge = null;
            byte[] nameVerify = null;
            byte[] idVerify = null;
            byte[] signature = null;

            byte[] nameAgent = null;
            byte[] customerReceipt = null;
            byte[] motto = null;

            SimpleDateFormat timeStampFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
            Date myDate = new Date();
            String timeCurrent = timeStampFormat.format(myDate);
            String aName = SharedPrefs.read(SharedPrefs.AGENT_NAME, null);
            //String depositorsName = deposited_by.getText().toString();

            String accountFrom = SharedPrefs.read(SharedPrefs.TRANSFER_ACC_ONE, null);
            String accountTo = SharedPrefs.read(SharedPrefs.TRANSFER_ACC_TWO, null);

            try {
                start = String.valueOf("              ").getBytes("GB2312");
                saccoName = String.valueOf(SharedPrefs.read(SharedPrefs.SACCO_NAME, null)).getBytes("GB2312");
                type = String.valueOf("      TRANSFER TO OTHER     ").getBytes("GB2312");

                terminalNo = String.valueOf("Terminal ID : " + SharedPrefs.read(SharedPrefs.DEVICE_ID, null)).getBytes("GB2312");
                transaction = String.valueOf("Transaction No : " + transactionCode).getBytes("GB2312");
                time = String.valueOf("Print Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                transDate = String.valueOf("Trans. Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                account_from = String.valueOf("Account From : " + accountFrom).getBytes("GB2312");
                from_name = String.valueOf(" Name : " + name.getText().toString()).getBytes("GB2312");
                account_to = String.valueOf("Account To : " + accountTo).getBytes("GB2312");
                to_name = String.valueOf(" Name : " + destination_name.getText().toString()).getBytes("GB2312");
                fullNames = String.valueOf("Customer Full Names : ").getBytes("GB2312");
                fName = String.valueOf(String.valueOf(SharedPrefs.read(SharedPrefs.ACC_NAME, null))).getBytes("GB2312");

                arryDepositAmt = String.valueOf("Transferred Amount : KSH " + the_amount).getBytes("GB2312");
                arryWordsHeader = String.valueOf("Amount in Words: ").getBytes("GB2312");
                arryInWords = String.valueOf(return_val_in_english + " Kenya Shillings Only").getBytes("GB2312");
                transactedBy = String.valueOf("Transacted By: " + transacted_by.getText().toString()).getBytes("GB2312");

                acknowLedge = String.valueOf("I acknowledge transfer of the above amount").getBytes("GB2312");
                nameVerify = String.valueOf("Name___________________________").getBytes("GB2312");
                idVerify = String.valueOf("ID No___________________________").getBytes("GB2312");
                signature = String.valueOf("Signature_______________________").getBytes("GB2312");

                nameAgent = String.valueOf("SERVED BY " + String.valueOf(aName)).getBytes("GB2312");
                customerReceipt = String.valueOf("         CUSTOMER COPY         ").getBytes("GB2312");

                motto = String.valueOf(SharedPrefs.read(SharedPrefs.SACCO_MOTTO, null)).getBytes("GB2312");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(start);
            writeLineBreak(1);
            write(saccoName);
            writeLineBreak(2);
            write(type);
            writeLineBreak(1);

            write(terminalNo);
            writeLineBreak(1);
            write(transaction);
            writeLineBreak(1);
            write(time);
            writeLineBreak(1);
            write(transDate);
            writeLineBreak(1);
            write(account_from);
            writeLineBreak(0);
            write(from_name);
            writeLineBreak(1);
            write(account_to);
            writeLineBreak(0);
            write(to_name);
            writeLineBreak(2);
            /*write(fullNames);
            writeLineBreak(0);
            write(fName);
            writeLineBreak(2);*/

            write(arryDepositAmt);
            writeLineBreak(1);
            write(arryWordsHeader);
            writeLineBreak(0);
            write(arryInWords);
            writeLineBreak(1);
            write(transactedBy);
            writeLineBreak(1);

            write(acknowLedge);
            writeLineBreak(1);
            write(nameVerify);
            writeLineBreak(1);
            write(idVerify);
            writeLineBreak(1);
            write(signature);
            writeLineBreak(2);

            write(nameAgent);
            writeLineBreak(2);
            write(customerReceipt);
            writeLineBreak(2);
            write(motto);
            // print line break
            writeLineBreak(4);

            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void write(final byte[] arryData) {
        int result = PrinterInterface.write(arryData, arryData.length);
    }

    // print line break
    private void writeLineBreak(int lineNumber) {
        // write(new byte[]{'\n'});
        write(PrinterCommand.getCmdEscDN(lineNumber));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                //Toast.makeText(Account.this, "Back has been presses!", Toast.LENGTH_LONG).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }

}

package com.coretec.agencyteller.activities.reports;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.coretec.agencyteller.R;
import com.coretec.agencyteller.api.Api;
import com.coretec.agencyteller.utils.SharedPrefs;

import org.json.JSONException;
import org.json.JSONObject;

public class BalanceInquiry extends AppCompatActivity {

    TextView agentName,  agentCode, agentNumber, agentBranch, accountBalance;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPrefs.init(getApplicationContext());
        setContentView(R.layout.balance_inquiry);

        agentName = findViewById(R.id.agent_name);
        agentCode = findViewById(R.id.agent_account);
        agentNumber = findViewById(R.id.agent_number);
        agentBranch = findViewById(R.id.agent_branch);
        accountBalance = findViewById(R.id.agent_account_balance);

        agentName.setText(SharedPrefs.read(SharedPrefs.AGENT_NAME, null));
        agentCode.setText(SharedPrefs.read(SharedPrefs.AGENT_CODE, null));
        agentNumber.setText(SharedPrefs.read(SharedPrefs.TELLER_ACCOUNT_NUMBER, null));
        agentBranch.setText(SharedPrefs.read(SharedPrefs.AGENT_BRANCH, null));
        //accountBalance.setText(SharedPrefs.read(SharedPrefs.ACCOUNT_BALANCE, null));

        progressDialog = new ProgressDialog(BalanceInquiry.this);
        progressDialog.setMessage("Updating information, please wait...");
        progressDialog.setCancelable(true);
        progressDialog.show();
        getBalance(SharedPrefs.read(SharedPrefs.AGENT_CODE, null));

    }

    private void getBalance(String agentCode) {

        Api.getVolley(this, Api.GetAgentAccount, "{\n" +
                "    \"agentCode\": \"" + agentCode + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                //progressBar.setVisibility(View.GONE);
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        progressDialog.dismiss();

                        JSONObject dataObject = jsonObject.getJSONObject("data");

                        String balance = dataObject.getString("balance");
                        accountBalance.setText(balance);

                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        progressDialog.dismiss();
                        String msg = jsonObject.getString("message");
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(BalanceInquiry.this);
                        builder1.setMessage(msg);
                        builder1.setTitle("Alert!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                        //Toast.makeText(TellerApprovals.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

}

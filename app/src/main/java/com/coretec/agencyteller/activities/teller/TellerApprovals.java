package com.coretec.agencyteller.activities.teller;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.coretec.agencyteller.R;
import com.coretec.agencyteller.adapters.TellerApprovalsAdapter;
import com.coretec.agencyteller.api.Api;
import com.coretec.agencyteller.model.ModelApprovals;
import com.coretec.agencyteller.model.ModelListings;
import com.coretec.agencyteller.utils.SharedPrefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TellerApprovals extends AppCompatActivity {

    public RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPrefs.init(getApplicationContext());
        setContentView(R.layout.teller_approvals);

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

        //Toolbar declarations...Reusable
        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView mTitle = toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getText(R.string.approved_transactions));//teller_approvals
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);

        mRecyclerView = findViewById(R.id.recycler_view);

        progressDialog = new ProgressDialog(TellerApprovals.this);
        progressDialog.setMessage("Fetching data, please wait...");
        progressDialog.setCancelable(true);
        progressDialog.show();
        getAgentTransactions(SharedPrefs.read(SharedPrefs.AGENT_CODE, null));

    }

    public void setuprecyclerview(List<ModelApprovals> modList) {

        mRecyclerView.setLayoutManager(new LinearLayoutManager(TellerApprovals.this));
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(mRecyclerView.getContext(), DividerItemDecoration.VERTICAL));

        mAdapter = new TellerApprovalsAdapter(TellerApprovals.this, modList);
        mRecyclerView.setAdapter(mAdapter);

    }

    private void getAgentTransactions(String accountNumber) {

        Api.getVolley(this, Api.GetAgentTransactions, "{\n" +
                "    \"AgentCode\": \"" + accountNumber + "\",\n" +
                "    \"dateFilter\": \"" + null + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                //progressBar.setVisibility(View.GONE);
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        progressDialog.dismiss();
                        List<ModelApprovals> modelApprovalsList= new ArrayList<>();

                        JSONObject dataObject = jsonObject.getJSONObject("data");

                        if (dataObject.getString("approvals").equals("null")) {
                            String msg = "There are no transactions that are waiting approvals at the moment";
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(TellerApprovals.this);
                            builder1.setMessage(msg);
                            builder1.setTitle("Alert!");
                            builder1.setCancelable(false);

                            builder1.setPositiveButton(
                                    "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                            onBackPressed();
                                        }
                                    });

                            AlertDialog alert11 = builder1.create();
                            alert11.show();
                        } else {
                            JSONArray jsonArray = dataObject.getJSONArray("approvals");

                            for (int i = 0; i < jsonArray.length(); i++) {

                                try {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    ModelApprovals newMod = new ModelApprovals(jsonObject1.getString("no"), jsonObject1.getString("date"),
                                            jsonObject1.getString("amount"), jsonObject1.getString("type"), jsonObject1.getString("account"));
                                    modelApprovalsList.add(newMod);

                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                }

                            }
                        }

                        setuprecyclerview(modelApprovalsList);
                        //mAdapter.notifyDataSetChanged();

                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        progressDialog.dismiss();
                        String msg = jsonObject.getString("message");
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(TellerApprovals.this);
                        builder1.setMessage(msg);
                        builder1.setTitle("Alert!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                        //Toast.makeText(TellerApprovals.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public void onResume() {
        super.onResume();
        mRecyclerView.setAdapter(mAdapter);
        //mAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                //Toast.makeText(Account.this, "Back has been presses!", Toast.LENGTH_LONG).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }

}

package com.coretec.agencyteller.activities.member;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyteller.R;
import com.coretec.agencyteller.api.Api;
import com.coretec.agencyteller.utils.SharedPrefs;
import com.coretec.agencyteller.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

public class StandingOrder extends AppCompatActivity {

    private Spinner stoSpinner;
    LinearLayout external, credit;
    private EditText identifier;
    private EditText amount, duration, frequency;
    TextView startDate;
    DatePickerDialog datePickerDialog;
    private Button submit;
    private Button search;

    private Spinner bankSpinner;
    private ArrayList<String> bankList = new ArrayList<>();

    private Spinner loanSpinner;
    private ArrayList<String> loanList = new ArrayList<>();

    private Spinner accountSpinner;
    private ArrayList<String> arrayList = new ArrayList<>();

    private Spinner recoverySpinner;
    private ArrayList<String> recoverList = new ArrayList<>();

    private ProgressBar bankPb;
    private ProgressBar loanPb;
    private ProgressBar progressBar;
    private ProgressBar recoverPb;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.standing_order);

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

        //Toolbar declarations...Reusable
        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView mTitle = toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getText(R.string.standing_order));
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);

        identifier = findViewById(R.id.identifier);
        progressBar = findViewById(R.id.accountsPB);
        accountSpinner = findViewById(R.id.accountsSpinner);
        recoverySpinner = findViewById(R.id.recoverySpinner);

        startDate =  findViewById(R.id.start_date);
        submit = findViewById(R.id.button_submit);
        search = findViewById(R.id.btn_search);
        amount = findViewById(R.id.et_amount);
        duration = findViewById(R.id.et_duration);
        frequency = findViewById(R.id.et_frequency);

        external = findViewById(R.id.external_selected);
        credit = findViewById(R.id.credit_selected);

        bankSpinner = findViewById(R.id.banksSpinner);
        bankPb = findViewById(R.id.banksPB);
        loanSpinner = findViewById(R.id.loanSpinner);
        loanPb = findViewById(R.id.loanPB);
        recoverPb = findViewById(R.id.recoveryPB);

        getRecoveryCode();

        startDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(StandingOrder.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                                startDate.setText(dayOfMonth + "-"
                                        + (monthOfYear + 1) + "-" + year);
                                String dateSelected = startDate.getText().toString();
                                SharedPrefs.write(SharedPrefs.START_DATE, dateSelected);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
                //datePickerDialog.updateDate(2001, 1, 1);
            }
        });

        accountSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //((TextView) parent.getChildAt(0)).setTextColor(Color.GRAY);
                String item = parent.getItemAtPosition(position).toString();
                String text = accountSpinner.getSelectedItem().toString();

                String account_number = text.substring(0, text.indexOf("-"));
                SharedPrefs.write(SharedPrefs.STO_DESTINATION_AC, account_number);
                Log.e("DESTINATION ACCOUNT", account_number);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        stoSpinner = findViewById(R.id.stoTypeSpinner);
        stoSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                String text = stoSpinner.getSelectedItem().toString();

                switch (text) {
                    case "0-External":
                        external.setVisibility(View.VISIBLE);
                        credit.setVisibility(View.GONE);
                        SharedPrefs.write(SharedPrefs.STO_LOAN_NUMBER, "null");
                        getBanks();

                        break;
                    case "2-Credit":
                        credit.setVisibility(View.VISIBLE);
                        external.setVisibility(View.GONE);
                        SharedPrefs.write(SharedPrefs.STO_BANK_NUMBER, "null");

                        break;
                    default:
                        credit.setVisibility(View.GONE);
                        external.setVisibility(View.GONE);
                        SharedPrefs.write(SharedPrefs.STO_LOAN_NUMBER, "null");
                        SharedPrefs.write(SharedPrefs.STO_BANK_NUMBER, "null");
                        break;
                }

                String sto_type = text.substring(0, text.indexOf("-"));
                SharedPrefs.write(SharedPrefs.STO_TYPE_CODE, sto_type);
                Log.e("STO TYPE CODE", sto_type);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        recoverySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //((TextView) parent.getChildAt(0)).setTextColor(Color.GRAY);
                String item = parent.getItemAtPosition(position).toString();
                String text = recoverySpinner.getSelectedItem().toString();

                String rec_code = text.substring(0, text.indexOf("-"));
                SharedPrefs.write(SharedPrefs.RECOVERY_CODE, rec_code);
                Log.e("RECOVERY CODE", rec_code);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        bankSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //((TextView) parent.getChildAt(0)).setTextColor(Color.GRAY);
                String item = parent.getItemAtPosition(position).toString();
                String text = bankSpinner.getSelectedItem().toString();

                String bank_no = text.substring(0, text.indexOf("-"));
                SharedPrefs.write(SharedPrefs.STO_BANK_NUMBER, bank_no);
                Log.e("BANK_NUMBER", bank_no);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        loanSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //((TextView) parent.getChildAt(0)).setTextColor(Color.GRAY);
                String item = parent.getItemAtPosition(position).toString();
                String text = loanSpinner.getSelectedItem().toString();

                String loan_account = text.substring(0, text.indexOf("-"));
                SharedPrefs.write(SharedPrefs.LOAN_ACCOUNT, loan_account);
                Log.e("LOAN ACCOUNT", loan_account);

                int indexOfDash = text.indexOf('-');
                String before = text.substring(0, indexOfDash);
                String after = text.substring(indexOfDash + 1);

                String loan_no = after.substring(0, after.indexOf("-"));
                SharedPrefs.write(SharedPrefs.STO_LOAN_NUMBER, loan_no);
                Log.e("LOAN NUMBER", loan_no);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = identifier.getText().toString();

                if (id.length()<7 && id.length()>0) {
                    identifier.setError("Please input a valid identifier!");
                } else if (id.length()==0){
                    identifier.setError("Cannot search an empty field!");
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(StandingOrder.this, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                    requestAccounts(id);
                    getLoans(id);
                }
            }
        });

        TextWatcher textWatcher = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String idNumber = identifier.getText().toString();

                /*if (idNumber.length() >= 8) {
                    requestAccounts(idNumber);
                    getLoans(idNumber);
                }*/

                if (idNumber.length() >= 5 && idNumber.length() <= 8) {
                    arrayList.clear();
                    accountSpinner = findViewById(R.id.accountsSpinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(StandingOrder.this, android.R.layout.simple_spinner_item, arrayList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    accountSpinner.setAdapter(adapter);

                    loanList.clear();
                    loanSpinner = findViewById(R.id.loanSpinner);
                    ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(StandingOrder.this, android.R.layout.simple_spinner_item, loanList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    loanSpinner.setAdapter(adapter1);
                }
                if (idNumber.length() < 5) {
                    //submitAccRegistrationRequest.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                }

            }
        };

        identifier.addTextChangedListener(textWatcher);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String idNo = identifier.getText().toString();
                String en_amount = amount.getText().toString();
                String du_ration = duration.getText().toString();
                String fre_quency = frequency.getText().toString();
                String dat_e = startDate.getText().toString();

                String text = stoSpinner.getSelectedItem().toString();

                if (text.equals("2-Credit")) {
                    SharedPrefs.write(SharedPrefs.STO_DESTINATION_AC, SharedPrefs.STO_LOAN_NUMBER);
                }

                if (idNo.isEmpty()) {
                    identifier.setError("Please input ID number!");
                } else if (en_amount.isEmpty()) {
                    amount.setError("Please enter amount!");
                } else if (du_ration.isEmpty()) {
                    duration.setError("Please input the duration!");
                } else if (fre_quency.isEmpty()) {
                    frequency.setError("Please input the frequency!");
                } else if (dat_e.isEmpty()) {
                    Toast.makeText(StandingOrder.this, "Please select the start date!", Toast.LENGTH_SHORT).show();
                } else {
                    progressDialog = new ProgressDialog(StandingOrder.this);
                    progressDialog.setMessage("Submitting your request...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    submitStandingOrder();
                }
            }
        });

    }

    void submitStandingOrder () {

        Api.getVolley(this, Api.StandingOrderTransaction, "{\n" +
                "    \"action\": \"" + 0 + "\",\n" +
                "    \"transactionType\": \"" + 13 + "\",\n" +
                "    \"deviceID\": \"" + SharedPrefs.read(SharedPrefs.DEVICE_ID, null) + "\",\n" +
                "    \"agentCode\": \"" + SharedPrefs.read(SharedPrefs.AGENT_CODE, null) + "\",\n" +
                "    \"duration\": \"" + duration.getText().toString() + "\",\n" +
                "    \"frequency\": \"" + frequency.getText().toString() + "\",\n" +
                "    \"startDate\": \"" + startDate.getText().toString() + "\",\n" +
                "    \"recovery\": \"" + SharedPrefs.read(SharedPrefs.RECOVERY_CODE, null) + "\",\n" +
                "    \"sourceAccount\": \"" + SharedPrefs.read(SharedPrefs.TELLER_ACCOUNT_NUMBER, null) + "\",\n" +
                "    \"destinationAccount\": \"" + SharedPrefs.read(SharedPrefs.STO_DESTINATION_AC, null) + "\",\n" +
                "    \"stoTpe\": \"" + SharedPrefs.read(SharedPrefs.STO_TYPE_CODE, null) + "\",\n" +
                "    \"amount\": \"" + amount.getText().toString() + "\",\n" +
                "    \"bank\": \"" + SharedPrefs.read(SharedPrefs.STO_BANK_NUMBER, null) + "\",\n" +
                "    \"loanNo\": \"" + SharedPrefs.read(SharedPrefs.STO_LOAN_NUMBER, null) + "\"\n" +
                "  }\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));
                        progressDialog.dismiss();
                        String success = jsonObject.getString("message");
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(StandingOrder.this);
                        builder1.setMessage(success);
                        builder1.setTitle("Success!");
                        builder1.setCancelable(true);

                        builder1.setPositiveButton(
                                "DONE",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        onBackPressed();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                        Toast.makeText(StandingOrder.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                        /*Utils.showAlertDialog(StandingOrder.this, "Cheque Deposited Successfully", " Press Ok to print another Receipt ", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                int result = PrinterInterface.open();
                                writetest1(Id, amou_nt);
                                PrinterInterface.close();
                                onBackPressed();
                            }
                        });*/

                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        progressDialog.dismiss();
                        String success = jsonObject.getString("message");
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(StandingOrder.this);
                        builder1.setMessage(success);
                        builder1.setTitle("Alert!");
                        builder1.setCancelable(true);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                        Toast.makeText(StandingOrder.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void requestAccounts(String id) {

        Api.getVolley(this, Api.GetMemberAccounts, "{\n" +
                "    \"identifier\": \"" + id + "\",\n" +
                "    \"identifierType\": \"" + 0 + "\",\n" +
                "    \"natureOfAccount\": \"" + 1 + "\"\n" +
                "  }\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                progressBar.setVisibility(View.GONE);
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));
                        arrayList.clear();
                        accountSpinner = findViewById(R.id.accountsSpinner);
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(StandingOrder.this, android.R.layout.simple_spinner_item, arrayList);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        accountSpinner.setAdapter(adapter);

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            String accountName = jsonObject1.getString("name");
                            String accountNo = jsonObject1.getString("no");
                            String desc = jsonObject1.getString("description");
                            String accountDetails = accountNo + "-" + desc;

                            SharedPrefs.write(SharedPrefs.ACC_NAME, accountName);

                            arrayList.add(accountDetails);

                            Log.e("ACCOUNT DETAILS", accountDetails);

                        }
                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(StandingOrder.this, "Could not fetch accounts! Please try again.", Toast.LENGTH_SHORT).show();
                    }

                    accountSpinner = findViewById(R.id.accountsSpinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(StandingOrder.this, android.R.layout.simple_spinner_item, arrayList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    accountSpinner.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void getLoans (String id) {

        Api.getVolley(this, Api.GetMemberLoans,"{\n" +
                "    \"identifier\": \"" + id + "\",\n" +
                "    \"identifierType\": \"" + 0 + "\"\n" +
                "  }\n" +
                "}",new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                loanPb.setVisibility(View.GONE);
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            String loan_no = jsonObject1.getString("no");
                            String loan_account = jsonObject1.getString("account");
                            String desc = jsonObject1.getString("description");
                            String accountDetails = loan_account + "-" + loan_no +"-" + desc;

                            //SharedPrefs.write(SharedPrefs.ACC_NAME, memberName);
                            loanList.add(accountDetails);

                            Log.e("LOAN DETAILS", accountDetails);

                        }
                    } if (jsonObject.getString("error").equalsIgnoreCase("true")){
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        loanPb.setVisibility(View.GONE);
                        Toast.makeText(StandingOrder.this, "Error!", Toast.LENGTH_SHORT).show();
                    }

                    loanSpinner = findViewById(R.id.loanSpinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(StandingOrder.this, android.R.layout.simple_spinner_item, loanList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    loanSpinner.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void getRecoveryCode () {

        Api.getVolley(this, Api.GetRecoveryModes,"{\n" +
                "}",new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                recoverPb.setVisibility(View.GONE);
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            String code = jsonObject1.getString("code");
                            String description = jsonObject1.getString("description");
                            String rec_details = code + "-" + description;

                            recoverList.add(rec_details);

                            Log.e("RECOVERY CODE", rec_details);

                        }

                    } if (jsonObject.getString("error").equalsIgnoreCase("true")){
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        recoverPb.setVisibility(View.GONE);
                        Toast.makeText(StandingOrder.this, "Could not fetch codes!", Toast.LENGTH_SHORT).show();
                    }

                    recoverySpinner = findViewById(R.id.recoverySpinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(StandingOrder.this, android.R.layout.simple_spinner_item, recoverList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    recoverySpinner.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void getBanks () {

        Api.getVolley(this, Api.GetBankBranches,"{\n" +
                "}",new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                bankPb.setVisibility(View.GONE);
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            String bankCode = jsonObject1.getString("code");
                            String bankName = jsonObject1.getString("name");
                            //String bankBal = jsonObject1.getString("balance");
                            String bankDetails = bankCode + "-" + bankName;

                            bankList.add(bankDetails);

                            Log.e("BANK DETAILS", bankDetails);

                        }
                    } if (jsonObject.getString("error").equalsIgnoreCase("true")){
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        bankPb.setVisibility(View.GONE);
                        Toast.makeText(StandingOrder.this, "Could not fetch bank!", Toast.LENGTH_SHORT).show();
                    }

                    bankSpinner = findViewById(R.id.banksSpinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(StandingOrder.this, android.R.layout.simple_spinner_item, bankList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    bankSpinner.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                //Toast.makeText(Account.this, "Back has been presses!", Toast.LENGTH_LONG).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }

}

package com.coretec.agencyteller.activities.treasury;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.coretec.agencyteller.R;
import com.coretec.agencyteller.utils.SharedPrefs;

public class TreasuryInfo extends AppCompatActivity {

    TextView agentName,  agentCode, agentNumber, agentBranch, accountBalance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPrefs.init(getApplicationContext());
        setContentView(R.layout.treasury_info);

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

        //Toolbar declarations...Reusable
        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView mTitle = toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getText(R.string.float_balance));
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);

        agentName = findViewById(R.id.agent_name);
        agentCode = findViewById(R.id.agent_account);
        agentNumber = findViewById(R.id.agent_number);
        agentBranch = findViewById(R.id.agent_branch);
        accountBalance = findViewById(R.id.agent_account_balance);

        agentName.setText(SharedPrefs.read(SharedPrefs.AGENT_NAME, null));
        agentCode.setText(SharedPrefs.read(SharedPrefs.AGENT_CODE, null));
        agentNumber.setText(SharedPrefs.read(SharedPrefs.TELLER_ACCOUNT_NUMBER, null));
        agentBranch.setText(SharedPrefs.read(SharedPrefs.AGENT_BRANCH, null));
        accountBalance.setText(SharedPrefs.read(SharedPrefs.ACCOUNT_BALANCE, null));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                //Toast.makeText(Account.this, "Back has been presses!", Toast.LENGTH_LONG).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }
}

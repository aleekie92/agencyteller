package com.coretec.agencyteller.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.coretec.agencyteller.R;
import com.coretec.agencyteller.utils.PreferenceFileKeys;


public class AgentInfomation extends AppCompatActivity {

    private TextView txt_agent_sacco_name;
    private TextView txt_agent_name;
    private TextView txt_agent_business_name;
    private TextView txt_agent_account_no;
    private TextView txt_agent_location;
    private TextView txt_agent_account_balance;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.agent_account_info);

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

        sharedPreferences = getSharedPreferences(PreferenceFileKeys.PREFS_FILE, Context.MODE_PRIVATE);

        txt_agent_sacco_name = findViewById(R.id.agent_sacco_name);
        txt_agent_name = findViewById(R.id.agent_name);
        txt_agent_business_name = findViewById(R.id.agent_business_name);
        txt_agent_account_no = findViewById(R.id.agent_account_no);
        txt_agent_location = findViewById(R.id.agent_location);
        txt_agent_account_balance = findViewById(R.id.agent_account_balance);

        final String saccoName = sharedPreferences.getString(PreferenceFileKeys.AGENT_SACCO_NAME, "");
        final String agentName = sharedPreferences.getString(PreferenceFileKeys.AGENT_ACCOUNT_NAME, "");
        final String agentBusinessName = sharedPreferences.getString(PreferenceFileKeys.AGENT_BUSINESS_NAME, "");
        final String agentAccountNo = sharedPreferences.getString(PreferenceFileKeys.AGENT_ACCOUNT_NO, "");
        final String agentLocation = sharedPreferences.getString(PreferenceFileKeys.AGENT_LOCATION, "");
        final String agentAccountBalance = sharedPreferences.getString(PreferenceFileKeys.AGENT_ACCOUNT_BALANCE, "");

        txt_agent_sacco_name.setText(saccoName);
        txt_agent_name.setText(agentName);
        txt_agent_business_name.setText(agentBusinessName);
        txt_agent_account_no.setText(agentAccountNo);
        txt_agent_location.setText(agentLocation);
        txt_agent_account_balance.setText(" KSH : " + agentAccountBalance);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }
}

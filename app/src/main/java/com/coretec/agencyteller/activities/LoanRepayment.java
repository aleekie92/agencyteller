package com.coretec.agencyteller.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyteller.R;
import com.coretec.agencyteller.api.ApiTest;
import com.coretec.agencyteller.api.requests.ActiveLoansRequest;
import com.coretec.agencyteller.api.requests.LoanRepaymentRequest;
import com.coretec.agencyteller.api.responses.ActiveLoansResponse;
import com.coretec.agencyteller.api.responses.LoanRepaymentResponse;
import com.coretec.agencyteller.model.Loans;
import com.coretec.agencyteller.utils.PreferenceFileKeys;
import com.coretec.agencyteller.utils.SharedPrefs;
import com.coretec.agencyteller.utils.Utils;
import com.coretec.agencyteller.wizarpos.function.printer.PrinterCommand;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import static com.coretec.agencyteller.utils.Utils.getFormattedDate;
import static com.coretec.agencyteller.utils.Utils.getLat;
import static com.coretec.agencyteller.utils.Utils.getLong;

/**
 * Created by kelvinoff on 17/10/05.
 */

public class LoanRepayment extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    private EditText loanIdentifier;
    private TextInputLayout input_layout_loan_identifier;
    private EditText txt_loan_amount;
    private TextInputLayout input_layout_loan_amount;
    private Spinner activeLoanSpinner;
    private Button btnConfirmLoanRepayment;
    private String identifier_number, payable_amount;
    private ArrayList<String> loansArrayList;
    private ArrayAdapter<String> adapter;
    private String loanAccountNo;
    private String splitedLoanAccNo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loan_repayment);

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

        sharedPreferences = getSharedPreferences(PreferenceFileKeys.PREFS_FILE, Context.MODE_PRIVATE);

        activeLoanSpinner = (Spinner) findViewById(R.id.loan_repayment_loans);
        adapter = new ArrayAdapter<String>(getApplicationContext(),
                R.layout.simple_spinner_dropdown_item, new ArrayList<String>());
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        activeLoanSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                loanAccountNo = (String) parent.getItemAtPosition(position);
                String[] separated = loanAccountNo.split(":");
                splitedLoanAccNo = separated[1];
                Toast.makeText(LoanRepayment.this, separated[1], Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        loanIdentifier = (EditText) findViewById(R.id.loan_repayment_identifier);
        txt_loan_amount = (EditText) findViewById(R.id.payable_txt_loan_amount);

        loansArrayList = new ArrayList<String>();

        input_layout_loan_identifier = (TextInputLayout) findViewById(R.id.input_layout_loan_repayment_identifier);
        input_layout_loan_amount = (TextInputLayout) findViewById(R.id.layout_amount_to_withdraw);

        TextWatcher textWatcher = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //here, after we introduced something in the EditText we get the string from it
                String loanidentifier = loanIdentifier.getText().toString();
                if (loanidentifier.length() >= 8) {
//                Toast.makeText(CashDeposit.this, accountNumber, Toast.LENGTH_SHORT).show();
                    requestActiveLoans(loanidentifier);
                }

            }
        };

        loanIdentifier.addTextChangedListener(textWatcher);

        btnConfirmLoanRepayment = (Button) findViewById(R.id.submitRepaymentRequest);
        btnConfirmLoanRepayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                identifier_number = loanIdentifier.getText().toString().trim();
                payable_amount = txt_loan_amount.getText().toString().trim();

                if (identifier_number.isEmpty()) {
                    input_layout_loan_identifier.setError("Enter the Member Account Number");
                } else if (payable_amount.isEmpty()) {
                    input_layout_loan_amount.setError("Enter the Amount to Pay Loan");
                } else {
                    Log.e("member_identifier", identifier_number);
                    Log.e("withdrawn amount", payable_amount);

                    requestLoanRepayment(identifier_number, Double.parseDouble(payable_amount));
                }
            }
        });

    }

    void requestActiveLoans(String identifier) {
        final String TAG = "MemberAccounts";

        Log.e(TAG, String.valueOf(identifier));

        String URL = ApiTest.MSACCO_AGENT + ApiTest.GetMemberActiveLoans;

        final ActiveLoansRequest accountDetails = new ActiveLoansRequest();

        //accountDetails.corporate_no = MainActivity.corporateno;
        accountDetails.accountidentifier = identifier;
        accountDetails.accountidentifiercode = "0";
        accountDetails.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
        accountDetails.terminalid = Utils.getIMEI(this);
        accountDetails.longitude = Utils.getLong(this);
        accountDetails.latitude = Utils.getLat(this);
        accountDetails.date = Utils.getFormattedDate();

        Log.e(TAG, accountDetails.getBody().toString());

        ApiTest.instance(this).request(URL, accountDetails, new ApiTest.RequestListener() {
            @Override
            public void onSuccess(String response) {
//                withdrawal_progressBar.setVisibility(View.GONE);

                ActiveLoansResponse activeLoansResponse = ApiTest.instance(LoanRepayment.this).mGson.fromJson(response, ActiveLoansResponse.class);

                if (activeLoansResponse.is_successful) {

                    for (Loans saccoAccounts : activeLoansResponse.getActive_loans()) {
                        loansArrayList.add(saccoAccounts.getLoantypename() + " : " + saccoAccounts.getLoan_no());
                    }

                    adapter.addAll(loansArrayList);
                    activeLoanSpinner.setAdapter(adapter);

                } else {
//                    Toast.makeText(LoanRepayment.this, "LoaRepayment Failed", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });


    }

    void requestLoanRepayment(String identifier, final double payable_amount) {
        final String TAG = "MemberAccounts";

        Log.e(TAG, String.valueOf(identifier));

        String URL = ApiTest.MSACCO_AGENT + ApiTest.LoanRepayment;

        final LoanRepaymentRequest loanRepaymentRequest = new LoanRepaymentRequest();

        //loanRepaymentRequest.corporate_no = MainActivity.corporateno;
        loanRepaymentRequest.accountidentifier = identifier;
        loanRepaymentRequest.accountidentifiercode = "0";
        loanRepaymentRequest.loan_no = splitedLoanAccNo;
        loanRepaymentRequest.repayment_amount = payable_amount;
        loanRepaymentRequest.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
        //loanRepaymentRequest.terminalid = MainActivity.imei;
        loanRepaymentRequest.longitude = getLong(this);
        loanRepaymentRequest.latitude = getLat(this);
        loanRepaymentRequest.date = getFormattedDate();

        Log.e(TAG, loanRepaymentRequest.getBody().toString());

        ApiTest.instance(this).request(URL, loanRepaymentRequest, new ApiTest.RequestListener() {
            @Override
            public void onSuccess(String response) {
//                withdrawal_progressBar.setVisibility(View.GONE);

                LoanRepaymentResponse loanRepaymentResponse = ApiTest.instance(LoanRepayment.this).mGson.fromJson(response, LoanRepaymentResponse.class);

                if (loanRepaymentResponse.is_successful) {

                    final String agentName = sharedPreferences.getString(PreferenceFileKeys.AGENT_ACCOUNT_NAME, "");

                    int result = PrinterInterface.open();
                    writetest(agentName, payable_amount, splitedLoanAccNo, loanRepaymentResponse.getSacconame(), loanRepaymentResponse.getReceiptno(), loanRepaymentResponse.getTransactiondate(), loanRepaymentResponse.getTransactiontype());
                    PrinterInterface.close();
                    Utils.showAlertDialog(LoanRepayment.this, "", "Loan Repayment Successful");
                } else {
                    Utils.showAlertDialog(LoanRepayment.this, "Loan Repayment Failed", loanRepaymentResponse.getError());
                }
            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });


    }


    public void writetest(String agentName, double loanAmt, String loanAccNo, String saccoName, String receiptNum, String transactionDate, String transactionType) {
        try {

            byte[] arryBeginText = null;
            byte[] arryAccType = null;
            byte[] arryAccNo = null;
            byte[] arryAmt = null;
            byte[] arryMotto = null;
            byte[] terminalNo = null;
            byte[] receiptNo = null;
            byte[] date = null;
            byte[] transactionTYpe = null;
            byte[] servedBy = null;

            try {
                arryBeginText = String.valueOf("     " + saccoName).getBytes("GB2312");
                terminalNo = String.valueOf(" Terminal ID" + Utils.getIMEI(LoanRepayment.this)).getBytes("GB2312");
                receiptNo = String.valueOf(" Receipt Number : " + receiptNum).getBytes("GB2312");
                date = String.valueOf(" Date : " + transactionDate).getBytes("GB2312");
                transactionTYpe = String.valueOf("    " + transactionType).getBytes("GB2312");
                arryAccType = "Your Loan Has been successfully repaid ".getBytes("GB2312");
                arryAccNo = String.valueOf(" Loan Account No : " + loanAccNo).getBytes("GB2312");
                arryAmt = String.valueOf("Loan Amount Paid : KSH" + loanAmt).getBytes("GB2312");
                servedBy = String.valueOf(" You were Served By : " + agentName).getBytes("GB2312");
                arryMotto = String.valueOf("            "+sharedPreferences.getString(PreferenceFileKeys.AGENT_SACCO_MOTTO, "")).getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(arryBeginText);
            writeLineBreak(2);
            write(terminalNo);
            writeLineBreak(2);
            write(receiptNo);
            writeLineBreak(2);
            write(date);
            writeLineBreak(2);
            write(transactionTYpe);
            // print line break
            writeLineBreak(4);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // print line break
            writeLineBreak(2);
            // print text
            write(arryAccType);
            write(arryAccNo);
            writeLineBreak(2);
            write(arryAmt);
            // print line break
            writeLineBreak(2);
            write(servedBy);
            writeLineBreak(6);
            write(arryMotto);
            writeLineBreak(5);
            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void write(final byte[] arryData) {
        int result = PrinterInterface.write(arryData, arryData.length);
    }

    // print line break
    private void writeLineBreak(int lineNumber) {
        // write(new byte[]{'\n'});
        write(PrinterCommand.getCmdEscDN(lineNumber));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }
}

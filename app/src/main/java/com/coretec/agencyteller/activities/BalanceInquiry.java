package com.coretec.agencyteller.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyteller.R;
import com.coretec.agencyteller.api.ApiTest;
import com.coretec.agencyteller.api.requests.AccountDetailsRequest;
import com.coretec.agencyteller.api.requests.GetBalanceRequest;
import com.coretec.agencyteller.api.responses.AccountDetailsResponse;
import com.coretec.agencyteller.api.responses.BalanceResponse;
import com.coretec.agencyteller.api.responses.SaccoAccounts;
import com.coretec.agencyteller.model.AccountDetails;
import com.coretec.agencyteller.utils.PreferenceFileKeys;
import com.coretec.agencyteller.utils.SharedPrefs;
import com.coretec.agencyteller.utils.Utils;
import com.coretec.agencyteller.wizarpos.function.ActionCallbackImpl;
import com.coretec.agencyteller.wizarpos.function.printer.PrinterCommand;
import com.coretec.agencyteller.wizarpos.mvc.base.ActionCallback;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import static com.coretec.agencyteller.utils.Utils.getFormattedDate;
import static com.coretec.agencyteller.utils.Utils.getLat;
import static com.coretec.agencyteller.utils.Utils.getLong;

/**
 * Created by kelvinoff on 17/11/01.
 */

public class BalanceInquiry extends AppCompatActivity {

    public static Handler handler;
    public static ActionCallback callback;
    private Button btnConfirm;
    private ProgressBar bal_progressBar;
    private TextInputLayout input_layout_bal_phone_number;
    private EditText txt_bal_phone_number;
    private String phone_number;
    private String identifierCode;
    private Spinner spinnerBalanceIdentifier;
    private ArrayAdapter<String> balanceIdentifierAdapter;
    private SharedPreferences sharedPreferences;

    private String accountNo = "";

    private Spinner spinner2;
    private ArrayList<String> arrayList2 = new ArrayList<>();
    private ArrayAdapter<AccountDetails> adapter2;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_balance_inquiry);

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

        sharedPreferences = getSharedPreferences(PreferenceFileKeys.PREFS_FILE, Context.MODE_PRIVATE);

        handler = new Handler();
        callback = new ActionCallbackImpl(handler);


        bal_progressBar = (ProgressBar) findViewById(R.id.bal_progressbar);
        bal_progressBar.setVisibility(View.GONE);

        txt_bal_phone_number = (EditText) findViewById(R.id.bal_identifier);
        input_layout_bal_phone_number = (TextInputLayout) findViewById(R.id.input_layout_bal_identifier);

        TextWatcher textWatcher = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //here, after we introduced something in the EditText we get the string from it
                String identifier_number = txt_bal_phone_number.getText().toString();
                if (identifier_number.length() >= 7) {
                    requestGetAccountDetails(identifier_number);
                }
//                Toast.makeText(CashDeposit.this, accountNumber, Toast.LENGTH_SHORT).show();
//                requestAccountName(accountNumber);

            }
        };

        txt_bal_phone_number.addTextChangedListener(textWatcher);

        spinnerBalanceIdentifier = (Spinner) findViewById(R.id.spinnerBalanceIdentifier);
        balanceIdentifierAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.simple_spinner_dropdown_item, arrayList2);
        balanceIdentifierAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinnerBalanceIdentifier.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                identifierCode = String.valueOf(position);
//                Toast.makeText(getApplicationContext(), String.valueOf(position), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner2 = (Spinner) findViewById(R.id.deposit_accounts);
        adapter2 = new ArrayAdapter<AccountDetails>(getApplicationContext(),
                R.layout.simple_spinner_dropdown_item, new ArrayList<AccountDetails>());
        adapter2.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                AccountDetails accountDetails = (AccountDetails) parent.getItemAtPosition(position);
                accountNo = accountDetails.getAccountno();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        btnConfirm = (Button) findViewById(R.id.submitBalanceInquiry);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                phone_number = txt_bal_phone_number.getText().toString().trim();

                if (phone_number.isEmpty()) {
                    input_layout_bal_phone_number.setError("Enter your Agent Phone Number ");
                } else {
                    Log.e("phone number", phone_number);
                    bal_progressBar.setVisibility(View.VISIBLE);

                    requestBalance(phone_number, accountNo);
                    btnConfirm.setActivated(false);
                    // memberAuthDialog();
                }
            }
        });

    }

    void requestGetAccountDetails(String identifier) {

        final String TAG = "MemberAccounts";
//        EasyLocationMod easyLocationMod = new EasyLocationMod(CashWithdrawal.this);

        String URL = ApiTest.MSACCO_AGENT + ApiTest.GetAllMemberSaccoDetails;

        final AccountDetailsRequest accountDetails = new AccountDetailsRequest();
        accountDetails.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
        //accountDetails.corporateno = MainActivity.corporateno;
        accountDetails.accountidentifier = identifier;
        accountDetails.accountidentifiercode = identifierCode;
        accountDetails.transactiontype = "A";
        //accountDetails.terminalid = MainActivity.imei;
        accountDetails.longitude = getLong(this);
        accountDetails.latitude = getLat(this);
        accountDetails.date = getFormattedDate();

        Log.e(TAG, accountDetails.getBody().toString());

        ApiTest.instance(this).request(URL, accountDetails, new ApiTest.RequestListener() {
            @Override
            public void onSuccess(String response) {
                bal_progressBar.setVisibility(View.GONE);

                AccountDetailsResponse accountDetailsResponse = ApiTest.instance(BalanceInquiry.this)
                        .mGson.fromJson(response, AccountDetailsResponse.class);
                if (accountDetailsResponse.is_successful) {
                    List<AccountDetails> accountDetailsList = new ArrayList<>();
                    for (SaccoAccounts saccoAccounts : accountDetailsResponse.getSaccoAccounts()) {
                        accountDetailsList.addAll(saccoAccounts.getAccountDetails());
                    }
                    adapter2.addAll(accountDetailsList);
                    spinner2.setAdapter(adapter2);

                } else {
//                    Toast.makeText(CashDeposit.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }


    void requestBalance(String identifier, String accountno) {
        final String TAG = "request balance";
        Log.e(TAG, identifier);

        String URL = ApiTest.MSACCO_AGENT + ApiTest.GetBalanceInquiry;

        GetBalanceRequest balrequest = new GetBalanceRequest();
        balrequest.accountidentifier = identifier;
        balrequest.accountidentifiercode = identifierCode;
        //balrequest.corporate_no = MainActivity.corporateno;
        balrequest.accountno = accountno;
        balrequest.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
       // balrequest.terminalid = MainActivity.imei;
        balrequest.longitude = getLong(this);
        balrequest.latitude = getLat(this);
        balrequest.date = getFormattedDate();

        Log.e(TAG, balrequest.getBody().toString());

        ApiTest.instance(this).request(URL, balrequest, new ApiTest.RequestListener() {
            @Override
            public void onSuccess(String response) {
                Log.e(TAG, response);
                displayBalance(response);


            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });

    }

    void displayBalance(String response) {


        final BalanceResponse balanceResponse = ApiTest.instance(this).mGson.fromJson(response, BalanceResponse.class);
        final double amount = balanceResponse.getBalance().getAmount();
        final String accountNo = balanceResponse.getBalance().getAccountno();
        final String accountName = balanceResponse.getBalance().getAccount();
        String TAG = "Display Balance " + balanceResponse.is_successful;
        if (balanceResponse.is_successful) {
            bal_progressBar.setVisibility(View.GONE);

            final String agentName = sharedPreferences.getString(PreferenceFileKeys.AGENT_ACCOUNT_NAME, "");

            int result = PrinterInterface.open();
            writetest(agentName, amount, accountNo, accountName, balanceResponse.getSacconame(), balanceResponse.getCustomername(), balanceResponse.getReceiptno(), balanceResponse.getTransactiondate(), balanceResponse.getTransactiontype());
            PrinterInterface.close();
            Utils.showAlertDialog(BalanceInquiry.this, "Balance Successful", "Balance successfully Retrieved");

        } else {
            Utils.showAlertDialog(BalanceInquiry.this, "Balance Retrieval Failed", balanceResponse.getError());
        }
    }

/*    void receiptBalanceDialog(BalanceResponse balanceResponse) {
        Intent intent = new Intent(this, BalanceReceipt.class);
        intent.putExtra(BalanceReceipt.BALANCE_RESPONSE_EXTRA, balanceResponse);
        startActivity(intent);
    }*/

    public void writetest(String agentName, double amount, String accountNo, String accountName, String saccoame, String customerName, String receiptNumber, String dateed, String transactionType) {
        try {

            byte[] arryBeginText = null;

            byte[] arryAmt = null;
            byte[] arryAccType = null;
            byte[] arryAccNo = null;
            byte[] memberName = null;
            byte[] arryMotto = null;
            byte[] terminalNo = null;
            byte[] receiptNo = null;
            byte[] date = null;
            byte[] transactionTYpe = null;
            byte[] servedBy = null;
            byte[] signature = null;

            try {
                arryBeginText = String.valueOf("       " + saccoame).getBytes("GB2312");
                terminalNo = String.valueOf(" Terminal ID :" + Utils.getIMEI(BalanceInquiry.this)).getBytes("GB2312");
                receiptNo = String.valueOf(" Receipt Number : " + receiptNumber).getBytes("GB2312");
                date = String.valueOf(" Date : " + dateed).getBytes("GB2312");
                memberName = String.valueOf(" Name : " + customerName).getBytes("GB2312");
                transactionTYpe = String.valueOf("       " + transactionType).getBytes("GB2312");
                arryAccType = String.valueOf(" Acc Type " + accountNo).getBytes("GB2312");
                arryAccNo = String.valueOf(" Acc No " + accountName).getBytes("GB2312");
                arryAmt = String.valueOf(" Total Balance is : " + "KSH " + amount).getBytes("GB2312");
                servedBy = String.valueOf(" You were Served By " + agentName).getBytes("GB2312");
                signature = String.valueOf("SIGNATURE___________________ ").getBytes("GB2312");
                arryMotto = String.valueOf("            "+sharedPreferences.getString(PreferenceFileKeys.AGENT_SACCO_MOTTO, "")).getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(arryBeginText);
            writeLineBreak(2);
            write(terminalNo);
            writeLineBreak(2);
            write(receiptNo);
            writeLineBreak(2);
            write(date);
            writeLineBreak(2);
            write(memberName);
            writeLineBreak(2);
            write(transactionTYpe);
            // print line break
            writeLineBreak(4);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // print line break
            writeLineBreak(2);
            // print text
//            write(arryAccType);
            write(arryAmt);
            writeLineBreak(2);
            write(arryAccType);
            writeLineBreak(2);
            write(arryAccNo);
            writeLineBreak(2);
            write(signature);
            writeLineBreak(2);
            write(servedBy);
            writeLineBreak(6);
            write(arryMotto);
            writeLineBreak(5);
            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void write(final byte[] arryData) {
        int result = PrinterInterface.write(arryData, arryData.length);
    }

    // print line break
    private void writeLineBreak(int lineNumber) {
        // write(new byte[]{'\n'});
        write(PrinterCommand.getCmdEscDN(lineNumber));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }
}

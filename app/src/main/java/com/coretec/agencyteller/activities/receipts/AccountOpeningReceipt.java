package com.coretec.agencyteller.activities.receipts;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.TextView;

import com.coretec.agencyteller.R;
import com.coretec.agencyteller.api.responses.AccountOpeningResponse;
import com.coretec.agencyteller.wizarpos.function.ActionCallbackImpl;
import com.coretec.agencyteller.wizarpos.mvc.base.ActionCallback;
import com.coretec.agencyteller.wizarpos.mvc.base.ActionManager;

import java.util.HashMap;
import java.util.Map;

public class AccountOpeningReceipt extends AppCompatActivity {
    public static final String ACCOUNT_RESPONSE_EXTRA = "ACCOUNT_RESPONSE_EXTRA";
    private TextView account_opening_transaction_date, account_opening_terminal_id, account_opening_receipt_number;
    private TextView account_opening_sacco_name;
    private AppCompatButton printAccOpeningReceipt;
    public static Handler handler;
    public static ActionCallback callback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.account_opening_receipt);


        account_opening_transaction_date = (TextView) findViewById(R.id.account_opening_transaction_date);
        account_opening_terminal_id = (TextView) findViewById(R.id.account_opening_terminal_id);
        account_opening_receipt_number = (TextView) findViewById(R.id.account_opening_receipt_number);
        account_opening_sacco_name = (TextView) findViewById(R.id.account_opening_sacco_name);


        handler = new Handler();
        callback = new ActionCallbackImpl(handler);

        AccountOpeningResponse accountOpeningResponse = (AccountOpeningResponse) getIntent().getSerializableExtra(ACCOUNT_RESPONSE_EXTRA);

        account_opening_transaction_date.setText(accountOpeningResponse.getTransactiondate());
        account_opening_terminal_id.setText("990008670366801");
        account_opening_receipt_number.setText(accountOpeningResponse.getReceiptno());
        account_opening_sacco_name.setText(accountOpeningResponse.getSacconame());

        final Map<String, Object> params = new HashMap<>();
        params.put("account_opening", accountOpeningResponse);


        printAccOpeningReceipt = (AppCompatButton) findViewById(R.id.btn_print_account_opening_receipt);
        printAccOpeningReceipt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ActionManager.doSubmit("AccountOpeningPrinterAction/open", AccountOpeningReceipt.this, params, callback);
                ActionManager.doSubmit("AccountOpeningPrinterAction/write", AccountOpeningReceipt.this, params, callback);
                ActionManager.doSubmit("AccountOpeningPrinterAction/close", AccountOpeningReceipt.this, params, callback);
            }
        });

    }
}

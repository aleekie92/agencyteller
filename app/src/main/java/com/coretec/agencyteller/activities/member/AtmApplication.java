package com.coretec.agencyteller.activities.member;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.coretec.agencyteller.R;
import com.coretec.agencyteller.api.Api;
import com.coretec.agencyteller.utils.SharedPrefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AtmApplication extends AppCompatActivity {

    private EditText limit;
    private EditText identifier;
    private Spinner identifierSpinner;
    private Spinner accountSpinner;
    private ArrayList<String> arrayList = new ArrayList<>();

    private Spinner cardTypeSpinner;
    private Spinner applicationType;
    private ArrayList<String> cardList = new ArrayList<>();
    private Button submit;
    private Button search;

    private ProgressBar progressBar, cardTypePb;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPrefs.init(getApplicationContext());
        setContentView(R.layout.atm_application);

        //Toolbar declarations...Reusable
        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView mTitle = toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getText(R.string.atm_app));
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);

        identifierSpinner = findViewById(R.id.identifier_spinner);
        accountSpinner = findViewById(R.id.accountsSpinner);
        applicationType = findViewById(R.id.appTypeSpinner);
        cardTypeSpinner = findViewById(R.id.cardType);
        identifier = findViewById(R.id.identifier);
        progressBar = findViewById(R.id.accountsPB);
        cardTypePb = findViewById(R.id.cardTypePB);
        search = findViewById(R.id.btn_search);
        submit = findViewById(R.id.button_submit);
        limit = findViewById(R.id.et_limit);

        getCardTypes();

        accountSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //((TextView) parent.getChildAt(0)).setTextColor(Color.GRAY);
                String item = parent.getItemAtPosition(position).toString();
                String text = accountSpinner.getSelectedItem().toString();

                String account_number = text.substring(0, text.indexOf("-"));
                SharedPrefs.write(SharedPrefs.MEMBER_ACCOUNT_NUMBER, account_number);
                Log.e("DESTINATION ACCOUNT", account_number);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        applicationType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //((TextView) parent.getChildAt(0)).setTextColor(Color.GRAY);
                String item = parent.getItemAtPosition(position).toString();
                String text = applicationType.getSelectedItem().toString();

                String app_type = text.substring(0, text.indexOf("-"));
                SharedPrefs.write(SharedPrefs.APPLICATION_TYPE, app_type);
                Log.e("APPLICATION TYPE", app_type);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        cardTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //((TextView) parent.getChildAt(0)).setTextColor(Color.GRAY);
                String item = parent.getItemAtPosition(position).toString();
                String text = cardTypeSpinner.getSelectedItem().toString();

                String card_type = text.substring(0, text.indexOf("-"));
                SharedPrefs.write(SharedPrefs.CARD_TYPE, card_type);
                Log.e("CARD TYPE", card_type);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        identifierSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                String item = parent.getItemAtPosition(position).toString();
                String text = identifierSpinner.getSelectedItem().toString();

                int idLength = 8;
                int acLength = 8;
                int memLength = 7;
                InputFilter[] fArray = new InputFilter[1];
                fArray[0] = new InputFilter.LengthFilter(idLength);

                InputFilter[] aArray = new InputFilter[1];
                aArray[0] = new InputFilter.LengthFilter(acLength);

                InputFilter[] mArray = new InputFilter[1];
                mArray[0] = new InputFilter.LengthFilter(memLength);

                if (text.equals("ID NUMBER")) {
                    SharedPrefs.write(SharedPrefs.IDENTIFIER, "0");
                    identifier.setText(null);
                    identifier.setInputType(InputType.TYPE_CLASS_NUMBER);
                    identifier.setFilters(fArray);
                } else if (text.equals("OLD A/C NUMBER")) {
                    SharedPrefs.write(SharedPrefs.IDENTIFIER, "4");
                    identifier.setText(null);
                    identifier.setInputType(InputType.TYPE_CLASS_TEXT);
                    identifier.setFilters(aArray);
                } else if (text.equals("MEMBER NUMBER")) {
                    identifier.setText(null);
                    identifier.setInputType(InputType.TYPE_CLASS_NUMBER);
                    SharedPrefs.write(SharedPrefs.IDENTIFIER, "1");
                    identifier.setFilters(mArray);
                }

                Log.e("SELECTED", text);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = identifier.getText().toString();

                if (id.length()<7 && id.length()>0) {
                    identifier.setError("Please input a valid identifier!");
                } else if (id.length()==0){
                    identifier.setError("Cannot search an empty field!");
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(AtmApplication.this, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                    requestAccounts(id);
                }
            }
        });

        TextWatcher textWatcher = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String idNumber = identifier.getText().toString();

                /*if (idNumber.length() >= 8) {
                    requestAccounts(idNumber);
                }*/

                if (idNumber.length() >= 5 && idNumber.length() <= 8) {

                    arrayList.clear();
                    accountSpinner = findViewById(R.id.accountsSpinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(AtmApplication.this, android.R.layout.simple_spinner_item, arrayList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    accountSpinner.setAdapter(adapter);

                }
                if (idNumber.length() < 5) {
                    //submitAccRegistrationRequest.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                }

            }
        };
        identifier.addTextChangedListener(textWatcher);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String idNo = identifier.getText().toString();
                String en_limit = limit.getText().toString();

                if (idNo.isEmpty()) {
                    identifier.setError("Please input ID number!");
                } else if (en_limit.isEmpty()) {
                    limit.setError("Please enter the limit!");
                } else {
                    progressDialog = new ProgressDialog(AtmApplication.this);
                    progressDialog.setMessage("Submitting your request...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    requestAtmApplication(limit.getText().toString());
                }
            }
        });

    }

    private void requestAccounts (String id) {

        int identifier = Integer.parseInt(SharedPrefs.read(SharedPrefs.IDENTIFIER, null));

        Api.getVolley(this, Api.GetMemberAccounts,"{\n" +
                "    \"identifier\": \"" + id + "\",\n" +
                "    \"identifierType\": \"" + identifier + "\",\n" +
                "    \"natureOfAccount\": \"" + 1 + "\"\n" +
                "  }\n" +
                "}",new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                progressBar.setVisibility(View.GONE);
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));
                        arrayList.clear();
                        accountSpinner = findViewById(R.id.accountsSpinner);
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(AtmApplication.this, android.R.layout.simple_spinner_item, arrayList);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        accountSpinner.setAdapter(adapter);

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            String accountName = jsonObject1.getString("name");
                            String accountNo = jsonObject1.getString("no");
                            String desc = jsonObject1.getString("description");
                            String accountDetails = accountNo + "-" + desc;

                            SharedPrefs.write(SharedPrefs.ACC_NAME, accountName);

                            arrayList.add(accountDetails);

                            Log.e("ACCOUNT DETAILS", accountDetails);

                        }
                    } if (jsonObject.getString("error").equalsIgnoreCase("true")){
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(AtmApplication.this, "Could not fetch accounts! Please try again.", Toast.LENGTH_SHORT).show();
                    }

                    accountSpinner = findViewById(R.id.accountsSpinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(AtmApplication.this, android.R.layout.simple_spinner_item, arrayList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    accountSpinner.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void getCardTypes () {

        Api.getVolley(this, Api.GetATMCardTypes,"{\n" +
                "  }\n" +
                "}",new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                cardTypePb.setVisibility(View.GONE);
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            String cardCode = jsonObject1.getString("code");
                            String cardDescription = jsonObject1.getString("description");
                            String cardDetails = cardCode + "-" + cardDescription;

                            cardList.add(cardDetails);

                            Log.e("CHEQUE DETAILS", cardDetails);

                        }
                    } if (jsonObject.getString("error").equalsIgnoreCase("true")){
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        cardTypePb.setVisibility(View.GONE);
                        Toast.makeText(AtmApplication.this, "Could not fetch card types!", Toast.LENGTH_SHORT).show();
                    }

                    cardTypeSpinner = findViewById(R.id.cardType);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(AtmApplication.this, android.R.layout.simple_spinner_item, cardList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    cardTypeSpinner.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    void requestAtmApplication (String amt_limit) {

        Api.getVolley(this, Api.AtmApplicationTransaction, "{\n" +
                "    \"action\": \"" + 0 + "\",\n" +
                "    \"transactionType\": \"" + 11 + "\",\n" +
                "    \"deviceID\": \"" + SharedPrefs.read(SharedPrefs.DEVICE_ID, null) + "\",\n" +
                "    \"agentCode\": \"" + SharedPrefs.read(SharedPrefs.AGENT_CODE, null) + "\",\n" +
                "    \"account\": \"" + SharedPrefs.read(SharedPrefs.MEMBER_ACCOUNT_NUMBER, null) + "\",\n" +
                "    \"applicationType\": \"" + SharedPrefs.read(SharedPrefs.APPLICATION_TYPE, null) + "\",\n" +
                "    \"cardType\": \"" + SharedPrefs.read(SharedPrefs.CARD_TYPE, null) + "\",\n" +
                "    \"limit\": \"" + amt_limit + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));
                        progressDialog.dismiss();

                        String success = jsonObject.getString("message");
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(AtmApplication.this);
                        builder1.setMessage(success);
                        builder1.setTitle("Success!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "DONE",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        onBackPressed();
                                    }
                                });

                        /*builder1.setNegativeButton(
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });*/

                        AlertDialog alert11 = builder1.create();
                        alert11.show();

                        Toast.makeText(AtmApplication.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                        /*int result = PrinterInterface.open();
                        writetest(Id, amou_nt);
                        PrinterInterface.close();
                        progressDialog.dismiss();*/

                        /*Utils.showAlertDialog(MsaccoApplication.this, "Cash Withdrawn Successfully", " Press Ok to print another Receipt ", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                int result = PrinterInterface.open();
                                writetest1(Id, amou_nt);
                                PrinterInterface.close();
                                onBackPressed();
                            }
                        });*/

                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        progressDialog.dismiss();
                        String success = jsonObject.getString("message");
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(AtmApplication.this);
                        builder1.setMessage(success);
                        builder1.setTitle("Alert!");
                        builder1.setCancelable(true);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                        Toast.makeText(AtmApplication.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                //Toast.makeText(Account.this, "Back has been presses!", Toast.LENGTH_LONG).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }

}

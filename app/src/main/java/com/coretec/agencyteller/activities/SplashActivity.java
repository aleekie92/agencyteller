package com.coretec.agencyteller.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.coretec.agencyteller.R;
import com.coretec.agencyteller.api.Server;
import com.coretec.agencyteller.api.responses.GenerateTokenResponse;
import com.coretec.agencyteller.utils.Const;
import com.coretec.agencyteller.utils.PreferenceFileKeys;
import com.coretec.agencyteller.utils.SharedPrefs;

import java.lang.reflect.Method;
import java.util.UUID;

import static com.coretec.agencyteller.utils.SharedPrefs.PREF_UNIQUE_ID;

public class SplashActivity extends AppCompatActivity {

    private View mContentView;

    static Context ctx;

    SharedPreferences mSharedPreferences;

    private int SLEEP_TIMER = 2;

    @SuppressLint("MissingPermission")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPrefs.init(getApplicationContext());
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash);

        final TelephonyManager tm = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);

        String serialnum = null;

        try {
            Class<?> c = Class.forName("android.os.SystemProperties");
            Method get = c.getMethod("get", String.class, String.class );
            serialnum = (String)(   get.invoke(c, "ro.serialno", "unknown" )  );
            SharedPrefs.write(SharedPrefs.DEVICE_ID, serialnum);
            Log.e("SERIAL NO", serialnum);
        }
        catch (Exception ignored)
        {
            Log.e("ERROR!", String.valueOf(ignored));
        }

        final String tmDevice, tmSerial, androidId;
        tmDevice = "" + tm.getDeviceId();
        tmSerial = "" + tm.getSimSerialNumber();
        androidId = "" + Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        UUID deviceUuid = new UUID(androidId.hashCode(), ((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
        String deviceId = deviceUuid.toString();
        Log.e("DEVICE ID", tmDevice);
        Log.e("SIM SERIAL", tmSerial);
        Log.e("ANDROID ID", androidId);

        SplashLauncher splashLauncher = new SplashLauncher();
        splashLauncher.start();

    }

    private class SplashLauncher extends Thread {
        public void run() {
            try {
                sleep(1000 * SLEEP_TIMER);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
            startActivity(intent);
            SplashActivity.this.finish();

        }
    }

    /*@Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }*/
}

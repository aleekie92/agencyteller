package com.coretec.agencyteller.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyteller.R;
import com.coretec.agencyteller.adapters.ReportsAdapter;
import com.coretec.agencyteller.api.ApiTest;
import com.coretec.agencyteller.api.requests.EodReportsRequest;
import com.coretec.agencyteller.api.responses.Accounts;
import com.coretec.agencyteller.api.responses.EodReportsResponse;
import com.coretec.agencyteller.utils.PreferenceFileKeys;
import com.coretec.agencyteller.utils.Utils;
import com.coretec.agencyteller.wizarpos.function.printer.PrinterCommand;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.coretec.agencyteller.utils.Utils.getFormattedDate;
import static com.coretec.agencyteller.utils.Utils.getLat;
import static com.coretec.agencyteller.utils.Utils.getLong;

/**
 * Created by kelvinoff on 17/10/05.
 */

public class Reports extends AppCompatActivity {

    private List<Accounts> accountsList;
    private RecyclerView reports_recyclerView;
    private ReportsAdapter reportsAdapter;
    private Button btnPrint;
    SharedPreferences sharedPreferences;
    private TextView reports_agent_name;
    private TextView reports_sacco_name;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.eod_report);

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

        accountsList = new ArrayList<>();
        sharedPreferences = getSharedPreferences(PreferenceFileKeys.PREFS_FILE, Context.MODE_PRIVATE);

        String agentName = sharedPreferences.getString(PreferenceFileKeys.AGENT_ACCOUNT_NAME, "");
        String saccoName = sharedPreferences.getString(PreferenceFileKeys.AGENT_SACCO_NAME, "");

        reports_agent_name = (TextView) findViewById(R.id.reports_agent_name);
        reports_agent_name.setText(agentName);

        reports_sacco_name = (TextView) findViewById(R.id.reports_sacco_name);
        reports_sacco_name.setText(saccoName);

        reports_recyclerView = (RecyclerView) findViewById(R.id.eodRecyclerView);
        reports_recyclerView.setHasFixedSize(true);
        reportsAdapter = new ReportsAdapter(this, accountsList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        reports_recyclerView.setLayoutManager(mLayoutManager);

        final Map<String, Object> params = new HashMap<>();
        params.put("accountsList", accountsList);


        final String TAG = "request reports";

        String URL = ApiTest.MSACCO_AGENT + ApiTest.GetEODReport;

        EodReportsRequest request = new EodReportsRequest();
        //request.corporateno = MainActivity.corporateno;
        request.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
        //request.terminalid = MainActivity.imei;
        request.longitude = getLong(this);
        request.latitude = getLat(this);
        request.requestdate = getFormattedDate();


        Log.e(TAG, request.getBody().toString());

        ApiTest.instance(this).request(URL, request, new ApiTest.RequestListener() {
            @Override
            public void onSuccess(String response) {
                Log.e(TAG, response);

                final EodReportsResponse eodReportsResponse = ApiTest.instance(Reports.this).mGson.fromJson(response, EodReportsResponse.class);


                if (eodReportsResponse.is_successful) {
                    accountsList.addAll(eodReportsResponse.getAccounts());
                    reports_recyclerView.setAdapter(reportsAdapter);

                    btnPrint = (Button) findViewById(R.id.btn_print_reports_receipt);
                    btnPrint.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            final String agentName = sharedPreferences.getString(PreferenceFileKeys.AGENT_ACCOUNT_NAME, "");
                            final String agentSaccoName = sharedPreferences.getString(PreferenceFileKeys.AGENT_SACCO_NAME, "");
                            final String agentBusinessName = sharedPreferences.getString(PreferenceFileKeys.AGENT_BUSINESS_NAME, "");
                            int result = PrinterInterface.open();
                            writetest(agentName, eodReportsResponse.getAccounts(), agentSaccoName, eodReportsResponse.getTransactiondate(), "REPORTS");
                            PrinterInterface.close();
                        }
                    });


                } else {
                    Utils.showAlertDialog(Reports.this, "Reports Retrieval Failed", eodReportsResponse.getError());
                }
            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });


        btnPrint = (Button) findViewById(R.id.btn_print_reports_receipt);
        btnPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }


    public void writetest(String agentName, List<Accounts> accountsList, String saccoame, String dateed, String transactionType) {
        try {

            byte[] arrySaccoName = null;
            byte[] arryDepositAmt = null;
            byte[] arryRefID = null;
            byte[] arryAmount = null;
            byte[] arryTransactionType = null;

            byte[] arryMotto = null;
            byte[] terminalNo = null;
            byte[] date = null;
            byte[] transactionTYpe = null;
            byte[] servedBy = null;
            byte[] signature = null;
            byte[] id = null;


            try {
                arrySaccoName = String.valueOf("       " + String.valueOf(saccoame)).getBytes("GB2312");
                terminalNo = String.valueOf(" Terminal ID :" + Utils.getIMEI(Reports.this)).getBytes("GB2312");

                date = String.valueOf("Date : " + dateed).getBytes("GB2312");

                transactionTYpe = String.valueOf("      " + transactionType).getBytes("GB2312");
                arryDepositAmt = String.valueOf("Ref No     Amount     Type  ").getBytes("GB2312");


                id = String.valueOf("ID :_________________________").getBytes("GB2312");
                signature = String.valueOf("SIGNATURE : _____________________").getBytes("GB2312");
                servedBy = String.valueOf(" You were Served By : " + agentName).getBytes("GB2312");
                arryMotto = String.valueOf("            "+sharedPreferences.getString(PreferenceFileKeys.AGENT_SACCO_MOTTO, "")).getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};


            write(cmd);
            write(arrySaccoName);
            writeLineBreak(2);
            write(terminalNo);
            writeLineBreak(2);
            write(date);
            writeLineBreak(2);
            write(transactionTYpe);
            writeLineBreak(2);
            write(arryDepositAmt);

            writeLineBreak(4);

            for (int i = 0; i < accountsList.size(); i++) {
                arryRefID = String.valueOf(accountsList.get(i).getReferenceno()).getBytes("GB2312");
                write(arryRefID);

                arryAmount = String.valueOf("  " + accountsList.get(i).getAmount()).getBytes("GB2312");
                write(arryAmount);

                arryTransactionType = String.valueOf("  " + accountsList.get(i).getTransactiontype()).getBytes("GB2312");
                write(arryTransactionType);
                writeLineBreak(2);

            }


//            for (Accounts accounts : accountsList) {
//
//                write(accounts.getReferenceno().getBytes("GB2312"));
//                writeLineBreak(2);
//                write(String.valueOf(accounts.getAmount()).getBytes("GB2312"));
//                writeLineBreak(2);
//                write(accounts.getTransactiontype().getBytes("GB2312"));
//
//                writeLineBreak(1);
//            }


            writeLineBreak(2);
            write(id);
            writeLineBreak(3);
            write(signature);
            writeLineBreak(2);
            write(servedBy);
            writeLineBreak(4);
            write(arryMotto);
            writeLineBreak(5);
            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void write(final byte[] arryData) {
        int result = PrinterInterface.write(arryData, arryData.length);
    }

    // print line break
    private void writeLineBreak(int lineNumber) {
        // write(new byte[]{'\n'});
        write(PrinterCommand.getCmdEscDN(lineNumber));
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }
}

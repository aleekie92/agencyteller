package com.coretec.agencyteller.activities.reports;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyteller.R;
import com.coretec.agencyteller.adapters.NewMiniAdapter;
import com.coretec.agencyteller.api.Api;
import com.coretec.agencyteller.model.MiniModel;
import com.coretec.agencyteller.utils.PreferenceFileKeys;
import com.coretec.agencyteller.utils.SharedPrefs;
import com.coretec.agencyteller.wizarpos.function.printer.PrinterCommand;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Ministatement extends AppCompatActivity {

    private EditText identifier;
    private Spinner identifierSpinner;
    private Spinner accountSpinner;
    private ArrayList<String> arrayList = new ArrayList<>();

    LinearLayout linear_rv;
    public RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;

    private Button submit;
    private Button search;
    private Button print;

    private ProgressBar progressBar;
    private ProgressDialog progressDialog;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPrefs.init(getApplicationContext());
        setContentView(R.layout.ministatement);

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

        //Toolbar declarations...Reusable
        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView mTitle = toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getText(R.string.mini_statement));
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);

        identifierSpinner = findViewById(R.id.identifier_spinner);
        accountSpinner = findViewById(R.id.accountsSpinner);
        identifier = findViewById(R.id.mini_identifier);
        progressBar = findViewById(R.id.accountsPB);
        submit = findViewById(R.id.button_submit);
        search = findViewById(R.id.btn_search);
        print = findViewById(R.id.button_print);

        linear_rv = findViewById(R.id.linear_recycler_view);
        mRecyclerView = findViewById(R.id.recycler_view);

        accountSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //((TextView) parent.getChildAt(0)).setTextColor(Color.GRAY);
                String item = parent.getItemAtPosition(position).toString();
                String text = accountSpinner.getSelectedItem().toString();

                String account_number = text.substring(0, text.indexOf("-"));
                SharedPrefs.write(SharedPrefs.MEMBER_ACCOUNT_NUMBER, account_number);
                Log.e("DESTINATION ACCOUNT", account_number);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        identifierSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.GRAY);
                String item = parent.getItemAtPosition(position).toString();
                String text = identifierSpinner.getSelectedItem().toString();

                int idLength = 8;
                int acLength = 14;
                int memLength = 14;

                InputFilter[] fArray = new InputFilter[1];
                fArray[0] = new InputFilter.LengthFilter(idLength);

                InputFilter[] aArray = new InputFilter[1];
                aArray[0] = new InputFilter.LengthFilter(acLength);

                InputFilter[] mArray = new InputFilter[1];
                mArray[0] = new InputFilter.LengthFilter(memLength);

                if (text.equals("ID NUMBER")) {
                    SharedPrefs.write(SharedPrefs.IDENTIFIER, "0");
                    identifier.setText(null);
                    identifier.setInputType(InputType.TYPE_CLASS_NUMBER);
                    identifier.setFilters(fArray);
                } else if (text.equals("OLD A/C NUMBER")) {
                    SharedPrefs.write(SharedPrefs.IDENTIFIER, "4");
                    identifier.setText(null);
                    identifier.setInputType(InputType.TYPE_CLASS_TEXT);
                    identifier.setFilters(aArray);
                } else if (text.equals("MEMBER NUMBER")) {
                    identifier.setText(null);
                    identifier.setInputType(InputType.TYPE_CLASS_NUMBER);
                    SharedPrefs.write(SharedPrefs.IDENTIFIER, "1");
                    identifier.setFilters(mArray);
                }

                Log.e("SELECTED", text);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = identifier.getText().toString();

                if (id.length()<7 && id.length()>0) {
                    identifier.setError("Please input a valid identifier!");
                } else if (id.length()==0){
                    identifier.setError("Cannot search an empty field!");
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(Ministatement.this, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                    linear_rv.setVisibility(View.GONE);
                    submit.setVisibility(View.VISIBLE);
                    requestAccounts(id);
                }
            }
        });

        print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    progressDialog = new ProgressDialog(Ministatement.this);
                    progressDialog.setMessage("Printing ministatement...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();

                    int result = PrinterInterface.open();
                    header();

                    Log.d("nameee", "nameeee---->"+SharedPrefs.read(SharedPrefs.SACCO_NAME, null));
                    Log.d("nameee", "nameeee---->"+SharedPrefs.read(SharedPrefs.SACCO_MOTTO, null));

                    PrinterInterface.close();
                    requestPrint();
            }
        });

        TextWatcher textWatcher = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String idNumber = identifier.getText().toString();

                /*if (idNumber.length() >= 8) {
                    requestAccounts(idNumber);
                }*/

                if (idNumber.length() >= 5 && idNumber.length() <= 8) {
                    arrayList.clear();
                    accountSpinner = findViewById(R.id.accountsSpinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(Ministatement.this, android.R.layout.simple_spinner_item, arrayList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    accountSpinner.setAdapter(adapter);
                }
                if (idNumber.length() < 5) {
                    //submitAccRegistrationRequest.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                }

            }
        };

        identifier.addTextChangedListener(textWatcher);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String idNo = identifier.getText().toString();

                if (idNo.isEmpty()) {
                    identifier.setError("Please input ID number!");
                } else {
                    progressDialog = new ProgressDialog(Ministatement.this);
                    progressDialog.setMessage("Submitting your request...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();

                    int result = PrinterInterface.open();
                    PrinterInterface.close();
                    requestMinistatement();
                }
            }
        });

    }

    public void setuprecyclerview(List<MiniModel> modList) {

        mRecyclerView.setLayoutManager(new LinearLayoutManager(Ministatement.this));
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(mRecyclerView.getContext(), DividerItemDecoration.VERTICAL));

        mAdapter = new NewMiniAdapter(Ministatement.this, modList);
        mRecyclerView.setAdapter(mAdapter);

    }

    private void requestAccounts(String id) {

        int identifier = Integer.parseInt(SharedPrefs.read(SharedPrefs.IDENTIFIER, null));

        Api.getVolley(this, Api.GetMemberAccounts, "{\n" +
                "    \"identifier\": \"" + id + "\",\n" +
                "    \"identifierType\": \"" + identifier + "\",\n" +
                "    \"natureOfAccount\": \"" + 1 + "\"\n" +
                "  }\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                progressBar.setVisibility(View.GONE);
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));
                        arrayList.clear();
                        accountSpinner = findViewById(R.id.accountsSpinner);
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(Ministatement.this, android.R.layout.simple_spinner_item, arrayList);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        accountSpinner.setAdapter(adapter);

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            String accountName = jsonObject1.getString("name");
                            String accountNo = jsonObject1.getString("no");
                            String desc = jsonObject1.getString("description");
                            String accountDetails = accountNo + "-" + desc;

                            SharedPrefs.write(SharedPrefs.ACC_NAME, accountName);

                            arrayList.add(accountDetails);

                            Log.e("ACCOUNT DETAILS", accountDetails);

                        }
                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(Ministatement.this, "Could not fetch accounts! Please try again.", Toast.LENGTH_SHORT).show();
                    }

                    accountSpinner = findViewById(R.id.accountsSpinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(Ministatement.this, android.R.layout.simple_spinner_item, arrayList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    accountSpinner.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    void  requestMinistatement() {

        Api.getVolley(this, Api.GetMemberStatement, "{\n" +
                "    \"accountNo\": \"" + SharedPrefs.read(SharedPrefs.MEMBER_ACCOUNT_NUMBER, null) + "\"\n" +
                "  }\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {

                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));
                        progressDialog.dismiss();
                        linear_rv.setVisibility(View.VISIBLE);
                        submit.setVisibility(View.GONE);
                        List<MiniModel> miniModelList= new ArrayList<>();

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            MiniModel newMod = new MiniModel(jsonObject1.getString("date"), jsonObject1.getString("description"),
                                    jsonObject1.getString("amount"));
                            miniModelList.add(newMod);

                        }

                        setuprecyclerview(miniModelList);

                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        progressDialog.dismiss();

                        String msg = jsonObject.getString("message");
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(Ministatement.this);
                        builder1.setMessage(msg);
                        builder1.setTitle("Alert!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        //onBackPressed();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

    }

    void requestPrint() {
        Api.getVolley(this, Api.GetMemberStatement, "{\n" +
                "    \"accountNo\": \"" + SharedPrefs.read(SharedPrefs.MEMBER_ACCOUNT_NUMBER, null) + "\"\n" +
                "  }\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {

                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            String date = jsonObject1.getString("date");
                            String desc = jsonObject1.getString("description");
                            String amount = jsonObject1.getString("amount");
                            int result = PrinterInterface.open();
                            writetest(date, desc, amount);
                            PrinterInterface.close();
                            progressDialog.dismiss();
                            String msg = "Ministatement printed successfully!";
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(Ministatement.this);
                            builder1.setMessage(msg);
                            builder1.setTitle("Success!");
                            builder1.setCancelable(false);

                            builder1.setPositiveButton(
                                    "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                            onBackPressed();
                                        }
                                    });

                            AlertDialog alert11 = builder1.create();
                            alert11.show();
                        }
                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        progressDialog.dismiss();
                        Toast.makeText(Ministatement.this, "Could not fetch ministatement!", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                int result = PrinterInterface.open();
                footer();
                PrinterInterface.close();

            }
        });

    }

    public void footer(){
        try {
            byte[] time = null;
            byte[] nameAgent = null;
            byte[] motto = null;

            SimpleDateFormat timeStampFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
            Date myDate = new Date();
            String timeCurrent = timeStampFormat.format(myDate);
            String aName = SharedPrefs.read(SharedPrefs.AGENT_NAME, null);

            try {
                time = String.valueOf("   Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                nameAgent = String.valueOf("SERVED BY " + String.valueOf(aName)).getBytes("GB2312");
                motto = String.valueOf(SharedPrefs.read(SharedPrefs.SACCO_MOTTO, null)).getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();
            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(time);
            writeLineBreak(2);
            write(nameAgent);
            writeLineBreak(1);
            write(motto);
            // print line break
            writeLineBreak(4);
            PrinterInterface.end();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void header(){

        //String middle = idNos.substring(2, 6);
        String accountNumber = SharedPrefs.read(SharedPrefs.MEMBER_ACCOUNT_NUMBER, null);
        String firstAc = accountNumber.substring(0, 4);
        String lastAc = accountNumber.substring(8, accountNumber.length());
        String maskedAc = firstAc + "****" + lastAc;
        try {
            byte[] start = null;
            byte[] saccoName = null;
            byte[] type = null;
            byte[] gno = null;
            byte[] accName = null;

            try {
                start = String.valueOf("              ").getBytes("GB2312");
                saccoName = String.valueOf(           SharedPrefs.read(SharedPrefs.SACCO_NAME, null)).getBytes("GB2312");
                type = String.valueOf("     MINISTATEMENT REQUEST     ").getBytes("GB2312");
                gno = String.valueOf("A/C Number : " + String.valueOf(maskedAc)).getBytes("GB2312");
                accName = String.valueOf("A/C Name : " + SharedPrefs.read(SharedPrefs.ACC_NAME, null)).getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();
            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(start);
            writeLineBreak(1);
            write(saccoName);
            writeLineBreak(2);
            write(type);
            writeLineBreak(1);
            write(gno);
            writeLineBreak(1);
            write(accName);
            writeLineBreak(1);
            PrinterInterface.end();

        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void writetest(String datePosting, String desc, String amnt) {
        try {
            byte[] titles = null;
            byte[] date = null;
            byte[] descriptn = null;
            byte[] mini_amount = null;

            try {
                //titles = String.valueOf("   TITLE    " ).getBytes("GB2312");
                date = String.valueOf("Date : " + String.valueOf(datePosting)).getBytes("GB2312");
                descriptn = String.valueOf("Description : " + String.valueOf(desc)).getBytes("GB2312");
                mini_amount = String.valueOf("Amount : " + String.valueOf(amnt)).getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            //write(titles);
            //writeLineBreak(1);
            write(date);
            writeLineBreak(0);
            write(descriptn);
            // print line break
            writeLineBreak(0);
            write(mini_amount);
            writeLineBreak(1);
            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void write(final byte[] arryData) {
        int result = PrinterInterface.write(arryData, arryData.length);
    }

    // print line break
    private void writeLineBreak(int lineNumber) {
        // write(new byte[]{'\n'});
        write(PrinterCommand.getCmdEscDN(lineNumber));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                //Toast.makeText(Account.this, "Back has been presses!", Toast.LENGTH_LONG).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }

}

package com.coretec.agencyteller.activities.receipts;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.TextView;

import com.coretec.agencyteller.R;
import com.coretec.agencyteller.api.responses.DepositResponse;
import com.coretec.agencyteller.utils.Utils;
import com.coretec.agencyteller.wizarpos.function.ActionCallbackImpl;
import com.coretec.agencyteller.wizarpos.mvc.base.ActionCallback;
import com.coretec.agencyteller.wizarpos.mvc.base.ActionManager;

import java.util.HashMap;
import java.util.Map;

public class DepositReceipt extends AppCompatActivity {
    public static final String DEPOSIT_RESPONSE_EXTRA = "DEPOSIT_RESPONSE_EXTRA";
    public static final String DEPOSIT_ACCOUNT_EXTRA = "DEPOSIT_ACCOUNT_EXTRA";
    private TextView deposit_transaction_date, deposit_terminal_id, deposit_receipt_number, deposit_customer_name;
    private TextView deposit_amount, deposit_sacco_name, depoAccountNumber, depositedBy;
    private AppCompatButton printDepositReceipt;
    public static Handler handler;
    public static ActionCallback callback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.deposit_receipt);


        deposit_transaction_date = (TextView) findViewById(R.id.deposit_transaction_date);
        deposit_terminal_id = (TextView) findViewById(R.id.deposit_terminal_id);
        deposit_receipt_number = (TextView) findViewById(R.id.deposit_receipt_number);
        deposit_customer_name = (TextView) findViewById(R.id.deposit_customer_name);
        deposit_amount = (TextView) findViewById(R.id.deposit_amount);
        deposit_sacco_name = (TextView) findViewById(R.id.deposit_sacco_name);
        depoAccountNumber = (TextView) findViewById(R.id.depoAccountNumber);
        depositedBy = (TextView) findViewById(R.id.depositedBy);

        handler = new Handler();
        callback = new ActionCallbackImpl(handler);

        DepositResponse depositResponse = (DepositResponse) getIntent().getSerializableExtra(DEPOSIT_RESPONSE_EXTRA);
        String memberAccno = getIntent().getStringExtra(DEPOSIT_ACCOUNT_EXTRA);

        deposit_transaction_date.setText(depositResponse.getTransactiondate());
        deposit_terminal_id.setText(Utils.getIMEI(DepositReceipt.this));
        deposit_receipt_number.setText(depositResponse.getReceiptno());
        deposit_customer_name.setText(depositResponse.getCustomername());
        deposit_amount.setText(" KES " + depositResponse.getAmount());
        deposit_sacco_name.setText(depositResponse.getSacconame());
        depoAccountNumber.setText(memberAccno);
        depositedBy.setText(depositResponse.getDepositedby());

        final Map<String, Object> params = new HashMap<>();
        params.put("deposit", depositResponse);


        printDepositReceipt = (AppCompatButton) findViewById(R.id.btn_print_deposit_receipt);
        printDepositReceipt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ActionManager.doSubmit("DepositPrinterAction/open", DepositReceipt.this, params, callback);
                ActionManager.doSubmit("DepositPrinterAction/write", DepositReceipt.this, params, callback);
                ActionManager.doSubmit("DepositPrinterAction/close", DepositReceipt.this, params, callback);
            }
        });

    }
}

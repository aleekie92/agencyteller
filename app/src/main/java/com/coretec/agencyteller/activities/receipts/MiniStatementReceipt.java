package com.coretec.agencyteller.activities.receipts;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.coretec.agencyteller.R;
import com.coretec.agencyteller.adapters.MiniStatementAdapter;
import com.coretec.agencyteller.api.responses.MiniStatementResponse;
import com.coretec.agencyteller.model.Transactions;
import com.coretec.agencyteller.utils.Utils;
import com.coretec.agencyteller.wizarpos.function.ActionCallbackImpl;
import com.coretec.agencyteller.wizarpos.mvc.base.ActionCallback;
import com.coretec.agencyteller.wizarpos.mvc.base.ActionManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by kelvinoff on 17/11/07.
 */

public class MiniStatementReceipt extends AppCompatActivity {

    public static final String MINISTATEMENT_RESPONSE_EXTRA = "MINISTATEMENT_RESPONSE_EXTRA";

    private RecyclerView ms_recyclerview;
    private MiniStatementAdapter miniStatementAdapter;
    private List<Transactions> ministatementList;
    private Transactions transactions;
    public static Handler handler;
    public static ActionCallback callback;
    private AppCompatButton btnPrintMiniStatement;
    private TextView ministatement_transaction_date, ministatement_terminal_id, ministatement_receipt_number, ministatement_customer_name;
    private TextView ministatement_sacco_name;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.receipt);

        handler = new Handler();
        callback = new ActionCallbackImpl(handler);
        ministatementList = new ArrayList<>();

        btnPrintMiniStatement = (AppCompatButton) findViewById(R.id.btn_print_ministatement_receipt);

        ministatement_transaction_date = (TextView) findViewById(R.id.ministatement_transaction_date);
        ministatement_terminal_id = (TextView) findViewById(R.id.ministatement_terminal_id);
        ministatement_receipt_number = (TextView) findViewById(R.id.ministatement_receipt_number);
        ministatement_customer_name = (TextView) findViewById(R.id.ministatement_customer_name);
        ministatement_sacco_name = (TextView) findViewById(R.id.ministatement_sacco_name);


        MiniStatementResponse miniStatementResponse = (MiniStatementResponse) getIntent().getSerializableExtra(MINISTATEMENT_RESPONSE_EXTRA);
        ministatementList.addAll(miniStatementResponse.getTransactions());

        ministatement_transaction_date.setText(miniStatementResponse.getTransactiondate());
        ministatement_terminal_id.setText(Utils.getIMEI(this));
        ministatement_receipt_number.setText(miniStatementResponse.getReceiptno());
        ministatement_customer_name.setText(miniStatementResponse.getCustomer_name());
        ministatement_sacco_name.setText(miniStatementResponse.getSacconame());


        ms_recyclerview = (RecyclerView) findViewById(R.id.msRecyclerView);
        ms_recyclerview.setHasFixedSize(true);
        miniStatementAdapter = new MiniStatementAdapter(this, ministatementList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        ms_recyclerview.setLayoutManager(mLayoutManager);
        ms_recyclerview.setAdapter(miniStatementAdapter);

        final Map<String, Object> params = new HashMap<>();
        params.put("transactions", ministatementList);
        btnPrintMiniStatement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActionManager.doSubmit("MiniStatementPrinterAction/open", MiniStatementReceipt.this, params, callback);
                ActionManager.doSubmit("MiniStatementPrinterAction/write", MiniStatementReceipt.this, params, callback);
                ActionManager.doSubmit("MiniStatementPrinterAction/close", MiniStatementReceipt.this, params, callback);
            }
        });

    }
}

package com.coretec.agencyteller.activities.teller;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyteller.R;
import com.coretec.agencyteller.activities.Dashboard;
import com.coretec.agencyteller.adapters.SignatoryAdapter;
import com.coretec.agencyteller.api.Api;
import com.coretec.agencyteller.model.SignatoryList;
import com.coretec.agencyteller.utils.EnglishNumberToWords;
import com.coretec.agencyteller.utils.PreferenceFileKeys;
import com.coretec.agencyteller.utils.SharedPrefs;
import com.coretec.agencyteller.utils.Utils;
import com.coretec.agencyteller.wizarpos.function.printer.PrinterCommand;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class CashWithdrawal extends AppCompatActivity {

    private EditText amount;
    private EditText identifier;
    private EditText transacted_by;
    private Spinner identifierSpinner;
    private Spinner accountSpinner;
    private ArrayList<String> arrayList = new ArrayList<>();

    private Button submit, btn_view;
    private Button search;
    private ImageView picture;
    private ImageView signature;
    LinearLayout pic_layout;
    LinearLayout customer_details, signatory_details;
    TextView name, id_number, book_balance, balance, info_base_area, signatories,tv_status;

    private ProgressBar progressBar,imagePB;
    AlertDialog pictureDialog;
    private ProgressDialog progressDialog;
    SharedPreferences sharedPreferences;
    public static String status = "";

    public static  String stat="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPrefs.init(getApplicationContext());
        setContentView(R.layout.teller_cash_withdrawal);

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

        //Toolbar declarations...Reusable
        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView mTitle = toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getText(R.string.cash_withdrawal));
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);

        identifierSpinner = findViewById(R.id.identifier_spinner);
        accountSpinner = findViewById(R.id.accountsSpinner);
        identifier = findViewById(R.id.withdrawal_identifier);
        transacted_by = findViewById(R.id.edittext_transacted_by);
        progressBar = findViewById(R.id.accountsPB);
        imagePB = findViewById(R.id.imagePB);
        submit = findViewById(R.id.button_submit);
        amount = findViewById(R.id.et_amount);
        search = findViewById(R.id.btn_search);
        tv_status = findViewById(R.id.tv_status);

        customer_details = findViewById(R.id.linear_details);
        signatory_details = findViewById(R.id.linear_signatories);
        name = findViewById(R.id.tv_name);
        id_number = findViewById(R.id.tv_id_number);
        balance = findViewById(R.id.tv_balance);
        book_balance = findViewById(R.id.tv_book_balance);
        info_base_area = findViewById(R.id.tv_info_base_area);
        signatories = findViewById(R.id.tv_signatories);
        btn_view = findViewById(R.id.btn_view);

        pic_layout = findViewById(R.id.pictures_layout);
        picture = findViewById(R.id.picture_imageview);
        signature = findViewById(R.id.signature_imageview);


        identifierSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                String item = parent.getItemAtPosition(position).toString();
                String text = identifierSpinner.getSelectedItem().toString();

                int idLength = 8;
                int acLength = 14;
                int memLength = 14;
                InputFilter[] fArray = new InputFilter[1];
                fArray[0] = new InputFilter.LengthFilter(idLength);

                InputFilter[] aArray = new InputFilter[1];
                aArray[0] = new InputFilter.LengthFilter(acLength);

                InputFilter[] mArray = new InputFilter[1];
                mArray[0] = new InputFilter.LengthFilter(memLength);

                if (text.equals("ID NUMBER")) {
                    SharedPrefs.write(SharedPrefs.IDENTIFIER, "0");
                    identifier.setText(null);
                    identifier.setInputType(InputType.TYPE_CLASS_NUMBER);
                    identifier.setFilters(fArray);
                } else if (text.equals("OLD A/C NUMBER")) {
                    SharedPrefs.write(SharedPrefs.IDENTIFIER, "4");
                    identifier.setText(null);
                    identifier.setInputType(InputType.TYPE_CLASS_TEXT);
                    identifier.setFilters(aArray);
                } else if (text.equals("MEMBER NUMBER")) {
                    identifier.setText(null);
                    identifier.setInputType(InputType.TYPE_CLASS_NUMBER);
                    SharedPrefs.write(SharedPrefs.IDENTIFIER, "1");
                    identifier.setFilters(mArray);
                }

                Log.e("SELECTED", text);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = identifier.getText().toString();

                if (id.length()<7 && id.length()>0) {
                    identifier.setError("Please input a valid identifier!");
                } else if (id.length()==0){
                    identifier.setError("Cannot search an empty field!");
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(CashWithdrawal.this, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                    requestAccounts(id);
                    imagePB.setVisibility(View.VISIBLE);
                    getPicture(id);
                    getSignature(id);

                }
            }
        });

        accountSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                String text = accountSpinner.getSelectedItem().toString();

                String statt=text.substring(text.lastIndexOf(" ")+1);

                String account_number = text.substring(0, text.indexOf("-"));
                SharedPrefs.write(SharedPrefs.MEMBER_ACCOUNT_NUMBER, account_number);
                Log.e("MEMBER ACCOUNT", account_number);

                switch (text) {
                    case "select-account":
                        break;
                    default:
                        requestBalance(account_number);
                        checkSignatories(account_number);

                        tv_status.setText(statt);
                        if (statt == "Active"){
                            tv_status.setText(statt);
                        }
                        if (statt == "Dormant"){
                            tv_status.setText(statt);
                        }
                        if (statt == "Frozen"){
                            tv_status.setText(statt);
                        }
                        if (statt == "Deceased"){
                            tv_status.setText(statt);
                        }
                        if (statt == "Closed"){
                            tv_status.setText(statt);
                        }

                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        TextWatcher textWatcher = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String idNumber = identifier.getText().toString();

                /*if (idNumber.length() >= 7) {
                    requestAccounts(idNumber);
                    getPicture(idNumber);
                    getSignature(idNumber);
                }*/

                if (idNumber.length() >= 5 && idNumber.length() <= 8) {

                    arrayList.clear();
                    accountSpinner = findViewById(R.id.accountsSpinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(CashWithdrawal.this, android.R.layout.simple_spinner_item, arrayList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    accountSpinner.setAdapter(adapter);
                    pic_layout.setVisibility(View.GONE);
                    customer_details.setVisibility(View.GONE);
                    signatory_details.setVisibility(View.GONE);

                }
                if (idNumber.length() < 5) {
                    progressBar.setVisibility(View.GONE);
                }

            }
        };

        identifier.addTextChangedListener(textWatcher);

        btn_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CashWithdrawal.this, DialogSignatories.class));
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String idNo = identifier.getText().toString();
                String en_amount = amount.getText().toString();
                String transactedBy = transacted_by.getText().toString();

                if (idNo.isEmpty()) {
                    identifier.setError("Please input ID number!");
                } else if (en_amount.isEmpty()) {
                    amount.setError("Please enter amount!");
                } else if (transactedBy.isEmpty()) {
                    transacted_by.setError("Please enter this field!");
                } else {
                    progressDialog = new ProgressDialog(CashWithdrawal.this);
                    progressDialog.setMessage("Submitting your request...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    requestCashWithdrawal(identifier.getText().toString(), amount.getText().toString());
                    submit.setClickable(false);
                }
            }
        });

    }

    public static Drawable LoadImageFromWebOperations(String url) {
        try {
            InputStream is = (InputStream) new URL(url).getContent();
            Drawable d = Drawable.createFromStream(is, "src name");
            return d;
        } catch (Exception e) {
            return null;
        }
    }

    public Bitmap StringToBitMap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    private void requestAccounts(String id) {

        int identifier = Integer.parseInt(SharedPrefs.read(SharedPrefs.IDENTIFIER, null));

        Api.getVolley(this, Api.GetMemberAccounts, "{\n" +
                "    \"identifier\": \"" + id + "\",\n" +
                "    \"identifierType\": \"" + identifier + "\",\n" +
                "    \"natureOfAccount\": \"" + 1 + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                progressBar.setVisibility(View.GONE);
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        arrayList.clear();
                        accountSpinner = findViewById(R.id.accountsSpinner);
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(CashWithdrawal.this, android.R.layout.simple_spinner_item, arrayList);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        accountSpinner.setAdapter(adapter);

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            String accountName = jsonObject1.getString("name");
                            String accountNo = jsonObject1.getString("no");
                            String accountBalance = jsonObject1.getString("balance");
                            String desc = jsonObject1.getString("description");
                            String infoBase = jsonObject1.getString("infoBaseArea");
                            status = jsonObject1.getString("status");


                            if (status.equals("2")){
                                stat = "Active";
                                //tv_status.setText("Active");
                            }
                            if (status.equals("3")){
                                stat = "Dormant";
                                //tv_status.setText("Dormant");
                            }
                            if (status.equals("4")){
                                stat="Frozen";
                                //tv_status.setText("Frozen");
                            }
                            if (status.equals("7")){
                                stat = "Deceased";
                                //tv_status.setText("Deceased");
                            }
                            if (status.equals("9")){
                                stat = "Closed";
                                //tv_status.setText("Closed");
                            }

                            String accountDetails = accountNo + "-" + desc + " " + "("+stat+")";
                            SharedPrefs.write(SharedPrefs.ACC_NAME, accountName);
                            arrayList.add(accountDetails);
                            Log.e("ACCOUNT DETAILS", accountDetails);

                        }
                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        progressBar.setVisibility(View.GONE);

                        AlertDialog.Builder builder1 = new AlertDialog.Builder(CashWithdrawal.this);
                        builder1.setMessage("Could not fetch account details");
                        builder1.setTitle("Alert!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();

                        //Toast.makeText(CashWithdrawal.this, "Could not fetch accounts! Please try again.", Toast.LENGTH_SHORT).show();
                    }

                    accountSpinner = findViewById(R.id.accountsSpinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(CashWithdrawal.this, android.R.layout.simple_spinner_item, arrayList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    accountSpinner.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void checkSignatories(String accountNumber) {

        Api.getVolley(this, Api.GetAccountSignatories, "{\n" +
                "    \"accountType\": \"" + 1 + "\",\n" +
                "    \"accountNo\": \"" + accountNumber + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                progressBar.setVisibility(View.GONE);
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        signatory_details.setVisibility(View.VISIBLE);
                        signatories.setText("Signatories found, press button below to view");
                        btn_view.setVisibility(View.VISIBLE);
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));

                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        signatory_details.setVisibility(View.VISIBLE);
                        signatories.setText(jsonObject.getString("message"));
                        btn_view.setVisibility(View.GONE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void requestBalance(String account_no) {

        Api.getVolley(this, Api.GetMemberAccounts, "{\n" +
                "    \"identifier\": \"" + account_no + "\",\n" +
                "    \"identifierType\": \"" + 3 + "\",\n" +
                "    \"natureOfAccount\": \"" + 1 + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                progressBar.setVisibility(View.GONE);
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {

                        customer_details.setVisibility(View.VISIBLE);

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            JSONObject balanceObject = jsonObject1.getJSONObject("balance");

                            String accountName = jsonObject1.getString("name");
                            String accountNo = jsonObject1.getString("no");
                            String idNumber = jsonObject1.getString("nationalID");
                            String accountBalance = balanceObject.getString("available");
                            String bookBalance = balanceObject.getString("actual");
                            String desc = jsonObject1.getString("description");
                            String infoBase = jsonObject1.getString("infoBaseArea");
                            //SharedPrefs.write(SharedPrefs.ACC_NAME, accountName);

                            name.setText(accountName);
                            id_number.setText(idNumber);
                            balance.setText(accountBalance);
                            book_balance.setText(bookBalance);

                            if (infoBase.equals("null")) {
                                info_base_area.setText("");
                            } else {
                                info_base_area.setText(infoBase);
                            }

                        }
                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(CashWithdrawal.this, "Could not fetch data! Please try again.", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void getPicture(String id) {

        int identifier = Integer.parseInt(SharedPrefs.read(SharedPrefs.IDENTIFIER, null));

        Api.getVolley(this, Api.GetMemberPictures, "{\n" +
                "    \"identifier\": \"" + id + "\",\n" +
                "    \"identifierType\": \"" + identifier + "\",\n" +
                "    \"fileType\": \"" + 0 + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                progressBar.setVisibility(View.GONE);
                imagePB.setVisibility(View.GONE);
                Log.e("RESULTS", Response);

                Log.d("imageee", "imageee---->"+Response);

               /* if (Response.length() == 71) {
                    picture.setImageDrawable(ContextCompat.getDrawable(CashWithdrawal.this, R.drawable.user));

                    pic_layout.setVisibility(View.VISIBLE);
                } else {

                    Bitmap decodeTxt = StringToBitMap(Response);

                    picture.setImageBitmap(decodeTxt);
                    pic_layout.setVisibility(View.VISIBLE);
                }*/

                try {
                    Bitmap decodeTxt = StringToBitMap(Response);
                    picture.setImageBitmap(decodeTxt);
                    pic_layout.setVisibility(View.VISIBLE);
                }catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(CashWithdrawal.this, "Passport photo too large", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    private void getSignature(String id) {

        int identifier = Integer.parseInt(SharedPrefs.read(SharedPrefs.IDENTIFIER, null));

        Api.getVolley(this, Api.GetMemberPictures, "{\n" +
                "    \"identifier\": \"" + id + "\",\n" +
                "    \"identifierType\": \"" + identifier + "\",\n" +
                "    \"fileType\": \"" + 1 + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                progressBar.setVisibility(View.GONE);
                Log.e("RESULTS", Response);
                Log.d("imageee", "imageee---->"+Response);

               /* if (Response.equals("{\"error\":true,\"message\":\"Member Signature Not Found\",\"data\":null}")) {
                    signature.setImageDrawable(ContextCompat.getDrawable(CashWithdrawal.this, R.drawable.signaturee));
                    pic_layout.setVisibility(View.VISIBLE);
                } else {

                    Bitmap decodeTxt = StringToBitMap(Response);

                    signature.setImageBitmap(decodeTxt);
                    pic_layout.setVisibility(View.VISIBLE);
                }*/


               try {
                   Bitmap decodeTxt = StringToBitMap(Response);
                   signature.setImageBitmap(decodeTxt);
                   pic_layout.setVisibility(View.VISIBLE);
               }catch (Exception e){
                   e.printStackTrace();
                   Toast.makeText(CashWithdrawal.this, "Signature photo too large", Toast.LENGTH_SHORT).show();
               }
            }
        });

    }

    void requestCashWithdrawal(final String Id, final String amou_nt) {

        Api.getVolley(this, Api.CashWithdrawalTransaction, "{\n" +
                "    \"action\": \"" + 0 + "\",\n" +
                "    \"transactionType\": \"" + 3 + "\",\n" +
                "    \"deviceID\": \"" + SharedPrefs.read(SharedPrefs.DEVICE_ID, null) + "\",\n" +
                "    \"agentCode\": \"" + SharedPrefs.read(SharedPrefs.AGENT_CODE, null) + "\",\n" +
                "    \"amount\": \"" + amou_nt + "\",\n" +
                "    \"sourceAccount\": \"" + SharedPrefs.read(SharedPrefs.MEMBER_ACCOUNT_NUMBER, null) + "\",\n" +
                "    \"destinationAccount\": \"" + SharedPrefs.read(SharedPrefs.TELLER_ACCOUNT_NUMBER, null) + "\",\n" +
                "    \"transactedby\": \"" + transacted_by.getText().toString() + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {

                        String mesg = jsonObject.getString("message");

                        if (mesg.equals("This Transaction has exceeded the Maximum Withdrawal Limit and has been Sent for Approval.")) {
                            progressDialog.dismiss();
                            String msg = jsonObject.getString("message")+"\n" + "Press OK to print receipt with transaction code.";
                            final String code = jsonObject.getString("data");
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(CashWithdrawal.this);
                            builder1.setMessage(msg);
                            builder1.setTitle("Alert!");
                            builder1.setCancelable(false);

                            builder1.setNegativeButton(
                                    "Cancel",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });

                            builder1.setPositiveButton(
                                    "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            int result = PrinterInterface.open();
                                            writeCode(code);
                                            PrinterInterface.close();
                                            dialog.cancel();
                                            onBackPressed();
                                        }
                                    });

                            AlertDialog alert11 = builder1.create();
                            alert11.show();
                        } else {

                            JSONObject dataObject = jsonObject.getJSONObject("data");

                            final String tCode = dataObject.getString("no");
                            final String baLance = dataObject.getString("balance");

                            int result = PrinterInterface.open();
                            writetest(Id, amou_nt, tCode, baLance);
                            PrinterInterface.close();
                            progressDialog.dismiss();

                            Utils.showAlertDialog(CashWithdrawal.this, "Cash Withdrawn Successfully", " Press Ok to print another Receipt ", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    int result = PrinterInterface.open();
                                    writetest1(Id, amou_nt, tCode, baLance);
                                    PrinterInterface.close();
                                    createDialogPictures();
                                    //onBackPressed();
                                }
                            });
                        }

                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        progressDialog.dismiss();
                        Toast.makeText(CashWithdrawal.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                        String msg = jsonObject.getString("message");
                        //final String qualKgs = SharedPrefs.read(SharedPrefs.LOAN_QUALIFIED_KGS, null);
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(CashWithdrawal.this);
                        builder1.setMessage(msg);
                        builder1.setTitle("Alert!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void createDialogPictures() {

        LayoutInflater myInflater = LayoutInflater.from(this);
        View v = myInflater.inflate(R.layout.dialog_images, null);

        ImageView user_image = v.findViewById(R.id.iv_image);
        ImageView user_signature = v.findViewById(R.id.iv_signature);
        Button dismiss = v.findViewById(R.id.btn_dismiss);

        user_image.setImageDrawable(picture.getDrawable());
        user_signature.setImageDrawable(signature.getDrawable());

        dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pictureDialog.dismiss();
                onBackPressed();
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(v);
        pictureDialog = builder.create();
        pictureDialog.setCancelable(false);
        pictureDialog.show();

    }

    public void writeCode(String transactionCode) {
        String the_amount = amount.getText().toString();
        int number = Integer.parseInt(the_amount);
        String return_val_in_english = EnglishNumberToWords.convert(number);
        try {

            byte[] start = null;
            byte[] saccoName = null;
            byte[] type = null;

            byte[] terminalNo = null;
            byte[] transaction = null;
            byte[] time = null;
            byte[] transDate = null;
            byte[] account_dposited_to = null;
            byte[] idNumber = null;
            byte[] fullNames = null;
            byte[] fName = null;

            //byte[] phoneNumber = null;
            byte[] arryWithdrawAmt = null;
            byte[] arryBalance = null;
            byte[] arryWordsHeader = null;
            byte[] arryInWords = null;

            byte[] arryMesg = null;

            byte[] acknowLedge = null;
            byte[] nameVerify = null;
            byte[] idVerify = null;
            byte[] signature = null;

            byte[] nameAgent = null;
            byte[] motto = null;

            SimpleDateFormat timeStampFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
            Date myDate = new Date();
            String timeCurrent = timeStampFormat.format(myDate);
            String aName = SharedPrefs.read(SharedPrefs.AGENT_NAME, null);
            String accountNumber = SharedPrefs.read(SharedPrefs.MEMBER_ACCOUNT_NUMBER, null);
            String idNo = identifier.getText().toString();

            try {
                start = String.valueOf("              ").getBytes("GB2312");
                saccoName = String.valueOf(SharedPrefs.read(SharedPrefs.SACCO_NAME, null)).getBytes("GB2312");
                type = String.valueOf("      APPROVE CASH WITHDRAWAL     ").getBytes("GB2312");

                terminalNo = String.valueOf("Terminal ID : " + SharedPrefs.read(SharedPrefs.DEVICE_ID, null)).getBytes("GB2312");
                transaction = String.valueOf("Transaction No : " + transactionCode).getBytes("GB2312");
                time = String.valueOf("Print Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                transDate = String.valueOf("Trans. Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                account_dposited_to = String.valueOf("Account NO : " + accountNumber).getBytes("GB2312");
                idNumber = String.valueOf("ID Number : " + String.valueOf(idNo)).getBytes("GB2312");

                fullNames = String.valueOf("Customer Full Names : ").getBytes("GB2312");
                fName = String.valueOf(String.valueOf(SharedPrefs.read(SharedPrefs.ACC_NAME, null))).getBytes("GB2312");

                //phoneNumber = String.valueOf("Phone Number : " + String.valueOf(maskedOP)).getBytes("GB2312");
                arryWithdrawAmt = String.valueOf("Amount requested : KSH " + the_amount).getBytes("GB2312");
                arryWordsHeader = String.valueOf("Amount in Words: ").getBytes("GB2312");
                arryInWords = String.valueOf(return_val_in_english + " Kenya Shillings Only").getBytes("GB2312");
                //depositedBy = String.valueOf("Deposited By " + depositorsName).getBytes("GB2312");

                arryMesg = String.valueOf("Please come back with this transaction number to get your approval status").getBytes("GB2312");

                acknowLedge = String.valueOf("I acknowledge request of the above amount").getBytes("GB2312");
                nameVerify = String.valueOf("Name___________________________").getBytes("GB2312");
                idVerify = String.valueOf("ID No___________________________").getBytes("GB2312");
                signature = String.valueOf("Signature_______________________").getBytes("GB2312");

                nameAgent = String.valueOf("SERVED BY " + String.valueOf(aName)).getBytes("GB2312");

                motto = String.valueOf(SharedPrefs.read(SharedPrefs.SACCO_MOTTO, null)).getBytes("GB2312");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(start);
            writeLineBreak(0);
            write(saccoName);
            writeLineBreak(1);
            write(type);
            writeLineBreak(0);

            write(terminalNo);
            writeLineBreak(0);
            write(transaction);
            writeLineBreak(0);
            write(time);
            writeLineBreak(0);
            write(transDate);
            writeLineBreak(0);
            write(account_dposited_to);
            writeLineBreak(0);
            write(idNumber);
            writeLineBreak(0);
            write(fullNames);
            writeLineBreak(0);
            write(fName);
            writeLineBreak(1);

            write(arryWithdrawAmt);
            writeLineBreak(0);
            write(arryWordsHeader);
            writeLineBreak(0);
            write(arryInWords);
            writeLineBreak(1);

            write(arryMesg);
            writeLineBreak(1);

            /*write(acknowLedge);
            writeLineBreak(1);
            write(nameVerify);
            writeLineBreak(1);
            write(idVerify);
            writeLineBreak(1);
            write(signature);
            writeLineBreak(2);*/

            write(nameAgent);
            /*writeLineBreak(2);
            write(motto);*/
            // print line break
            writeLineBreak(4);

            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void writetest(String idNo, String amount, String transactionCode, String balance) {

        String accountNumber = SharedPrefs.read(SharedPrefs.MEMBER_ACCOUNT_NUMBER, null);
        String firstAc = accountNumber.substring(0, 4);
        String lastAc = accountNumber.substring(8, accountNumber.length());
        String maskedAc = firstAc + "****" + lastAc;

        int number = Integer.parseInt(amount);

        double bal = Double.parseDouble(balance);
        String currentBal = String.format("%.2f", bal);

        String return_val_in_english = EnglishNumberToWords.convert(number);

        try {

            byte[] start = null;
            byte[] saccoName = null;
            byte[] type = null;

            byte[] terminalNo = null;
            byte[] transaction = null;
            byte[] time = null;
            byte[] transDate = null;
            byte[] account_dposited_to = null;
            byte[] idNumber = null;
            byte[] fullNames = null;
            byte[] fName = null;

            //byte[] phoneNumber = null;
            byte[] arryWithdrawAmt = null;
            byte[] arryBalance = null;
            byte[] arryWordsHeader = null;
            byte[] arryInWords = null;
            byte[] arrytransactedBy = null;

            byte[] acknowLedge = null;
            byte[] nameVerify = null;
            byte[] idVerify = null;
            byte[] signature = null;

            byte[] customerReceipt = null;
            byte[] nameAgent = null;
            byte[] motto = null;

            SimpleDateFormat timeStampFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
            Date myDate = new Date();
            String timeCurrent = timeStampFormat.format(myDate);
            String aName = SharedPrefs.read(SharedPrefs.AGENT_NAME, null);

            try {
                start = String.valueOf("              ").getBytes("GB2312");
                saccoName = String.valueOf(SharedPrefs.read(SharedPrefs.SACCO_NAME, null)).getBytes("GB2312");
                type = String.valueOf("        CASH WITHDRAWAL     ").getBytes("GB2312");

                terminalNo = String.valueOf("Terminal ID : " + SharedPrefs.read(SharedPrefs.DEVICE_ID, null)).getBytes("GB2312");
                transaction = String.valueOf("Transaction No : " + transactionCode).getBytes("GB2312");
                time = String.valueOf("Print Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                transDate = String.valueOf("Trans. Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                account_dposited_to = String.valueOf("Account NO : " + accountNumber).getBytes("GB2312");
                idNumber = String.valueOf("ID Number : " + String.valueOf(id_number.getText().toString())).getBytes("GB2312");

                fullNames = String.valueOf("Customer Full Names : ").getBytes("GB2312");
                fName = String.valueOf(String.valueOf(name.getText().toString())).getBytes("GB2312");

                //phoneNumber = String.valueOf("Phone Number : " + String.valueOf(maskedOP)).getBytes("GB2312");
                arryWithdrawAmt = String.valueOf("Amount withdrawn : KSH " + amount).getBytes("GB2312");
                arryWordsHeader = String.valueOf("Amount in Words: ").getBytes("GB2312");
                arryInWords = String.valueOf(return_val_in_english + " Kenya Shillings Only").getBytes("GB2312");
                arryBalance = String.valueOf("Current Balance : KSH " + currentBal).getBytes("GB2312");
                arrytransactedBy = String.valueOf("Transacted By " + transacted_by.getText().toString()).getBytes("GB2312");

                acknowLedge = String.valueOf("I acknowledge receipt of the above amount").getBytes("GB2312");
                nameVerify = String.valueOf("Name___________________________").getBytes("GB2312");
                idVerify = String.valueOf("ID No___________________________").getBytes("GB2312");
                signature = String.valueOf("Signature_______________________").getBytes("GB2312");

                nameAgent = String.valueOf("SERVED BY " + String.valueOf(aName)).getBytes("GB2312");
                customerReceipt = String.valueOf("         CUSTOMER COPY         ").getBytes("GB2312");

                motto = String.valueOf(SharedPrefs.read(SharedPrefs.SACCO_MOTTO, null)).getBytes("GB2312");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(start);
            writeLineBreak(1);
            write(saccoName);
            writeLineBreak(2);
            write(type);
            writeLineBreak(1);

            write(terminalNo);
            writeLineBreak(1);
            write(transaction);
            writeLineBreak(1);
            write(time);
            writeLineBreak(1);
            write(transDate);
            writeLineBreak(1);
            write(account_dposited_to);
            writeLineBreak(1);
            write(idNumber);
            writeLineBreak(1);
            write(fullNames);
            writeLineBreak(0);
            write(fName);
            writeLineBreak(2);

            write(arryWithdrawAmt);
            writeLineBreak(1);
            write(arryWordsHeader);
            writeLineBreak(0);
            write(arryInWords);
            writeLineBreak(1);
            write(arryBalance);
            writeLineBreak(1);
            write(arrytransactedBy);
            writeLineBreak(2);

            write(acknowLedge);
            writeLineBreak(1);
            write(nameVerify);
            writeLineBreak(1);
            write(idVerify);
            writeLineBreak(1);
            write(signature);
            writeLineBreak(2);

            write(nameAgent);
            writeLineBreak(2);
            write(customerReceipt);
            writeLineBreak(2);
            write(motto);
            // print line break
            writeLineBreak(4);

            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void writetest1(String idNo, String amount, String transactionCode, String balance) {
        String accountNumber = SharedPrefs.read(SharedPrefs.MEMBER_ACCOUNT_NUMBER, null);
        String firstAc = accountNumber.substring(0, 4);
        String lastAc = accountNumber.substring(8, accountNumber.length());
        String maskedAc = firstAc + "****" + lastAc;

        int number = Integer.parseInt(amount);

        double bal = Double.parseDouble(balance);
        String currentBal = String.format("%.2f", bal);

        String return_val_in_english = EnglishNumberToWords.convert(number);

        try {

            byte[] start = null;
            byte[] saccoName = null;
            byte[] type = null;

            byte[] terminalNo = null;
            byte[] transaction = null;
            byte[] time = null;
            byte[] transDate = null;
            byte[] account_dposited_to = null;
            byte[] idNumber = null;
            byte[] fullNames = null;
            byte[] fName = null;

            //byte[] phoneNumber = null;
            byte[] arryWithdrawAmt = null;
            byte[] arryBalance = null;
            byte[] arryWordsHeader = null;
            byte[] arryInWords = null;
            byte[] arrytransactedBy = null;

            byte[] acknowLedge = null;
            byte[] nameVerify = null;
            byte[] idVerify = null;
            byte[] signature = null;

            byte[] customerReceipt = null;
            byte[] nameAgent = null;
            byte[] motto = null;

            SimpleDateFormat timeStampFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
             Date myDate = new Date();
            String timeCurrent = timeStampFormat.format(myDate);
            String aName = SharedPrefs.read(SharedPrefs.AGENT_NAME, null);

            try {
                start = String.valueOf("              ").getBytes("GB2312");
                saccoName = String.valueOf(SharedPrefs.read(SharedPrefs.SACCO_NAME, null)).getBytes("GB2312");
                type = String.valueOf("        CASH WITHDRAWAL     ").getBytes("GB2312");

                terminalNo = String.valueOf("Terminal ID : " + SharedPrefs.read(SharedPrefs.DEVICE_ID, null)).getBytes("GB2312");
                transaction = String.valueOf("Transaction No : " + transactionCode).getBytes("GB2312");
                time = String.valueOf("Print Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                transDate = String.valueOf("Trans. Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                account_dposited_to = String.valueOf("Account NO : " + accountNumber).getBytes("GB2312");
                idNumber = String.valueOf("ID Number : " + String.valueOf(id_number.getText().toString())).getBytes("GB2312");

                fullNames = String.valueOf("Customer Full Names : ").getBytes("GB2312");
                fName = String.valueOf(String.valueOf(name.getText().toString())).getBytes("GB2312");

                //phoneNumber = String.valueOf("Phone Number : " + String.valueOf(maskedOP)).getBytes("GB2312");
                arryWithdrawAmt = String.valueOf("Amount withdrawn : KSH " + amount).getBytes("GB2312");
                arryWordsHeader = String.valueOf("Amount in Words: ").getBytes("GB2312");
                arryInWords = String.valueOf(return_val_in_english + " Kenya Shillings Only").getBytes("GB2312");
                arryBalance = String.valueOf("Current Balance : KSH " + currentBal).getBytes("GB2312");
                arrytransactedBy = String.valueOf("Transacted By " + transacted_by.getText().toString()).getBytes("GB2312");

                acknowLedge = String.valueOf("I acknowledge receipt of the above amount").getBytes("GB2312");
                nameVerify = String.valueOf("Name___________________________").getBytes("GB2312");
                idVerify = String.valueOf("ID No___________________________").getBytes("GB2312");
                signature = String.valueOf("Signature_______________________").getBytes("GB2312");

                nameAgent = String.valueOf("SERVED BY " + String.valueOf(aName)).getBytes("GB2312");
                customerReceipt = String.valueOf("         AGENT COPY         ").getBytes("GB2312");

                motto = String.valueOf(SharedPrefs.read(SharedPrefs.SACCO_MOTTO, null)).getBytes("GB2312");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(start);
            writeLineBreak(1);
            write(saccoName);
            writeLineBreak(2);
            write(type);
            writeLineBreak(1);

            write(terminalNo);
            writeLineBreak(1);
            write(transaction);
            writeLineBreak(1);
            write(time);
            writeLineBreak(1);
            write(transDate);
            writeLineBreak(1);
            write(account_dposited_to);
            writeLineBreak(1);
            write(idNumber);
            writeLineBreak(1);
            write(fullNames);
            writeLineBreak(0);
            write(fName);
            writeLineBreak(2);

            write(arryWithdrawAmt);
            writeLineBreak(1);
            write(arryWordsHeader);
            writeLineBreak(0);
            write(arryInWords);
            writeLineBreak(1);
            write(arryBalance);
            writeLineBreak(1);
            write(arrytransactedBy);
            writeLineBreak(2);

            write(acknowLedge);
            writeLineBreak(1);
            write(nameVerify);
            writeLineBreak(1);
            write(idVerify);
            writeLineBreak(1);
            write(signature);
            writeLineBreak(2);

            write(nameAgent);
            writeLineBreak(2);
            write(customerReceipt);
            writeLineBreak(2);
            write(motto);
            // print line break
            writeLineBreak(4);

            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void write(final byte[] arryData) {
        int result = PrinterInterface.write(arryData, arryData.length);
    }

    // print line break
    private void writeLineBreak(int lineNumber) {
        // write(new byte[]{'\n'});
        write(PrinterCommand.getCmdEscDN(lineNumber));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                //Toast.makeText(Account.this, "Back has been presses!", Toast.LENGTH_LONG).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }

}

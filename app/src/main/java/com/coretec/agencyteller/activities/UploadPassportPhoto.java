package com.coretec.agencyteller.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.coretec.agencyteller.R;

/**
 * Created by Jeremy on 18/02/11.
 */

public class UploadPassportPhoto extends AppCompatActivity {

    private CardView card_passport_photo;
    private static final int CAMERA_REQUEST = 1888;
    private ImageView passport_imageView;
    private Button submitPassportPhoto;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.upload_document);


        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

        passport_imageView = (ImageView) findViewById(R.id.imageview_passport);

        card_passport_photo = (CardView) findViewById(R.id.card_passport_photo);
        card_passport_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        });

        submitPassportPhoto = (Button) findViewById(R.id.submitPassportPhoto);
        submitPassportPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(UploadPassportPhoto.this, UploadID.class));
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            passport_imageView.setImageBitmap(photo);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }
}

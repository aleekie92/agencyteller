package com.coretec.agencyteller.activities.teller;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyteller.R;
import com.coretec.agencyteller.api.Api;
import com.coretec.agencyteller.utils.EnglishNumberToWords;
import com.coretec.agencyteller.utils.PreferenceFileKeys;
import com.coretec.agencyteller.utils.SharedPrefs;
import com.coretec.agencyteller.utils.Utils;
import com.coretec.agencyteller.wizarpos.function.printer.PrinterCommand;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class CashReceipt extends AppCompatActivity {

    EditText teller_account;
    private Spinner accountSpinner;
    private ArrayList<String> arrayList = new ArrayList<>();

    private EditText amount, deposited_by, narration;
    RadioButton radio_gl, radio_customer;
    private Button submit;

    private ProgressBar progressBar;
    private ProgressDialog progressDialog;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPrefs.init(getApplicationContext());
        setContentView(R.layout.teller_cash_receipt);

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

        //Toolbar declarations...Reusable
        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView mTitle = toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getText(R.string.cash_receipt));
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);

        teller_account = findViewById(R.id.et_teller_account);
        teller_account.setFocusable(false);
        teller_account.setText(SharedPrefs.read(SharedPrefs.TELLER_ACCOUNT_NUMBER, null));

        radio_gl = findViewById(R.id.radio_gl_account);
        radio_customer = findViewById(R.id.radio_customer_ac);

        accountSpinner = findViewById(R.id.account_spinner);
        progressBar = findViewById(R.id.accountsPB);

        amount = findViewById(R.id.et_amount);
        deposited_by = findViewById(R.id.edittext_deposited_by);
        narration = findViewById(R.id.edittext_narration);
        submit = findViewById(R.id.button_submit);

        getIncomeAccounts();

        radio_gl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                getIncomeAccounts();
            }
        });

        radio_customer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                getIncomeAccounts();
            }
        });

        accountSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //((TextView) parent.getChildAt(0)).setTextColor(Color.GRAY);
                String item = parent.getItemAtPosition(position).toString();
                String text = accountSpinner.getSelectedItem().toString();

                String accountNo = text.substring(0, text.indexOf(":"));
                SharedPrefs.write(SharedPrefs.INCOME_ACCOUNT_NUMBER, accountNo);
                Log.e("INCOME_ACCOUNT_NUMBER", accountNo);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String en_amount = amount.getText().toString();
                String depositor = deposited_by.getText().toString();
                String the_narration = narration.getText().toString();

                if (en_amount.isEmpty()) {
                    amount.setError("Please enter amount!");
                } else if (depositor.isEmpty()) {
                    deposited_by.setError("Please input this field!");
                } else if (the_narration.isEmpty()) {
                    narration.setError("Please input the narration!");
                } else {
                    progressDialog = new ProgressDialog(CashReceipt.this);
                    progressDialog.setMessage("Submitting your request...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    requestPostDeposit();
                    submit.setClickable(false);
                }
            }
        });

    }

    private void getIncomeAccounts () {

        String account_type = null;

        if (radio_gl.isChecked()) {
            account_type = "0";
        } else if (radio_customer.isChecked()) {
            account_type = "1";
        }

        Api.getVolley(this, Api.GetIncomeAccounts,"{\n" +
                "    \"accountType\": \"" + account_type + "\"\n" +
                "}",new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                progressBar.setVisibility(View.GONE);
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));
                        arrayList.clear();
                        accountSpinner = findViewById(R.id.account_spinner);
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(CashReceipt.this, android.R.layout.simple_spinner_item, arrayList);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        accountSpinner.setAdapter(adapter);

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            String code = jsonObject1.getString("code");
                            String desc = jsonObject1.getString("description");
                            String accountBalance = jsonObject1.getString("balance");

                            String incomeDetails = code + ":" + desc;

                            arrayList.add(incomeDetails);

                            //Log.e("INCOME DETAILS", incomeDetails);

                        }
                    } if (jsonObject.getString("error").equalsIgnoreCase("true")){
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(CashReceipt.this, "Could not fetch accounts! Please try again.", Toast.LENGTH_SHORT).show();
                    }

                    accountSpinner = findViewById(R.id.account_spinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(CashReceipt.this, android.R.layout.simple_spinner_item, arrayList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    accountSpinner.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    void requestPostDeposit() {

        Api.getVolley(this, Api.CashDepositTransaction, "{\n" +
                "    \"action\": \"" + 0 + "\",\n" +
                "    \"transactionType\": \"" + 9 + "\",\n" +
                "    \"deviceID\": \"" + SharedPrefs.read(SharedPrefs.DEVICE_ID, null) + "\",\n" +
                "    \"agentCode\": \"" + SharedPrefs.read(SharedPrefs.AGENT_CODE, null) + "\",\n" +
                "    \"amount\": \"" + amount.getText().toString() + "\",\n" +
                "    \"narration\": \"" + narration.getText().toString() + "\",\n" +
                "    \"depositedBy\": \"" + deposited_by.getText().toString() + "\",\n" +
                "    \"sourceAccount\": \"" + SharedPrefs.read(SharedPrefs.TELLER_ACCOUNT_NUMBER, null) + "\",\n" +
                "    \"destinationAccount\": \"" + SharedPrefs.read(SharedPrefs.INCOME_ACCOUNT_NUMBER, null) + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));
                        Toast.makeText(CashReceipt.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                        JSONObject dataObject = jsonObject.getJSONObject("data");

                        final String tCode = dataObject.getString("no");
                        final String baLance = dataObject.getString("balance");

                        int result = PrinterInterface.open();
                        writetest(amount.getText().toString(), tCode);
                        PrinterInterface.close();
                        progressDialog.dismiss();

                        Utils.showAlertDialog(CashReceipt.this, "Success!", jsonObject.getString("message")+" Press Ok to print another Receipt ", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                int result = PrinterInterface.open();
                                writetest1(amount.getText().toString(), tCode);
                                PrinterInterface.close();
                                onBackPressed();
                            }
                        });

                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        progressDialog.dismiss();
                        Toast.makeText(CashReceipt.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                        String msg = jsonObject.getString("message");
                        //final String qualKgs = SharedPrefs.read(SharedPrefs.LOAN_QUALIFIED_KGS, null);
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(CashReceipt.this);
                        builder1.setMessage(msg);
                        builder1.setTitle("Alert!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public void writetest(String the_amount, String transactionCode) {

        int number = Integer.parseInt(the_amount);
        String return_val_in_english = EnglishNumberToWords.convert(number);

        try {

            byte[] start = null;
            byte[] saccoName = null;
            byte[] type = null;

            byte[] terminalNo = null;
            byte[] transaction = null;
            byte[] time = null;
            byte[] transDate = null;
            byte[] account_dposited_to = null;
            byte[] idNumber = null;
            byte[] fullNames = null;
            byte[] fName = null;
            byte[] depositedBy = null;
            byte[] arryNarration = null;

            //byte[] phoneNumber = null;
            byte[] arryDepositAmt = null;
            byte[] arryWordsHeader = null;
            byte[] arryInWords = null;

            byte[] acknowLedge = null;
            byte[] nameVerify = null;
            byte[] idVerify = null;
            byte[] signature = null;

            byte[] nameAgent = null;
            byte[] customerReceipt = null;
            byte[] motto = null;

            SimpleDateFormat timeStampFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
            Date myDate = new Date();
            String timeCurrent = timeStampFormat.format(myDate);
            String aName = SharedPrefs.read(SharedPrefs.AGENT_NAME, null);
            String depositorsName = deposited_by.getText().toString();

            try {
                start = String.valueOf("              ").getBytes("GB2312");
                saccoName = String.valueOf(SharedPrefs.read(SharedPrefs.SACCO_NAME, null)).getBytes("GB2312");
                type = String.valueOf("        CASH RECEIPT     ").getBytes("GB2312");

                terminalNo = String.valueOf("Terminal ID : " + SharedPrefs.read(SharedPrefs.DEVICE_ID, null)).getBytes("GB2312");
                transaction = String.valueOf("Transaction No : " + transactionCode).getBytes("GB2312");
                time = String.valueOf("Print Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                transDate = String.valueOf("Trans. Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                //account_dposited_to = String.valueOf("Account NO : " + accountNumber).getBytes("GB2312");
                //idNumber = String.valueOf("ID Number : " + String.valueOf(idNo)).getBytes("GB2312");
                //fullNames = String.valueOf("Customer Full Names : ").getBytes("GB2312");
                //fName = String.valueOf(String.valueOf(deposited_by.getText().toString())).getBytes("GB2312");

                arryDepositAmt = String.valueOf("Amount : KSH" + the_amount).getBytes("GB2312");
                arryWordsHeader = String.valueOf("Amount in Words: ").getBytes("GB2312");
                arryInWords = String.valueOf(return_val_in_english + " Kenya Shillings Only").getBytes("GB2312");
                depositedBy = String.valueOf("Transacted By " + depositorsName).getBytes("GB2312");
                arryNarration = String.valueOf("Narration : " + narration.getText().toString()).getBytes("GB2312");

                acknowLedge = String.valueOf("I acknowledge payment of the above amount").getBytes("GB2312");
                nameVerify = String.valueOf("Name___________________________").getBytes("GB2312");
                idVerify = String.valueOf("ID No___________________________").getBytes("GB2312");
                signature = String.valueOf("Signature_______________________").getBytes("GB2312");

                nameAgent = String.valueOf("SERVED BY " + String.valueOf(aName)).getBytes("GB2312");
                customerReceipt = String.valueOf("         CUSTOMER COPY         ").getBytes("GB2312");

                motto = String.valueOf(SharedPrefs.read(SharedPrefs.SACCO_MOTTO, null)).getBytes("GB2312");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(start);
            writeLineBreak(1);
            write(saccoName);
            writeLineBreak(2);
            write(type);
            writeLineBreak(1);

            write(terminalNo);
            writeLineBreak(1);
            write(transaction);
            writeLineBreak(1);
            write(time);
            writeLineBreak(1);
            write(transDate);
            writeLineBreak(2);

            write(arryDepositAmt);
            writeLineBreak(1);
            write(arryWordsHeader);
            writeLineBreak(0);
            write(arryInWords);
            writeLineBreak(1);
            write(depositedBy);
            writeLineBreak(1);
            write(arryNarration);
            writeLineBreak(2);

            write(acknowLedge);
            writeLineBreak(1);
            write(nameVerify);
            writeLineBreak(1);
            write(idVerify);
            writeLineBreak(1);
            write(signature);
            writeLineBreak(2);

            write(nameAgent);
            writeLineBreak(2);
            write(customerReceipt);
            writeLineBreak(2);
            write(motto);
            // print line break
            writeLineBreak(4);

            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void writetest1(String the_amount, String transactionCode) {
        int number = Integer.parseInt(the_amount);
        String return_val_in_english = EnglishNumberToWords.convert(number);

        try {

            byte[] start = null;
            byte[] saccoName = null;
            byte[] type = null;

            byte[] terminalNo = null;
            byte[] transaction = null;
            byte[] time = null;
            byte[] transDate = null;
            byte[] account_dposited_to = null;
            byte[] idNumber = null;
            byte[] fullNames = null;
            byte[] fName = null;
            byte[] depositedBy = null;
            byte[] arryNarration = null;

            //byte[] phoneNumber = null;
            byte[] arryDepositAmt = null;
            byte[] arryWordsHeader = null;
            byte[] arryInWords = null;

            byte[] acknowLedge = null;
            byte[] nameVerify = null;
            byte[] idVerify = null;
            byte[] signature = null;

            byte[] nameAgent = null;
            byte[] customerReceipt = null;
            byte[] motto = null;

            SimpleDateFormat timeStampFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
            Date myDate = new Date();
            String timeCurrent = timeStampFormat.format(myDate);
            String aName = SharedPrefs.read(SharedPrefs.AGENT_NAME, null);
            String depositorsName = deposited_by.getText().toString();

            try {
                start = String.valueOf("              ").getBytes("GB2312");
                saccoName = String.valueOf(SharedPrefs.read(SharedPrefs.SACCO_NAME, null)).getBytes("GB2312");
                type = String.valueOf("        CASH RECEIPT     ").getBytes("GB2312");

                terminalNo = String.valueOf("Terminal ID : " + SharedPrefs.read(SharedPrefs.DEVICE_ID, null)).getBytes("GB2312");
                transaction = String.valueOf("Transaction No : " + transactionCode).getBytes("GB2312");
                time = String.valueOf("Print Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                transDate = String.valueOf("Trans. Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                //account_dposited_to = String.valueOf("Account NO : " + accountNumber).getBytes("GB2312");
                //idNumber = String.valueOf("ID Number : " + String.valueOf(idNo)).getBytes("GB2312");
                //fullNames = String.valueOf("Customer Full Names : ").getBytes("GB2312");
                //fName = String.valueOf(String.valueOf(deposited_by.getText().toString())).getBytes("GB2312");

                arryDepositAmt = String.valueOf("Amount : KSH" + the_amount).getBytes("GB2312");
                arryWordsHeader = String.valueOf("Amount in Words: ").getBytes("GB2312");
                arryInWords = String.valueOf(return_val_in_english + " Kenya Shillings Only").getBytes("GB2312");
                depositedBy = String.valueOf("Transacted By " + depositorsName).getBytes("GB2312");
                arryNarration = String.valueOf("Narration : " + narration.getText().toString()).getBytes("GB2312");

                acknowLedge = String.valueOf("I acknowledge payment of the above amount").getBytes("GB2312");
                nameVerify = String.valueOf("Name___________________________").getBytes("GB2312");
                idVerify = String.valueOf("ID No___________________________").getBytes("GB2312");
                signature = String.valueOf("Signature_______________________").getBytes("GB2312");

                nameAgent = String.valueOf("SERVED BY " + String.valueOf(aName)).getBytes("GB2312");
                customerReceipt = String.valueOf("         AGENT COPY         ").getBytes("GB2312");

                motto = String.valueOf(SharedPrefs.read(SharedPrefs.SACCO_MOTTO, null)).getBytes("GB2312");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(start);
            writeLineBreak(1);
            write(saccoName);
            writeLineBreak(2);
            write(type);
            writeLineBreak(1);

            write(terminalNo);
            writeLineBreak(1);
            write(transaction);
            writeLineBreak(1);
            write(time);
            writeLineBreak(1);
            write(transDate);
            writeLineBreak(2);

            write(arryDepositAmt);
            writeLineBreak(1);
            write(arryWordsHeader);
            writeLineBreak(0);
            write(arryInWords);
            writeLineBreak(1);
            write(depositedBy);
            writeLineBreak(1);
            write(arryNarration);
            writeLineBreak(2);

            write(acknowLedge);
            writeLineBreak(1);
            write(nameVerify);
            writeLineBreak(1);
            write(idVerify);
            writeLineBreak(1);
            write(signature);
            writeLineBreak(2);

            write(nameAgent);
            writeLineBreak(2);
            write(customerReceipt);
            writeLineBreak(2);
            write(motto);
            // print line break
            writeLineBreak(4);

            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void write(final byte[] arryData) {
        int result = PrinterInterface.write(arryData, arryData.length);
    }

    // print line break
    private void writeLineBreak(int lineNumber) {
        // write(new byte[]{'\n'});
        write(PrinterCommand.getCmdEscDN(lineNumber));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                //Toast.makeText(Account.this, "Back has been presses!", Toast.LENGTH_LONG).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }

}

package com.coretec.agencyteller.activities.receipts;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.TextView;

import com.coretec.agencyteller.R;
import com.coretec.agencyteller.api.responses.WithdrawalResponse;
import com.coretec.agencyteller.utils.Utils;
import com.coretec.agencyteller.wizarpos.function.ActionCallbackImpl;
import com.coretec.agencyteller.wizarpos.mvc.base.ActionCallback;
import com.coretec.agencyteller.wizarpos.mvc.base.ActionManager;

import java.util.HashMap;
import java.util.Map;

public class WithdrawalReceipt extends AppCompatActivity {
    public static final String WITHDRAWAL_RESPONSE_EXTRA = "WITHDRAWAL_RESPONSE_EXTRA";
    private AppCompatButton printWithdrawalReceipt;
    public static Handler handler;
    public static ActionCallback callback;
    private TextView withdrawal_transaction_date, withdrawal_terminal_id, withdrawal_receipt_number, withdrawal_customer_name;
    private TextView withdrawal_amount, withdrawal_sacco_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.withdrawal_receipt);

        withdrawal_transaction_date = (TextView) findViewById(R.id.withdrawal_transaction_date);
        withdrawal_terminal_id = (TextView) findViewById(R.id.withdrawal_terminal_id);
        withdrawal_receipt_number = (TextView) findViewById(R.id.withdrawal_receipt_number);
        withdrawal_customer_name = (TextView) findViewById(R.id.withdrawal_customer_name);
        withdrawal_amount = (TextView) findViewById(R.id.withdrawal_amount);
        withdrawal_sacco_name = (TextView) findViewById(R.id.withdrawal_sacco_name);

        WithdrawalResponse withdrawalResponse = (WithdrawalResponse) getIntent().getSerializableExtra(WITHDRAWAL_RESPONSE_EXTRA);

        withdrawal_transaction_date.setText(withdrawalResponse.getTransactiondate());
        withdrawal_terminal_id.setText(Utils.getIMEI(WithdrawalReceipt.this));
        withdrawal_receipt_number.setText(withdrawalResponse.getReceiptno());
        withdrawal_customer_name.setText(withdrawalResponse.getCustomername());
        withdrawal_amount.setText(" KES " + withdrawalResponse.getAmount());
        withdrawal_sacco_name.setText(withdrawalResponse.getSacconame());

        handler = new Handler();
        callback = new ActionCallbackImpl(handler);

        final Map<String, Object> params = new HashMap<>();

        params.put("withdrawal", withdrawalResponse);

        printWithdrawalReceipt = (AppCompatButton) findViewById(R.id.btn_print_withdrawal_receipt);
        printWithdrawalReceipt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (ActionManager.doSubmit("WithdrawalPrinterAction");
                ActionManager.doSubmit("WithdrawalPrinterAction/open", WithdrawalReceipt.this, params, callback);
                ActionManager.doSubmit("WithdrawalPrinterAction/write", WithdrawalReceipt.this, params, callback);
                ActionManager.doSubmit("WithdrawalPrinterAction/close", WithdrawalReceipt.this, params, callback);
            }
        });

    }
}

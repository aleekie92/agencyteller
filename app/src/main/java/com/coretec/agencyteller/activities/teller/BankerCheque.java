package com.coretec.agencyteller.activities.teller;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyteller.R;
import com.coretec.agencyteller.api.Api;
import com.coretec.agencyteller.utils.EnglishNumberToWords;
import com.coretec.agencyteller.utils.PreferenceFileKeys;
import com.coretec.agencyteller.utils.SharedPrefs;
import com.coretec.agencyteller.utils.Utils;
import com.coretec.agencyteller.wizarpos.function.printer.PrinterCommand;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BankerCheque extends AppCompatActivity {

    private Spinner chequeSpinner;
    private ArrayList<String> chequeList = new ArrayList<>();

    RadioButton radio_cash, radio_account;
    LinearLayout linearAccount;

    private EditText amount;
    private EditText identifier;
    private EditText payee;
    private Spinner identifierSpinner;
    private Spinner accountSpinner;
    private ArrayList<String> arrayList = new ArrayList<>();

    TextInputLayout input_applicant_name, input_transacted_by;
    EditText transacted_by, applicant_name;

    private Button submit, btn_view;
    private Button search;
    private ImageView picture;
    private ImageView signature;
    LinearLayout pic_layout;
    LinearLayout customer_details, signatory_details;
    TextView name, book_balance, balance, info_base_area, signatories;

    ProgressBar chequePb;
    private ProgressBar progressBar;
    private ProgressDialog progressDialog;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPrefs.init(getApplicationContext());
        setContentView(R.layout.teller_banker_cheque);

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

        //Toolbar declarations...Reusable
        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView mTitle = toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getText(R.string.banker_cheque));
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);

        radio_cash = findViewById(R.id.radio_cash);
        radio_account = findViewById(R.id.radio_account);
        linearAccount = findViewById(R.id.linear_account);

        input_applicant_name = findViewById(R.id.input_applicant_name);
        input_transacted_by = findViewById(R.id.input_transacted_by);
        applicant_name = findViewById(R.id.et_applicant_name);
        transacted_by = findViewById(R.id.et_transacted_by);

        //Beginning Signatories declarations
        identifierSpinner = findViewById(R.id.identifier_spinner);
        accountSpinner = findViewById(R.id.accountsSpinner);
        identifier = findViewById(R.id.withdrawal_identifier);
        amount = findViewById(R.id.et_amount);
        payee = findViewById(R.id.et_payee);
        progressBar = findViewById(R.id.accountsPB);
        submit = findViewById(R.id.button_submit);
        search = findViewById(R.id.btn_search);

        customer_details = findViewById(R.id.linear_details);
        signatory_details = findViewById(R.id.linear_signatories);
        name = findViewById(R.id.tv_name);
        balance = findViewById(R.id.tv_balance);
        book_balance = findViewById(R.id.tv_book_balance);
        info_base_area = findViewById(R.id.tv_info_base_area);
        signatories = findViewById(R.id.tv_signatories);
        btn_view = findViewById(R.id.btn_view);
        //End of Signatories declarations

        pic_layout = findViewById(R.id.pictures_layout);
        picture = findViewById(R.id.picture_imageview);
        signature = findViewById(R.id.signature_imageview);

        chequeSpinner = findViewById(R.id.spinner_cheque_no);
        chequePb = findViewById(R.id.chq_progress_bar);

        amount.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(6,2)});

        chequePb.setVisibility(View.VISIBLE);
        getChequeNos();

        radio_cash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearAccount.setVisibility(View.GONE);
                input_applicant_name.setVisibility(View.VISIBLE);
                input_transacted_by.setVisibility(View.GONE);
            }
        });

        radio_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearAccount.setVisibility(View.VISIBLE);
                input_applicant_name.setVisibility(View.GONE);
                input_transacted_by.setVisibility(View.VISIBLE);
            }
        });

        chequeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                String text = chequeSpinner.getSelectedItem().toString();

                String cheqNo = text.substring(0, text.indexOf("-"));
                SharedPrefs.write(SharedPrefs.CHEQUE_NUMBER, cheqNo);
                Log.e("CHEQUE NO", cheqNo);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Signatories Implementations beginning
        accountSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                String text = accountSpinner.getSelectedItem().toString();

                String account_number = text.substring(0, text.indexOf("-"));
                SharedPrefs.write(SharedPrefs.MEMBER_ACCOUNT_NUMBER, account_number);
                Log.e("MEMBER ACCOUNT", account_number);

                switch (text) {
                    case "select-account":
                        break;
                    default:
                        requestBalance(account_number);
                        checkSignatories(account_number);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        identifierSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                String item = parent.getItemAtPosition(position).toString();
                String text = identifierSpinner.getSelectedItem().toString();

                int idLength = 8;
                int acLength = 14;
                int memLength = 14;
                InputFilter[] fArray = new InputFilter[1];
                fArray[0] = new InputFilter.LengthFilter(idLength);

                InputFilter[] aArray = new InputFilter[1];
                aArray[0] = new InputFilter.LengthFilter(acLength);

                InputFilter[] mArray = new InputFilter[1];
                mArray[0] = new InputFilter.LengthFilter(memLength);

                if (text.equals("ID NUMBER")) {
                    SharedPrefs.write(SharedPrefs.IDENTIFIER, "0");
                    identifier.setText(null);
                    identifier.setInputType(InputType.TYPE_CLASS_NUMBER);
                    identifier.setFilters(fArray);
                } else if (text.equals("OLD A/C NUMBER")) {
                    SharedPrefs.write(SharedPrefs.IDENTIFIER, "4");
                    identifier.setText(null);
                    identifier.setInputType(InputType.TYPE_CLASS_TEXT);
                    identifier.setFilters(aArray);
                } else if (text.equals("MEMBER NUMBER")) {
                    identifier.setText(null);
                    identifier.setInputType(InputType.TYPE_CLASS_NUMBER);
                    SharedPrefs.write(SharedPrefs.IDENTIFIER, "1");
                    identifier.setFilters(mArray);
                }

                Log.e("SELECTED", text);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = identifier.getText().toString();

                if (id.length()<7 && id.length()>0) {
                    identifier.setError("Please input a valid identifier!");
                } else if (id.length()==0){
                    identifier.setError("Cannot search an empty field!");
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(BankerCheque.this, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                    requestAccounts(id);
                    getPicture(id);
                    getSignature(id);
                }
            }
        });

        TextWatcher textWatcher = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String idNumber = identifier.getText().toString();

                /*if (idNumber.length() >= 7) {
                    requestAccounts(idNumber);
                    getPicture(idNumber);
                    getSignature(idNumber);
                }*/

                if (idNumber.length() >= 5 && idNumber.length() <= 8) {

                    arrayList.clear();
                    accountSpinner = findViewById(R.id.accountsSpinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(BankerCheque.this, android.R.layout.simple_spinner_item, arrayList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    accountSpinner.setAdapter(adapter);
                    pic_layout.setVisibility(View.GONE);
                    customer_details.setVisibility(View.GONE);
                    signatory_details.setVisibility(View.GONE);

                }
                if (idNumber.length() < 5) {
                    progressBar.setVisibility(View.GONE);
                }

            }
        };

        identifier.addTextChangedListener(textWatcher);

        btn_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BankerCheque.this, DialogSignatories.class));
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String en_amount = amount.getText().toString();
                String pay_ee = payee.getText().toString();
                String app_name = applicant_name.getText().toString();
                String trans_by = transacted_by.getText().toString();

                if (radio_cash.isChecked()) {
                    if (en_amount.isEmpty()) {
                        amount.setError("Please enter amount!");
                    } else if (app_name.isEmpty()) {
                        applicant_name.setError("Please enter the applicant name!");
                    } else if (pay_ee.isEmpty()) {
                        payee.setError("Please enter the payee!");
                    } else {
                        progressDialog = new ProgressDialog(BankerCheque.this);
                        progressDialog.setMessage("Submitting your request...");
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                        submitBankerChequeCash();
                        submit.setClickable(false);
                    }

                } else if (radio_account.isChecked()) {
                    if (en_amount.isEmpty()) {
                        amount.setError("Please enter amount!");
                    } else if (pay_ee.isEmpty()) {
                        payee.setError("Please enter the payee!");
                    } else if (trans_by.isEmpty()) {
                        transacted_by.setError("Please enter this field");
                    } else {
                        progressDialog = new ProgressDialog(BankerCheque.this);
                        progressDialog.setMessage("Submitting your request...");
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                        submitBankerCheque();
                        submit.setClickable(false);
                    }
                }

            }
        });

    }

    public class DecimalDigitsInputFilter implements InputFilter {

        Pattern mPattern;

        public DecimalDigitsInputFilter(int digitsBeforeZero,int digitsAfterZero) {
            mPattern= Pattern.compile("[0-9]{0," + (digitsBeforeZero-1) + "}+((\\.[0-9]{0," + (digitsAfterZero-1) + "})?)||(\\.)?");
        }

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            Matcher matcher=mPattern.matcher(dest);
            if(!matcher.matches())
                return "";
            return null;
        }

    }

    public Bitmap StringToBitMap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    private void getChequeNos () {

        Api.getVolley(this, Api.GetChequeNos,"{\n" +
                "    \"agentCode\": \"" + SharedPrefs.read(SharedPrefs.AGENT_CODE, null) + "\"\n" +
                "}",new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                chequePb.setVisibility(View.GONE);
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            String chequeNo = jsonObject1.getString("no");
                            String chequeBank = jsonObject1.getString("bank");
                            String chequeLimit = jsonObject1.getString("limit");
                            String chqDetails = chequeNo + "-" + chequeBank;

                            chequeList.add(chqDetails);

                            Log.e("CHEQUE DETAILS", chqDetails);

                        }
                    } if (jsonObject.getString("error").equalsIgnoreCase("true")){
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        chequePb.setVisibility(View.GONE);
                        Toast.makeText(BankerCheque.this, "Could not fetch cheque numbers!", Toast.LENGTH_SHORT).show();
                    }

                    chequeSpinner = findViewById(R.id.spinner_cheque_no);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(BankerCheque.this, android.R.layout.simple_spinner_item, chequeList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    chequeSpinner.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    //Signatories API implementations
    private void requestAccounts(String id) {

        int identifier = Integer.parseInt(SharedPrefs.read(SharedPrefs.IDENTIFIER, null));

        Api.getVolley(this, Api.GetMemberAccounts, "{\n" +
                "    \"identifier\": \"" + id + "\",\n" +
                "    \"identifierType\": \"" + identifier + "\",\n" +
                "    \"natureOfAccount\": \"" + 1 + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                progressBar.setVisibility(View.GONE);
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));
                        arrayList.clear();
                        accountSpinner = findViewById(R.id.accountsSpinner);
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(BankerCheque.this, android.R.layout.simple_spinner_item, arrayList);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        accountSpinner.setAdapter(adapter);

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            String accountName = jsonObject1.getString("name");
                            String accountNo = jsonObject1.getString("no");
                            String accountBalance = jsonObject1.getString("balance");
                            String desc = jsonObject1.getString("description");
                            String infoBase = jsonObject1.getString("infoBaseArea");
                            String accountDetails = accountNo + "-" + desc;

                            SharedPrefs.write(SharedPrefs.ACC_NAME, accountName);

                            arrayList.add(accountDetails);

                            Log.e("ACCOUNT DETAILS", accountDetails);

                        }
                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(BankerCheque.this, "Could not fetch accounts! Please try again.", Toast.LENGTH_SHORT).show();
                    }

                    accountSpinner = findViewById(R.id.accountsSpinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(BankerCheque.this, android.R.layout.simple_spinner_item, arrayList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    accountSpinner.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void getPicture(String id) {

        int identifier = Integer.parseInt(SharedPrefs.read(SharedPrefs.IDENTIFIER, null));

        Api.getVolley(this, Api.GetMemberPictures, "{\n" +
                "    \"identifier\": \"" + id + "\",\n" +
                "    \"identifierType\": \"" + identifier + "\",\n" +
                "    \"fileType\": \"" + 0 + "\"\n" +
                "  }\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                progressBar.setVisibility(View.GONE);
                Log.e("RESULTS", Response);

                /*if (Response.length() == 71) {
                    picture.setImageDrawable(ContextCompat.getDrawable(CashWithdrawal.this, R.drawable.no_image));
                    pic_layout.setVisibility(View.VISIBLE);
                } else {

                    Bitmap decodeTxt = StringToBitMap(Response);

                    picture.setImageBitmap(decodeTxt);
                    pic_layout.setVisibility(View.VISIBLE);
                }*/

                Bitmap decodeTxt = StringToBitMap(Response);

                picture.setImageBitmap(decodeTxt);
                pic_layout.setVisibility(View.VISIBLE);
            }
        });

    }

    private void getSignature(String id) {

        int identifier = Integer.parseInt(SharedPrefs.read(SharedPrefs.IDENTIFIER, null));

        Api.getVolley(this, Api.GetMemberPictures, "{\n" +
                "    \"identifier\": \"" + id + "\",\n" +
                "    \"identifierType\": \"" + identifier + "\",\n" +
                "    \"fileType\": \"" + 1 + "\"\n" +
                "  }\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                progressBar.setVisibility(View.GONE);
                Log.e("RESULTS", Response);

                /*if (Response.equals("{\"error\":true,\"message\":\"Member Signature Not Found\",\"data\":null}")) {
                    signature.setImageDrawable(ContextCompat.getDrawable(CashWithdrawal.this, R.drawable.no_image));
                    pic_layout.setVisibility(View.VISIBLE);
                } else {

                    Bitmap decodeTxt = StringToBitMap(Response);

                    signature.setImageBitmap(decodeTxt);
                    pic_layout.setVisibility(View.VISIBLE);
                }*/

                Bitmap decodeTxt = StringToBitMap(Response);

                signature.setImageBitmap(decodeTxt);
                pic_layout.setVisibility(View.VISIBLE);
            }
        });

    }

    private void checkSignatories(String accountNumber) {

        Api.getVolley(this, Api.GetAccountSignatories, "{\n" +
                "    \"accountType\": \"" + 1 + "\",\n" +
                "    \"accountNo\": \"" + accountNumber + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                progressBar.setVisibility(View.GONE);
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        signatory_details.setVisibility(View.VISIBLE);
                        signatories.setText("Signatories found, press button below to view");
                        btn_view.setVisibility(View.VISIBLE);
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));

                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        signatory_details.setVisibility(View.VISIBLE);
                        signatories.setText(jsonObject.getString("message"));
                        btn_view.setVisibility(View.GONE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void requestBalance(String account_no) {

        Api.getVolley(this, Api.GetMemberAccounts, "{\n" +
                "    \"identifier\": \"" + account_no + "\",\n" +
                "    \"identifierType\": \"" + 3 + "\",\n" +
                "    \"natureOfAccount\": \"" + 1 + "\"\n" +
                "  }\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                progressBar.setVisibility(View.GONE);
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {

                        customer_details.setVisibility(View.VISIBLE);

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            JSONObject balanceObject = jsonObject1.getJSONObject("balance");

                            String accountName = jsonObject1.getString("name");
                            String accountNo = jsonObject1.getString("no");
                            String accountBalance = balanceObject.getString("available");
                            String bookBalance = balanceObject.getString("actual");
                            String desc = jsonObject1.getString("description");
                            String infoBase = jsonObject1.getString("infoBaseArea");
                            //SharedPrefs.write(SharedPrefs.ACC_NAME, accountName);

                            name.setText(accountName);
                            balance.setText(accountBalance);
                            book_balance.setText(bookBalance);

                            if (infoBase.equals("null")) {
                                info_base_area.setText("");
                            } else {
                                info_base_area.setText(infoBase);
                            }

                        }
                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(BankerCheque.this, "Could not fetch data! Please try again.", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    void submitBankerChequeCash () {
        final String accountNumber = SharedPrefs.read(SharedPrefs.TELLER_ACCOUNT_NUMBER, null);
        Api.getVolley(this, Api.BankersChequeTransaction, "{\n" +
                "    \"action\": \"" + 0 + "\",\n" +
                "    \"transactionType\": \"" + 14 + "\",\n" +
                "    \"deviceID\": \"" + SharedPrefs.read(SharedPrefs.DEVICE_ID, null) + "\",\n" +
                "    \"agentCode\": \"" + SharedPrefs.read(SharedPrefs.AGENT_CODE, null) + "\",\n" +
                "    \"amount\": \"" + amount.getText().toString() + "\",\n" +
                "    \"chequeNo\": \"" + SharedPrefs.read(SharedPrefs.CHEQUE_NUMBER, null) + "\",\n" +
                "    \"account\": \"" + accountNumber + "\",\n" +
                "    \"payee\": \"" + payee.getText().toString() + "\",\n" +
                "    \"applicant\": \"" + applicant_name.getText().toString() + "\",\n" +
                "    \"transactedBy\": \"" + null + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));
                        Toast.makeText(BankerCheque.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                        JSONObject dataObject = jsonObject.getJSONObject("data");

                        final String tCode = dataObject.getString("no");
                        final String baLance = dataObject.getString("balance");

                        int result = PrinterInterface.open();
                        writeCash(accountNumber,tCode);
                        PrinterInterface.close();
                        progressDialog.dismiss();

                        Utils.showAlertDialog(BankerCheque.this, "Success!", jsonObject.getString("message")+" Press OK to print another receipt", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                int result = PrinterInterface.open();
                                writeCash1(accountNumber, tCode);
                                PrinterInterface.close();
                                onBackPressed();
                            }
                        });

                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        progressDialog.dismiss();
                        Toast.makeText(BankerCheque.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                        String msg = jsonObject.getString("message");
                        //final String qualKgs = SharedPrefs.read(SharedPrefs.LOAN_QUALIFIED_KGS, null);
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(BankerCheque.this);
                        builder1.setMessage(msg);
                        builder1.setTitle("Alert!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    void submitBankerCheque () {
        String accountNumber = SharedPrefs.read(SharedPrefs.MEMBER_ACCOUNT_NUMBER, null);

        final String finalAccountNumber = accountNumber;
        Api.getVolley(this, Api.BankersChequeTransaction, "{\n" +
                "    \"action\": \"" + 0 + "\",\n" +
                "    \"transactionType\": \"" + 14 + "\",\n" +
                "    \"deviceID\": \"" + SharedPrefs.read(SharedPrefs.DEVICE_ID, null) + "\",\n" +
                "    \"agentCode\": \"" + SharedPrefs.read(SharedPrefs.AGENT_CODE, null) + "\",\n" +
                "    \"amount\": \"" + amount.getText().toString() + "\",\n" +
                "    \"chequeNo\": \"" + SharedPrefs.read(SharedPrefs.CHEQUE_NUMBER, null) + "\",\n" +
                "    \"account\": \"" + accountNumber + "\",\n" +
                "    \"payee\": \"" + payee.getText().toString() + "\",\n" +
                "    \"applicant\": \"" + name.getText().toString() + "\",\n" +
                "    \"transactedBy\": \"" + transacted_by.getText().toString() + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));
                        Toast.makeText(BankerCheque.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                        JSONObject dataObject = jsonObject.getJSONObject("data");

                        final String tCode = dataObject.getString("no");
                        final String baLance = dataObject.getString("balance");

                        int result = PrinterInterface.open();
                        writetest(finalAccountNumber,tCode);
                        PrinterInterface.close();
                        progressDialog.dismiss();

                        Utils.showAlertDialog(BankerCheque.this, "Success!", jsonObject.getString("message")+" Press OK to print another receipt", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                int result = PrinterInterface.open();
                                writetest1(finalAccountNumber, tCode);
                                PrinterInterface.close();
                                onBackPressed();
                            }
                        });

                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        progressDialog.dismiss();
                        Toast.makeText(BankerCheque.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                        String msg = jsonObject.getString("message");
                        //final String qualKgs = SharedPrefs.read(SharedPrefs.LOAN_QUALIFIED_KGS, null);
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(BankerCheque.this);
                        builder1.setMessage(msg);
                        builder1.setTitle("Alert!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public void writeCash(String account_number, String transactionCode) {
        String the_amount = amount.getText().toString();
        String return_val_in_english;

        if (the_amount.contains(".")) {
            String firstPart = the_amount.substring(0, the_amount.indexOf("."));

            String decimalPart = the_amount.substring(the_amount.lastIndexOf(".") + 1);

            int wholeNo = Integer.parseInt(firstPart);
            int cents = Integer.parseInt(decimalPart);

            String whole_value = EnglishNumberToWords.convert(wholeNo);
            String cent_value = EnglishNumberToWords.convert(cents);
            return_val_in_english = whole_value + " Kenya Shillings and " + cent_value + " cents";

        } else {

            int number = Integer.parseInt(the_amount);

            return_val_in_english = EnglishNumberToWords.convert(number);

        }

        try {

            byte[] start = null;
            byte[] saccoName = null;
            byte[] type = null;

            byte[] terminalNo = null;
            byte[] transaction = null;
            byte[] time = null;
            byte[] transDate = null;
            //byte[] account_dposited_to = null;
            byte[] idNumber = null;
            byte[] fullNames = null;
            byte[] fName = null;
            byte[] arryChqNo = null;
            byte[] arryPayee = null;

            //byte[] phoneNumber = null;
            byte[] arryWithdrawAmt = null;
            byte[] arryBalance = null;
            byte[] arryWordsHeader = null;
            byte[] arryInWords = null;

            byte[] acknowLedge = null;
            byte[] nameVerify = null;
            byte[] idVerify = null;
            byte[] signature = null;

            byte[] customerReceipt = null;
            byte[] nameAgent = null;
            byte[] motto = null;

            SimpleDateFormat timeStampFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
            Date myDate = new Date();
            String timeCurrent = timeStampFormat.format(myDate);
            String aName = SharedPrefs.read(SharedPrefs.AGENT_NAME, null);
            String cheque_no = SharedPrefs.read(SharedPrefs.CHEQUE_NUMBER, null);

            try {
                start = String.valueOf("              ").getBytes("GB2312");
                saccoName = String.valueOf(SharedPrefs.read(SharedPrefs.SACCO_NAME, null)).getBytes("GB2312");
                type = String.valueOf("        BANKER CHEQUE     ").getBytes("GB2312");

                terminalNo = String.valueOf("Terminal ID : " + SharedPrefs.read(SharedPrefs.DEVICE_ID, null)).getBytes("GB2312");
                transaction = String.valueOf("Transaction No : " + transactionCode).getBytes("GB2312");
                time = String.valueOf("Print Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                transDate = String.valueOf("Trans. Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                //account_dposited_to = String.valueOf("Account NO : " + account_number).getBytes("GB2312");

                fullNames = String.valueOf("Applicant Names : ").getBytes("GB2312");
                fName = String.valueOf(applicant_name.getText().toString()).getBytes("GB2312");
                arryChqNo = String.valueOf("Cheque Number : " + cheque_no).getBytes("GB2312");
                arryPayee = String.valueOf("Payee :" + payee.getText().toString()).getBytes("GB2312");
                //phoneNumber = String.valueOf("Phone Number : " + String.valueOf(maskedOP)).getBytes("GB2312");
                arryWithdrawAmt = String.valueOf("Amount : KSH " + the_amount).getBytes("GB2312");
                arryWordsHeader = String.valueOf("Amount in Words: ").getBytes("GB2312");
                arryInWords = String.valueOf(return_val_in_english).getBytes("GB2312");
                //depositedBy = String.valueOf("Deposited By " + depositorsName).getBytes("GB2312");

                acknowLedge = String.valueOf("I acknowledge receipt of the banker cheque of the above amount").getBytes("GB2312");
                nameVerify = String.valueOf("Name___________________________").getBytes("GB2312");
                idVerify = String.valueOf("ID No___________________________").getBytes("GB2312");
                signature = String.valueOf("Signature_______________________").getBytes("GB2312");

                nameAgent = String.valueOf("SERVED BY " + String.valueOf(aName)).getBytes("GB2312");
                customerReceipt = String.valueOf("         CUSTOMER COPY         ").getBytes("GB2312");

                motto = String.valueOf(SharedPrefs.read(SharedPrefs.SACCO_MOTTO, null)).getBytes("GB2312");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(start);
            writeLineBreak(1);
            write(saccoName);
            writeLineBreak(2);
            write(type);
            writeLineBreak(1);

            write(terminalNo);
            writeLineBreak(1);
            write(transaction);
            writeLineBreak(1);
            write(time);
            writeLineBreak(1);
            write(transDate);
            writeLineBreak(1);
            write(fullNames);
            writeLineBreak(0);
            write(fName);
            writeLineBreak(1);
            write(arryChqNo);
            writeLineBreak(1);
            write(arryPayee);
            writeLineBreak(2);

            write(arryWithdrawAmt);
            writeLineBreak(1);
            write(arryWordsHeader);
            writeLineBreak(0);
            write(arryInWords);
            writeLineBreak(2);

            write(acknowLedge);
            writeLineBreak(1);
            write(nameVerify);
            writeLineBreak(1);
            write(idVerify);
            writeLineBreak(1);
            write(signature);
            writeLineBreak(2);

            write(nameAgent);
            writeLineBreak(2);
            write(customerReceipt);
            writeLineBreak(2);
            write(motto);
            // print line break
            writeLineBreak(4);

            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void writeCash1(String account_number, String transactionCode) {
        String the_amount = amount.getText().toString();
        String return_val_in_english;

        if (the_amount.contains(".")) {
            String firstPart = the_amount.substring(0, the_amount.indexOf("."));

            String decimalPart = the_amount.substring(the_amount.lastIndexOf(".") + 1);

            int wholeNo = Integer.parseInt(firstPart);
            int cents = Integer.parseInt(decimalPart);

            String whole_value = EnglishNumberToWords.convert(wholeNo);
            String cent_value = EnglishNumberToWords.convert(cents);
            return_val_in_english = whole_value + " Kenya Shillings and " + cent_value + " cents";

        } else {

            int number = Integer.parseInt(the_amount);

            return_val_in_english = EnglishNumberToWords.convert(number);

        }

        try {

            byte[] start = null;
            byte[] saccoName = null;
            byte[] type = null;

            byte[] terminalNo = null;
            byte[] transaction = null;
            byte[] time = null;
            byte[] transDate = null;
            //byte[] account_dposited_to = null;
            byte[] idNumber = null;
            byte[] fullNames = null;
            byte[] fName = null;
            byte[] arryChqNo = null;
            byte[] arryPayee = null;

            //byte[] phoneNumber = null;
            byte[] arryWithdrawAmt = null;
            byte[] arryBalance = null;
            byte[] arryWordsHeader = null;
            byte[] arryInWords = null;

            byte[] acknowLedge = null;
            byte[] nameVerify = null;
            byte[] idVerify = null;
            byte[] signature = null;

            byte[] customerReceipt = null;
            byte[] nameAgent = null;
            byte[] motto = null;

            SimpleDateFormat timeStampFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
            Date myDate = new Date();
            String timeCurrent = timeStampFormat.format(myDate);
            String aName = SharedPrefs.read(SharedPrefs.AGENT_NAME, null);
            String cheque_no = SharedPrefs.read(SharedPrefs.CHEQUE_NUMBER, null);

            try {
                start = String.valueOf("              ").getBytes("GB2312");
                saccoName = String.valueOf(SharedPrefs.read(SharedPrefs.SACCO_NAME, null)).getBytes("GB2312");
                type = String.valueOf("        BANKER CHEQUE     ").getBytes("GB2312");

                terminalNo = String.valueOf("Terminal ID : " + SharedPrefs.read(SharedPrefs.DEVICE_ID, null)).getBytes("GB2312");
                transaction = String.valueOf("Transaction No : " + transactionCode).getBytes("GB2312");
                time = String.valueOf("Print Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                transDate = String.valueOf("Trans. Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                //account_dposited_to = String.valueOf("Account NO : " + account_number).getBytes("GB2312");

                fullNames = String.valueOf("Applicant Names : ").getBytes("GB2312");
                fName = String.valueOf(applicant_name.getText().toString()).getBytes("GB2312");
                arryChqNo = String.valueOf("Cheque Number : " + cheque_no).getBytes("GB2312");
                arryPayee = String.valueOf("Payee :" + payee.getText().toString()).getBytes("GB2312");
                //phoneNumber = String.valueOf("Phone Number : " + String.valueOf(maskedOP)).getBytes("GB2312");
                arryWithdrawAmt = String.valueOf("Amount : KSH " + the_amount).getBytes("GB2312");
                arryWordsHeader = String.valueOf("Amount in Words: ").getBytes("GB2312");
                arryInWords = String.valueOf(return_val_in_english).getBytes("GB2312");
                //depositedBy = String.valueOf("Deposited By " + depositorsName).getBytes("GB2312");

                acknowLedge = String.valueOf("I acknowledge receipt of the banker cheque of the above amount").getBytes("GB2312");
                nameVerify = String.valueOf("Name___________________________").getBytes("GB2312");
                idVerify = String.valueOf("ID No___________________________").getBytes("GB2312");
                signature = String.valueOf("Signature_______________________").getBytes("GB2312");

                nameAgent = String.valueOf("SERVED BY " + String.valueOf(aName)).getBytes("GB2312");
                customerReceipt = String.valueOf("         AGENT COPY         ").getBytes("GB2312");

                motto = String.valueOf(SharedPrefs.read(SharedPrefs.SACCO_MOTTO, null)).getBytes("GB2312");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(start);
            writeLineBreak(1);
            write(saccoName);
            writeLineBreak(2);
            write(type);
            writeLineBreak(1);

            write(terminalNo);
            writeLineBreak(1);
            write(transaction);
            writeLineBreak(1);
            write(time);
            writeLineBreak(1);
            write(transDate);
            writeLineBreak(1);
            write(fullNames);
            writeLineBreak(0);
            write(fName);
            writeLineBreak(1);
            write(arryChqNo);
            writeLineBreak(1);
            write(arryPayee);
            writeLineBreak(2);

            write(arryWithdrawAmt);
            writeLineBreak(1);
            write(arryWordsHeader);
            writeLineBreak(0);
            write(arryInWords);
            writeLineBreak(2);

            write(acknowLedge);
            writeLineBreak(1);
            write(nameVerify);
            writeLineBreak(1);
            write(idVerify);
            writeLineBreak(1);
            write(signature);
            writeLineBreak(2);

            write(nameAgent);
            writeLineBreak(2);
            write(customerReceipt);
            writeLineBreak(2);
            write(motto);
            // print line break
            writeLineBreak(4);

            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void writetest(String account_number, String transactionCode) {
        String the_amount = amount.getText().toString();
        String return_val_in_english;

        if (the_amount.contains(".")) {
            String firstPart = the_amount.substring(0, the_amount.indexOf("."));

            String decimalPart = the_amount.substring(the_amount.lastIndexOf(".") + 1);

            int wholeNo = Integer.parseInt(firstPart);
            int cents = Integer.parseInt(decimalPart);

            String whole_value = EnglishNumberToWords.convert(wholeNo);
            String cent_value = EnglishNumberToWords.convert(cents);
            return_val_in_english = whole_value + " Kenya Shillings and " + cent_value + " cents";

        } else {

            int number = Integer.parseInt(the_amount);

            return_val_in_english = EnglishNumberToWords.convert(number);

        }

        try {

            byte[] start = null;
            byte[] saccoName = null;
            byte[] type = null;

            byte[] terminalNo = null;
            byte[] transaction = null;
            byte[] time = null;
            byte[] transDate = null;
            byte[] idNumber = null;
            byte[] fullNames = null;
            byte[] fName = null;
            byte[] arryChqNo = null;
            byte[] arryPayee = null;

            //byte[] phoneNumber = null;
            byte[] arryWithdrawAmt = null;
            byte[] arryWordsHeader = null;
            byte[] arryInWords = null;
            byte[] depositedBy = null;

            byte[] acknowLedge = null;
            byte[] nameVerify = null;
            byte[] idVerify = null;
            byte[] signature = null;

            byte[] customerReceipt = null;
            byte[] nameAgent = null;
            byte[] motto = null;

            SimpleDateFormat timeStampFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
            Date myDate = new Date();
            String timeCurrent = timeStampFormat.format(myDate);
            String aName = SharedPrefs.read(SharedPrefs.AGENT_NAME, null);
            String cheque_no = SharedPrefs.read(SharedPrefs.CHEQUE_NUMBER, null);

            try {
                start = String.valueOf("              ").getBytes("GB2312");
                saccoName = String.valueOf("      CAPITAL SACCO LTD     ").getBytes("GB2312");
                type = String.valueOf("        BANKER CHEQUE     ").getBytes("GB2312");

                terminalNo = String.valueOf("Terminal ID : " + SharedPrefs.read(SharedPrefs.DEVICE_ID, null)).getBytes("GB2312");
                transaction = String.valueOf("Transaction No : " + transactionCode).getBytes("GB2312");
                time = String.valueOf("Print Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                transDate = String.valueOf("Trans. Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                //account_dposited_to = String.valueOf("Account NO : " + account_number).getBytes("GB2312");

                fullNames = String.valueOf("Applicant Names : ").getBytes("GB2312");
                fName = String.valueOf(String.valueOf(SharedPrefs.read(SharedPrefs.ACC_NAME, null))).getBytes("GB2312");
                arryChqNo = String.valueOf("Cheque Number : " + cheque_no).getBytes("GB2312");
                arryPayee = String.valueOf("Payee :" + payee.getText().toString()).getBytes("GB2312");

                //phoneNumber = String.valueOf("Phone Number : " + String.valueOf(maskedOP)).getBytes("GB2312");
                arryWithdrawAmt = String.valueOf("Amount : KSH " + the_amount).getBytes("GB2312");
                arryWordsHeader = String.valueOf("Amount in Words: ").getBytes("GB2312");
                arryInWords = String.valueOf(return_val_in_english).getBytes("GB2312");
                depositedBy = String.valueOf("Deposited By " + transacted_by.getText().toString()).getBytes("GB2312");

                acknowLedge = String.valueOf("I acknowledge receipt of the banker cheque of the above amount").getBytes("GB2312");
                nameVerify = String.valueOf("Name___________________________").getBytes("GB2312");
                idVerify = String.valueOf("ID No___________________________").getBytes("GB2312");
                signature = String.valueOf("Signature_______________________").getBytes("GB2312");

                nameAgent = String.valueOf("SERVED BY " + String.valueOf(aName)).getBytes("GB2312");
                customerReceipt = String.valueOf("         CUSTOMER COPY         ").getBytes("GB2312");

                motto = String.valueOf(SharedPrefs.read(SharedPrefs.SACCO_MOTTO, null)).getBytes("GB2312");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(start);
            writeLineBreak(1);
            write(saccoName);
            writeLineBreak(2);
            write(type);
            writeLineBreak(1);

            write(terminalNo);
            writeLineBreak(1);
            write(transaction);
            writeLineBreak(1);
            write(time);
            writeLineBreak(1);
            write(transDate);
            writeLineBreak(1);
            write(fullNames);
            writeLineBreak(0);
            write(fName);
            writeLineBreak(1);
            write(arryChqNo);
            writeLineBreak(1);
            write(arryPayee);
            writeLineBreak(2);

            write(arryWithdrawAmt);
            writeLineBreak(1);
            write(arryWordsHeader);
            writeLineBreak(0);
            write(arryInWords);
            writeLineBreak(1);
            write(depositedBy);
            writeLineBreak(2);

            write(acknowLedge);
            writeLineBreak(1);
            write(nameVerify);
            writeLineBreak(1);
            write(idVerify);
            writeLineBreak(1);
            write(signature);
            writeLineBreak(2);

            write(nameAgent);
            writeLineBreak(2);
            write(customerReceipt);
            writeLineBreak(2);
            write(motto);
            // print line break
            writeLineBreak(4);

            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void writetest1(String account_number, String transactionCode) {
        String the_amount = amount.getText().toString();
        String return_val_in_english;

        if (the_amount.contains(".")) {
            String firstPart = the_amount.substring(0, the_amount.indexOf("."));

            String decimalPart = the_amount.substring(the_amount.lastIndexOf(".") + 1);

            int wholeNo = Integer.parseInt(firstPart);
            int cents = Integer.parseInt(decimalPart);

            String whole_value = EnglishNumberToWords.convert(wholeNo);
            String cent_value = EnglishNumberToWords.convert(cents);
            return_val_in_english = whole_value + " Kenya Shillings and " + cent_value + " cents";

        } else {

            int number = Integer.parseInt(the_amount);

            return_val_in_english = EnglishNumberToWords.convert(number);

        }

        try {

            byte[] start = null;
            byte[] saccoName = null;
            byte[] type = null;

            byte[] terminalNo = null;
            byte[] transaction = null;
            byte[] time = null;
            byte[] transDate = null;
            byte[] idNumber = null;
            byte[] fullNames = null;
            byte[] fName = null;
            byte[] arryChqNo = null;
            byte[] arryPayee = null;

            //byte[] phoneNumber = null;
            byte[] arryWithdrawAmt = null;
            byte[] arryWordsHeader = null;
            byte[] arryInWords = null;
            byte[] depositedBy = null;

            byte[] acknowLedge = null;
            byte[] nameVerify = null;
            byte[] idVerify = null;
            byte[] signature = null;

            byte[] customerReceipt = null;
            byte[] nameAgent = null;
            byte[] motto = null;

            SimpleDateFormat timeStampFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
            Date myDate = new Date();
            String timeCurrent = timeStampFormat.format(myDate);
            String aName = SharedPrefs.read(SharedPrefs.AGENT_NAME, null);
            String cheque_no = SharedPrefs.read(SharedPrefs.CHEQUE_NUMBER, null);

            try {
                start = String.valueOf("              ").getBytes("GB2312");
                saccoName = String.valueOf("      CAPITAL SACCO LTD     ").getBytes("GB2312");
                type = String.valueOf("        BANKER CHEQUE     ").getBytes("GB2312");

                terminalNo = String.valueOf("Terminal ID : " + SharedPrefs.read(SharedPrefs.DEVICE_ID, null)).getBytes("GB2312");
                transaction = String.valueOf("Transaction No : " + transactionCode).getBytes("GB2312");
                time = String.valueOf("Print Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                transDate = String.valueOf("Trans. Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                //account_dposited_to = String.valueOf("Account NO : " + account_number).getBytes("GB2312");

                fullNames = String.valueOf("Applicant Names : ").getBytes("GB2312");
                fName = String.valueOf(String.valueOf(SharedPrefs.read(SharedPrefs.ACC_NAME, null))).getBytes("GB2312");
                arryChqNo = String.valueOf("Cheque Number : " + cheque_no).getBytes("GB2312");
                arryPayee = String.valueOf("Payee :" + payee.getText().toString()).getBytes("GB2312");

                //phoneNumber = String.valueOf("Phone Number : " + String.valueOf(maskedOP)).getBytes("GB2312");
                arryWithdrawAmt = String.valueOf("Amount : KSH " + the_amount).getBytes("GB2312");
                arryWordsHeader = String.valueOf("Amount in Words: ").getBytes("GB2312");
                arryInWords = String.valueOf(return_val_in_english).getBytes("GB2312");
                depositedBy = String.valueOf("Deposited By " + transacted_by.getText().toString()).getBytes("GB2312");

                acknowLedge = String.valueOf("I acknowledge receipt of the banker cheque of the above amount").getBytes("GB2312");
                nameVerify = String.valueOf("Name___________________________").getBytes("GB2312");
                idVerify = String.valueOf("ID No___________________________").getBytes("GB2312");
                signature = String.valueOf("Signature_______________________").getBytes("GB2312");

                nameAgent = String.valueOf("SERVED BY " + String.valueOf(aName)).getBytes("GB2312");
                customerReceipt = String.valueOf("         AGENT COPY         ").getBytes("GB2312");

                motto = String.valueOf(SharedPrefs.read(SharedPrefs.SACCO_MOTTO, null)).getBytes("GB2312");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(start);
            writeLineBreak(1);
            write(saccoName);
            writeLineBreak(2);
            write(type);
            writeLineBreak(1);

            write(terminalNo);
            writeLineBreak(1);
            write(transaction);
            writeLineBreak(1);
            write(time);
            writeLineBreak(1);
            write(transDate);
            writeLineBreak(1);
            write(fullNames);
            writeLineBreak(0);
            write(fName);
            writeLineBreak(1);
            write(arryChqNo);
            writeLineBreak(1);
            write(arryPayee);
            writeLineBreak(2);

            write(arryWithdrawAmt);
            writeLineBreak(1);
            write(arryWordsHeader);
            writeLineBreak(0);
            write(arryInWords);
            writeLineBreak(1);
            write(depositedBy);
            writeLineBreak(2);

            write(acknowLedge);
            writeLineBreak(1);
            write(nameVerify);
            writeLineBreak(1);
            write(idVerify);
            writeLineBreak(1);
            write(signature);
            writeLineBreak(2);

            write(nameAgent);
            writeLineBreak(2);
            write(customerReceipt);
            writeLineBreak(2);
            write(motto);
            // print line break
            writeLineBreak(4);

            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void write(final byte[] arryData) {
        int result = PrinterInterface.write(arryData, arryData.length);
    }

    // print line break
    private void writeLineBreak(int lineNumber) {
        // write(new byte[]{'\n'});
        write(PrinterCommand.getCmdEscDN(lineNumber));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                //Toast.makeText(Account.this, "Back has been presses!", Toast.LENGTH_LONG).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }

}

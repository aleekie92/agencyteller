package com.coretec.agencyteller.activities.teller;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.coretec.agencyteller.R;
import com.coretec.agencyteller.api.Api;
import com.coretec.agencyteller.utils.SharedPrefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class LienClearing extends AppCompatActivity {

    private Spinner identifierSpinner;
    private Spinner accountSpinner;
    private EditText identifier;
    private ArrayList<String> arrayList = new ArrayList<>();

    private Spinner lienSpinner;
    private ArrayList<String> lienList = new ArrayList<>();

    private EditText transaction_code;
    Button submit;
    Button search;

    ProgressBar progressBar, lien_progress;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPrefs.init(getApplicationContext());
        setContentView(R.layout.lien_clearing);

        //Toolbar declarations...Reusable
        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView mTitle = toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getText(R.string.lien_clearing));
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);

        identifierSpinner = findViewById(R.id.identifier_spinner);
        accountSpinner = findViewById(R.id.accountsSpinner);
        lienSpinner = findViewById(R.id.lienSpinner);
        identifier = findViewById(R.id.lien_identifier);
        search = findViewById(R.id.btn_search);
        progressBar = findViewById(R.id.accountsPB);
        lien_progress = findViewById(R.id.lienPB);

        transaction_code = findViewById(R.id.et_account);

        accountSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                String text = accountSpinner.getSelectedItem().toString();

                String account_number = text.substring(0, text.indexOf("-"));
                SharedPrefs.write(SharedPrefs.MEMBER_ACCOUNT_NUMBER, account_number);
                Log.e("MEMBER ACCOUNT", account_number);

                switch (text) {
                    case "select-account":
                        break;
                    default:
                        lien_progress.setVisibility(View.VISIBLE);
                        lien_progress.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(LienClearing.this, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                        getLienPlaced(account_number);
                        //checkSignatories(account_number);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        identifierSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                String item = parent.getItemAtPosition(position).toString();
                String text = identifierSpinner.getSelectedItem().toString();

                int idLength = 8;
                int acLength = 14;
                int memLength = 14;
                InputFilter[] fArray = new InputFilter[1];
                fArray[0] = new InputFilter.LengthFilter(idLength);

                InputFilter[] aArray = new InputFilter[1];
                aArray[0] = new InputFilter.LengthFilter(acLength);

                InputFilter[] mArray = new InputFilter[1];
                mArray[0] = new InputFilter.LengthFilter(memLength);

                if (text.equals("ID NUMBER")) {
                    SharedPrefs.write(SharedPrefs.IDENTIFIER, "0");
                    identifier.setText(null);
                    identifier.setInputType(InputType.TYPE_CLASS_NUMBER);
                    identifier.setFilters(fArray);
                } else if (text.equals("OLD A/C NUMBER")) {
                    SharedPrefs.write(SharedPrefs.IDENTIFIER, "4");
                    identifier.setText(null);
                    identifier.setInputType(InputType.TYPE_CLASS_TEXT);
                    identifier.setFilters(aArray);
                } else if (text.equals("MEMBER NUMBER")) {
                    identifier.setText(null);
                    identifier.setInputType(InputType.TYPE_CLASS_NUMBER);
                    SharedPrefs.write(SharedPrefs.IDENTIFIER, "1");
                    identifier.setFilters(mArray);
                }

                Log.e("SELECTED", text);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        lienSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //((TextView) parent.getChildAt(0)).setTextColor(Color.GRAY);
                String item = parent.getItemAtPosition(position).toString();
                String text = lienSpinner.getSelectedItem().toString();

                if (!text.equals("lien-not-found") && !text.equals("select-lien")) {
                    String lien_no = text.substring(0, text.indexOf("-"));
                    SharedPrefs.write(SharedPrefs.LIEN_NUMBER, lien_no);
                    Log.e("LIEN ACCOUNT", lien_no);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = identifier.getText().toString();

                if (id.length() < 7 && id.length() > 0) {
                    identifier.setError("Please input a valid identifier!");
                } else if (id.length() == 0) {
                    identifier.setError("Cannot search an empty field!");
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(LienClearing.this, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                    requestAccounts(id);

                }
            }
        });

        TextWatcher textWatcher = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String idNumber = identifier.getText().toString();

                /*if (idNumber.length() >= 7) {
                    requestAccounts(idNumber);
                    getPicture(idNumber);
                    getSignature(idNumber);
                }*/

                if (idNumber.length() >= 5 && idNumber.length() <= 8) {

                    arrayList.clear();
                    accountSpinner = findViewById(R.id.accountsSpinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(LienClearing.this, android.R.layout.simple_spinner_item, arrayList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    accountSpinner.setAdapter(adapter);

                }
                if (idNumber.length() < 5) {
                    progressBar.setVisibility(View.GONE);
                }

            }
        };

        identifier.addTextChangedListener(textWatcher);


        submit = findViewById(R.id.button_submit);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = identifier.getText().toString();
                if (code.isEmpty()) {
                    identifier.setError("Please input the identifier!");
                } else {
                    String msg = "Are you sure you want to clear the lien?";
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(LienClearing.this);
                    builder1.setMessage(msg);
                    builder1.setTitle("Warning!");
                    builder1.setCancelable(false);

                    builder1.setNegativeButton(
                            "CANCEL",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    builder1.setPositiveButton(
                            "Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    progressDialog = new ProgressDialog(LienClearing.this);
                                    progressDialog.setMessage("Submitting your request...");
                                    progressDialog.setCancelable(false);
                                    progressDialog.show();
                                    clearLien();
                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }
            }
        });

    }

    void getLienPlaced(String accountNumber) {

        Api.getVolley(this, Api.GetLienPlaced, "{\n" +
                "    \"accountNo\": \"" + accountNumber + "\",\n" +
                "    \"agentCode\": \"" + SharedPrefs.read(SharedPrefs.AGENT_CODE, null) + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                lien_progress.setVisibility(View.GONE);
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));
                        lienList.clear();
                        lienSpinner = findViewById(R.id.lienSpinner);
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(LienClearing.this, android.R.layout.simple_spinner_item, lienList);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        lienSpinner.setAdapter(adapter);

                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                //String accountName = jsonObject1.getString("name");
                                String lienNo = jsonObject1.getString("no");
                                String lienAmount = jsonObject1.getString("amount");
                                String lienDate = jsonObject1.getString("date");
                                //String infoBase = jsonObject1.getString("infoBaseArea");
                                String lienDetails = lienNo + "-  " + lienDate + "  - " + lienAmount;

                                lienList.add(lienDetails);

                                Log.e("LIEN DETAILS", lienDetails);
                            }

                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        progressBar.setVisibility(View.GONE);
                        String msg = jsonObject.getString("message");
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(LienClearing.this);
                        builder1.setMessage(msg);
                        builder1.setTitle("Alert!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        String lienDetails = "lien-not-found";
                                        lienList.add(lienDetails);
                                        //onBackPressed();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                        //Toast.makeText(LienClearing.this, "Could not fetch accounts! Please try again.", Toast.LENGTH_SHORT).show();
                    }

                    lienSpinner = findViewById(R.id.lienSpinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(LienClearing.this, android.R.layout.simple_spinner_item, lienList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    lienSpinner.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void requestAccounts(String id) {

        int identifier = Integer.parseInt(SharedPrefs.read(SharedPrefs.IDENTIFIER, null));

        Api.getVolley(this, Api.GetMemberAccounts, "{\n" +
                "    \"identifier\": \"" + id + "\",\n" +
                "    \"identifierType\": \"" + identifier + "\",\n" +
                "    \"natureOfAccount\": \"" + 1 + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                progressBar.setVisibility(View.GONE);
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));
                        arrayList.clear();
                        accountSpinner = findViewById(R.id.accountsSpinner);
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(LienClearing.this, android.R.layout.simple_spinner_item, arrayList);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        accountSpinner.setAdapter(adapter);

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            String accountName = jsonObject1.getString("name");
                            String accountNo = jsonObject1.getString("no");
                            String accountBalance = jsonObject1.getString("balance");
                            String desc = jsonObject1.getString("description");
                            String infoBase = jsonObject1.getString("infoBaseArea");
                            String accountDetails = accountNo + "-" + desc;

                            SharedPrefs.write(SharedPrefs.ACC_NAME, accountName);

                            arrayList.add(accountDetails);

                            Log.e("ACCOUNT DETAILS", accountDetails);

                        }
                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(LienClearing.this, "Could not fetch accounts! Please try again.", Toast.LENGTH_SHORT).show();
                    }

                    accountSpinner = findViewById(R.id.accountsSpinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(LienClearing.this, android.R.layout.simple_spinner_item, arrayList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    accountSpinner.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    void clearLien() {

        Api.getVolley(this, Api.ClearLien, "{\n" +
                "    \"action\": \"" + 3 + "\",\n" +
                "    \"agentCode\": \"" + SharedPrefs.read(SharedPrefs.AGENT_CODE, null) + "\",\n" +
                "    \"transactionNo\": \"" + SharedPrefs.read(SharedPrefs.LIEN_NUMBER, null) + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));
                        Toast.makeText(LienClearing.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                        String msg = jsonObject.getString("message");
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(LienClearing.this);
                        builder1.setMessage(msg);
                        builder1.setTitle("Success!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        onBackPressed();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();

                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        progressDialog.dismiss();
                        Toast.makeText(LienClearing.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                        String msg = jsonObject.getString("message");
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(LienClearing.this);
                        builder1.setMessage(msg);
                        builder1.setTitle("Alert!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                //Toast.makeText(Account.this, "Back has been presses!", Toast.LENGTH_LONG).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }

}

package com.coretec.agencyteller.activities.teller;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.coretec.agencyteller.R;
import com.coretec.agencyteller.adapters.SignatoryAdapter;
import com.coretec.agencyteller.api.Api;
import com.coretec.agencyteller.model.SignatoryList;
import com.coretec.agencyteller.utils.SharedPrefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DialogSignatories extends AppCompatActivity {

    public RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;

    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPrefs.init(getApplicationContext());
        setContentView(R.layout.signatories_dialog);

        mRecyclerView = findViewById(R.id.recycler_view);
        //recyclerView = findViewById(R.id.recycler_view);

        progressBar = findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.VISIBLE);

        displaySignatories(SharedPrefs.read(SharedPrefs.MEMBER_ACCOUNT_NUMBER, null));

        //mRecyclerView.setAdapter(mAdapter);
        //mAdapter.notifyDataSetChanged();

    }

    public void setuprecyclerview(List<SignatoryList> catList) {
        /*GridLayoutManager manager = new GridLayoutManager(getApplicationContext(), 1, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);*/
        mRecyclerView.setLayoutManager(new LinearLayoutManager(DialogSignatories.this));
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(mRecyclerView.getContext(), DividerItemDecoration.VERTICAL));

        mAdapter = new SignatoryAdapter(DialogSignatories.this, catList);
        mRecyclerView.setAdapter(mAdapter);

    }

    public Bitmap StringToBitMap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    private void displaySignatories(String accountNumber) {

        Api.getVolley(this, Api.GetAccountSignatories, "{\n" +
                "    \"accountType\": \"" + 1 + "\",\n" +
                "    \"accountNo\": \"" + accountNumber + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                //progressBar.setVisibility(View.GONE);
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        progressBar.setVisibility(View.GONE);
                        List<SignatoryList> signatoryLists= new ArrayList<>();
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            try {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                SignatoryList newSig = new SignatoryList(jsonObject1.getInt("no"),jsonObject1.getString("name"),
                                        jsonObject1.getString("account"),jsonObject1.getString("memberNo"),
                                        jsonObject1.getString("nationalID"),jsonObject1.getBoolean("signatory"),
                                        jsonObject1.getBoolean("mustSign"),jsonObject1.getBoolean("mustBePresent"));
                                signatoryLists.add(newSig);

                                //getSignatoryPictures(jsonObject1.getString("account"), String.valueOf(jsonObject1.getInt("no")));
                                //getSignatorySign(jsonObject1.getString("account"), String.valueOf(jsonObject1.getInt("no")));

                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }

                        }

                        setuprecyclerview(signatoryLists);
                        //mAdapter.notifyDataSetChanged();

                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(DialogSignatories.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void getSignatoryPictures(String account, String sigNo) {

        Api.getVolley(this, Api.GetSignatoryPictures, "{\n" +
                "    \"accountNo\": \"" + account + "\",\n" +
                "    \"signatoryNo\": \"" + sigNo + "\",\n" +
                "    \"fileType\": \"" + 0 + "\"\n" +
                "  }\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                Log.e("RESULTS", Response);

                Bitmap decodeTxt = StringToBitMap(Response);

            }
        });

    }

    private void getSignatorySign(String account, String sigNo) {

        Api.getVolley(this, Api.GetSignatoryPictures, "{\n" +
                "    \"accountNo\": \"" + account + "\",\n" +
                "    \"signatoryNo\": \"" + sigNo + "\",\n" +
                "    \"fileType\": \"" + 1 + "\"\n" +
                "  }\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                Log.e("RESULTS", Response);

                Bitmap decodeTxt = StringToBitMap(Response);

            }
        });

    }

    public void onResume() {
        super.onResume();
        mRecyclerView.setAdapter(mAdapter);
        //mAdapter.notifyDataSetChanged();
    }

}

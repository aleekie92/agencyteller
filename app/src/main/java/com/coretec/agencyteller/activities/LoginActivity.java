package com.coretec.agencyteller.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.coretec.agencyteller.R;
import com.coretec.agencyteller.api.Api;
import com.coretec.agencyteller.utils.SharedPrefs;
import com.coretec.agencyteller.utils.Utils;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;


public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "String Agency Banking";
    private AppCompatButton btnLogin;
    private ImageView imgFingerprint;
    private EditText input_agent_phone_number;
    private TextInputLayout til_input_agent_phone_number;
    private EditText input_agent_password;
    private TextInputLayout til_input_agent_password;
    private String agentId, password;
    private ProgressBar progressBar;

    AlertDialog changePasswordDialog;
    ProgressDialog progressDialog;

    SharedPreferences sharedPref;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPrefs.init(getApplicationContext());

        switch (Api.URL) {
            case "http://192.168.86.8:2020/Api/AgencyBanking/":
                setContentView(R.layout.activity_capital_login);
                SharedPrefs.write(SharedPrefs.SACCO_NAME, "      CAPITAL SACCO LTD     ");
                SharedPrefs.write(SharedPrefs.SACCO_MOTTO, "         Base For Growth       ");

                break;

            case "http://192.168.1.247:8080/Api/AgencyBanking/":
                //setContentView(R.layout.activity_capital_login);
                setContentView(R.layout.activity_wakenya_login);
                SharedPrefs.write(SharedPrefs.SACCO_NAME, "      WAKENYA PAMOJA SACCO LTD     ");
                SharedPrefs.write(SharedPrefs.SACCO_MOTTO, "         You, Our Concern       ");
                break;

            default:
                setContentView(R.layout.activity_capital_login);
                break;

        }

        Utils.hideKeyboard(LoginActivity.this);
        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);
        progressBar = findViewById(R.id.loadingPB);
        btnLogin =  findViewById(R.id.btn_login);
        input_agent_phone_number = findViewById(R.id.input_agent_phone_number);
        til_input_agent_phone_number = findViewById(R.id.til_input_agent_phone_number);
        input_agent_password = findViewById(R.id.input_agent_password);
        til_input_agent_password = findViewById(R.id.til_input_agent_password);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                agentId = input_agent_phone_number.getText().toString().trim();
                password = input_agent_password.getText().toString().trim();

                if (agentId.isEmpty()) {
                    input_agent_phone_number.setText(agentId);
                    til_input_agent_phone_number.setError("Enter your User Code ");
                } else if (password.isEmpty()) {
                    til_input_agent_password.setError("Enter your password");
                } else {
                    Log.e(TAG, agentId);
                    Log.e(TAG, password);
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(LoginActivity.this, R.color.colorPrimary), PorterDuff.Mode.SRC_IN );
                    loginRequest(agentId, password);
                }
            }
        });
    }

    public void loginRequest(final String agentcode, final String pin) {

        Api.getVolley(this, Api.TellerLogin, "{\n" +
                "    \"userCode\": \"" + agentcode + "\",\n" +
                "   \"userPassword\": \"" + pin + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String result) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    Log.e("RESULTS", new Gson().toJson(jsonObject.toString()));

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        Log.e("Success Response", new Gson().toJson(jsonObject.toString()));

                        JSONObject dataObject = jsonObject.getJSONObject("data");
                        JSONObject accountObject = dataObject.getJSONObject("account");

                        String agentCode = dataObject.getString("code");
                        String agentName = dataObject.getString("name");
                        String agentType = dataObject.getString("type");
                        String agentBranch = dataObject.getString("branch");
                        String agentActivity = dataObject.getString("activity");
                        Boolean changePassword = dataObject.getBoolean("changePassword");
                        String accountNumber = accountObject.getString("no");
                        String accountBal = accountObject.getString("balance");

                        SharedPrefs.write(SharedPrefs.TELLER_ACCOUNT_NUMBER, accountNumber);
                        SharedPrefs.write(SharedPrefs.ACCOUNT_BALANCE, accountBal);
                        SharedPrefs.write(SharedPrefs.AGENT_CODE, agentCode);

                        SharedPrefs.write(SharedPrefs.AGENT_NAME, agentName);
                        SharedPrefs.write(SharedPrefs.AGENT_TYPE, agentType);
                        SharedPrefs.write(SharedPrefs.AGENT_BRANCH, agentBranch);
                        SharedPrefs.write(SharedPrefs.AGENT_ACTIVITY, agentActivity);

                        Log.e("ACCOUNT NUMBER", accountNumber);
                        Log.e("ACCOUNT BALANCE", accountBal);
                        Log.e("AGENT NAME", agentName);

                        progressBar.setVisibility(View.GONE);

                        if (changePassword) {
                            createDialogChangePassword(agentType);
                        } else {

                            if (agentType.equals("Cashier")) {
                                startActivity(new Intent(LoginActivity.this, Dashboard.class));
                                input_agent_phone_number.setText("");
                                input_agent_password.setText("");
                                //Toast.makeText(LoginActivity.this, "Successful", Toast.LENGTH_SHORT).show();
                            } else if (agentType.equals("Treasury")) {
                                input_agent_phone_number.setText("");
                                input_agent_password.setText("");
                                startActivity(new Intent(LoginActivity.this, TDashboard.class));
                            }
                        }

                    } if (jsonObject.getString("error").equalsIgnoreCase("true")){
                        Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        progressBar.setVisibility(View.GONE);
                        String msg = jsonObject.getString("message");
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(LoginActivity.this);
                        builder1.setMessage(msg);
                        builder1.setTitle("Alert!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

    }

    private void createDialogChangePassword(final String agentType) {

        LayoutInflater myInflater = LayoutInflater.from(this);
        View v = myInflater.inflate(R.layout.dialog_change_password, null);

        final EditText old_pass = v.findViewById(R.id.et_old_pass);
        final EditText new_pass = v.findViewById(R.id.et_new_pass);
        final EditText confirm_pass = v.findViewById(R.id.et_confirm_pass);
        Button submit = v.findViewById(R.id.button_submit);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pass_word = input_agent_password.getText().toString();
                String oldPass = old_pass.getText().toString();
                String newPass = new_pass.getText().toString();
                String confirmPass = confirm_pass.getText().toString();

                if (!oldPass.equals(pass_word)) {
                    old_pass.setError("Password does not match with the Old Password!");
                } else if (oldPass.isEmpty()) {
                    old_pass.setError("Please enter your old password!");
                } else if (newPass.isEmpty()) {
                    new_pass.setError("Please enter your new password!");
                } else if (confirmPass.isEmpty()) {
                    confirm_pass.setError("Please confirm your new password!");
                } else if (!confirmPass.equals(newPass)) {
                    confirm_pass.setError("Password does not match with the new password!");
                } else {
                    progressDialog = new ProgressDialog(LoginActivity.this);
                    progressDialog.setMessage("Processing your request...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    requestPasswordChange(pass_word, confirmPass, agentType);
                }

            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(v);
        changePasswordDialog = builder.create();
        changePasswordDialog.show();
        changePasswordDialog.setCancelable(false);

    }

    private void requestPasswordChange(String userPass, String newPass, final String agent_type) {

        Api.getVolley(this, Api.ChangePassword, "{\n" +
                "    \"userCode\": \"" + input_agent_phone_number.getText().toString() + "\",\n" +
                "    \"oldPassword\": \"" + userPass + "\",\n" +
                "    \"newPassword\": \"" + newPass + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        progressDialog.dismiss();
                        String msg = jsonObject.getString("message");
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(LoginActivity.this);
                        builder1.setMessage(msg);
                        builder1.setTitle("Success");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        if (agent_type.equals("Cashier")) {
                                            startActivity(new Intent(LoginActivity.this, Dashboard.class));
                                        } else if (agent_type.equals("Treasury")) {
                                            startActivity(new Intent(LoginActivity.this, TDashboard.class));
                                        }
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        progressDialog.dismiss();
                        String msg = jsonObject.getString("message");
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(LoginActivity.this);
                        builder1.setMessage(msg);
                        builder1.setTitle("Alert!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }
}

package com.coretec.agencyteller.activities.reports;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.coretec.agencyteller.R;
import com.coretec.agencyteller.api.Api;
import com.coretec.agencyteller.utils.SharedPrefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Locale;

public class TellerSummary extends AppCompatActivity {

    TextView from_date, to_date;
    Button submit;

    LinearLayout summary_details;
    TextView agent, account, opening, credit, debit, closing;

    DatePickerDialog datePickerDialog;
    ProgressDialog progressDialog;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.teller_summary);

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

        //Toolbar declarations...Reusable
        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView mTitle = toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getText(R.string.teller_summary));
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);

        from_date = findViewById(R.id.from_date);
        to_date = findViewById(R.id.to_date);
        submit = findViewById(R.id.button_submit);

        summary_details = findViewById(R.id.linear_summary);
        agent = findViewById(R.id.tv_agent_name);
        account = findViewById(R.id.tv_account);
        opening = findViewById(R.id.tv_opening);
        credit = findViewById(R.id.tv_credit);
        debit = findViewById(R.id.tv_debit);
        closing = findViewById(R.id.tv_closing);
        progressBar = findViewById(R.id.progress_bar);

        from_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(TellerSummary.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                                /*date.setText(year + "-"
                                        + (monthOfYear + 1) + "-" + dayOfMonth);*/

                                Calendar calendar= Calendar.getInstance();
                                calendar.set(year,monthOfYear,dayOfMonth);
                                String s =String.format(Locale.getDefault(),"%1$tY-%1$tm-%1$td",calendar);
                                from_date.setText(s);

                                String dateSelected = from_date.getText().toString();
                                //SharedPrefs.write(SharedPrefs.DOB, dateSelected);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
                //datePickerDialog.updateDate(2001, 1, 1);
            }
        });

        to_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(TellerSummary.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                                /*date.setText(year + "-"
                                        + (monthOfYear + 1) + "-" + dayOfMonth);*/

                                Calendar calendar= Calendar.getInstance();
                                calendar.set(year,monthOfYear,dayOfMonth);
                                String s =String.format(Locale.getDefault(),"%1$tY-%1$tm-%1$td",calendar);
                                to_date.setText(s);

                                String dateSelected = to_date.getText().toString();
                                //SharedPrefs.write(SharedPrefs.DOB, dateSelected);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
                //datePickerDialog.updateDate(2001, 1, 1);
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fromText = from_date.getText().toString();
                String toText = to_date.getText().toString();

                if (fromText.isEmpty()) {
                    from_date.setError("Please select from date!");
                } else if (toText.isEmpty()) {
                    to_date.setError("Please select from date!");
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                    summary_details.setVisibility(View.GONE);
                    getTellerSummary();
                }
            }
        });

    }

    void getTellerSummary() {

        Api.getVolley(this, Api.GetTellerSummary, "{\n" +
                "    \"agentCode\": \"" + SharedPrefs.read(SharedPrefs.AGENT_CODE, null) + "\",\n" +
                "    \"StartDate\": \"" + from_date.getText().toString() + "\",\n" +
                "    \"EndDate\": \"" + to_date.getText().toString() + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));
                        progressBar.setVisibility(View.GONE);
                        summary_details.setVisibility(View.VISIBLE);

                        JSONObject dataObject = jsonObject.getJSONObject("data");

                        agent.setText(dataObject.getString("agent"));
                        account.setText(dataObject.getString("account"));
                        opening.setText(dataObject.getString("opening"));
                        credit.setText(dataObject.getString("credit"));
                        debit.setText(dataObject.getString("debit"));
                        closing.setText(dataObject.getString("closing"));

                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        progressBar.setVisibility(View.GONE);
                        summary_details.setVisibility(View.GONE);
                        //Toast.makeText(TellerSummary.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                        String msg = jsonObject.getString("message");
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(TellerSummary.this);
                        builder1.setMessage(msg);
                        builder1.setTitle("Alert!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                //Toast.makeText(Account.this, "Back has been presses!", Toast.LENGTH_LONG).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }

}

package com.coretec.agencyteller.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.coretec.agencyteller.R;
import com.coretec.agencyteller.activities.member.AtmApplication;
import com.coretec.agencyteller.activities.member.MsaccoApplication;
import com.coretec.agencyteller.activities.member.StandingOrder;
import com.coretec.agencyteller.activities.reports.Ministatement;
import com.coretec.agencyteller.activities.reports.BalanceInquiry;
import com.coretec.agencyteller.activities.reports.TellerSummary;
import com.coretec.agencyteller.activities.treasury.TreasuryInfo;
import com.coretec.agencyteller.activities.treasury.TellerIssue;
import com.coretec.agencyteller.activities.treasury.TellerTransfers;
import com.coretec.agencyteller.activities.treasury.TreasuryReturns;

public class TDashboard extends AppCompatActivity {

    CardView member_reg, treasury_trans, reports, member_app;
    AlertDialog reportsDialog;
    AlertDialog memberApplicationDialog;
    AlertDialog treasuryDialog;
    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tdashboard);

        member_reg = findViewById(R.id.card_member_registration);
        treasury_trans = findViewById(R.id.card_treasury_transactions);
        reports = findViewById(R.id.card_reports);
        member_app = findViewById(R.id.card_member_applications);

        member_reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(TDashboard.this, MemberRegistration.class));
                startActivity(new Intent(TDashboard.this, TellerIssue.class));
            }
        });
        treasury_trans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(TDashboard.this, TreasuryTrans.class));
                startActivity(new Intent(TDashboard.this, TreasuryInfo.class));
                //createDialogTreaseury();
            }
        });

        reports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(Dashboard.this, ReportsActivity.class));
                createDialogReports();
            }
        });

        member_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDialogApp();
            }
        });

    }

    private void createDialogReports() {

        LayoutInflater myInflater = LayoutInflater.from(this);
        View v = myInflater.inflate(R.layout.dialog_reports, null);

        LinearLayout mini_statement = v.findViewById(R.id.option_ministatement);
        LinearLayout teller_summary = v.findViewById(R.id.option_teller);
        LinearLayout payment_receipt = v.findViewById(R.id.option_payment);
        LinearLayout balance = v.findViewById(R.id.option_balance);

        //on click listeners
        mini_statement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TDashboard.this, Ministatement.class));
            }
        });
        teller_summary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TDashboard.this, TellerSummary.class));
            }
        });
        payment_receipt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(TDashboard.this, BalanceInquiry.class));
            }
        });

        balance.setVisibility(View.GONE);
        /*balance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TDashboard.this, BalanceInquiry.class));
            }
        });*/

        /*WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        Window windowAlDl = reportsDialog.getWindow();

        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;

        windowAlDl.setAttributes(layoutParams);*/

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(v);
        reportsDialog = builder.create();
        reportsDialog.show();

    }

    private void createDialogApp() {

        LayoutInflater myInflater = LayoutInflater.from(this);
        View v = myInflater.inflate(R.layout.dialog_member_app, null);

        //define and access view attributes
        //EditText amount = v.findViewById(R.id.amountEt);
        //Buy airtime static options
        LinearLayout msacco_app = v.findViewById(R.id.msacco_app);
        LinearLayout atm_app = v.findViewById(R.id.atm_app);
        LinearLayout sto_app = v.findViewById(R.id.standing_order);

        //on click listeners
        msacco_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TDashboard.this, MsaccoApplication.class));
            }
        });
        atm_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TDashboard.this, AtmApplication.class));
            }
        });
        sto_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TDashboard.this, StandingOrder.class));
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(v);
        memberApplicationDialog = builder.create();
        memberApplicationDialog.show();

    }

    private void createDialogTreaseury() {

        LayoutInflater myInflater = LayoutInflater.from(this);
        View v = myInflater.inflate(R.layout.dialog_treasury_trans, null);

        //define and access view attributes
        //EditText amount = v.findViewById(R.id.amountEt);
        //Buy airtime static options
        LinearLayout teller_issue = v.findViewById(R.id.issue_to_teller);
        LinearLayout treasury_return = v.findViewById(R.id.return_to_treasury);
        LinearLayout teller_transfers = v.findViewById(R.id.inter_t_transfers);
        LinearLayout float_balance = v.findViewById(R.id.floatBalance);

        //on click listeners
        teller_issue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TDashboard.this, TellerIssue.class));
            }
        });
        treasury_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TDashboard.this, TreasuryReturns.class));
            }
        });
        teller_transfers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TDashboard.this, TellerTransfers.class));
            }
        });
        float_balance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TDashboard.this, TreasuryInfo.class));
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(v);
        treasuryDialog = builder.create();
        treasuryDialog.show();

    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if (doubleBackToExitPressedOnce) {
            String msg = "Are you sure you want to exit app? To exit just press yes below!";
            android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(TDashboard.this);
            builder1.setMessage(msg);
            builder1.setTitle("Exit!");
            builder1.setCancelable(false);

            builder1.setPositiveButton(
                    "yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            System.exit(1);
                            //onBackPressed();
                        }
                    });

            builder1.setNegativeButton(
                    "CANCEL",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            //onBackPressed();
                        }
                    });

            android.app.AlertDialog alert11 = builder1.create();
            alert11.show();
            //super.onBackPressed();
            //System.exit(1);
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        //Toast.makeText(this, "Press back again to EXIT app!", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

}

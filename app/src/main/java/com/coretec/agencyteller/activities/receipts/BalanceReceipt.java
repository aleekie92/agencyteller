package com.coretec.agencyteller.activities.receipts;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.coretec.agencyteller.R;
import com.coretec.agencyteller.adapters.BalanceInquiryAdapter;
import com.coretec.agencyteller.api.responses.AccBalance;
import com.coretec.agencyteller.utils.SharedPrefs;
import com.coretec.agencyteller.wizarpos.function.ActionCallbackImpl;
import com.coretec.agencyteller.wizarpos.mvc.base.ActionCallback;
import com.coretec.agencyteller.wizarpos.mvc.base.ActionManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BalanceReceipt extends AppCompatActivity {

    public static final String BALANCE_RESPONSE_EXTRA = "BALANCE_RESPONSE_EXTRA";
    private static final String TAG = "balanceInquiry";
    private AppCompatButton printBalanceReceipt;
    public static Handler handler;
    public static ActionCallback callback;
    private RecyclerView bal_recyclerview;
    private BalanceInquiryAdapter balanceInquiryAdapter;
    private List<AccBalance> balanceList;
    private TextView tvSaccoName, tvTerminalID, tvReceiptNumber;
    private TextView transaction_date, customer_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.balance_receipt);

        tvSaccoName = (TextView) findViewById(R.id.balance_inquiry_sacco_name);
        tvTerminalID = (TextView) findViewById(R.id.balance_inquiry_terminal_id);
        tvReceiptNumber = (TextView) findViewById(R.id.balance_inquiry_receipt_number);
        customer_name = (TextView) findViewById(R.id.balance_inquiry_customer_name);
        transaction_date = (TextView) findViewById(R.id.balance_inquiry_transaction_date);

        tvSaccoName.setText(SharedPrefs.read(SharedPrefs.SACCO_NAME, null));


        handler = new Handler();
        callback = new ActionCallbackImpl(handler);

        balanceList = new ArrayList<>();

//        BalanceResponse balanceResponse = (BalanceResponse) getIntent().getSerializableExtra(BALANCE_RESPONSE_EXTRA);
//        balanceList.addAll(balanceResponse.getBalance());

//        tvSaccoName.setText(balanceResponse.sacconame);
//        tvTerminalID.setText(Utils.getIMEI(BalanceReceipt.this));
//        tvReceiptNumber.setText(balanceResponse.receiptno);
//        balance_amount.setText(String.valueOf("KSH " + balanceResponse.balance.getAmount()));
//        account_balance_inquiry.setText(balanceResponse.balance.getAccount());
//        customer_name.setText(balanceResponse.customername);
//        transaction_date.setText(balanceResponse.transactiondate);

        bal_recyclerview = (RecyclerView) findViewById(R.id.balRecyclerView);
        bal_recyclerview.setHasFixedSize(true);
        balanceInquiryAdapter = new BalanceInquiryAdapter(this, balanceList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        bal_recyclerview.setLayoutManager(mLayoutManager);
        bal_recyclerview.setAdapter(balanceInquiryAdapter);

        final Map<String, Object> params = new HashMap<>();

        params.put("balance", balanceList);
        params.put("terminalid", tvTerminalID);

        printBalanceReceipt = (AppCompatButton) findViewById(R.id.btn_print_balance_receipt);
        printBalanceReceipt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActionManager.doSubmit("BalancePrinterAction/open", BalanceReceipt.this, params, callback);
                ActionManager.doSubmit("BalancePrinterAction/write", BalanceReceipt.this, params, callback);
                ActionManager.doSubmit("BalancePrinterAction/close", BalanceReceipt.this, params, callback);
            }
        });
    }
}

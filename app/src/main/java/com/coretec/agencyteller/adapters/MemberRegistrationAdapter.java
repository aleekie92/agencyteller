package com.coretec.agencyteller.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;

import com.coretec.agencyteller.fragments.AgencyBackIdFragment;
import com.coretec.agencyteller.fragments.AgencyCustomerFragment;
import com.coretec.agencyteller.fragments.AgencyFrontIdFragment;
import com.coretec.agencyteller.fragments.AgencyMoreFragment;
import com.coretec.agencyteller.fragments.AgencyPhotoFragment;
import com.coretec.agencyteller.fragments.AgencySignature;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;

public class MemberRegistrationAdapter extends AbstractFragmentStepAdapter {

    private static final String CURRENT_STEP_POSITION_KEY = "messageResourceId";
    public MemberRegistrationAdapter(FragmentManager fm, Context context) {
        super(fm, context);
    }
    @Override
    public Step createStep(int position) {
        switch (position){
            case 0:
                final AgencyCustomerFragment step1 = new AgencyCustomerFragment();
                Bundle b1 = new Bundle();
                b1.putInt(CURRENT_STEP_POSITION_KEY, position);
                step1.setArguments(b1);
                return step1;
            /*case 1:
                final AgencyMoreFragment step2 = new AgencyMoreFragment();
                Bundle b2 = new Bundle();
                b2.putInt(CURRENT_STEP_POSITION_KEY, position);
                step2.setArguments(b2);
                return step2;*/
            case 1:
                final AgencyPhotoFragment step3 = new AgencyPhotoFragment();
                Bundle b3 = new Bundle();
                b3.putInt(CURRENT_STEP_POSITION_KEY, position);
                step3.setArguments(b3);
                return step3;
            /*case 2:
                final AgencyFrontIdFragment step4 = new AgencyFrontIdFragment();
                Bundle b4 = new Bundle();
                b4.putInt(CURRENT_STEP_POSITION_KEY, position);
                step4.setArguments(b4);
                return step4;
            case 3:
                final AgencyBackIdFragment step5 = new AgencyBackIdFragment();
                Bundle b5 = new Bundle();
                b5.putInt(CURRENT_STEP_POSITION_KEY, position);
                step5.setArguments(b5);
                return step5;*/
            case 2:
                final AgencySignature step6 = new AgencySignature();
                Bundle b6 = new Bundle();
                b6.putInt(CURRENT_STEP_POSITION_KEY, position);
                step6.setArguments(b6);
                return step6;
        }
        return null;
    }
    @Override
    public int getCount() {
        return 3;
    }
    @NonNull
    @Override
    public StepViewModel getViewModel(@IntRange(from = 0) int position) {
        //Override this method to set Step title for the Tabs, not necessary for other stepper types
        switch (position){
            case 0:
                return new StepViewModel.Builder(context)
                        .setTitle("Customer Details") //can be a CharSequence instead
                        .create();
            /*case 1:
                return new StepViewModel.Builder(context)
                        .setTitle("More Details") //can be a CharSequence instead
                        .create();*/
            case 1:
                return new StepViewModel.Builder(context)
                        .setTitle("Passport Photo") //can be a CharSequence instead
                        .create();
            /*case 2:
                return new StepViewModel.Builder(context)
                        .setTitle("Front ID") //can be a CharSequence instead
                        .create();
            case 3:
                return new StepViewModel.Builder(context)
                        .setTitle("Back ID") //can be a CharSequence instead
                        .create();*/
            case 2:
                return new StepViewModel.Builder(context)
                        .setTitle("Signature") //can be a CharSequence instead
                        .create();
        }
        return null;
    }

}

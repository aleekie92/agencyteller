package com.coretec.agencyteller.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.coretec.agencyteller.R;
import com.coretec.agencyteller.api.Api;
import com.coretec.agencyteller.model.SignatoryList;

import java.util.List;

public class SignatoryAdapter extends RecyclerView.Adapter<SignatoryAdapter.ViewHolder> {

    private Context mContext ;
    private List<SignatoryList> mData ;

    public SignatoryAdapter(Context mContext, List<SignatoryList> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name, id, signatory, mustSign, present;
        ImageView picture, signature;
        LinearLayout view_container;

        public ViewHolder(View itemView) {
            super(itemView);
            picture = itemView.findViewById(R.id.img_1);
            signature = itemView.findViewById(R.id.sig_1);
            name = itemView.findViewById(R.id.tv_name);
            id = itemView.findViewById(R.id.tv_id);
            signatory = itemView.findViewById(R.id.tv_signatory);
            mustSign = itemView.findViewById(R.id.tv_must_sign);
            present = itemView.findViewById(R.id.tv_present);

        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view ;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        view = inflater.inflate(R.layout.signatory_item,parent,false) ;
        ViewHolder viewHolder = new ViewHolder(view) ;

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        final SignatoryList list = mData.get(position);

        holder.name.setText(list.getName());
        holder.id.setText(list.getNationalID());
        holder.signatory.setText(list.getSignatory().toString());
        holder.mustSign.setText(list.getMustSign().toString());
        holder.present.setText(list.getMustBePresent().toString());

        getSignatoryPictures(list.getAccount(), list.getNo().toString(), holder, position);
        getSignatorySign(list.getAccount(), list.getNo().toString(), holder, position);

    }

    private void getSignatoryPictures(String account, String sigNo, final ViewHolder holder, int position) {

        Api.getVolley(mContext, Api.GetSignatoryPictures, "{\n" +
                "    \"accountNo\": \"" + account + "\",\n" +
                "    \"signatoryNo\": \"" + sigNo + "\",\n" +
                "    \"fileType\": \"" + 0 + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                Log.e("RESULTS", Response);

                Bitmap decodeTxt = StringToBitMap(Response);
                holder.picture.setImageBitmap(decodeTxt);

            }
        });

    }

    private void getSignatorySign(String account, String sigNo, final ViewHolder holder, int position) {

        Api.getVolley(mContext, Api.GetSignatoryPictures, "{\n" +
                "    \"accountNo\": \"" + account + "\",\n" +
                "    \"signatoryNo\": \"" + sigNo + "\",\n" +
                "    \"fileType\": \"" + 1 + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                Log.e("RESULTS", Response);

                Bitmap decodeTxt = StringToBitMap(Response);
                holder.signature.setImageBitmap(decodeTxt);

            }
        });

    }

    public Bitmap StringToBitMap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


}

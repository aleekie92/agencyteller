package com.coretec.agencyteller.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.coretec.agencyteller.R;
import com.coretec.agencyteller.model.BalanceModel;

import java.util.List;

public class BalanceAdapter extends RecyclerView.Adapter<BalanceAdapter.ViewHolder> {

    private Context mContext ;
    private List<BalanceModel> mData ;

    public BalanceAdapter(Context mContext, List<BalanceModel> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView account, description, balance;

        public ViewHolder(View itemView) {
            super(itemView);
            account = itemView.findViewById(R.id.tv_date);
            description = itemView.findViewById(R.id.tv_description);
            balance = itemView.findViewById(R.id.tv_amount);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view ;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        view = inflater.inflate(R.layout.statement_item,parent,false) ;
        final ViewHolder viewHolder = new ViewHolder(view) ;

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(BalanceAdapter.ViewHolder holder, final int position) {

        final BalanceModel list = mData.get(position);

        holder.account.setText(list.getAccount());
        holder.description.setText(list.getDescription());
        holder.balance.setText(list.getBalance());
        //holder.destination.setText(list.getDestination());

        /*getSignatoryPictures(list.getAccount(), list.getNo().toString(), holder, position);
        getSignatorySign(list.getAccount(), list.getNo().toString(), holder, position);*/

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

}

package com.coretec.agencyteller.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.coretec.agencyteller.R;
import com.coretec.agencyteller.model.MiniModel;

import java.util.List;

public class NewMiniAdapter extends RecyclerView.Adapter<NewMiniAdapter.ViewHolder> {

    private Context mContext ;
    private List<MiniModel> mData ;

    public NewMiniAdapter(Context mContext, List<MiniModel> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView date, description, amount;

        public ViewHolder(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.tv_date);
            description = itemView.findViewById(R.id.tv_description);
            amount = itemView.findViewById(R.id.tv_amount);

        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view ;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        view = inflater.inflate(R.layout.statement_item,parent,false) ;
        final ViewHolder viewHolder = new ViewHolder(view) ;

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(NewMiniAdapter.ViewHolder holder, final int position) {

        final MiniModel list = mData.get(position);

        holder.date.setText(list.getDate());
        holder.description.setText(list.getDescription());
        holder.amount.setText(list.getAmount());
        //holder.destination.setText(list.getDestination());

        /*getSignatoryPictures(list.getAccount(), list.getNo().toString(), holder, position);
        getSignatorySign(list.getAccount(), list.getNo().toString(), holder, position);*/

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

}

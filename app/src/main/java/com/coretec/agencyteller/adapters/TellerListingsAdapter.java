package com.coretec.agencyteller.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.coretec.agencyteller.R;
import com.coretec.agencyteller.activities.reports.PaymentReceipt;
import com.coretec.agencyteller.model.ModelListings;

import java.util.List;

public class TellerListingsAdapter extends RecyclerView.Adapter<TellerListingsAdapter.ViewHolder> {

    private Context mContext ;
    private List<ModelListings> mData ;

    public TellerListingsAdapter(Context mContext, List<ModelListings> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView transaction_no, transaction_type, date, amount, source_ac, destination;
        LinearLayout container;
        Button print;

        public ViewHolder(View itemView) {
            super(itemView);
            transaction_no = itemView.findViewById(R.id.tv_transaction_no);
            transaction_type = itemView.findViewById(R.id.tv_type);
            date = itemView.findViewById(R.id.tv_date);
            amount = itemView.findViewById(R.id.tv_amount);
            source_ac = itemView.findViewById(R.id.tv_source);
            //destination = itemView.findViewById(R.id.tv_destination);
            container = itemView.findViewById(R.id.linear_container);
            print = itemView.findViewById(R.id.button_print);

        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view ;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        view = inflater.inflate(R.layout.listings_item,parent,false) ;
        final ViewHolder viewHolder = new ViewHolder(view) ;

        viewHolder.print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //String category = mData.get()

                //createStakeDialog();
                Intent i = new Intent(mContext, PaymentReceipt.class);
                i.putExtra("transaction_number",mData.get(viewHolder.getAdapterPosition()).getNo());
                /*i.putExtra("anime_description",mData.get(viewHolder.getAdapterPosition()).getDescription());
                i.putExtra("anime_studio",mData.get(viewHolder.getAdapterPosition()).getStudio());
                i.putExtra("anime_category",mData.get(viewHolder.getAdapterPosition()).getCategorie());
                i.putExtra("anime_nb_episode",mData.get(viewHolder.getAdapterPosition()).getNb_episode());
                i.putExtra("anime_rating",mData.get(viewHolder.getAdapterPosition()).getRating());
                i.putExtra("anime_img",mData.get(viewHolder.getAdapterPosition()).getImage_url());*/

                mContext.startActivity(i);
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(TellerListingsAdapter.ViewHolder holder, final int position) {

        final ModelListings list = mData.get(position);

        holder.transaction_no.setText(list.getNo());
        holder.transaction_type.setText(list.getType());
        holder.date.setText(list.getDate());
        holder.amount.setText(list.getAmount());
        holder.source_ac.setText(list.getAccount());
        //holder.destination.setText(list.getDestination());

        /*getSignatoryPictures(list.getAccount(), list.getNo().toString(), holder, position);
        getSignatorySign(list.getAccount(), list.getNo().toString(), holder, position);*/

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

}

package com.coretec.agencyteller.adapters;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.coretec.agencyteller.R;
import com.coretec.agencyteller.activities.teller.Transactions;
import com.coretec.agencyteller.api.Api;
import com.coretec.agencyteller.model.ModelPending;
import com.coretec.agencyteller.utils.SharedPrefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class PendingApprovalsAdapter extends RecyclerView.Adapter<PendingApprovalsAdapter.ViewHolder> {

    private Context mContext;
    private List<ModelPending> mData;

    ProgressDialog progressDialog;

    public PendingApprovalsAdapter(Context mContext, List<ModelPending> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView transaction_no, transaction_type, date, amount, approver_txt;
        LinearLayout container;
        Button delegate, reject;

        public ViewHolder(View itemView) {
            super(itemView);
            transaction_no = itemView.findViewById(R.id.tv_transaction_no);
            transaction_type = itemView.findViewById(R.id.tv_type);
            date = itemView.findViewById(R.id.tv_date);
            amount = itemView.findViewById(R.id.tv_amount);
            approver_txt = itemView.findViewById(R.id.tv_approver);
            container = itemView.findViewById(R.id.linear_container);
            delegate = itemView.findViewById(R.id.button_delegate);
            reject = itemView.findViewById(R.id.button_reject);

        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        view = inflater.inflate(R.layout.pending_item, parent, false);
        final ViewHolder viewHolder = new ViewHolder(view);

        viewHolder.delegate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String msg = "Are you sure you want to proceed with the selected action?";
                AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
                builder1.setMessage(msg);
                builder1.setTitle("Alert!");
                builder1.setCancelable(false);

                builder1.setPositiveButton(
                        "ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                String transactionNo = mData.get(viewHolder.getAdapterPosition()).getNo();
                                progressDialog = new ProgressDialog(mContext);
                                progressDialog.setMessage("Sending request...");
                                progressDialog.setCancelable(true);
                                progressDialog.show();
                                delegateApproval(transactionNo);
                            }
                        });

                builder1.setNegativeButton(
                        "CANCEL",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });

        viewHolder.reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String msg = "Are you sure you want to REJECT?";
                AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
                builder1.setMessage(msg);
                builder1.setTitle("Alert!");
                builder1.setCancelable(false);

                builder1.setPositiveButton(
                        "yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                String transactionNo = mData.get(viewHolder.getAdapterPosition()).getNo();
                                progressDialog = new ProgressDialog(mContext);
                                progressDialog.setMessage("Cancelling please wait...");
                                progressDialog.setCancelable(true);
                                progressDialog.show();
                                cancelApproval(transactionNo);
                            }
                        });

                builder1.setNegativeButton(
                        "CANCEL",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PendingApprovalsAdapter.ViewHolder holder, final int position) {

        final ModelPending list = mData.get(position);

        holder.transaction_no.setText(list.getNo());
        holder.transaction_type.setText(list.getType());
        holder.date.setText(list.getDate());
        holder.amount.setText(list.getAmount());
        holder.approver_txt.setText(list.getApprover());
        //holder.destination.setText(list.getDestination());

        /*getSignatoryPictures(list.getAccount(), list.getNo().toString(), holder, position);
        getSignatorySign(list.getAccount(), list.getNo().toString(), holder, position);*/

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    private void delegateApproval(String transaction_no) {

        Api.getVolley(mContext, Api.DelegateApproval, "{\n" +
                "    \"action\": \"" + 6 + "\",\n" +
                "    \"agentCode\": \"" + SharedPrefs.read(SharedPrefs.AGENT_CODE, null) + "\",\n" +
                "    \"transactionNo\": \"" + transaction_no + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                Log.e("RESULTS", Response);

                try {
                    final JSONObject jsonObject = new JSONObject(Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        //final JSONObject dataObj = jsonObject.getJSONObject("data");
                        progressDialog.dismiss();
                        String msg = jsonObject.getString("message");
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
                        builder1.setMessage(msg);
                        builder1.setTitle("Success!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        Intent i = new Intent(mContext, Transactions.class);
                                        mContext.startActivity(i);
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();

                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        progressDialog.dismiss();
                        String msg = jsonObject.getString("message");
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
                        builder1.setMessage(msg);
                        builder1.setTitle("Alert!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                        //Toast.makeText(TellerApprovals.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

    }

    private void cancelApproval(String transaction_no) {

        Api.getVolley(mContext, Api.CancelApproval, "{\n" +
                "    \"action\": \"" + 5 + "\",\n" +
                "    \"agentCode\": \"" + SharedPrefs.read(SharedPrefs.AGENT_CODE, null) + "\",\n" +
                "    \"transactionNo\": \"" + transaction_no + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {

                        progressDialog.dismiss();
                        String msg = jsonObject.getString("message");
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
                        builder1.setMessage(msg);
                        builder1.setTitle("Success!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        Intent i = new Intent(mContext, Transactions.class);
                                        mContext.startActivity(i);

                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();

                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        progressDialog.dismiss();
                        String msg = jsonObject.getString("message");
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
                        builder1.setMessage(msg);
                        builder1.setTitle("Alert!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                        //Toast.makeText(TellerApprovals.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

    }

}

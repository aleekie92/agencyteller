package com.coretec.agencyteller.adapters;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.coretec.agencyteller.R;
import com.coretec.agencyteller.activities.reports.PaymentReceipt;
import com.coretec.agencyteller.activities.teller.Transactions;
import com.coretec.agencyteller.api.Api;
import com.coretec.agencyteller.model.ModelApprovals;
import com.coretec.agencyteller.model.ModelListings;
import com.coretec.agencyteller.utils.SharedPrefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class TellerApprovalsAdapter extends RecyclerView.Adapter<TellerApprovalsAdapter.ViewHolder> {

    private Context mContext;
    private List<ModelApprovals> mData;

    ProgressDialog progressDialog;
    AlertDialog reasonDialog;

    public TellerApprovalsAdapter(Context mContext, List<ModelApprovals> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView transaction_no, transaction_type, date, amount, source_ac, destination;
        LinearLayout container;
        Button accept, reject;

        public ViewHolder(View itemView) {
            super(itemView);
            transaction_no = itemView.findViewById(R.id.tv_transaction_no);
            transaction_type = itemView.findViewById(R.id.tv_type);
            date = itemView.findViewById(R.id.tv_date);
            amount = itemView.findViewById(R.id.tv_amount);
            source_ac = itemView.findViewById(R.id.tv_source);
            //destination = itemView.findViewById(R.id.tv_destination);
            container = itemView.findViewById(R.id.linear_container);
            accept = itemView.findViewById(R.id.button_accept);
            reject = itemView.findViewById(R.id.button_reject);

        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        view = inflater.inflate(R.layout.approvals_item, parent, false);
        final ViewHolder viewHolder = new ViewHolder(view);

        viewHolder.accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String msg = "Are you sure you want to POST this transaction";
                AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
                builder1.setMessage(msg);
                builder1.setTitle("Alert!");
                builder1.setCancelable(false);

                builder1.setPositiveButton(
                        "ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                String transactionNo = mData.get(viewHolder.getAdapterPosition()).getNo();
                                progressDialog = new ProgressDialog(mContext);
                                progressDialog.setMessage("Sending approval...");
                                progressDialog.setCancelable(true);
                                progressDialog.show();
                                acceptTreasuryTransaction(transactionNo);
                            }
                        });

                builder1.setNegativeButton(
                        "CANCEL",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });

        viewHolder.reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String msg = "Are you sure you want to REJECT this transaction";
                AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
                builder1.setMessage(msg);
                builder1.setTitle("Alert!");
                builder1.setCancelable(false);

                builder1.setPositiveButton(
                        "yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                String transactionNo = mData.get(viewHolder.getAdapterPosition()).getNo();
                                createDialogReason(transactionNo);

                            }
                        });

                builder1.setNegativeButton(
                        "CANCEL",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(TellerApprovalsAdapter.ViewHolder holder, final int position) {

        final ModelApprovals list = mData.get(position);

        holder.transaction_no.setText(list.getNo());
        holder.transaction_type.setText(list.getType());
        holder.date.setText(list.getDate());
        holder.amount.setText(list.getAmount());
        holder.source_ac.setText(list.getAccount());
        //holder.destination.setText(list.getDestination());

        /*getSignatoryPictures(list.getAccount(), list.getNo().toString(), holder, position);
        getSignatorySign(list.getAccount(), list.getNo().toString(), holder, position);*/

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    private void createDialogReason(final String transactionNo) {

        LayoutInflater myInflater = LayoutInflater.from(mContext);
        View v = myInflater.inflate(R.layout.dialog_reason, null);

        final EditText reason = v.findViewById(R.id.et_reason);
        Button submit = v.findViewById(R.id.button_submit);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sababu = reason.getText().toString();
                if (sababu.isEmpty()) {
                    reason.setError("Please enter the reason to proceed!");
                } else {
                    progressDialog = new ProgressDialog(mContext);
                    progressDialog.setMessage("Rejecting transaction...");
                    progressDialog.setCancelable(true);
                    progressDialog.show();
                    rejectTreasuryTransaction(transactionNo, reason.getText().toString());
                }
            }
        });


        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setView(v);
        reasonDialog = builder.create();
        reasonDialog.show();
    }

    private void acceptTreasuryTransaction(String transaction_no) {

        Api.getVolley(mContext, Api.AcceptTreasuryTransaction, "{\n" +
                "    \"action\": \"" + 4 + "\",\n" +
                "    \"agentCode\": \"" + SharedPrefs.read(SharedPrefs.AGENT_CODE, null) + "\",\n" +
                "    \"transactionNo\": \"" + transaction_no + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                Log.e("RESULTS", Response);

                try {
                    final JSONObject jsonObject = new JSONObject(Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        final JSONObject dataObj = jsonObject.getJSONObject("data");
                        progressDialog.dismiss();
                        String msg = jsonObject.getString("message");
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
                        builder1.setMessage(msg);
                        builder1.setTitle("Success!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        Intent i = new Intent(mContext, PaymentReceipt.class);
                                        try {
                                            i.putExtra("transaction_number",dataObj.getString("no"));
                                            mContext.startActivity(i);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();

                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        progressDialog.dismiss();
                        String msg = jsonObject.getString("message");
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
                        builder1.setMessage(msg);
                        builder1.setTitle("Alert!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                        //Toast.makeText(TellerApprovals.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

    }

    private void rejectTreasuryTransaction(String transaction_no, String reason) {

        Api.getVolley(mContext, Api.RejectTreasuryTransaction, "{\n" +
                "    \"action\": \"" + 2 + "\",\n" +
                "    \"agentCode\": \"" + SharedPrefs.read(SharedPrefs.AGENT_CODE, null) + "\",\n" +
                "    \"transactionNo\": \"" + transaction_no + "\",\n" +
                "    \"reason\": \"" + reason + "\"\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {

                        progressDialog.dismiss();
                        String msg = jsonObject.getString("message");
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
                        builder1.setMessage(msg);
                        builder1.setTitle("Success!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        reasonDialog.dismiss();
                                        Intent i = new Intent(mContext, Transactions.class);
                                        mContext.startActivity(i);

                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();

                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        progressDialog.dismiss();
                        String msg = jsonObject.getString("message");
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
                        builder1.setMessage(msg);
                        builder1.setTitle("Alert!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                        //Toast.makeText(TellerApprovals.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

    }

}

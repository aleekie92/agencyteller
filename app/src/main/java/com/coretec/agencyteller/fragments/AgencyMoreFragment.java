package com.coretec.agencyteller.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.coretec.agencyteller.R;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;

public class AgencyMoreFragment extends Fragment implements BlockingStep {

  private Spinner status, citizen, employer;
  private EditText email;
  private TextView employer_name;
  String[] employers;
  private ArrayList<String> arrayList = new ArrayList<>();
  private AutoCompleteTextView actv;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.adapter_more_details, container, false);
    //SharedPrefs.init(getContext());

    //initialize your UI
    email = v.findViewById(R.id.registration_email);
    employer_name = v.findViewById(R.id.employerName);
    status = v.findViewById(R.id.maritalStatus);
    status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String item = parent.getItemAtPosition(position).toString();
        String text = status.getSelectedItem().toString();

        //SharedPrefs.write(SharedPrefs.VR_STATUS, text);

        //Log.e("GROWER NUMBER", text);
      }

      @Override
      public void onNothingSelected(AdapterView<?> parent) {

      }
    });

    actv = v.findViewById(R.id.autoCompleteTextView);
    TextWatcher textWatcher = new TextWatcher() {

      @Override
      public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void afterTextChanged(Editable editable) {
        //here, after we introduced something in the EditText we get the string from it
        String accountNumber = actv.getText().toString();

        if (accountNumber.length() >= 2) {
//                    Toast.makeText(CustomerActivation.this, accountNumber, Toast.LENGTH_SHORT).show();
          //requestEmployer(accountNumber);
          //Log.e("AGENT ID", SharedPrefs.read(SharedPrefs.AGENT_ID_1, null));
        }

      }
    };

    actv.addTextChangedListener(textWatcher);

    //Creating the instance of ArrayAdapter containing list of language names

    //Getting the instance of AutoCompleteTextView
    //actv.setThreshold(1);//will start working from first character
    //setting the adapter data into the AutoCompleteTextView
    actv.setTextColor(Color.RED);

    citizen = v.findViewById(R.id.citizenship);
    citizen.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String item = parent.getItemAtPosition(position).toString();
        String text = citizen.getSelectedItem().toString();

        //SharedPrefs.write(SharedPrefs.VR_CITIZEN, text);
        //Log.e("GROWER NUMBER", text);
      }

      @Override
      public void onNothingSelected(AdapterView<?> parent) {

      }
    });

    employer = v.findViewById(R.id.employerSpinner);
    employer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String item = parent.getItemAtPosition(position).toString();
        String text = employer.getSelectedItem().toString();

        employer_name.setText(text);
        //SharedPrefs.write(SharedPrefs.VR_EMPLOYER, text);

        Log.e("Selected Employer", text);
      }

      @Override
      public void onNothingSelected(AdapterView<?> parent) {

      }
    });

    return v;
  }


  @Override
  public void onNextClicked(final StepperLayout.OnNextClickedCallback callback) {
    new Handler().postDelayed(new Runnable() {
      @Override
      public void run() {
        if (email.getText().toString().matches("")) {
          //Toast.makeText(getContext(), "Please make sure all details are filled!", Toast.LENGTH_SHORT).show();
          //SharedPrefs.write(SharedPrefs.VR_EMAIL, "null");
          callback.goToNextStep();
        } else {
          //SharedPrefs.write(SharedPrefs.VR_EMAIL, email.getText().toString());
          callback.goToNextStep();
        }
      }
    }, 0L);// delay open another fragment,
  }

  @Override
  public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {
  }

  @Override
  public void onBackClicked(final StepperLayout.OnBackClickedCallback callback) {
    callback.goToPrevStep();
  }

  @Override
  public VerificationError verifyStep() {
    return null;
  }

  @Override
  public void onSelected() {
  }

  @Override
  public void onError(@NonNull VerificationError error) {
  }

}

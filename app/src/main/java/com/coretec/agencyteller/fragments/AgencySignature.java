package com.coretec.agencyteller.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyteller.R;
import com.coretec.agencyteller.activities.Dashboard;
import com.coretec.agencyteller.api.Api;
import com.coretec.agencyteller.utils.SharedPrefs;
import com.coretec.agencyteller.utils.Utils;
import com.github.gcacace.signaturepad.views.SignaturePad;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.coretec.agencyteller.utils.Utils.getFormattedDate;
import static com.coretec.agencyteller.utils.Utils.getLat;
import static com.coretec.agencyteller.utils.Utils.getLong;

public class AgencySignature extends Fragment implements BlockingStep {

    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private SignaturePad mSignaturePad;
    private Button mClearButton;
    private Button mSaveButton;
    ProgressDialog progressDialog;

    SharedPreferences sharedPreferences;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.signature_fragment, container, false);
        //SharedPrefs.init(getContext());

        //initialize your UI
        mSignaturePad = v.findViewById(R.id.signature_pad);
        mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
//                Toast.makeText(Signature.this, "OnStartSigning", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSigned() {
                mSaveButton.setEnabled(true);
                mClearButton.setEnabled(true);
            }

            @Override
            public void onClear() {
                mSaveButton.setEnabled(false);
                mClearButton.setEnabled(false);
            }
        });

        mClearButton =  v.findViewById(R.id.clear_button);
        mSaveButton =  v.findViewById(R.id.save_button);

        mClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSignaturePad.clear();
            }
        });

        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bitmap signatureBitmap = mSignaturePad.getSignatureBitmap();
                if (mSignaturePad.isEmpty()){
                    Toast.makeText(getContext(), "Cannot submit empty signature!", Toast.LENGTH_LONG).show();
                } else {
                    if (addJpgSignatureToGallery(signatureBitmap)) {
                        Toast.makeText(getContext(), "Signature saved, press complete to submit registration", Toast.LENGTH_SHORT).show();
                        String encodedImage = bitmapToBase64(signatureBitmap);

                        SharedPrefs.write(SharedPrefs.SIGNATURE_IMAGE, encodedImage);
                        Log.e("SIGNATURE STRING", encodedImage);

                    /*Utils.showAlertDialog(getContext(), "Successful", "Registration Completed Successfully", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //finish();
                            startActivity(new Intent(getContext(), MainActivity.class));
                        }
                    });*/
                    } else {
                        Toast.makeText(getContext(), "Unable to store the signature", Toast.LENGTH_SHORT).show();
                    }
                    /*if (addSvgSignatureToGallery(mSignaturePad.getSignatureSvg())) {
                        Toast.makeText(getContext(), "Signature Saved", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getContext(), "Unable to store the SVG signature", Toast.LENGTH_SHORT).show();
                    }*/
                }
            }
        });

        return v;
    }

    private String bitmapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length <= 0
                        || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getContext(), "Cannot write images to external storage", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public File getAlbumStorageDir(String albumName) {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), albumName);
        if (!file.mkdirs()) {
            Log.e("SignaturePad", "Directory not created");
        }
        return file;
    }

    public void saveBitmapToJPG(Bitmap bitmap, File photo) throws IOException {
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(newBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(bitmap, 0, 0, null);
        OutputStream stream = new FileOutputStream(photo);
        newBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
        stream.close();
    }

    public boolean addJpgSignatureToGallery(Bitmap signature) {
        boolean result = false;
        try {
            File photo = new File(getAlbumStorageDir("SignaturePad"), String.format("Signature_%d.jpg", System.currentTimeMillis()));
            saveBitmapToJPG(signature, photo);
            scanMediaFile(photo);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void scanMediaFile(File photo) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(photo);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

    public boolean addSvgSignatureToGallery(String signatureSvg) {
        boolean result = false;
        try {
            File svgFile = new File(getAlbumStorageDir("SignaturePad"), String.format("Signature_%d.svg", System.currentTimeMillis()));
            OutputStream stream = new FileOutputStream(svgFile);
            OutputStreamWriter writer = new OutputStreamWriter(stream);
            writer.write(signatureSvg);
            writer.close();
            stream.flush();
            stream.close();
            scanMediaFile(svgFile);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Checks if the app has permission to write to device storage
     * <p/>
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity the activity from which permissions are checked
     */
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    @Override
    public void onNextClicked(final StepperLayout.OnNextClickedCallback callback) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //you can do anythings you want
                callback.goToNextStep();
            }
        }, 0L);// delay open another fragment,
    }
    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

        if (mSignaturePad.isEmpty()) {
            Toast.makeText(getContext(), "Cannot submit empty signature!", Toast.LENGTH_LONG).show();
        } else {
            String msg = "Are you sure you want to submit customer registration?";
            //final String qualKgs = SharedPrefs.read(SharedPrefs.LOAN_QUALIFIED_KGS, null);
            AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
            builder1.setMessage(msg);
            builder1.setTitle("Confirm!");
            builder1.setCancelable(false);

            builder1.setPositiveButton(
                    "YES",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            progressDialog = new ProgressDialog(getContext());
                            progressDialog.setMessage("Submitting customer registration details...");
                            progressDialog.setCancelable(false);
                            progressDialog.show();
                            submitCustomerDetails();
                            //startActivity(new Intent(getContext(), Loans.class));
                        }
                    });

            builder1.setNegativeButton(
                    "Cancel",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();
        }

    }

    @Override
    public void onBackClicked(final StepperLayout.OnBackClickedCallback callback) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                //you can do anythings you want
                /*callback.goToNextStep();*/
                callback.goToPrevStep();
            }
        }, 0L);// delay open another fragment,
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }
    @Override
    public void onSelected() {
    }
    @Override
    public void onError(@NonNull VerificationError error) {
    }

    void submitSignature() {

        Api.getVolley(getContext(), Api.SetMemberPictures, "{\n" +
                "    \"identifier\": \"" + SharedPrefs.read(SharedPrefs.ID_NUMBER, null)  + "\",\n" +
                "    \"identifierType\": \"" + 0 + "\",\n" +
                "    \"fileType\": \"" + 1 + "\",\n" +
                "    \"base64Image\": \"" + SharedPrefs.read(SharedPrefs.SIGNATURE_IMAGE, null) + "\"\n" +
                "  }\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));
                        //progressDialog.dismiss();
                        Toast.makeText(getContext(), "User signature submitted successfully!", Toast.LENGTH_LONG).show();

                        //submitPicture();

                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        progressDialog.dismiss();
                        String msg = jsonObject.getString("message");
                        //final String qualKgs = SharedPrefs.read(SharedPrefs.LOAN_QUALIFIED_KGS, null);
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                        builder1.setMessage(msg);
                        builder1.setTitle("Alert!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        //startActivity(new Intent(getContext(), Dashboard.class));
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                        Toast.makeText(getContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    void submitPicture () {

        Api.getVolley(getContext(), Api.SetMemberPictures, "{\n" +
                "    \"identifier\": \"" + SharedPrefs.read(SharedPrefs.ID_NUMBER, null)  + "\",\n" +
                "    \"identifierType\": \"" + 0 + "\",\n" +
                "    \"fileType\": \"" + 0 + "\",\n" +
                "    \"base64Image\": \"" + SharedPrefs.read(SharedPrefs.USER_IMAGE, null) + "\"\n" +
                "  }\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));
                        //progressDialog.dismiss();
                        Toast.makeText(getContext(), "User Image submitted successfully!", Toast.LENGTH_LONG).show();

                        //submitCustomerDetails();

                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        progressDialog.dismiss();
                        String msg = jsonObject.getString("message");
                        //final String qualKgs = SharedPrefs.read(SharedPrefs.LOAN_QUALIFIED_KGS, null);
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                        builder1.setMessage(msg);
                        builder1.setTitle("Alert!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        //startActivity(new Intent(getContext(), Dashboard.class));
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                        Toast.makeText(getContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    void submitCustomerDetails () {

        String dob = SharedPrefs.read(SharedPrefs.DOB, null);
        String updatedDob = dob + "T00:00:00Z";

        Api.getVolley(getContext(), Api.MemberRegistration, "{\n" +
                "    \"action\": \"" + 0  + "\",\n" +
                "    \"transactionType\": \"" + 1 + "\",\n" +
                "    \"deviceID\": \"" + SharedPrefs.read(SharedPrefs.DEVICE_ID, null) + "\",\n" +
                "    \"agentCode\": \"" + SharedPrefs.read(SharedPrefs.AGENT_CODE, null) + "\",\n" +
                "    \"county\": \"" + SharedPrefs.read(SharedPrefs.COUNTY, null) + "\",\n" +
                "    \"gender\": \"" + SharedPrefs.read(SharedPrefs.GENDER, null) + "\",\n" +
                "    \"phoneNo\": \"" + SharedPrefs.read(SharedPrefs.PHONE_NUMBER, null) + "\",\n" +
                "    \"fullName\": \"" + SharedPrefs.read(SharedPrefs.FULL_NAMES, null) + "\",\n" +
                "    \"nationalID\": \"" + SharedPrefs.read(SharedPrefs.ID_NUMBER, null) + "\",\n" +
                "    \"dateOfBirth\": \"" + updatedDob + "\"\n" +
                "  }\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));
                        progressDialog.dismiss();
                        Toast.makeText(getContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                        submitSignature();
                        submitPicture();

                        String msg = jsonObject.getString("message");
                        //final String qualKgs = SharedPrefs.read(SharedPrefs.LOAN_QUALIFIED_KGS, null);
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                        builder1.setMessage(msg);
                        builder1.setTitle("Success!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "DONE",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        startActivity(new Intent(getContext(), Dashboard.class));
                                    }
                                });

                        /*builder1.setNegativeButton(
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });*/

                        AlertDialog alert11 = builder1.create();
                        alert11.show();

                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        progressDialog.dismiss();

                        String msg = jsonObject.getString("message");
                        //final String qualKgs = SharedPrefs.read(SharedPrefs.LOAN_QUALIFIED_KGS, null);
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                        builder1.setMessage(msg);
                        builder1.setTitle("Alert!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "YES",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        //startActivity(new Intent(getContext(), Dashboard.class));
                                    }
                                });

                        builder1.setNegativeButton(
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();

                        Toast.makeText(getContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

}

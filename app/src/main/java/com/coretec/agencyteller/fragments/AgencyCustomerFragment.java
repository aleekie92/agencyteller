package com.coretec.agencyteller.fragments;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.coretec.agencyteller.R;
import com.coretec.agencyteller.activities.Dashboard;
import com.coretec.agencyteller.api.Api;
import com.coretec.agencyteller.utils.SharedPrefs;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class AgencyCustomerFragment extends Fragment implements BlockingStep {

    private EditText registration_name, registration_id, registration_phone_no;
    private TextInputLayout input_layout_registration_name;
    private TextInputLayout input_layout_registration_id;
    private TextInputLayout input_layout_registration_phone_no;
    TextView date;
    DatePickerDialog datePickerDialog;
    private Spinner genderSpinner;
    private Spinner countySpinner;
    private ArrayList<String> countyList = new ArrayList<>();

    ProgressBar progressBar;
   // private TextView idMessage;
    SharedPreferences sharedPreferences;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.adapter_customer_details, container, false);
        SharedPrefs.init(getContext());

        input_layout_registration_name = v.findViewById(R.id.input_layout_registration_name);
        input_layout_registration_id = v.findViewById(R.id.input_layout_registration_id);
        input_layout_registration_phone_no = v.findViewById(R.id.input_layout_registration_phone_no);
        registration_name = v.findViewById(R.id.registration_name);
        registration_id = v.findViewById(R.id.registration_id);
        registration_phone_no = v.findViewById(R.id.registration_phone_no);
        progressBar = v.findViewById(R.id.countyPB);
        date =  v.findViewById(R.id.dob);

        //progressBar = v.findViewById(R.id.idPb);
        //idMessage = v.findViewById(R.id.status);

        progressBar.setVisibility(View.VISIBLE);
        requestCounties();

        // perform click event on edit text
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                                Calendar calendar= Calendar.getInstance();
                                calendar.set(year,monthOfYear,dayOfMonth);
                                String s =String.format(Locale.getDefault(),"%1$tY-%1$tm-%1$td",calendar);
                                date.setText(s);
                                String dateSelected = date.getText().toString();
                                SharedPrefs.write(SharedPrefs.DOB, dateSelected);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
                datePickerDialog.updateDate(2001, 1, 1);
            }
        });
        genderSpinner = v.findViewById(R.id.gender_spinner);
        genderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.GRAY);
                String item = parent.getItemAtPosition(position).toString();
                String text = genderSpinner.getSelectedItem().toString();

                SharedPrefs.write(SharedPrefs.GENDER, text);
                //Log.e("GROWER NUMBER", text);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        countySpinner = v.findViewById(R.id.county_spinner);
        countySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //((TextView) parent.getChildAt(0)).setTextColor(Color.GRAY);
                String item = parent.getItemAtPosition(position).toString();
                String text = countySpinner.getSelectedItem().toString();

                SharedPrefs.write(SharedPrefs.COUNTY, text);
                //Log.e("GROWER NUMBER", text);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        TextWatcher textWatcher1 = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String inputPhone = registration_phone_no.getText().toString();

                if (inputPhone.length() == 1) {
                    if (inputPhone.equals("0") || inputPhone.equals("7") || inputPhone.equals("+")) {
                        registration_phone_no.setText("+2547");
                        registration_phone_no.setSelection(registration_phone_no.getText().length());
                        Toast.makeText(getContext(), "Correct format auto completed. Please proceed!", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        };
        registration_phone_no.addTextChangedListener(textWatcher1);

        TextWatcher textWatcher = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //here, after we introduced something in the EditText we get the string from it
                String accountNumber = registration_id.getText().toString();

                /*if (accountNumber.length() <= 7 ) {
                    loading.setVisibility(View.VISIBLE);
                }

                if (accountNumber.length() <= 5) {
                    loading.setVisibility(View.GONE);
                    userExists.setVisibility(View.GONE);
                    proceed.setVisibility(View.GONE);
                }*/

                if (accountNumber.length() >= 7) {
//                    Toast.makeText(CustomerActivation.this, accountNumber, Toast.LENGTH_SHORT).show();
                    //requestAccountName(accountNumber);
                    //Log.e("AGENT ID", SharedPrefs.read(SharedPrefs.AGENT_ID_1, null));
                }

                if (accountNumber.length() >= 5 && accountNumber.length() <= 8) {
                    //submitAccRegistrationRequest.setVisibility(View.VISIBLE);
                    /*progressBar.setVisibility(View.VISIBLE);
                    progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getContext(), R.color.colorPrimary), PorterDuff.Mode.SRC_IN );*/
                }
                if (accountNumber.length() < 5) {
                    //submitAccRegistrationRequest.setVisibility(View.VISIBLE);
                    //progressBar.setVisibility(View.GONE);
                }

            }
        };

        registration_id.addTextChangedListener(textWatcher);

        return v;
    }

    @Override
    public void onNextClicked(final StepperLayout.OnNextClickedCallback callback) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                String name = registration_name.getText().toString().trim();
                String id = registration_id.getText().toString().trim();
                String phone_number = registration_phone_no.getText().toString().trim();
                String gender = SharedPrefs.read(SharedPrefs.GENDER, null);
                String county = SharedPrefs.read(SharedPrefs.COUNTY, null);

                if (name.isEmpty()) {
                    registration_name.setError("Enter the Full Account Name");
                } else if (id.isEmpty()) {
                    registration_id.setError("Enter the National ID ");
                } else if (phone_number.isEmpty()) {
                    registration_phone_no.setError("Enter the Phone Number ");
                } else if (gender.equals("-- Select Gender --")) {
                    Toast.makeText(getContext(), "Please select gender!", Toast.LENGTH_SHORT).show();
                } else if (county.equals("-- Select County --")) {
                    Toast.makeText(getContext(), "Please select county!", Toast.LENGTH_SHORT).show();
                } else if (date.getText().toString().isEmpty()) {
                    Toast.makeText(getContext(), "Please select your date of birth", Toast.LENGTH_SHORT).show();
                } else {
                    SharedPrefs.write(SharedPrefs.FULL_NAMES, registration_name.getText().toString());
                    SharedPrefs.write(SharedPrefs.ID_NUMBER, registration_id.getText().toString());
                    SharedPrefs.write(SharedPrefs.PHONE_NUMBER, registration_phone_no.getText().toString());
                    callback.goToNextStep();
                }
            }
        }, 0L);// delay open another fragment,
    }
    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {
    }
    @Override
    public void onBackClicked(final StepperLayout.OnBackClickedCallback callback) {

    }
    @Override
    public VerificationError verifyStep() {
        return null;
    }
    @Override
    public void onSelected() {
    }
    @Override
    public void onError(@NonNull VerificationError error) {
    }

    public void onBackPressed() {
        startActivity(new Intent(getContext(), Dashboard.class));
    }

    private void requestCounties() {

        Api.getVolley(getContext(), Api.GetCounties, "{\n" +
                "}", new Api.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                progressBar.setVisibility(View.GONE);
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);
                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            String countyCode = jsonObject1.getString("code");
                            String countyName = jsonObject1.getString("name");
                            String accountDetails = countyCode + "-" + countyName;

                            countyList.add(accountDetails);

                            Log.e("ACCOUNT DETAILS", accountDetails);

                        }
                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        //Log.e("Failed response :", new Gson().toJson(jsonObject.toString()));
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(getContext(), "Could not fetch accounts! Please try again.", Toast.LENGTH_SHORT).show();
                    }

                    //accountSpinner = findViewById(R.id.accountsSpinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, countyList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    countySpinner.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

}

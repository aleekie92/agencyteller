package com.coretec.agencyteller.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.coretec.agencyteller.R;
import com.coretec.agencyteller.utils.SharedPrefs;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import java.io.ByteArrayOutputStream;

public class AgencyPhotoFragment extends Fragment implements BlockingStep {

    private CardView card_passport_photo;
    private static final int CAMERA_REQUEST = 1888;
    private ImageView passport_imageView;
    private Button submitPassportPhoto;
    ProgressDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.adapter_passport_photo, container, false);
        SharedPrefs.init(getContext());

        //initialize your UI
        passport_imageView =  v.findViewById(R.id.imageview_passport);

        card_passport_photo =  v.findViewById(R.id.card_passport_photo);
        card_passport_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        });

        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case CAMERA_REQUEST:
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        Bitmap photo = (Bitmap) data.getExtras().get("data");
                        passport_imageView.setImageBitmap(photo);

                        //imageView.setImageBitmap(bitmap);
                        /*Toast.makeText(this, imageview_reason.toString(),
                                Toast.LENGTH_SHORT).show();*/
                    } catch (Exception e) {
                        Toast.makeText(getContext(), "Failed to load", Toast.LENGTH_SHORT)
                                .show();
                        Log.e("Camera", e.toString());
                    }
                }
        }
    }

    private String bitmapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);

        //suding this function
        /*Bitmap img = ((BitmapDrawable) passport_imageView.getDrawable()).getBitmap();
        String encodedImage = bitmapToBase64(img);*/
    }

    @Override
    public void onNextClicked(final StepperLayout.OnNextClickedCallback callback) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Bitmap img = ((BitmapDrawable) passport_imageView.getDrawable()).getBitmap();
                final String encodedImage = bitmapToBase64(img);

                SharedPrefs.write(SharedPrefs.USER_IMAGE, encodedImage);
                callback.goToNextStep();
            }
        }, 0L);// delay open another fragment,
    }
    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {
        /*Bitmap img = ((BitmapDrawable) passport_imageView.getDrawable()).getBitmap();
        final String encodedImage = bitmapToBase64(img);

        //SharedPrefs.write(SharedPrefs.VR_PASSPORT, encodedImage);
        String msg = "Are you sure you want to submit customer registration?";
        //final String qualKgs = SharedPrefs.read(SharedPrefs.LOAN_QUALIFIED_KGS, null);
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
        builder1.setMessage(msg);
        builder1.setTitle("Confirm!");
        builder1.setCancelable(false);

        builder1.setPositiveButton(
                "YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        progressDialog = new ProgressDialog(getContext());
                        progressDialog.setMessage("Submitting customer registration details...");
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                        submitVR(encodedImage);
                        //startActivity(new Intent(getContext(), Loans.class));
                    }
                });

        builder1.setNegativeButton(
                "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();*/

    }
    @Override
    public void onBackClicked(final StepperLayout.OnBackClickedCallback callback) {
        callback.goToPrevStep();
    }
    @Override
    public VerificationError verifyStep() {
        return null;
    }
    @Override
    public void onSelected() {
    }
    @Override
    public void onError(@NonNull VerificationError error) {
    }

    /*void submitVR(String passportPhoto){

        final String TAG = "CustomerVR";

        String URL = GFL.MSACCO_AGENT + GFL.GlobalVR;

        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy/MM/dd - HH:mm");
        Date myDate = new Date();

        final GlobalVRequest globalVRequest = new GlobalVRequest();
        globalVRequest.fullnames = SharedPrefs.read(SharedPrefs.VR_NAMES, null);
        globalVRequest.idno = SharedPrefs.read(SharedPrefs.VR_ID, null);
        globalVRequest.phoneno = SharedPrefs.read(SharedPrefs.VR_PHONE, null);
        globalVRequest.gender = SharedPrefs.read(SharedPrefs.VR_GENDER, null);
        globalVRequest.dob = myDate;
        globalVRequest.passportphoto = passportPhoto;
        globalVRequest.maritalstatus = SharedPrefs.read(SharedPrefs.VR_STATUS, null);
        globalVRequest.citizenship = SharedPrefs.read(SharedPrefs.VR_CITIZEN, null);
        globalVRequest.email = SharedPrefs.read(SharedPrefs.VR_EMAIL, null);
        globalVRequest.employer = SharedPrefs.read(SharedPrefs.VR_EMPLOYER, null);
        globalVRequest.corporate_no = "CAP0006";
        globalVRequest.corporate_no2 = "600110";
        globalVRequest.transactiontype = "1";
        globalVRequest.agentid = "GFL0001";
        globalVRequest.terminalid = "990008670366801";
        globalVRequest.longitude = getLong(getContext());
        globalVRequest.latitude = getLat(getContext());
        globalVRequest.date = getFormattedDate();

        Log.e(TAG, globalVRequest.getBody().toString());

        GFL.instance(getContext()).request(URL, globalVRequest, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                *//*deposit_progressBar.setVisibility(View.GONE);*//*

                GlobalVResponse globalVResponse = GFL.instance(getContext())
                        .mGson.fromJson(response, GlobalVResponse.class);
                if (globalVResponse.operationsuccess) {
                    progressDialog.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        String receiptNo = jsonObject.getString("receiptNo");
                        Toast.makeText(getContext(), receiptNo, Toast.LENGTH_SHORT).show();
                        Log.e("SUCCESSFUL", receiptNo);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    String msg = "Customer registration submitted successfully!";
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                    builder1.setMessage(msg);
                    builder1.setTitle("Success!");
                    builder1.setCancelable(false);

                    builder1.setPositiveButton(
                            "DONE",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    startActivity(new Intent(getContext(), MainActivity.class));
                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), "Customer registration failed. Try again later!", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });

    }*/

}
